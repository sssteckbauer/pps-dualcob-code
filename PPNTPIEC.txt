000010**************************************************************/   52131415
000020*  PROGRAM: PPNTPIEC                                         */   52131415
000030*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000040*  NAME:_______MLG_______ MODIFICATION DATE:  __06/10/02____ */   52131415
000050*  DESCRIPTION:                                              */   52131415
000060*     ADDED HTML FORMATTING AND CHANGED TEXT FORMATTING      */   52131415
000070*     FOR PAN ENHANCEMENT.                                   */   52131415
000080*                                                            */   52131415
000090**************************************************************/   52131415
000100**************************************************************/   25991140
000200*  PROGRAM: PPNTPIEC                                         */   25991140
000300*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12599____  */   25991140
000400*  NAME:______M Sano_____ CREATION DATE:      ___05/30/97__  */   25991140
000500*  DESCRIPTION:                                              */   25991140
000600*  - CREATE THE PIE END DATE NOTIFICATION.                   */   25991140
000700**************************************************************/   25991140
000800*----------------------------------------------------------------*PPNTPIEC
000900 IDENTIFICATION DIVISION.                                         PPNTPIEC
001000 PROGRAM-ID. PPNTPIEC.                                            PPNTPIEC
001100 AUTHOR. UCOP.                                                    PPNTPIEC
001200 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPNTPIEC
001300*REMARKS.                                                         PPNTPIEC
001400******************************************************************PPNTPIEC
001500*THE PIE END DATE NOTIFICATION                                   *PPNTPIEC
001600*  DISPLAYS PERIOD OF INITIAL ELIGIBILITY END DATE AND           *PPNTPIEC
001700*  MOST RECENT HIRE DATE                                         *PPNTPIEC
001800*    NOTIFICATION PRESENTS LABELED LINES OF DATA WITH THE CURRENT*PPNTPIEC
001900*    AND PRIOR VALUES                                            *PPNTPIEC
002000******************************************************************PPNTPIEC
002100 DATE-WRITTEN.  MAY. 1997.                                        PPNTPIEC
002200 DATE-COMPILED. MAY 1997.                                         PPNTPIEC
002300 ENVIRONMENT DIVISION.                                            PPNTPIEC
002400 CONFIGURATION SECTION.                                           PPNTPIEC
002500 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPNTPIEC
002600 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPNTPIEC
002700 INPUT-OUTPUT SECTION.                                            PPNTPIEC
002800 FILE-CONTROL.                                                    PPNTPIEC
002900     EJECT                                                        PPNTPIEC
003000 DATA DIVISION.                                                   PPNTPIEC
003100 FILE SECTION.                                                    PPNTPIEC
003200*----------------------------------------------------------------*PPNTPIEC
003300 WORKING-STORAGE SECTION.                                         PPNTPIEC
003400*----------------------------------------------------------------*PPNTPIEC
003500 01  A-STND-PROG-ID               PIC X(8) VALUE 'PPNTPIEC'.      PPNTPIEC
003600* *                                                               PPNTPIEC
003700 01  PIE-LINE-TEXT-WORK-AREA.                                     PPNTPIEC
003800     05  PIE-LINE                    PIC X(79).                   PPNTPIEC
003900     05  PIE-LINE-HEADER.                                         PPNTPIEC
004000         10  FILLER                  PIC X(15)  VALUE             PPNTPIEC
004100             'Employee Name: '.                                   PPNTPIEC
004200         10  PIE-LINE-NAME           PIC X(26).                   PPNTPIEC
004300         10  FILLER                  PIC X(13)  VALUE             PPNTPIEC
004400             '  ID Number: '.                                     PPNTPIEC
004500         10  PIE-LINE-ID             PIC X(9).                    PPNTPIEC
004600     05  PIE-LINE-1.                                              PPNTPIEC
004700         10  FILLER                  PIC X(46) VALUE              PPNTPIEC
004800             'A change to the Period of Initial Eligibility '.    PPNTPIEC
004900         10  FILLER                  PIC X(33) VALUE              PPNTPIEC
005000             '(PIE) End Date has been recorded.'.                 PPNTPIEC
005100     05  PIE-LINE-2.                                              PPNTPIEC
005200         10  FILLER                  PIC X(24) VALUE SPACES.      PPNTPIEC
005300         10  FILLER                  PIC X(33) VALUE              PPNTPIEC
005400             'Previous        Current          '.                 PPNTPIEC
005500     05  PIE-LINE-3.                                              PPNTPIEC
005600         10  PIE-LINE3-FLAG          PIC X(01) VALUE SPACE.       PPNTPIEC
005700         10  FILLER                  PIC X(23) VALUE              PPNTPIEC
005800             'PIE End Date           '.                           PPNTPIEC
005900         10  PIE-LINE3-DATE-PREV     PIC X(08).                   PPNTPIEC
006000         10  FILLER                  PIC X(08) VALUE SPACES.      PPNTPIEC
006100         10  PIE-LINE3-DATE          PIC X(08).                   PPNTPIEC
006200     05  PIE-LINE-4.                                              PPNTPIEC
006300         10  PIE-LINE4-FLAG          PIC X(01) VALUE SPACE.       PPNTPIEC
006400         10  FILLER                  PIC X(23) VALUE              PPNTPIEC
006500             'Hire Date              '.                           PPNTPIEC
006600         10  PIE-LINE4-DATE-PREV     PIC X(08).                   PPNTPIEC
006700         10  FILLER                  PIC X(08) VALUE SPACES.      PPNTPIEC
006800         10  PIE-LINE4-DATE          PIC X(08).                   PPNTPIEC
006900*                                                                 PPNTPIEC
007000 01  WS-MISC-DATA.                                                PPNTPIEC
007100     05  FIRST-TIME-SW                PIC 9   VALUE ZERO.         PPNTPIEC
007200         88  FIRST-TIME-SW-OFF                VALUE ZERO.         PPNTPIEC
007300         88  FIRST-TIME-SW-ON                 VALUE 1.            PPNTPIEC
007400     05  SUB1                  PIC S9(03) COMP SYNC VALUE +0.     PPNTPIEC
007500     05  WS-ACTION-ON          PIC X          VALUE 'Y'.          PPNTPIEC
007600     05  WS-FIX-VAR-DESC       PIC X(8)       VALUE SPACES.       PPNTPIEC
007700     05  WS-ISO-ZERO-DATE      PIC X(10) VALUE '0001-01-01'.      PPNTPIEC
007800     05  WS-ISO-END-DATE       PIC X(10) VALUE '9999-12-31'.      PPNTPIEC
007900     05  WS-USA-CURRENT-DATE   PIC X(10)      VALUE SPACES.       PPNTPIEC
008000*                                                                 PPNTPIEC
008100*                                                                 PPNTPIEC
008200*                                                                 PPNTPIEC
008300     05  ELEMENT-SUB               PIC 9(01)  VALUE ZERO.         PPNTPIEC
008400*                                                                 PPNTPIEC
008410 01  HTML-WORK.                                                   52131415
008420     COPY CPWSHTMW.                                               52131415
008500*                                                                 PPNTPIEC
008600     EJECT                                                        PPNTPIEC
008700 01  XWHC-COMPILE-WORK-AREA.                     COPY CPWSXWHC.   PPNTPIEC
008800*                                                                 PPNTPIEC
008900 01  DATE-CONVERSION.                            COPY CPWSDAYS.   PPNTPIEC
009000     EJECT                                                        PPNTPIEC
009100 01  PPDB2MSG-INTERFACE.                         COPY CPLNKDB2.   PPNTPIEC
009200     EJECT                                                        PPNTPIEC
009300******************************************************************PPNTPIEC
009400**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPNTPIEC
009500**--------------------------------------------------------------**PPNTPIEC
009600 01 XACN-ACTION-ARRAY              EXTERNAL. COPY CPWSXACN.       PPNTPIEC
009700*                                                                 PPNTPIEC
009800 01  CPWSPTXT                      EXTERNAL. COPY CPWSPTXT.       PPNTPIEC
009900*                                                                 PPNTPIEC
010000 01  CPWSNGWB                      EXTERNAL. COPY CPWSNGWB.       PPNTPIEC
010100*                                                                 PPNTPIEC
010110 01  CPWSPHTM                      EXTERNAL.  COPY CPWSPHTM.      52131415
010200*                                                                 PPNTPIEC
010300     EJECT                                                        PPNTPIEC
010400******************************************************************PPNTPIEC
010500**  DATA BASE AREAS                                             **PPNTPIEC
010600******************************************************************PPNTPIEC
010700 01  PER-ROW                           EXTERNAL.                  PPNTPIEC
010800     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPNTPIEC
010900     EJECT                                                        PPNTPIEC
011000 01  BEN-ROW                           EXTERNAL.                  PPNTPIEC
011100     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPNTPIEC
011200     EJECT                                                        PPNTPIEC
011300 01  PER-ROW-HOLD                      EXTERNAL.   COPY CPWSRPER. PPNTPIEC
011400     EJECT                                                        PPNTPIEC
011500 01  BEN-ROW-HOLD                        EXTERNAL. COPY CPWSRBEN. PPNTPIEC
011600     EJECT                                                        PPNTPIEC
011700     EXEC SQL                                                     PPNTPIEC
011800         INCLUDE SQLCA                                            PPNTPIEC
011900     END-EXEC.                                                    PPNTPIEC
012000     EJECT                                                        PPNTPIEC
012100*----------------------------------------------------------------*PPNTPIEC
012200 PROCEDURE DIVISION.                                              PPNTPIEC
012300     EXEC SQL                                                     PPNTPIEC
012400          INCLUDE CPPDXE99                                        PPNTPIEC
012500     END-EXEC.                                                    PPNTPIEC
012600     IF FIRST-TIME-SW-OFF                                         PPNTPIEC
012700         SET  FIRST-TIME-SW-ON   TO TRUE                          PPNTPIEC
012800         PERFORM 1000-INITIALIZATION                              PPNTPIEC
012900     END-IF.                                                      PPNTPIEC
013000*                                                                 PPNTPIEC
013100     SET CPWSNGWB-RETURN-OK    TO TRUE.                           PPNTPIEC
013200*                                                                 PPNTPIEC
013300     PERFORM 2100-CREATE-TEXT-HEADER.                             PPNTPIEC
013301                                                                  52131415
013310     PERFORM 3000-CREATE-HTML.                                    52131415
013400*                                                                 PPNTPIEC
013500 0500-PROGRAM-EXIT.                                               PPNTPIEC
013600**                                                                PPNTPIEC
013700     GOBACK.                                                      PPNTPIEC
013800*                                                                 PPNTPIEC
013900**--------------------------------------------------------------  PPNTPIEC
014000 1000-INITIALIZATION   SECTION.                                   PPNTPIEC
014100**--------------------------------------------------------------  PPNTPIEC
014200*                                                                 PPNTPIEC
014300     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID                         PPNTPIEC
014400                            XWHC-DISPLAY-MODULE.                  PPNTPIEC
014410     SET HTML-SUBTYPE-PIEC TO TRUE.                               52131415
014500*                                                                 PPNTPIEC
014600     PERFORM 4100-GET-CURRENT-DATE.                               52131415
014610**** PERFORM 3100-GET-CURRENT-DATE.                               52131415
014700**--------------------------------------------------------------  PPNTPIEC
014800**            SET UP DATE CONVERSION WORK AREAS                   PPNTPIEC
014900**--------------------------------------------------------------  PPNTPIEC
015000     MOVE WS-ISO-DATE-MASK TO CEEDAYS-MASK.                       PPNTPIEC
015100     MOVE +10 TO CEEDAYS-DATE-LENGTH, CEEDAYS-MASK-LENGTH.        PPNTPIEC
015200*                                                 *-------------* PPNTPIEC
015300                                                   COPY CPPDXWHC. PPNTPIEC
015400*                                                 *-------------* PPNTPIEC
015500 1000-EXIT.    EXIT.                                              PPNTPIEC
015600*                                                                 PPNTPIEC
015700*                                                                 PPNTPIEC
015800******************************************************************PPNTPIEC
015900**        CREATE HEADER LINE FOR TEXT                             PPNTPIEC
016000******************************************************************PPNTPIEC
016100 2100-CREATE-TEXT-HEADER      SECTION.                            PPNTPIEC
016200*                                                                 PPNTPIEC
016300**** MOVE SPACES   TO PIE-LINE.                                   52131415
016400*****MOVE CPWSNGWB-CONVERTED-EMP-NAME  TO PIE-LINE-NAME.          52131415
016500*****MOVE CPWSNGWB-EMPLOYEE-ID TO PIE-LINE-ID.                    52131415
016600*****MOVE PIE-LINE-HEADER TO PIE-LINE.                            52131415
016700*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
016800*****                                                             52131415
016900*****MOVE SPACES   TO PIE-LINE.                                   52131415
017000*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
017100*****                                                             52131415
017200*****MOVE SPACES   TO PIE-LINE.                                   52131415
017300*****MOVE PIE-LINE-1 TO PIE-LINE.                                 52131415
017400*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
017500*****                                                             52131415
017600*****MOVE SPACES   TO PIE-LINE.                                   52131415
017700*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
017800*****                                                             52131415
017900*****MOVE SPACES   TO PIE-LINE.                                   52131415
018000*****MOVE PIE-LINE-2 TO PIE-LINE.                                 52131415
018100*****                                                             52131415
018200*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
018201                                                                  52131415
018202     MOVE SPACES TO PIE-LINE.                                     52131415
018203                                                                  52131415
018204     MOVE SPACES TO WS-STRING                                     52131415
018205                    WS-STRING-OUT.                                52131415
018206     MOVE ZEROES TO WS-STRING-LENGTH                              52131415
018207                    WS-STRING-OUT-LENGTH.                         52131415
018210     MOVE 'PIE End Date: ' TO WS-STRING.                          52131415
018220     MOVE 14 TO WS-STRING-LENGTH.                                 52131415
018230     PERFORM 7200-STRING-IT.                                      52131415
018240                                                                  52131415
018251     IF PIE-END-DATE OF BEN-ROW = SPACES OR WS-ISO-ZERO-DATE      52131415
018252        CONTINUE                                                  52131415
018253     ELSE                                                         52131415
018260        MOVE PIE-END-DATE OF BEN-ROW TO CEEDAYS-DATE              52131415
018270        PERFORM 4000-CALL-CEEDAYS                                 52131415
018280        MOVE CEE-FORMATTED-DATE TO WS-STRING                      52131415
018281        PERFORM 7200-STRING-IT                                    52131415
018282     END-IF.                                                      52131415
018283                                                                  52131415
018284     MOVE WS-STRING-OUT TO PIE-LINE.                              52131415
018290     PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
018291                                                                  52131415
018292     MOVE SPACES   TO PIE-LINE.                                   52131415
018293     MOVE SPACES TO WS-STRING                                     52131415
018294                    WS-STRING-OUT.                                52131415
018295     MOVE ZEROES TO WS-STRING-LENGTH                              52131415
018296                    WS-STRING-OUT-LENGTH.                         52131415
018297                                                                  52131415
018298     MOVE 13 TO WS-STRING-LENGTH.                                 52131415
018299     PERFORM 7200-STRING-IT.                                      52131415
018300                                                                  52131415
018301     IF PIE-END-DATE OF BEN-ROW-HOLD NOT = PIE-END-DATE OF BEN-ROW52131415
018302        IF PIE-END-DATE OF BEN-ROW-HOLD =                         52131415
018303                                        SPACES OR WS-ISO-ZERO-DATE52131415
018304           MOVE '<none>' TO WS-STRING                             52131415
018305           PERFORM 7200-STRING-IT                                 52131415
018306        ELSE                                                      52131415
018307           MOVE '<' TO WS-STRING                                  52131415
018308           MOVE 1 TO WS-STRING-LENGTH                             52131415
018309           PERFORM 7200-STRING-IT                                 52131415
018310           MOVE PIE-END-DATE OF BEN-ROW-HOLD TO CEEDAYS-DATE      52131415
018311           PERFORM 4000-CALL-CEEDAYS                              52131415
018312           MOVE CEE-FORMATTED-DATE TO WS-STRING                   52131415
018313           PERFORM 7200-STRING-IT                                 52131415
018314           MOVE '>' TO WS-STRING                                  52131415
018315           MOVE 1 TO WS-STRING-LENGTH                             52131415
018316           PERFORM 7200-STRING-IT                                 52131415
018317        END-IF                                                    52131415
018318        MOVE WS-STRING-OUT TO PIE-LINE                            52131415
018319        PERFORM 5500-MOVE-TEXT-LINE                               52131415
018320     END-IF.                                                      52131415
018321                                                                  52131415
018322 2100-EXIT.    EXIT.                                              52131415
018323                                                                  52131415
018330*****MOVE SPACES   TO PIE-LINE.                                   52131415
018400*****MOVE WS-STD-DATE-MASK TO CEEDATE-MASK.                       52131415
018500*****MOVE +8  TO CEEDATE-MASK-LENGTH.                             52131415
018600*****IF  PIE-END-DATE OF BEN-ROW NOT EQUAL WS-ISO-ZERO-DATE       52131415
018700*****    MOVE PIE-END-DATE OF BEN-ROW TO CEEDAYS-DATE             52131415
018800*****    PERFORM 3000-CALL-CEEDAYS                                52131415
018900*****    MOVE CEE-FORMATTED-DATE TO PIE-LINE3-DATE                52131415
019000*****ELSE                                                         52131415
019100*****    MOVE SPACES TO PIE-LINE3-DATE                            52131415
019200*****END-IF.                                                      52131415
019300*****MOVE WS-STD-DATE-MASK TO CEEDATE-MASK.                       52131415
019400*****MOVE +8  TO CEEDATE-MASK-LENGTH.                             52131415
019500*****IF  PIE-END-DATE OF BEN-ROW-HOLD NOT EQUAL WS-ISO-ZERO-DATE  52131415
019600*****    MOVE PIE-END-DATE OF BEN-ROW-HOLD TO CEEDAYS-DATE        52131415
019700*****    PERFORM 3000-CALL-CEEDAYS                                52131415
019800*****    MOVE CEE-FORMATTED-DATE TO PIE-LINE3-DATE-PREV           52131415
019900*****ELSE                                                         52131415
020000*****    MOVE SPACES TO PIE-LINE3-DATE-PREV                       52131415
020100*****END-IF.                                                      52131415
020200*****IF PIE-END-DATE OF BEN-ROW NOT = PIE-END-DATE OF BEN-ROW-HOLD52131415
020300*****   MOVE '*' TO PIE-LINE3-FLAG                                52131415
020400*****ELSE                                                         52131415
020500*****   MOVE SPACE TO PIE-LINE3-FLAG                              52131415
020600*****END-IF.                                                      52131415
020700*****MOVE PIE-LINE-3 TO PIE-LINE.                                 52131415
020800*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
020900*****                                                             52131415
021000*****                                                             52131415
021100*****MOVE SPACES   TO PIE-LINE.                                   52131415
021200*****MOVE WS-STD-DATE-MASK TO CEEDATE-MASK.                       52131415
021300*****MOVE +8  TO CEEDATE-MASK-LENGTH.                             52131415
021400*****IF  HIRE-DATE OF PER-ROW NOT EQUAL WS-ISO-ZERO-DATE          52131415
021500*****    MOVE HIRE-DATE OF PER-ROW TO CEEDAYS-DATE                52131415
021600*****    PERFORM 3000-CALL-CEEDAYS                                52131415
021700*****    MOVE CEE-FORMATTED-DATE TO PIE-LINE4-DATE                52131415
021800*****ELSE                                                         52131415
021900*****    MOVE SPACES TO PIE-LINE4-DATE                            52131415
022000*****END-IF.                                                      52131415
022100*****MOVE WS-STD-DATE-MASK TO CEEDATE-MASK.                       52131415
022200*****MOVE +8  TO CEEDATE-MASK-LENGTH.                             52131415
022300*****IF  HIRE-DATE OF PER-ROW-HOLD NOT EQUAL WS-ISO-ZERO-DATE     52131415
022400*****    MOVE HIRE-DATE OF PER-ROW-HOLD TO CEEDAYS-DATE           52131415
022500*****    PERFORM 3000-CALL-CEEDAYS                                52131415
022600*****    MOVE CEE-FORMATTED-DATE TO PIE-LINE4-DATE-PREV           52131415
022700*****ELSE                                                         52131415
022800*****    MOVE SPACES TO PIE-LINE4-DATE-PREV                       52131415
022900*****END-IF.                                                      52131415
023000*****IF HIRE-DATE OF PER-ROW NOT = HIRE-DATE OF PER-ROW-HOLD      52131415
023100*****   MOVE '*' TO PIE-LINE4-FLAG                                52131415
023200*****ELSE                                                         52131415
023300*****   MOVE SPACE TO PIE-LINE4-FLAG                              52131415
023400*****END-IF.                                                      52131415
023500*****MOVE PIE-LINE-4 TO PIE-LINE.                                 52131415
023600*****PERFORM 5500-MOVE-TEXT-LINE.                                 52131415
023700*                                                                 52131415
023800*2100-EXIT.    EXIT.                                              52131415
023900*                                                                 52131415
024000*2500-EXIT.     EXIT.                                             52131415
024010***************************************************************/  52131415
024020*   3000-CREATE-HTML                                          */  52131415
024030***************************************************************/  52131415
024040 3000-CREATE-HTML SECTION.                                        52131415
024050                                                                  52131415
024061     MOVE '15' TO HTML-COLUMN-WIDTH (1).                          52131415
024070     MOVE '25' TO HTML-COLUMN-WIDTH (2).                          52131415
024080     MOVE '15' TO HTML-COLUMN-WIDTH (3).                          52131415
024090     MOVE '30' TO HTML-COLUMN-WIDTH (4).                          52131415
024091                                                                  52131415
024092     MOVE SPACES TO WS-STRING-OUT.                                52131415
024093                                                                  52131415
024094     STRING                                                       52131415
024095         '<BR><TABLE BORDER="0" CELLPADDING="0"' DELIMITED BY SIZE52131415
024096         ' CELLSPACING="0" WIDTH="90%">' DELIMITED BY SIZE        52131415
024097       INTO WS-STRING-OUT                                         52131415
024098     END-STRING.                                                  52131415
024099                                                                  52131415
024100     PERFORM 7300-MOVE-HTML-SEGMENT.                              52131415
024101                                                                  52131415
024102**** PIE-END-DATE ****                                            52131415
024103                                                                  52131415
024104     MOVE 'PIE End Date' TO HTML-LABEL.                           52131415
024105     MOVE PIE-END-DATE OF BEN-ROW TO HTML-CURRENT-DATA.           52131415
024106     MOVE PIE-END-DATE OF BEN-ROW-HOLD TO HTML-PRIOR-DATA.        52131415
024107     SET HTML-DATA-DATE TO TRUE.                                  52131415
024108     MOVE 'N' TO HTML-DATA-TRANSLATED-SW.                         52131415
024109     MOVE 'Y' TO HTML-NEW-ROW-SW.                                 52131415
024110                                                                  52131415
024111     PERFORM 7000-WRITE-FIELD.                                    52131415
024113                                                                  52131415
024115     MOVE HTML-BLANK-CELL TO WS-STRING-OUT.                       52131415
024116     PERFORM 7300-MOVE-HTML-SEGMENT.                              52131415
024117                                                                  52131415
024119     MOVE '</TABLE>' TO WS-STRING-OUT.                            52131415
024120     PERFORM 7300-MOVE-HTML-SEGMENT.                              52131415
024121                                                                  52131415
024122 3000-EXIT.                                                       52131415
024123     EXIT.                                                        52131415
024124                                                                  52131415
024125     EJECT                                                        52131415
024126                                                                  52131415
024100*----------------------------------------------------------------*PPNTPIEC
024200 4000-CALL-CEEDAYS   SECTION.                                     52131415
024210*3000-CALL-CEEDAYS   SECTION.                                     52131415
024300*----------------------------------------------------------------*PPNTPIEC
024400     CALL PGM-CEEDAYS USING CEEDAYS-INPUT, CEEDAYS-FORMAT,        PPNTPIEC
024500                           LILIAN-DATE, CEE-RETURN-CODE.          PPNTPIEC
024600     IF CEE-RETURN-CODE = LOW-VALUES                              PPNTPIEC
024700         CALL PGM-CEEDATE USING LILIAN-DATE, CEEDATE-FORMAT,      PPNTPIEC
024800                    CEE-FORMATTED-DATE, CEE-RETURN-CODE           PPNTPIEC
024900         IF CEE-RETURN-CODE NOT = LOW-VALUES                      PPNTPIEC
025000             MOVE '?' TO CEE-FORMATTED-DATE                       PPNTPIEC
025100             SET CPWSNGWB-CEEDATE-ERROR TO TRUE                   PPNTPIEC
025200             MOVE 'CEEDATE ERROR FROM PPNTPIEC'                   PPNTPIEC
025300                                    TO CPWSNGWB-ABEND-REF1        PPNTPIEC
025400         END-IF                                                   PPNTPIEC
025500     ELSE                                                         PPNTPIEC
025600         MOVE  '?' TO CEE-FORMATTED-DATE                          PPNTPIEC
025700         SET  CPWSNGWB-CEEDATE-ERROR TO TRUE                      PPNTPIEC
025800         MOVE 'CEEDAYS ERROR FROM PPNTPIEC'                       PPNTPIEC
025900                                TO CPWSNGWB-ABEND-REF1            PPNTPIEC
026000     END-IF.                                                      PPNTPIEC
026100*                                                                 PPNTPIEC
026200 4000-EXIT.      EXIT.                                            52131415
026300*----------------------------------------------------------------*PPNTPIEC
026400 4100-GET-CURRENT-DATE  SECTION.                                  52131415
026410*3100-GET-CURRENT-DATE  SECTION.                                  52131415
026500*----------------------------------------------------------------*PPNTPIEC
026600     CALL PGM-CEELOCT USING LILIAN-DATE, CEE-SECONDS,             PPNTPIEC
026700                        CEE-GREGORIAN, CEE-RETURN-CODE.           PPNTPIEC
026800     IF CEE-RETURN-CODE = LOW-VALUES                              PPNTPIEC
026900         MOVE WS-USA-DATE-MASK TO CEEDATE-MASK                    PPNTPIEC
027000         MOVE +10 TO CEEDATE-MASK-LENGTH                          PPNTPIEC
027100         CALL PGM-CEEDATE USING LILIAN-DATE, CEEDATE-FORMAT,      PPNTPIEC
027200                    CEE-FORMATTED-DATE, CEE-RETURN-CODE           PPNTPIEC
027300         IF CEE-RETURN-CODE NOT = LOW-VALUES                      PPNTPIEC
027400             MOVE '?' TO CEE-FORMATTED-DATE                       PPNTPIEC
027500             SET CPWSNGWB-CEEDATE-ERROR TO TRUE                   PPNTPIEC
027600             MOVE 'CEEDATE ERROR FROM PPNTPIEC'                   PPNTPIEC
027700                                    TO CPWSNGWB-ABEND-REF1        PPNTPIEC
027800         ELSE                                                     PPNTPIEC
027900             MOVE CEE-FORMATTED-DATE TO WS-USA-CURRENT-DATE       PPNTPIEC
028000         END-IF                                                   PPNTPIEC
028100     ELSE                                                         PPNTPIEC
028200         SET CPWSNGWB-CEEDATE-ERROR TO TRUE                       PPNTPIEC
028300         MOVE 'CEELOCT ERROR FROM PPNTPIEC'                       PPNTPIEC
028400                                TO CPWSNGWB-ABEND-REF1            PPNTPIEC
028500     END-IF.                                                      PPNTPIEC
028600*                                                                 PPNTPIEC
028700 4100-EXIT.      EXIT.                                            52131415
028710*3100-EXIT.      EXIT.                                            52131415
028800**--------------------------------------------------------------  PPNTPIEC
028900*          MOVE TEXT LINE TO NOTIFICATION                         PPNTPIEC
029000**--------------------------------------------------------------  PPNTPIEC
029100*                                                                 PPNTPIEC
029200 5500-MOVE-TEXT-LINE  SECTION.                                    PPNTPIEC
029300     ADD +1        TO CPWSPTXT-LINES-RETURNED.                    PPNTPIEC
029400     IF CPWSPTXT-LINES-RETURNED  > CPWSPTXT-MAX-LINES             PPNTPIEC
029500         MOVE CPWSPTXT-MAX-LINES  TO CPWSPTXT-LINES-RETURNED      PPNTPIEC
029600         SET  CPWSNGWB-TEXT-OVRFLO-ERROR TO TRUE                  PPNTPIEC
029700     ELSE                                                         PPNTPIEC
029800         MOVE PIE-LINE TO CPWSPTXT-LINE (CPWSPTXT-LINES-RETURNED) PPNTPIEC
029900     END-IF.                                                      PPNTPIEC
030000 5500-EXIT.    EXIT.                                              PPNTPIEC
030100*                                                                 PPNTPIEC
030200*                                                                 PPNTPIEC
030210**---------------------------------------------------------------*52131415
030220** CPWSHTML CONTAINS HTML PROCEDURES.                             52131415
030230**---------------------------------------------------------------*52131415
030240     COPY CPPDHTMW.                                               52131415
030250                                                                  52131415
030300******************************************************************PPNTPIEC
030400*999999-SQL-ERROR.                                               *PPNTPIEC
030500******************************************************************PPNTPIEC
030600*    THIS CODE IS EXECUTED IF A NEGATIVE SQLCODE IS RETURNED     *PPNTPIEC
030700******************************************************************PPNTPIEC
030800     EXEC SQL                                                     PPNTPIEC
030900          INCLUDE CPPDXP99                                        PPNTPIEC
031000     END-EXEC.                                                    PPNTPIEC
031100     SET CPWSNGWB-SQL-ERROR TO TRUE.                              PPNTPIEC
031200     MOVE 'SQL ERROR FROM PPNTPIEC'  TO CPWSNGWB-ABEND-REF1.      PPNTPIEC
031300     GO TO 0500-PROGRAM-EXIT.                                     PPNTPIEC
031400******************************************************************PPNTPIEC
031500**   E N D  S O U R C E   ----  PPNTPIEC ----                   **PPNTPIEC
031600******************************************************************PPNTPIEC
