000001**************************************************************/   28521087
000002*  PROGRAM: PPEI125                                          */   28521087
000003*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000004*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/25/96__  */   28521087
000005*  DESCRIPTION:                                              */   28521087
000006*  ** DATE CONVERSION II **                                  */   28521087
000007*  - USE THE FIRST OF MONTH DATE (CALC FROM SCR-CURRENT-DATE)*/   28521087
000008*    IN THE CALCULATION OF THE EMPLOYEE'S JAN 1 AGE, NOT THE */   28521087
000009*    SCR PROCESS DATE.                                       */   28521087
000010*  - ADDED EDITS ON EMPLOYEE'S AGE AND BIRTHDATE.            */   28521087
000011**************************************************************/   28521087
000000**************************************************************/   28521025
000001*  PROGRAM: PPEI125                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/28/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
000100**************************************************************/   36410849
000200*  PROGRAM: PPEI125                                          */   36410849
000040*  RELEASE: 0849  REF REL: 717  SERVICE REQUEST(S): _3641__  */   36410849
000400*  NAME:_____JLT_________ MODIFICATION DATE:  ___12/21/93__  */   36410849
000500*  DESCRIPTION:                                              */   36410849
000600*  - CORRECT JAN 1 AGE CALC SO THAT AGE IS RECALCULATED ON   */   36410849
000700*    MONTHLY MAINTENANCE FOR BEGINNING OF JAN; E.R. 1097.    */   36410849
000800**************************************************************/   36410849
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEI125                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    JANUARY 1ST AGE CALCULATION                             *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEI125
001200 IDENTIFICATION DIVISION.                                         PPEI125
001300 PROGRAM-ID. PPEI125.                                             PPEI125
001400 AUTHOR. BUSINESS INFORMATION TECHNOLOGY.                         PPEI125
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEI125
001600                                                                  PPEI125
001700 DATE-WRITTEN.  SEPTEMBER 8, 1992.                                PPEI125
001800 DATE-COMPILED.                                                   PPEI125
001900                                                                  PPEI125
002500 ENVIRONMENT DIVISION.                                            PPEI125
002600 CONFIGURATION SECTION.                                           PPEI125
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEI125
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEI125
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEI125
003000                                                                  PPEI125
003100 INPUT-OUTPUT SECTION.                                            PPEI125
003200 FILE-CONTROL.                                                    PPEI125
003300                                                                  PPEI125
003400 DATA DIVISION.                                                   PPEI125
003500 FILE SECTION.                                                    PPEI125
003600     EJECT                                                        PPEI125
003700*----------------------------------------------------------------*PPEI125
003800 WORKING-STORAGE SECTION.                                         PPEI125
003900*----------------------------------------------------------------*PPEI125
004000                                                                  PPEI125
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEI125
004200*****                                                          CD 28521087
004210*****'PPEI125 /080195'.                                           28521087
004220     'PPEI125 /080196'.                                           28521087
004300                                                                  PPEI125
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEI125
004500     A-STND-PROG-ID.                                              PPEI125
004600     05  FILLER                       PIC X(03).                  PPEI125
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEI125
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEI125
004900     05  FILLER                       PIC X(08).                  PPEI125
005000                                                                  PPEI125
005100 01  ELEMENTS-USED.                                               PPEI125
005200     05  E0107                        PIC 9(04)  VALUE 0107.      PPEI125
005300     05  E0254                        PIC 9(04)  VALUE 0254.      PPEI125
005400                                                                  PPEI125
005500 01  MESSAGES-USED.                                               PPEI125
005600*****05  M12316                       PIC 9(05)  VALUE 12316.     28521087
005601     05  M12806                       PIC 9(05)  VALUE 12806.     28521087
005602     05  M12807                       PIC 9(05)  VALUE 12807.     28521087
005610     05  M08806                       PIC 9(05)  VALUE 08806.     28521087
005620     05  M08807                       PIC 9(05)  VALUE 08807.     28521087
005700                                                                  PPEI125
005800 01  MISC-WORK-AREAS.                                             PPEI125
005900     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEI125
006000         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEI125
006100         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEI125
006200     03  ISO-HIRE-DATE.                                           PPEI125
006300         05  ISO-HIRE-DATE-YYYY-MM.                               PPEI125
006400             07  ISO-HIRE-DATE-YYYY   PIC 9(4).                   PPEI125
006500             07  FILLER               PIC X(1)       VALUE '-'.   PPEI125
006600             07  ISO-HIRE-DATE-MM     PIC 9(2).                   PPEI125
006700         05  FILLER                   PIC X(1)       VALUE '-'.   PPEI125
006800         05  ISO-HIRE-DATE-DD         PIC 9(2).                   PPEI125
006900     03  ISO-BRTH-DATE.                                           PPEI125
007000         05  ISO-BRTH-DATE-YYYY-MM.                               PPEI125
007100             07  ISO-BRTH-DATE-YYYY   PIC 9(4).                   PPEI125
007200             07  FILLER               PIC X(1)       VALUE '-'.   PPEI125
007300             07  ISO-BRTH-DATE-MM     PIC 9(2).                   PPEI125
007400         05  FILLER                   PIC X(1)       VALUE '-'.   PPEI125
007500         05  ISO-BRTH-DATE-DD         PIC 9(2).                   PPEI125
007600                                                                  PPEI125
007700     EJECT                                                        PPEI125
007800 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEI125
007900                                                                  PPEI125
008000     EJECT                                                        PPEI125
008010*****                                                          CD 28521087
008400 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEI125
008410     EJECT                                                        28521087
008430 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. 28521087
008200                                                                  PPEI125
008300     EJECT                                                        PPEI125
008700 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEI125
008500                                                                  PPEI125
008600     EJECT                                                        PPEI125
008910*****                                                          CD 28521087
009300 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEI125
009400                                                                  PPEI125
009500     EJECT                                                        PPEI125
009600 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEI125
009700                                                                  PPEI125
009800     EJECT                                                        PPEI125
009900******************************************************************PPEI125
010000**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEI125
010100**--------------------------------------------------------------**PPEI125
010200** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEI125
010300******************************************************************PPEI125
010400                                                                  PPEI125
010500 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEI125
010600                                                                  PPEI125
010700     EJECT                                                        PPEI125
010800 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEI125
010900                                                                  PPEI125
011000     EJECT                                                        PPEI125
011100 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEI125
011200                                                                  PPEI125
011300     EJECT                                                        PPEI125
011400 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEI125
011500                                                                  PPEI125
011600 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEI125
011700                                                                  PPEI125
011800 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEI125
011900                                                                  PPEI125
012000 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEI125
012100                                                                  PPEI125
012200     EJECT                                                        PPEI125
012300 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEI125
012400                                                                  PPEI125
012500     EJECT                                                        PPEI125
012600 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEI125
012700                                                                  PPEI125
012800     EJECT                                                        PPEI125
012900 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEI125
013000                                                                  PPEI125
013100     EJECT                                                        PPEI125
013200******************************************************************PPEI125
013300**  DATA BASE AREAS                                             **PPEI125
013400******************************************************************PPEI125
013500                                                                  PPEI125
013600 01  SCR-ROW                             EXTERNAL.                PPEI125
013700     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEI125
013800                                                                  PPEI125
013900     EJECT                                                        PPEI125
014000 01  PER-ROW                             EXTERNAL.                PPEI125
014100     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEI125
014200                                                                  PPEI125
014300     EJECT                                                        PPEI125
014400 01  PAY-ROW                             EXTERNAL.                PPEI125
014500     EXEC SQL INCLUDE PPPVPAY1 END-EXEC.                          PPEI125
014600                                                                  PPEI125
014700     EJECT                                                        PPEI125
014800 01  BEL-ROW                             EXTERNAL.                PPEI125
014900     EXEC SQL INCLUDE PPPVBEL1 END-EXEC.                          PPEI125
015000                                                                  PPEI125
015100     EJECT                                                        PPEI125
015200 01  BEN-ROW                             EXTERNAL.                PPEI125
015300     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPEI125
015400                                                                  PPEI125
015500                                                                  PPEI125
015510     EJECT                                                        28521087
015520******************************************************************28521087
015530**  DATA BASE SAVE AREAS                                        **28521087
015540******************************************************************28521087
015550                                                                  28521087
015560 01  BEN-ROW-HOLD                        EXTERNAL. COPY CPWSRBEN  28521087
015570                                                                  28521087
015580         REPLACING  AGE-JAN1          BY SAVE-AGE-JAN1.           28521087
015592                                                                  28521087
015593     EJECT                                                        28521087
015594 01  PER-ROW-HOLD                        EXTERNAL. COPY CPWSRPER  28521087
015595                                                                  28521087
015596         REPLACING  BIRTH-DATE        BY SAVE-BIRTH-DATE.         28521087
015597                                                                  28521087
015600     EJECT                                                        PPEI125
015700******************************************************************PPEI125
015800**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEI125
015900******************************************************************PPEI125
016000                                                                  PPEI125
016100*----------------------------------------------------------------*PPEI125
016200 LINKAGE SECTION.                                                 PPEI125
016300*----------------------------------------------------------------*PPEI125
016400                                                                  PPEI125
016500******************************************************************PPEI125
016600**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEI125
016700**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEI125
016800******************************************************************PPEI125
016900                                                                  PPEI125
017000 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEI125
017100                                                                  PPEI125
017200     EJECT                                                        PPEI125
017300******************************************************************PPEI125
017400**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEI125
017500**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEI125
017600******************************************************************PPEI125
017700                                                                  PPEI125
017800 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEI125
017900                                                                  PPEI125
018000     EJECT                                                        PPEI125
018100******************************************************************PPEI125
018200**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEI125
018300******************************************************************PPEI125
018400                                                                  PPEI125
018500 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEI125
018600                          KMTA-MESSAGE-TABLE-ARRAY.               PPEI125
018700                                                                  PPEI125
018800                                                                  PPEI125
018900*----------------------------------------------------------------*PPEI125
019000 0000-DRIVER.                                                     PPEI125
019100*----------------------------------------------------------------*PPEI125
019200                                                                  PPEI125
019300******************************************************************PPEI125
019400**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEI125
019500******************************************************************PPEI125
019600                                                                  PPEI125
019700     IF  THIS-IS-THE-FIRST-TIME                                   PPEI125
019800         PERFORM 0100-INITIALIZE                                  PPEI125
019900     END-IF.                                                      PPEI125
020000                                                                  PPEI125
020100******************************************************************PPEI125
020200**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEI125
020300**  TO THE CALLING PROGRAM                                      **PPEI125
020400******************************************************************PPEI125
020500                                                                  PPEI125
020600     PERFORM 1000-MAINLINE-ROUTINE.                               PPEI125
020700                                                                  PPEI125
020800     EXIT PROGRAM.                                                PPEI125
020900                                                                  PPEI125
021000                                                                  PPEI125
021100     EJECT                                                        PPEI125
021200*----------------------------------------------------------------*PPEI125
021300 0100-INITIALIZE    SECTION.                                      PPEI125
021400*----------------------------------------------------------------*PPEI125
021500                                                                  PPEI125
021600******************************************************************PPEI125
021700**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEI125
021800******************************************************************PPEI125
021900                                                                  PPEI125
022000     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEI125
022100                                                                  PPEI125
022200*                                               *-------------*   PPEI125
022300                                                 COPY CPPDXWHC.   PPEI125
022400*                                               *-------------*   PPEI125
022500                                                                  PPEI125
022600*****                                                          CD 28521087
022900                                                                  PPEI125
023200******************************************************************PPEI125
023300**   SET FIRST CALL SWITCH OFF                                  **PPEI125
023400******************************************************************PPEI125
023500                                                                  PPEI125
023600     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEI125
023700                                                                  PPEI125
023800     EJECT                                                        PPEI125
023900*----------------------------------------------------------------*PPEI125
024000 1000-MAINLINE-ROUTINE SECTION.                                   PPEI125
024100*----------------------------------------------------------------*PPEI125
024200******************************************************************PPEI125
024300**   ZERO BIRTH DATE WILL BE DETECTED AND ERRORED IN THE        **PPEI125
024400**   MINIMUM RECORD EDIT (PPEIMIN/799)                          **PPEI125
024500******************************************************************PPEI125
024600                                                                  PPEI125
024601     IF  BIRTH-DATE NOT = XDTS-ISO-ZERO-DATE                      28521087
024610         IF KRMI-REQUESTED-BY-08                                  28521087
024620            PERFORM 1100-CONEDITS-MAINLINE                        28521087
024630         ELSE                                                     28521087
024640            PERFORM 2100-IMPLIED-MAINLINE                         28521087
024650         END-IF                                                   28521087
024660     END-IF.                                                      28521087
024700*****                                                          CD 28521087
024701*****                                                             28521087
024710*****IF  BIRTH-DATE NOT = XDTS-ISO-ZERO-DATE                      28521087
024800*****                                                             28521087
025401*****    IF  KRMI-REQUESTED-BY-12                                 28521087
025410*****     OR SCR-NEXTCYCLE-MO  = 01                               28521087
025500*****                                                             28521087
025600*****        MOVE BIRTH-DATE              TO ISO-BRTH-DATE        28521087
025610*****                                                          CD 28521087
025710*****        MOVE SCR-PROCESS-DATE        TO XDTS-ISO-TEMP-DATE   28521087
025800*****                                                             28521087
025810*****                                                          CD 28521087
025910*****        COMPUTE AGE-JAN1 = XDTS-ISO-TEMP-DATE-CCYY           28521087
026000*****                         - ISO-BRTH-DATE-YYYY - 1            28521087
026100*****                                                             28521087
026200*****        IF      ISO-BRTH-DATE-MM = 1                         28521087
026300*****            AND ISO-BRTH-DATE-DD = 1                         28521087
026400*****            ADD 1 TO AGE-JAN1                                28521087
026500*****        END-IF                                               28521087
026600*****                                                             28521087
026700*****        MOVE E0254                   TO DL-FIELD             28521087
026800*****                                                             28521087
026900*****        PERFORM 9050-AUDITING-RESPONSIBILITIES               28521087
027000*****                                                             28521087
027100*****    END-IF                                                   28521087
027200*****                                                             28521087
027300*****END-IF.                                                      28521087
025100                                                                  PPEI125
025800                                                                  PPEI125
027510     EJECT                                                        28521087
027520******************************************************************28521087
027530**     C O N S I S T E N C Y    E D I T S   M A I N L I N E     **28521087
027540******************************************************************28521087
027550*----------------------------------------------------------------*28521087
027560 1100-CONEDITS-MAINLINE SECTION.                                  28521087
027570*----------------------------------------------------------------*28521087
027580******************************************************************28521087
027590**   CALCULATION OF EMPLOYEE'S AGE AS OF JANUARY 1              **28521087
027591******************************************************************28521087
027592                                                                  28521087
027596     MOVE BIRTH-DATE              TO ISO-BRTH-DATE.               28521087
027597     MOVE XDTS-ISO-FIRST-OF-MONTH-DATE                            28521087
027598                                  TO XDTS-ISO-TEMP-DATE.          28521087
027600                                                                  28521087
027601     COMPUTE AGE-JAN1 = XDTS-ISO-TEMP-DATE-CCYY                   28521087
027602                           - ISO-BRTH-DATE-YYYY - 1.              28521087
027603                                                                  28521087
027604     IF      ISO-BRTH-DATE-MM = 1                                 28521087
027605         AND ISO-BRTH-DATE-DD = 1                                 28521087
027606             ADD 1 TO AGE-JAN1                                    28521087
027607     END-IF.                                                      28521087
027608                                                                  28521087
027614     IF   AGE-JAN1   <  16  OR  >  79                             28521087
027615          ADD 1           TO   KMTA-CTR                           28521087
027616          MOVE E0107      TO   KMTA-FIELD-NUM      (KMTA-CTR)     28521087
027617          MOVE BIRTH-DATE TO   XDC3-ISO-DATE                      28521087
027618          PERFORM XDC3-CONVERT-ISO-TO-STD                         28521087
027619          MOVE XDC3-STD-DATE TO   KMTA-DATA-FIELD  (KMTA-CTR)     28521087
027620          MOVE 6          TO   KMTA-LENGTH         (KMTA-CTR)     28521087
027621          MOVE M08806     TO   KMTA-MSG-NUMBER     (KMTA-CTR)     28521087
027622          MOVE WS-ROUTINE-TYPE                                    28521087
027623                          TO   KMTA-ROUTINE-TYPE   (KMTA-CTR)     28521087
027624          MOVE WS-ROUTINE-NUM                                     28521087
027625                          TO   KMTA-ROUTINE-NUM    (KMTA-CTR)     28521087
027626     END-IF.                                                      28521087
027627                                                                  28521087
027628     IF   ISO-BRTH-DATE-YYYY = XDTS-ISO-DATE-TODAY-CCYY           28521087
027629          ADD 1              TO   KMTA-CTR                        28521087
027630          MOVE E0107         TO   KMTA-FIELD-NUM      (KMTA-CTR)  28521087
027631          MOVE BIRTH-DATE    TO   XDC3-ISO-DATE                   28521087
027632          PERFORM XDC3-CONVERT-ISO-TO-STD                         28521087
027633          MOVE XDC3-STD-DATE TO   KMTA-DATA-FIELD     (KMTA-CTR)  28521087
027634          MOVE 6             TO   KMTA-LENGTH         (KMTA-CTR)  28521087
027635          MOVE M08807        TO   KMTA-MSG-NUMBER     (KMTA-CTR)  28521087
027636          MOVE WS-ROUTINE-TYPE                                    28521087
027637                             TO   KMTA-ROUTINE-TYPE   (KMTA-CTR)  28521087
027638          MOVE WS-ROUTINE-NUM                                     28521087
027639                             TO   KMTA-ROUTINE-NUM    (KMTA-CTR)  28521087
027640     END-IF.                                                      28521087
027641                                                                  28521087
027642     EJECT                                                        28521087
027643******************************************************************28521087
027644**   I M P L I E D   M A I N T E N A N C E   M A I N L I N E    **28521087
027645******************************************************************28521087
027646*----------------------------------------------------------------*28521087
027647 2100-IMPLIED-MAINLINE SECTION.                                   28521087
027648*----------------------------------------------------------------*28521087
027649                                                                  28521087
027650     IF    KRMI-REQUESTED-BY-12                                   28521087
027651        OR SCR-NEXTCYCLE-MO  = 01                                 28521087
027652                                                                  28521087
027653        MOVE BIRTH-DATE              TO ISO-BRTH-DATE             28521087
027657        MOVE XDTS-ISO-FIRST-OF-MONTH-DATE                         28521087
027658                                     TO XDTS-ISO-TEMP-DATE        28521087
027660                                                                  28521087
027661        COMPUTE AGE-JAN1 = XDTS-ISO-TEMP-DATE-CCYY                28521087
027662                         - ISO-BRTH-DATE-YYYY - 1                 28521087
027663                                                                  28521087
027664        IF      ISO-BRTH-DATE-MM = 1                              28521087
027665            AND ISO-BRTH-DATE-DD = 1                              28521087
027666                ADD 1 TO AGE-JAN1                                 28521087
027667        END-IF                                                    28521087
027668                                                                  28521087
027669        IF  (AGE-JAN1   <  16  OR  >  79)                         28521087
027670            ADD 1           TO   KMTA-CTR                         28521087
027671            MOVE E0107      TO   KMTA-FIELD-NUM   (KMTA-CTR)      28521087
027675            MOVE 6          TO   KMTA-LENGTH      (KMTA-CTR)      28521087
027676            MOVE M12806  TO   KMTA-MSG-NUMBER     (KMTA-CTR)      28521087
027677            MOVE WS-ROUTINE-TYPE                                  28521087
027678                         TO   KMTA-ROUTINE-TYPE   (KMTA-CTR)      28521087
027679            MOVE WS-ROUTINE-NUM                                   28521087
027680                         TO   KMTA-ROUTINE-NUM    (KMTA-CTR)      28521087
027681                                                                  28521087
027682        END-IF                                                    28521087
027683                                                                  28521087
027684        IF    KRMI-REQUESTED-BY-12                                28521087
027685            IF   ISO-BRTH-DATE-YYYY = XDTS-ISO-DATE-TODAY-CCYY    28521087
027686                ADD  1        TO   KMTA-CTR                       28521087
027687                MOVE E0107    TO   KMTA-FIELD-NUM    (KMTA-CTR)   28521087
027688                MOVE 6        TO   KMTA-LENGTH       (KMTA-CTR)   28521087
027689                MOVE M12807   TO   KMTA-MSG-NUMBER   (KMTA-CTR)   28521087
027690                MOVE WS-ROUTINE-TYPE                              28521087
027691                              TO   KMTA-ROUTINE-TYPE (KMTA-CTR)   28521087
027692                MOVE WS-ROUTINE-NUM                               28521087
027693                              TO   KMTA-ROUTINE-NUM  (KMTA-CTR)   28521087
027694                MOVE  SAVE-AGE-JAN1    TO  AGE-JAN1               28521087
027695                MOVE  SAVE-BIRTH-DATE  TO  BIRTH-DATE             28521087
027696            ELSE                                                  28521087
027697                MOVE E0254    TO DL-FIELD                         28521087
027698                                                                  28521087
027699                PERFORM 9050-AUDITING-RESPONSIBILITIES            28521087
027700                                                                  28521087
027701            END-IF                                                28521087
027702        ELSE                                                      28521087
027703           MOVE E0254    TO DL-FIELD                              28521087
027704                                                                  28521087
027705           PERFORM 9050-AUDITING-RESPONSIBILITIES                 28521087
027706        END-IF                                                    28521087
027707                                                                  28521087
027708     END-IF.                                                      28521087
027709                                                                  28521087
027600     EJECT                                                        PPEI125
027700*----------------------------------------------------------------*PPEI125
027800 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEI125
027900*----------------------------------------------------------------*PPEI125
028000                                                                  PPEI125
028100*                                                 *-------------* PPEI125
028200                                                   COPY CPPDXDEC. PPEI125
028300*                                                 *-------------* PPEI125
028400                                                                  PPEI125
028500*----------------------------------------------------------------*PPEI125
028600 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEI125
028700*----------------------------------------------------------------*PPEI125
028800                                                                  PPEI125
028900*                                                 *-------------* PPEI125
029000                                                   COPY CPPDADLC. PPEI125
029100*                                                 *-------------* PPEI125
029200                                                                  PPEI125
029210*****                                                          CD 28521087
029220     EJECT                                                        28521087
029230*----------------------------------------------------------------*28521087
029240 9300-DATE-CONVERSION-DB2  SECTION.                               28521087
029250*----------------------------------------------------------------*28521087
029260                                                                  28521087
029270*                                                 *-------------* 28521087
029290                                                   COPY CPPDXDC3. 28521087
029300*                                                 *-------------* 28521087
029400                                                                  28521087
030200******************************************************************PPEI125
030300**   E N D  S O U R C E   ----  PPEI125 ----                    **PPEI125
030400******************************************************************PPEI125
