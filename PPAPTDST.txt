000100**************************************************************    WARNING*
000200*  *************** CROSS SYSTEM WARNING *******************  *    WARNING*
000300**************************************************************    WARNING*
000400*                                                            *    WARNING*
000500*  ******* LEAVE THIS BOX AT THE TOP OF THE PROGRAM *******  *    WARNING*
000600*                                                            *    WARNING*
000700*  THIS PROGRAM IS USED BY THE REPLACEMENT STAFFING LIST     *    WARNING*
000800*  (RSL) SYSTEM. IF IT IS MODIFIED IN ANY WAY, PLEASE        *    WARNING*
000900*  NOTIFY BRUCE JAMES IN ADVANCE OF THE RELEASE. A CHANGE    *    WARNING*
001000*  TO THIS PROGRAM WILL ALMOST ALWAYS REQUIRE CORRESPONDING  *    WARNING*
001100*  CHANGES TO RSL.                                           *    WARNING*
001200*                                                            *    WARNING*
001300*  ******* LEAVE THIS BOX AT THE TOP OF THE PROGRAM *******  *    WARNING*
001400**************************************************************    WARNING*
001410**************************************************************/   69031419
001420*  PROGRAM: PPAPTDST                                         */   69031419
001430*  RELEASE: ___1419______ SERVICE REQUEST(S): ____16903____  */   69031419
001450*  NAME:______RIDER______ MODIFICATION DATE:  ___05/15/02__  */   69031419
001470*  DESCRIPTION:                                              */   69031419
001480*  - REPLACE PROGRAM-SPECIFIC VIEW PPPVAPT1 WITH PPPVAPP1.   */   69031419
001490*                                                            */   69031419
001491**************************************************************/   69031419
000100**************************************************************/   36500893
000200*  PROGRAM: PPAPTDST                                         */   36500893
000030*  RELEASE: ___0893______ SERVICE REQUEST(S): _____3650____  */   36500893
000040*  NAME: _RIZ C. (BIT)___ MODIFICATION DATE:  __03/15/94____ */   36500893
000050*  DESCRIPTION: CHANGED VIEW USAGE FROM PPPVAPT2 TO PPPVDIS1 */   36500893
000070*                                                            */   36500893
000080**************************************************************/   36500893
000100**************************************************************/   36060628
000200*  PROGRAM: PPAPTDST                                         */   36060628
000300*  RELEASE# __0628___   REF: _____  SERVICE REQUEST __3606__ */   36060628
000400*  NAME:_G._STEINITZ___  MODIFICATION DATE:   __01/10/92__   */   36060628
000500*  DESCRIPTION:                                              */   36060628
000600*  - FIXED BUGS, ELIMINATED TRASH, SIMPLIFIED, CLEANED UP    */   36060628
000010**************************************************************/   36090479
000020*  PROGRAM: PPAPTDST                                         */   36090479
000030*  RELEASE# __0479___   REF: _____  SERVICE REQUEST __3609__ */   36090479
000040*  NAME:_P._PARKER_____  MODIFICATION DATE:   __06/21/90__   */   36090479
000050*  DESCRIPTION:                                              */   36090479
000060*  - FIXED TWO     BUGS IN "LOAD" FUNCTION.                  */   36090479
000070*    1. ERROR IN SETTING KAPT-FIRST-DIST-INDX                */   36090479
000080*    2. ERROR IN LOADING KAPT-DIST-ARRAY                     */   36090479
000090**************************************************************/   36090479
000800**************************************************************/   30950468
000900*  PROGRAM: PPAPTDST                                         */   30950468
001000*  RELEASE# __0468___   REF: _____  SERVICE REQUEST __3095__ */   30950468
001100*  NAME:_M. BAPTISTA___  MODIFICATION DATE:   __05/04/90__   */   30950468
001200*  DESCRIPTION:                                              */   30950468
001300*  - CHANGED ALL REFERRENCES OF NPP TO PPP.                  */   30950468
001400**************************************************************/   30950468
000100**************************************************************/   30630454
000200*  PROGRAM: PPAPTDST                                         */   30630454
000210*  RELEASE# __0454___   REF: _____  SERVICE REQUEST __3063__ */   30630454
000300*  NAME:__BRUCE JAMES__  MODIFICATION DATE:   __03/08/90__   */   30630454
000400*  DESCRIPTION:                                              */   30630454
000500*  - CHANGED FOR NATIVE DB2 ACCESS.                          */   30630454
000600*                                                            */   30630454
000700**************************************************************/   30630454
000800 IDENTIFICATION DIVISION.                                         PPAPTDST
000900 PROGRAM-ID. PPAPTDST.                                            PPAPTDST
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPAPTDST
001100 AUTHOR.                                                          PPAPTDST
001300******************************************************************PPAPTDST
001400 DATE-WRITTEN.                                                    PPAPTDST
001500 DATE-COMPILED.                                                   PPAPTDST
001600*REMARKS.                                                         PPAPTDST
001700******************************************************************PPAPTDST
001800*                                                                *PPAPTDST
001900*  - PPAPTDST WILL LOAD APPOINTMENT DISTRIBUTION DATA FROM THE   *PPAPTDST
002000*    EMPLOYEE DATA BASE INTO TWO (2) ARRAYS WITHIN THE APTDST    *PPAPTDST
002100*    -INTERFACE (CPLNKAPT) CONTAINING ALL AVAILABLE APPOINTMENTS *PPAPTDST
002200*    AND ALL AVAILABLE DISTRIBUTION, OR WILL FIND AN INDIVIDUAL  *PPAPTDST
002300*    APPOINTMENT OR DISTRIBUTION AND PLACE IT IN EITHER THE      *PPAPTDST
002400*    KTAP-TABLE        (CPLNKTAP) OR KTDS-TABLE                  *PPAPTDST
002500*    (CPLNKTDS) AREAS PASSED FROM THE CALLING PROGRAM.           *PPAPTDST
002600*                                                                *PPAPTDST
002700******************************************************************PPAPTDST
002800 ENVIRONMENT DIVISION.                                            PPAPTDST
002900 CONFIGURATION SECTION.                                           PPAPTDST
003000 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       PPAPTDST
003100 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       PPAPTDST
003200 SPECIAL-NAMES.                                                   PPAPTDST
003300 INPUT-OUTPUT SECTION.                                            PPAPTDST
003400 FILE-CONTROL.                                                    PPAPTDST
003500     SKIP3                                                        PPAPTDST
003600******************************************************************PPAPTDST
003700*                                                                *PPAPTDST
003800*                         DATA DIVISION                          *PPAPTDST
003900*                                                                *PPAPTDST
004000******************************************************************PPAPTDST
004100 DATA DIVISION.                                                   PPAPTDST
004200 FILE SECTION.                                                    PPAPTDST
004300     EJECT                                                        PPAPTDST
004400******************************************************************PPAPTDST
004500*                                                                *PPAPTDST
004600*                    WORKING-STORAGE SECTION                     *PPAPTDST
004700*                                                                *PPAPTDST
004800******************************************************************PPAPTDST
004900 WORKING-STORAGE SECTION.                                         PPAPTDST
005000     SKIP1                                                        PPAPTDST
007300*                                                                 36060628
007400 01  WA-COMPILE-WORK-AREA.       COPY CPWSXWHC.                   36060628
007500*                                                                 36060628
007600 01  77-LEVELS.                                                   36060628
007700     05  APPT-INDX               PIC S9(04)  COMP    VALUE ZERO.  36060628
007800     05  APPOINT-NUM             PIC S9(04)  COMP    VALUE ZERO.  36060628
007900     05  DIST-INDX               PIC S9(04)  COMP    VALUE ZERO.  36060628
008000     05  DISTRIB-NUM             PIC S9(04)  COMP    VALUE ZERO.  36060628
008100     05  QUOTIENT                PIC S9(01)  COMP-3  VALUE ZERO.  36060628
008200     05  Y-REMAINDER             PIC S9(01)  COMP-3  VALUE ZERO.  36060628
008300     05  WS-EMPLOYEE-ID          PIC  X(09)          VALUE SPACE. 36060628
008400                                                                  36060628
010510*****                                                          CD 69031419
006891     SKIP3                                                        PPAPTDST
006892 01  PRIOR-KEY-HOLD-FIELDS.                                       PPAPTDST
006893     05  LAST-APPT-EMPID         PIC X(9).                        PPAPTDST
006894     05  LAST-DIST-EMPID         PIC X(9).                        PPAPTDST
006898     05  LAST-APPT-NUM           PIC S9(04) COMP.                 PPAPTDST
006899     05  LAST-DIST-NUM           PIC S9(04) COMP.                 PPAPTDST
006900     05  LAST-APPT-SQLCODE       PIC S9(04) COMP.                 PPAPTDST
006901     05  LAST-DIST-SQLCODE       PIC S9(04) COMP.                 PPAPTDST
011100     05  FIRST-TIME-SW           PIC  X(01)       VALUE 'Y'.      36060628
011200         88  FIRST-TIME                           VALUE 'Y'.      36060628
011300         88  NOT-FIRST-TIME                       VALUE 'N'.      36060628
006902     SKIP3                                                        PPAPTDST
006903 01  DATE-CONVERSION-WORK-AREAS.             COPY CPWSXDC2.       PPAPTDST
006904*                                                                 PPAPTDST
006905 01  PPDB2MSG-INTERFACE.                     COPY CPLNKDB2.       PPAPTDST
006910     EJECT                                                        PPAPTDST
006911******************************************************************PPAPTDST
006912*            WORKING-STORAGE SQL:                                *PPAPTDST
006913*            DB2 DCLGENS                                         *PPAPTDST
006914******************************************************************PPAPTDST
006920 01  APPOINT-DATA.                                                PPAPTDST
006930     EXEC SQL                                                     PPAPTDST
010200********INCLUDE NPPVAPT1                                          30950468
014700*****   INCLUDE PPPVAPT1                                          69031419
014710        INCLUDE PPPVAPP1                                          69031419
006950     END-EXEC.                                                    PPAPTDST
006960     SKIP1                                                        PPAPTDST
006970 01  DISTRIB-DATA.                                                PPAPTDST
006980     EXEC SQL                                                     PPAPTDST
010800********INCLUDE NPPVAPT2                                          30950468
013200***     INCLUDE PPPVAPT2                                          36500893
013210        INCLUDE PPPVDIS1                                          36500893
006991     END-EXEC.                                                    PPAPTDST
006993     SKIP1                                                        PPAPTDST
006994     EXEC SQL                                                     PPAPTDST
006995        INCLUDE SQLCA                                             PPAPTDST
006996     END-EXEC.                                                    PPAPTDST
007000******************************************************************PPAPTDST
007100*                                                                *PPAPTDST
007200*                        CURSOR DECLARATIONS                     *PPAPTDST
007300*                                                                *PPAPTDST
007400******************************************************************PPAPTDST
007401     EXEC SQL                                                     PPAPTDST
007402          DECLARE APPT_ROW CURSOR FOR                             PPAPTDST
012200**************SELECT * FROM NPPVAPT1_APP                          30950468
016800*****         SELECT * FROM PPPVAPT1_APP                          69031419
016810              SELECT * FROM PPPVAPP1_APP                          69031419
007404              WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                 PPAPTDST
007405              ORDER BY APPT_NUM                                   PPAPTDST
007406     END-EXEC.                                                    PPAPTDST
007407*                                                                 PPAPTDST
007408     EXEC SQL                                                     PPAPTDST
007409          DECLARE DIST_ROW CURSOR FOR                             PPAPTDST
013000**************SELECT * FROM NPPVAPT2_DIS                          30950468
015400****          SELECT * FROM PPPVAPT2_DIS                          36500893
015410              SELECT * FROM PPPVDIS1_DIS                          36500893
007411              WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID AND             PPAPTDST
007412                    APPT_NUM = :APPOINT-NUM                       PPAPTDST
007413              ORDER BY DIST_NUM                                   PPAPTDST
007414     END-EXEC.                                                    PPAPTDST
007415******************************************************************PPAPTDST
007420*                                                                *PPAPTDST
007430*                        LINKAGE SECTION                         *PPAPTDST
007440*                                                                *PPAPTDST
007450******************************************************************PPAPTDST
007500 LINKAGE SECTION.                                                 PPAPTDST
007600     SKIP1                                                        PPAPTDST
007700 01  KAPT-INTERFACE.              COPY 'CPLNKAP2'.                PPAPTDST
007800     EJECT                                                        PPAPTDST
008500 01  KTAP-APPOINTMENT.            COPY 'CPLNKTA2'.                PPAPTDST
008600     EJECT                                                        PPAPTDST
008700 01  KTDS-DISTRIBUTION.           COPY 'CPLNKDS2'.                PPAPTDST
008800     EJECT                                                        PPAPTDST
008900******************************************************************PPAPTDST
009000*                                                                *PPAPTDST
009100*                       PROCEDURE DIVISION                       *PPAPTDST
009200*                                                                *PPAPTDST
009300******************************************************************PPAPTDST
009400*                                                                *PPAPTDST
009500 PROCEDURE DIVISION USING KAPT-INTERFACE                          PPAPTDST
009900                          KTAP-APPOINTMENT                        PPAPTDST
010000                          KTDS-DISTRIBUTION.                      PPAPTDST
010001*                                                                *PPAPTDST
010010******************************************************************PPAPTDST
010020*                SQL FROM PRE-COMPILER                           *PPAPTDST
010030******************************************************************PPAPTDST
010040     SKIP2                                                        PPAPTDST
010050******************************************************************PPAPTDST
010060*   THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO       *PPAPTDST
010070*         CAUSE A BRANCH TO 999999-ERROR.                        *PPAPTDST
010080******************************************************************PPAPTDST
010090     EXEC SQL                                                     PPAPTDST
010091         WHENEVER SQLERROR GO TO 999999-SQL-ERROR                 PPAPTDST
010092     END-EXEC.                                                    PPAPTDST
010100     SKIP1                                                        PPAPTDST
010110******************************************************************PPAPTDST
010200 1000-MAINLINE SECTION.                                           PPAPTDST
010300     SKIP2                                                        PPAPTDST
019700     INITIALIZE KAPT-STATUS-FLAG.                                 36060628
010301     MOVE 'PPAPTDST' TO DB2MSG-PGM-ID.                            PPAPTDST
019900     SKIP1                                                        36060628
010310     IF FIRST-TIME                                                PPAPTDST
010400        PERFORM 3000-INITIALIZATION.                              PPAPTDST
010500     SKIP1                                                        PPAPTDST
020300     MOVE KAPT-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                     36060628
020400     SKIP1                                                        36060628
020500     IF KAPT-LOAD-APTDST                                          36060628
020600         PERFORM 5000-LOAD-APTDST THRU 5099-EXIT                  36060628
020700     ELSE                                                         36060628
020800         IF KAPT-FIND-APTDST                                      36060628
020900             PERFORM 7000-FIND-APTDST THRU 7099-EXIT              36060628
021000         ELSE                                                     36060628
021100             SET KAPT-INVALID-FUNCTION TO TRUE.                   36060628
021200     SKIP2                                                        36060628
023510*****                                                          CD 69031419
010700     SKIP3                                                        PPAPTDST
010800 2000-RETURN-CONTROL SECTION.                                     PPAPTDST
010900     SKIP1                                                        PPAPTDST
011000     GOBACK.                                                      PPAPTDST
011100     EJECT                                                        PPAPTDST
011200 3000-INITIALIZATION SECTION.                                     PPAPTDST
011300     SKIP1                                                        PPAPTDST
011400     SET NOT-FIRST-TIME TO TRUE.                                  PPAPTDST
022200     MOVE 'PPAPTDST' TO XWHC-DISPLAY-MODULE.                      36060628
022300     COPY CPPDXWHC.                                               36060628
024610*****                                                          CD 69031419
012300     EJECT                                                        PPAPTDST
013400 5000-LOAD-APTDST SECTION.                                        PPAPTDST
013500     SKIP1                                                        PPAPTDST
013600     MOVE SPACES TO KAPT-APPOINTMENT-GROUP                        PPAPTDST
013700                    KAPT-DISTRIBUTION-GROUP.                      PPAPTDST
013800     SKIP1                                                        PPAPTDST
024700     MOVE 1 TO DIST-INDX.                                         36060628
027010*****                                                          CD 69031419
013810     SKIP1                                                        PPAPTDST
013900     PERFORM SQL-OPEN-APPT-ROW-9812.                              PPAPTDST
014000     PERFORM SQL-FETCH-APPT-ROW-9813.                             PPAPTDST
025200     SKIP1                                                        36060628
025300***   CHECK IF ANY APPOINTMENTS WERE FOUND.                       36060628
025400     IF SQLCODE NOT = ZERO                                        36060628
025500         SET KAPT-APTDST-NOT-FOUND TO TRUE                        36060628
025600         MOVE ZERO TO KAPT-APPT-NUM OF KAPT-APPT-ARRAY (1)        36060628
025700         PERFORM SQL-CLOSE-APPT-ROW-9814                          36060628
025800         GO TO 5099-EXIT.                                         36060628
025900     SKIP1                                                        36060628
026000     SET KAPT-APTDST-FOUND TO TRUE.                               36060628
014100     PERFORM VARYING APPT-INDX FROM 1 BY 1 UNTIL SQLCODE = +100   PPAPTDST
014200         OR APPT-INDX > 9                                         PPAPTDST
014300         MOVE APPOINT-DATA TO KAPT-APPT-ARRAY (APPT-INDX)         PPAPTDST
014301         MOVE APPT-NUM OF APPOINT-DATA TO APPOINT-NUM             PPAPTDST
026500     SKIP1                                                        36060628
014310         PERFORM SQL-OPEN-DIST-ROW-9822                           PPAPTDST
014320         PERFORM SQL-FETCH-DIST-ROW-9823                          PPAPTDST
026800     SKIP1                                                        36060628
014321         IF SQLCODE = 0                                           PPAPTDST
014323             COMPUTE KAPT-FIRST-DIST-INDX (APPT-INDX) = DIST-INDX PPAPTDST
029310*****                                                          CD 69031419
027200             PERFORM UNTIL SQLCODE = +100 OR DIST-INDX > 72       36060628
027300                 MOVE DISTRIB-DATA TO KAPT-DIST-ARRAY (DIST-INDX) 36060628
027400                 PERFORM SQL-FETCH-DIST-ROW-9823                  36060628
027500                 ADD 1 TO DIST-INDX                               36060628
027600             END-PERFORM                                          36060628
027700             COMPUTE KAPT-LAST-DIST-INDX (APPT-INDX) = DIST-INDX  36060628
027800                     - 1                                          36060628
027900         ELSE                                                     36060628
028000             MOVE ZERO TO KAPT-FIRST-DIST-INDX (APPT-INDX)        36060628
028100             MOVE ZERO TO KAPT-LAST-DIST-INDX (APPT-INDX)         36060628
014327         END-IF                                                   PPAPTDST
028300     SKIP1                                                        36060628
030610*****                                                          CD 69031419
029100     SKIP1                                                        36060628
014380         PERFORM SQL-CLOSE-DIST-ROW-9824                          PPAPTDST
014400         PERFORM SQL-FETCH-APPT-ROW-9813                          PPAPTDST
014500     END-PERFORM.                                                 PPAPTDST
029500     SKIP1                                                        36060628
014600     PERFORM SQL-CLOSE-APPT-ROW-9814.                             PPAPTDST
014700*                                                                 PPAPTDST
014710     SKIP1                                                        PPAPTDST
018900 5099-EXIT.                                                       PPAPTDST
019000     SKIP1                                                        PPAPTDST
019100     EXIT.                                                        PPAPTDST
019200     EJECT                                                        PPAPTDST
019300 7000-FIND-APTDST SECTION.                                        PPAPTDST
019400     SKIP1                                                        PPAPTDST
019500     MOVE SPACES TO KTAP-TABLE                                    PPAPTDST
019600                    KTDS-TABLE.                                   PPAPTDST
019601     SET KAPT-APTDST-NOT-FOUND TO TRUE.                           PPAPTDST
019602     IF   KAPT-SEARCH-ARG IS NOT NUMERIC                          PPAPTDST
019603       OR KAPT-SEARCH-ARG = ZEROS                                 PPAPTDST
019604          GO TO 7099-EXIT.                                        PPAPTDST
019605     SKIP1                                                        PPAPTDST
031200***   APPT NUMBERS ARE MULTIPLES OF 10. ALL OTHERS ARE DISTS      36060628
019610     DIVIDE 10 INTO KAPT-SEARCH-ARG GIVING QUOTIENT REMAINDER     PPAPTDST
019620         Y-REMAINDER.                                             PPAPTDST
019630     IF Y-REMAINDER = 0                                           PPAPTDST
019631         MOVE KAPT-SEARCH-ARG TO APPOINT-NUM                      PPAPTDST
019640         PERFORM SQL-SELECT-APPT-DATA-9810                        PPAPTDST
019641         MOVE APPOINT-DATA TO KTAP-TABLE                          PPAPTDST
019650     ELSE                                                         PPAPTDST
019651         MOVE KAPT-SEARCH-ARG TO DISTRIB-NUM                      PPAPTDST
019660         PERFORM SQL-SELECT-DIST-DATA-9820                        PPAPTDST
019670         MOVE DISTRIB-DATA TO KTDS-TABLE.                         PPAPTDST
019700     IF SQLCODE = 0                                               PPAPTDST
019800           SET KAPT-APTDST-FOUND  TO TRUE.                        PPAPTDST
020300     SKIP1                                                        PPAPTDST
020400 7099-EXIT.                                                       PPAPTDST
020500     SKIP1                                                        PPAPTDST
020600     EXIT.                                                        PPAPTDST
023800     SKIP1                                                        PPAPTDST
035210*****                                                          CD 69031419
023920 EJECT                                                            PPAPTDST
023960******************************************************************PPAPTDST
023970*                                                                *PPAPTDST
023980*                SQL- SELECTS                                    *PPAPTDST
023990*                                                                *PPAPTDST
025500*                                                                *PPAPTDST
025510******************************************************************PPAPTDST
025511*                                                                *PPAPTDST
025512*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPAPTDST
025513*                                                                *PPAPTDST
025514*          VIEW                 VIEW         DCL                 *PPAPTDST
025515*                                DDL        INCLUDE              *PPAPTDST
036800*****   PPPVAPT1_APP           PPPVAPT1     PPPVAPT1             *69031419
036810*       PPPVAPP1_APP           PPPVAPP1     PPPVAPP1             *69031419
036900*****   PPPVAPT2_DIS           PPPVAPT2     PPPVAPT2             *69031419
036910*       PPPVDIS1_DIS           PPPVDIS1     PPPVDIS1             *69031419
025518*                                                                *PPAPTDST
025525******************************************************************PPAPTDST
025526*               APPOINTMENT SQL                                  *PPAPTDST
025530******************************************************************PPAPTDST
035100     SKIP1                                                        36060628
025600 SQL-SELECT-APPT-DATA-9810 SECTION.                               PPAPTDST
025700     IF WS-EMPLOYEE-ID = LAST-APPT-EMPID AND                      PPAPTDST
025800       APPOINT-NUM = LAST-APPT-NUM                                PPAPTDST
025900       MOVE LAST-APPT-SQLCODE TO SQLCODE                          PPAPTDST
026000       GO TO SQL-SELECT-APPT-DATA-9811.                           PPAPTDST
026100     MOVE 'SELECT APPOINTMENT DATA' TO DB2MSG-TAG.                PPAPTDST
026200     EXEC SQL                                                     PPAPTDST
026300       SELECT *                                                   PPAPTDST
026400       INTO :APPOINT-DATA                                         PPAPTDST
029500*******FROM NPPVAPT1_APP                                          30950468
038500*****  FROM PPPVAPT1_APP                                          69031419
038510       FROM PPPVAPP1_APP                                          69031419
026600       WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                        PPAPTDST
026700       AND APPT_NUM = :APPOINT-NUM                                PPAPTDST
026800     END-EXEC.                                                    PPAPTDST
026900     MOVE SQLCODE TO LAST-APPT-SQLCODE.                           PPAPTDST
027000     MOVE WS-EMPLOYEE-ID TO LAST-APPT-EMPID.                      PPAPTDST
027100     MOVE APPOINT-NUM TO LAST-APPT-NUM.                           PPAPTDST
036900     SKIP1                                                        36060628
027200 SQL-SELECT-APPT-DATA-9811. EXIT.                                 PPAPTDST
027300*                                                                 PPAPTDST
027310 SQL-OPEN-APPT-ROW-9812 SECTION.                                  PPAPTDST
027320     MOVE 'OPEN APPT CURSOR' TO DB2MSG-TAG.                       PPAPTDST
027330     EXEC SQL                                                     PPAPTDST
027340          OPEN APPT_ROW                                           PPAPTDST
027350     END-EXEC.                                                    PPAPTDST
027360*                                                                 PPAPTDST
027370 SQL-FETCH-APPT-ROW-9813 SECTION.                                 PPAPTDST
027380     MOVE 'FETCH APPT ROW' TO DB2MSG-TAG.                         PPAPTDST
027390     EXEC SQL                                                     PPAPTDST
027391          FETCH APPT_ROW INTO :APPOINT-DATA                       PPAPTDST
027392     END-EXEC.                                                    PPAPTDST
027393*                                                                 PPAPTDST
027394 SQL-CLOSE-APPT-ROW-9814 SECTION.                                 PPAPTDST
027395     MOVE 'CLOSE APPT CURSOR' TO DB2MSG-TAG.                      PPAPTDST
027396     EXEC SQL                                                     PPAPTDST
027397          CLOSE APPT_ROW                                          PPAPTDST
027398     END-EXEC.                                                    PPAPTDST
038900     SKIP3                                                        36060628
027399******************************************************************PPAPTDST
027400*               DISTRIBUTION SQL                                 *PPAPTDST
027401******************************************************************PPAPTDST
039300     SKIP1                                                        36060628
027410 SQL-SELECT-DIST-DATA-9820 SECTION.                               PPAPTDST
027500     IF WS-EMPLOYEE-ID = LAST-DIST-EMPID AND                      PPAPTDST
027600       DISTRIB-NUM = LAST-DIST-NUM                                PPAPTDST
027700       MOVE LAST-DIST-SQLCODE TO SQLCODE                          PPAPTDST
027800       GO TO SQL-SELECT-DIST-DATA-9821.                           PPAPTDST
027900     MOVE 'SELECT DISTRIBUTION DATA' TO DB2MSG-TAG.               PPAPTDST
028000     EXEC SQL                                                     PPAPTDST
028100       SELECT *                                                   PPAPTDST
028200       INTO :DISTRIB-DATA                                         PPAPTDST
033400*******FROM NPPVAPT2_DIS                                          30950468
040400****   FROM PPPVAPT2_DIS                                          36500893
040410       FROM PPPVDIS1_DIS                                          36500893
028400       WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                        PPAPTDST
028500       AND DIST_NUM = :DISTRIB-NUM                                PPAPTDST
028600     END-EXEC.                                                    PPAPTDST
028700     MOVE SQLCODE TO LAST-DIST-SQLCODE.                           PPAPTDST
028800     MOVE WS-EMPLOYEE-ID TO LAST-DIST-EMPID.                      PPAPTDST
028900     MOVE DISTRIB-NUM TO LAST-DIST-NUM.                           PPAPTDST
041100     SKIP1                                                        36060628
029000 SQL-SELECT-DIST-DATA-9821.  EXIT.                                PPAPTDST
029010*                                                                 PPAPTDST
029020 SQL-OPEN-DIST-ROW-9822 SECTION.                                  PPAPTDST
029030     MOVE 'OPEN DIST CURSOR' TO DB2MSG-TAG.                       PPAPTDST
029040     EXEC SQL                                                     PPAPTDST
029050          OPEN DIST_ROW                                           PPAPTDST
029060     END-EXEC.                                                    PPAPTDST
029070*                                                                 PPAPTDST
029080 SQL-FETCH-DIST-ROW-9823 SECTION.                                 PPAPTDST
029090     MOVE 'FETCH DIST ROW' TO DB2MSG-TAG.                         PPAPTDST
029091     EXEC SQL                                                     PPAPTDST
029092          FETCH DIST_ROW INTO :DISTRIB-DATA                       PPAPTDST
029093     END-EXEC.                                                    PPAPTDST
029094*                                                                 PPAPTDST
029095 SQL-CLOSE-DIST-ROW-9824 SECTION.                                 PPAPTDST
029096     MOVE 'CLOSE DIST CURSOR' TO DB2MSG-TAG.                      PPAPTDST
029097     EXEC SQL                                                     PPAPTDST
029098          CLOSE DIST_ROW                                          PPAPTDST
029099     END-EXEC.                                                    PPAPTDST
029100*                                                                 PPAPTDST
029110********************** DB2 ABEND ROUTINE *************************PPAPTDST
029200     COPY CPPDXP99.                                               PPAPTDST
043400     SKIP3                                                        36060628
029300     SET KAPT-INVALID-FUNCTION TO TRUE.                           PPAPTDST
029310     GOBACK.                                                      PPAPTDST
029400******************************************************************PPAPTDST
