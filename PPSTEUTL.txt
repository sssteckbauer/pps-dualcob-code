000000**************************************************************/   EFIX1078
000001*  PROGRAM: PPSTEUTL                                         */   EFIX1078
000002*  RELEASE: ___1078______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1078
000003*  NAME:__SRS____________ MODIFICATION DATE:  ___07/17/96__  */   EFIX1078
000004*  DESCRIPTION: ERROR REPORT 1415:                           */   EFIX1078
000005*  - CORRECT LOADING OF STC TAX TABLE.                       */   EFIX1078
000007**************************************************************/   EFIX1078
000100**************************************************************/   36430911
000200*  PROGRAM:  PPSTEUTL                                        */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME __SRS__________   MODIFICATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*  REPLACEMENT MODULE FOR DB2 ACCESS TO STATE TAX TABLES     */   36430911
000700**************************************************************/   36430911
000800 IDENTIFICATION DIVISION.                                         PPSTEUTL
000900 PROGRAM-ID. PPSTEUTL.                                            PPSTEUTL
001000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPSTEUTL
001100 DATE-COMPILED.                                                   PPSTEUTL
001200 ENVIRONMENT DIVISION.                                            PPSTEUTL
001300 DATA DIVISION.                                                   PPSTEUTL
001400 WORKING-STORAGE SECTION.                                         PPSTEUTL
001500 01  WS-MISC.                                                     PPSTEUTL
001600     05  IX1                      PIC S9(03) COMP.                PPSTEUTL
001700     05  IX2                      PIC S9(03) COMP.                PPSTEUTL
001710     05  IX3                      PIC S9(03) COMP.                EFIX1078
001800     05  SGL                      PIC S9(03) COMP.                PPSTEUTL
001900     05  MRY                      PIC S9(03) COMP.                PPSTEUTL
002000     05  HSE                      PIC S9(03) COMP.                PPSTEUTL
002100     05  MP0549                   PIC X(05)  VALUE 'P0549'.       PPSTEUTL
002200     05  WS-PERIOD-TYPE           PIC X      VALUE SPACE.         PPSTEUTL
002300 01  PPDB2MSG-INTERFACE.          COPY CPLNKDB2.                  PPSTEUTL
002400 01  STX-INFORMATION EXTERNAL.                                    PPSTEUTL
002500     EXEC SQL                                                     PPSTEUTL
002600        INCLUDE CPWSXSTT                                          PPSTEUTL
002700     END-EXEC.                                                    PPSTEUTL
002800 01  STATE-TAX-INTERFACE EXTERNAL.                                PPSTEUTL
002900     EXEC SQL                                                     PPSTEUTL
003000         INCLUDE CPLNKSTE                                         PPSTEUTL
003100     END-EXEC.                                                    PPSTEUTL
003200 01  WS-STA.                                                      PPSTEUTL
003300     EXEC SQL                                                     PPSTEUTL
003400         INCLUDE PPPVZSTA                                         PPSTEUTL
003500     END-EXEC.                                                    PPSTEUTL
003600 01  WS-STC.                                                      PPSTEUTL
003700     EXEC SQL                                                     PPSTEUTL
003800         INCLUDE PPPVZSTC                                         PPSTEUTL
003900     END-EXEC.                                                    PPSTEUTL
004000 01  WS-STI.                                                      PPSTEUTL
004100     EXEC SQL                                                     PPSTEUTL
004200         INCLUDE PPPVZSTI                                         PPSTEUTL
004300     END-EXEC.                                                    PPSTEUTL
004400 01  WS-STR.                                                      PPSTEUTL
004500     EXEC SQL                                                     PPSTEUTL
004600         INCLUDE PPPVZSTR                                         PPSTEUTL
004700     END-EXEC.                                                    PPSTEUTL
004800     EXEC SQL                                                     PPSTEUTL
004900        INCLUDE SQLCA                                             PPSTEUTL
005000     END-EXEC.                                                    PPSTEUTL
005100     EXEC SQL                                                     PPSTEUTL
005200      DECLARE STC_ROW CURSOR FOR                                  PPSTEUTL
005300     SELECT                                                       PPSTEUTL
005400              STC_MARITAL_STATUS,                                 PPSTEUTL
005500              STC_AMT                                             PPSTEUTL
005600     FROM     PPPVZSTC_STC                                        PPSTEUTL
005700     WHERE    STC_PERIOD_TYPE = :WS-PERIOD-TYPE                   PPSTEUTL
005800     ORDER BY STC_MARITAL_STATUS,                                 PPSTEUTL
005900              STC_AMT                                             PPSTEUTL
006000     END-EXEC.                                                    PPSTEUTL
006100     EXEC SQL                                                     PPSTEUTL
006200      DECLARE STI_ROW CURSOR FOR                                  PPSTEUTL
006300     SELECT                                                       PPSTEUTL
006400              STI_AMT                                             PPSTEUTL
006500     FROM     PPPVZSTI_STI                                        PPSTEUTL
006600     WHERE    STI_PERIOD_TYPE = :WS-PERIOD-TYPE                   PPSTEUTL
006700     ORDER BY STI_AMT                                             PPSTEUTL
006800     END-EXEC.                                                    PPSTEUTL
006900     EXEC SQL                                                     PPSTEUTL
007000      DECLARE STR_ROW CURSOR FOR                                  PPSTEUTL
007100     SELECT                                                       PPSTEUTL
007200              STR_MARITAL_STATUS,                                 PPSTEUTL
007300              STR_MIN_SALARY,                                     PPSTEUTL
007400              STR_TAX_AMT,                                        PPSTEUTL
007500              STR_TAX_PERCENT                                     PPSTEUTL
007600     FROM     PPPVZSTR_STR                                        PPSTEUTL
007700     WHERE    STR_PERIOD_TYPE = :WS-PERIOD-TYPE                   PPSTEUTL
007800     ORDER BY STR_MARITAL_STATUS,                                 PPSTEUTL
007900              STR_MIN_SALARY                                      PPSTEUTL
008000     END-EXEC.                                                    PPSTEUTL
008100                                                                  PPSTEUTL
008200 PROCEDURE DIVISION.                                              PPSTEUTL
008300                                                                  PPSTEUTL
008400 0000-LOAD-TAX-TABLES.                                            PPSTEUTL
008500                                                                  PPSTEUTL
008600     EXEC SQL                                                     PPSTEUTL
008700        INCLUDE CPPDXE99                                          PPSTEUTL
008800     END-EXEC.                                                    PPSTEUTL
008900     INITIALIZE STX-INFORMATION.                                  PPSTEUTL
009000     PERFORM VARYING IX1 FROM +1 BY +1                            PPSTEUTL
009100         UNTIL IX1 > +4 OR KSTE-CYC (IX1) = SPACE                 PPSTEUTL
009200      MOVE KSTE-CYC (IX1) TO WS-PERIOD-TYPE                       PPSTEUTL
009300      IF WS-PERIOD-TYPE NOT = ('M' AND 'S' AND 'B')               PPSTEUTL
009400         MOVE 'M' TO WS-PERIOD-TYPE                               PPSTEUTL
009500      END-IF                                                      PPSTEUTL
009600      PERFORM 1000-GET-STA-DATA                                   PPSTEUTL
009700      PERFORM 1100-GET-STC-DATA                                   PPSTEUTL
009800      PERFORM 1200-GET-STI-DATA                                   PPSTEUTL
009900      PERFORM 1300-GET-STR-DATA                                   PPSTEUTL
010000     END-PERFORM.                                                 PPSTEUTL
010100                                                                  PPSTEUTL
010200 0100-END.                                                        PPSTEUTL
010300                                                                  PPSTEUTL
010400     GOBACK.                                                      PPSTEUTL
010500                                                                  PPSTEUTL
010600 1000-GET-STA-DATA.                                               PPSTEUTL
010700                                                                  PPSTEUTL
010800      PERFORM 2000-SELECT-STA                                     PPSTEUTL
010900      MOVE WS-PERIOD-TYPE    TO STX-PERIOD-TYPE (IX1)             PPSTEUTL
011000      MOVE STA-A-LI-SINGLE   TO STX-A-SINGLE   (IX1)              PPSTEUTL
011100      MOVE STA-A-LI-MARRIED1 TO STX-A-MARRIED1 (IX1)              PPSTEUTL
011200      MOVE STA-A-LI-MARRIED2 TO STX-A-MARRIED2 (IX1)              PPSTEUTL
011300      MOVE STA-A-LI-HEAD-HSE TO STX-A-HEAD-HSE (IX1)              PPSTEUTL
011400      MOVE STA-C-SD-SINGLE   TO STX-C-SINGLE   (IX1)              PPSTEUTL
011500      MOVE STA-C-SD-MARRIED1 TO STX-C-MARRIED1 (IX1)              PPSTEUTL
011600      MOVE STA-C-SD-MARRIED2 TO STX-C-MARRIED2 (IX1)              PPSTEUTL
011700      MOVE STA-C-SD-HEAD-HSE TO STX-C-HEAD-HSE (IX1).             PPSTEUTL
011800                                                                  PPSTEUTL
011900 1100-GET-STC-DATA.                                               PPSTEUTL
012000                                                                  PPSTEUTL
012100******PERFORM 2100-OPEN-STC.                                      EFIX1078
012200******PERFORM 2200-FETCH-STC.                                     EFIX1078
012300******PERFORM VARYING IX2 FROM 1 BY 1                             EFIX1078
012400********UNTIL SQLCODE NOT = 0                                     EFIX1078
012500***********OR IX2 > 10                                            EFIX1078
012600*******EVALUATE STC-MARITAL-STATUS                                EFIX1078
012700*********WHEN 'S'                                                 EFIX1078
012800**********MOVE STC-AMT TO STX-D-SINGLE (IX1 IX2)                  EFIX1078
012900*********WHEN 'O'                                                 EFIX1078
013000**********MOVE STC-AMT TO STX-D-OTHER  (IX1 IX2)                  EFIX1078
013010      PERFORM 2100-OPEN-STC.                                      EFIX1078
013020      PERFORM 2200-FETCH-STC.                                     EFIX1078
013021      MOVE ZERO TO IX2                                            EFIX1078
013022                   IX3.                                           EFIX1078
013030      PERFORM UNTIL SQLCODE NOT = 0                               EFIX1078
013060       EVALUATE STC-MARITAL-STATUS                                EFIX1078
013070         WHEN 'O'                                                 EFIX1078
013071          ADD 1 TO IX2                                            EFIX1078
013080          MOVE STC-AMT TO STX-D-OTHER  (IX1 IX2)                  EFIX1078
013090         WHEN 'S'                                                 EFIX1078
013091          ADD 1 TO IX3                                            EFIX1078
013092          MOVE STC-AMT TO STX-D-SINGLE (IX1 IX3)                  EFIX1078
013100       END-EVALUATE                                               PPSTEUTL
013200       PERFORM 2200-FETCH-STC                                     PPSTEUTL
013300      END-PERFORM.                                                PPSTEUTL
013400      PERFORM 2300-CLOSE-STC.                                     PPSTEUTL
013500                                                                  PPSTEUTL
013600 1200-GET-STI-DATA.                                               PPSTEUTL
013700                                                                  PPSTEUTL
013800      PERFORM 2400-OPEN-STI.                                      PPSTEUTL
013900      PERFORM 2500-FETCH-STI.                                     PPSTEUTL
014000      PERFORM VARYING IX2 FROM 1 BY 1                             PPSTEUTL
014100        UNTIL SQLCODE NOT = 0                                     PPSTEUTL
014200           OR IX2 > 10                                            PPSTEUTL
014300       MOVE STI-AMT TO STX-B-ID-AMT (IX1 IX2)                     PPSTEUTL
014400       PERFORM 2500-FETCH-STI                                     PPSTEUTL
014500      END-PERFORM.                                                PPSTEUTL
014600      PERFORM 2600-CLOSE-STI.                                     PPSTEUTL
014700                                                                  PPSTEUTL
014800 1300-GET-STR-DATA.                                               PPSTEUTL
014900                                                                  PPSTEUTL
015000      MOVE ZERO TO SGL                                            PPSTEUTL
015100                   MRY                                            PPSTEUTL
015200                   HSE.                                           PPSTEUTL
015300      PERFORM 2700-OPEN-STR.                                      PPSTEUTL
015400      PERFORM 2800-FETCH-STR.                                     PPSTEUTL
015500      PERFORM VARYING IX2 FROM 1 BY 1                             PPSTEUTL
015600        UNTIL SQLCODE NOT = 0                                     PPSTEUTL
015700       EVALUATE STR-MARITAL-STATUS                                PPSTEUTL
015800         WHEN 'S'                                                 PPSTEUTL
015900          IF SGL < 11                                             PPSTEUTL
016000             ADD 1 TO SGL                                         PPSTEUTL
016100             MOVE STR-MIN-SALARY TO STX-E-S-MIN-SAL (IX1 SGL)     PPSTEUTL
016200             MOVE STR-TAX-AMT  TO STX-E-S-TAX-AMT (IX1 SGL)       PPSTEUTL
016300             MOVE STR-TAX-PERCENT TO STX-E-S-TAX-PCT (IX1 SGL)    PPSTEUTL
016400          END-IF                                                  PPSTEUTL
016500         WHEN 'M'                                                 PPSTEUTL
016600          IF MRY < 11                                             PPSTEUTL
016700             ADD 1 TO MRY                                         PPSTEUTL
016800             MOVE STR-MIN-SALARY TO STX-E-M-MIN-SAL (IX1 MRY)     PPSTEUTL
016900             MOVE STR-TAX-AMT  TO STX-E-M-TAX-AMT (IX1 MRY)       PPSTEUTL
017000             MOVE STR-TAX-PERCENT TO STX-E-M-TAX-PCT (IX1 MRY)    PPSTEUTL
017100           END-IF                                                 PPSTEUTL
017200         WHEN 'H'                                                 PPSTEUTL
017300          IF HSE < 11                                             PPSTEUTL
017400             ADD 1 TO HSE                                         PPSTEUTL
017500             MOVE STR-MIN-SALARY TO STX-E-H-MIN-SAL (IX1 HSE)     PPSTEUTL
017600             MOVE STR-TAX-AMT  TO STX-E-H-TAX-AMT (IX1 HSE)       PPSTEUTL
017700             MOVE STR-TAX-PERCENT TO STX-E-H-TAX-PCT (IX1 HSE)    PPSTEUTL
017800          END-IF                                                  PPSTEUTL
017900       END-EVALUATE                                               PPSTEUTL
018000       PERFORM 2800-FETCH-STR                                     PPSTEUTL
018100      END-PERFORM.                                                PPSTEUTL
018200      PERFORM 2900-CLOSE-STR.                                     PPSTEUTL
018300      MOVE SGL TO STX-E-NO-SINGLE (IX1).                          PPSTEUTL
018400      MOVE MRY TO STX-E-NO-MARRIED (IX1).                         PPSTEUTL
018500      MOVE HSE TO STX-E-NO-HEAD-HSE (IX1).                        PPSTEUTL
018600                                                                  PPSTEUTL
018700 2000-SELECT-STA.                                                 PPSTEUTL
018800                                                                  PPSTEUTL
018900     MOVE '2000-SELECT-STA' TO DB2MSG-TAG.                        PPSTEUTL
019000     EXEC SQL                                                     PPSTEUTL
019100        SELECT                                                    PPSTEUTL
019200              STA_A_LI_SINGLE,                                    PPSTEUTL
019300              STA_A_LI_MARRIED1,                                  PPSTEUTL
019400              STA_A_LI_MARRIED2,                                  PPSTEUTL
019500              STA_A_LI_HEAD_HSE,                                  PPSTEUTL
019600              STA_C_SD_SINGLE,                                    PPSTEUTL
019700              STA_C_SD_MARRIED1,                                  PPSTEUTL
019800              STA_C_SD_MARRIED2,                                  PPSTEUTL
019900              STA_C_SD_HEAD_HSE                                   PPSTEUTL
020000         INTO                                                     PPSTEUTL
020100              :STA-A-LI-SINGLE,                                   PPSTEUTL
020200              :STA-A-LI-MARRIED1,                                 PPSTEUTL
020300              :STA-A-LI-MARRIED2,                                 PPSTEUTL
020400              :STA-A-LI-HEAD-HSE,                                 PPSTEUTL
020500              :STA-C-SD-SINGLE,                                   PPSTEUTL
020600              :STA-C-SD-MARRIED1,                                 PPSTEUTL
020700              :STA-C-SD-MARRIED2,                                 PPSTEUTL
020800              :STA-C-SD-HEAD-HSE                                  PPSTEUTL
020900     FROM     PPPVZSTA_STA                                        PPSTEUTL
021000     WHERE    STA_PERIOD_TYPE = :WS-PERIOD-TYPE                   PPSTEUTL
021100     END-EXEC.                                                    PPSTEUTL
021200     IF SQLCODE NOT = ZERO                                        PPSTEUTL
021300        MOVE MP0549 TO KSTE-MSSG-NO                               PPSTEUTL
021400        GO TO 0100-END                                            PPSTEUTL
021500     END-IF.                                                      PPSTEUTL
021600                                                                  PPSTEUTL
021700 2100-OPEN-STC.                                                   PPSTEUTL
021800                                                                  PPSTEUTL
021900     MOVE '2100-OPEN-STC' TO DB2MSG-TAG.                          PPSTEUTL
022000     EXEC SQL                                                     PPSTEUTL
022100         OPEN STC_ROW                                             PPSTEUTL
022200     END-EXEC.                                                    PPSTEUTL
022300                                                                  PPSTEUTL
022400 2200-FETCH-STC.                                                  PPSTEUTL
022500                                                                  PPSTEUTL
022600     MOVE '2200-FETCH-STC' TO DB2MSG-TAG.                         PPSTEUTL
022700     EXEC SQL                                                     PPSTEUTL
022800         FETCH STC_ROW                                            PPSTEUTL
022900         INTO :STC-MARITAL-STATUS,                                PPSTEUTL
023000              :STC-AMT                                            PPSTEUTL
023100     END-EXEC.                                                    PPSTEUTL
023200                                                                  PPSTEUTL
023300 2300-CLOSE-STC.                                                  PPSTEUTL
023400                                                                  PPSTEUTL
023500     MOVE '2300-CLOSE-STC' TO DB2MSG-TAG.                         PPSTEUTL
023600     EXEC SQL                                                     PPSTEUTL
023700         CLOSE STC_ROW                                            PPSTEUTL
023800     END-EXEC.                                                    PPSTEUTL
023900                                                                  PPSTEUTL
024000 2400-OPEN-STI.                                                   PPSTEUTL
024100                                                                  PPSTEUTL
024200     MOVE '2400-OPEN-STI' TO DB2MSG-TAG.                          PPSTEUTL
024300     EXEC SQL                                                     PPSTEUTL
024400         OPEN STI_ROW                                             PPSTEUTL
024500     END-EXEC.                                                    PPSTEUTL
024600                                                                  PPSTEUTL
024700 2500-FETCH-STI.                                                  PPSTEUTL
024800                                                                  PPSTEUTL
024900     MOVE '2500-FETCH-STI' TO DB2MSG-TAG.                         PPSTEUTL
025000     EXEC SQL                                                     PPSTEUTL
025100         FETCH STI_ROW                                            PPSTEUTL
025200         INTO :STI-AMT                                            PPSTEUTL
025300     END-EXEC.                                                    PPSTEUTL
025400                                                                  PPSTEUTL
025500 2600-CLOSE-STI.                                                  PPSTEUTL
025600                                                                  PPSTEUTL
025700     MOVE '2600-CLOSE-STI' TO DB2MSG-TAG.                         PPSTEUTL
025800     EXEC SQL                                                     PPSTEUTL
025900         CLOSE STI_ROW                                            PPSTEUTL
026000     END-EXEC.                                                    PPSTEUTL
026100                                                                  PPSTEUTL
026200 2700-OPEN-STR.                                                   PPSTEUTL
026300                                                                  PPSTEUTL
026400     MOVE '2700-OPEN-STR' TO DB2MSG-TAG.                          PPSTEUTL
026500     EXEC SQL                                                     PPSTEUTL
026600         OPEN STR_ROW                                             PPSTEUTL
026700     END-EXEC.                                                    PPSTEUTL
026800                                                                  PPSTEUTL
026900 2800-FETCH-STR.                                                  PPSTEUTL
027000                                                                  PPSTEUTL
027100     MOVE '2800-FETCH-STR' TO DB2MSG-TAG.                         PPSTEUTL
027200     EXEC SQL                                                     PPSTEUTL
027300         FETCH STR_ROW                                            PPSTEUTL
027400         INTO :STR-MARITAL-STATUS,                                PPSTEUTL
027500              :STR-MIN-SALARY,                                    PPSTEUTL
027600              :STR-TAX-AMT,                                       PPSTEUTL
027700              :STR-TAX-PERCENT                                    PPSTEUTL
027800     END-EXEC.                                                    PPSTEUTL
027900                                                                  PPSTEUTL
028000 2900-CLOSE-STR.                                                  PPSTEUTL
028100                                                                  PPSTEUTL
028200     MOVE '2900-CLOSE-STR' TO DB2MSG-TAG.                         PPSTEUTL
028300     EXEC SQL                                                     PPSTEUTL
028400         CLOSE STR_ROW                                            PPSTEUTL
028500     END-EXEC.                                                    PPSTEUTL
028600                                                                  PPSTEUTL
028700*999999-SQL-ERROR.                                                PPSTEUTL
028800     EXEC SQL                                                     PPSTEUTL
028900        INCLUDE CPPDXP99                                          PPSTEUTL
029000     END-EXEC.                                                    PPSTEUTL
029100     MOVE MP0549 TO KSTE-MSSG-NO.                                 PPSTEUTL
029200     GO TO 0100-END.                                              PPSTEUTL
