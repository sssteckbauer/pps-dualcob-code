000100**************************************************************/  *36070535
000200*  PROGRAM: PPACTUTW                                         */  *36070535
000300*  RELEASE: ____0535____  SERVICE REQUEST(S): ____3607____   */  *36070535
000400*  NAME:  ______JAG_____  CREATION DATE:      __02/06/91__   */  *36070535
000500*  DESCRIPTION:                                              */  *36070535
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT ACT ROWS.     */  *36070535
000700**************************************************************/  *36070535
000800 IDENTIFICATION DIVISION.                                         PPACTUTW
000900 PROGRAM-ID. PPACTUTW                                             PPACTUTW
001000 AUTHOR. UCOP.                                                    PPACTUTW
001100 DATE-WRITTEN.  JANUARY, 1991.                                    PPACTUTW
001200 DATE-COMPILED.                                                   PPACTUTW
001300*REMARKS.                                                         PPACTUTW
001400*            CALL 'PPACTUTW' USING                                PPACTUTW
001500*                            PPXXXUTW-INTERFACE                   PPACTUTW
001600*                            ACT-ROW.                             PPACTUTW
001700*                                                                 PPACTUTW
001800*                                                                 PPACTUTW
001900     EJECT                                                        PPACTUTW
002000 ENVIRONMENT DIVISION.                                            PPACTUTW
002100 CONFIGURATION SECTION.                                           PPACTUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPACTUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPACTUTW
002400      EJECT                                                       PPACTUTW
002500******************************************************************PPACTUTW
002600*                                                                 PPACTUTW
002700*                D A T A  D I V I S I O N                         PPACTUTW
002800*                                                                 PPACTUTW
002900******************************************************************PPACTUTW
003000 DATA DIVISION.                                                   PPACTUTW
003100 WORKING-STORAGE SECTION.                                         PPACTUTW
003200 01  MISCELLANEOUS-WS.                                            PPACTUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPACTUTW'.         PPACTUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPACTUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPACTUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPACTUTW
003700     EJECT                                                        PPACTUTW
003800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPACTUTW
003900******************************************************************PPACTUTW
004000*          SQL - WORKING STORAGE                                 *PPACTUTW
004100******************************************************************PPACTUTW
004200 01  ACT-ROW-DATA.                                                PPACTUTW
004300     EXEC SQL                                                     PPACTUTW
004400          INCLUDE PPPVACT2                                        PPACTUTW
004500     END-EXEC.                                                    PPACTUTW
004600     EJECT                                                        PPACTUTW
004700     EXEC SQL                                                     PPACTUTW
004800          INCLUDE SQLCA                                           PPACTUTW
004900     END-EXEC.                                                    PPACTUTW
005000******************************************************************PPACTUTW
005100*                                                                *PPACTUTW
005200*                SQL- SELECTS                                    *PPACTUTW
005300*                                                                *PPACTUTW
005400******************************************************************PPACTUTW
005500*                                                                *PPACTUTW
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPACTUTW
005700*                                                                *PPACTUTW
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPACTUTW
005900*                        MEMBER NAMES                            *PPACTUTW
006000*                                                                *PPACTUTW
006100*  PPPVACT2_ACT          PPPVACT2                                *PPACTUTW
006200*                                                                *PPACTUTW
006300******************************************************************PPACTUTW
006400*                                                                 PPACTUTW
006500 LINKAGE SECTION.                                                 PPACTUTW
006600*                                                                 PPACTUTW
006700******************************************************************PPACTUTW
006800*                L I N K A G E   S E C T I O N                   *PPACTUTW
006900******************************************************************PPACTUTW
007000 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPACTUTW
007100 01  ACT-ROW.                       COPY CPWSRACT.                PPACTUTW
007200*                                                                 PPACTUTW
007300 PROCEDURE DIVISION USING                                         PPACTUTW
007400                          PPXXXUTW-INTERFACE                      PPACTUTW
007500                          ACT-ROW.                                PPACTUTW
007600******************************************************************PPACTUTW
007700*            P R O C E D U R E  D I V I S I O N                  *PPACTUTW
007800******************************************************************PPACTUTW
007900******************************************************************PPACTUTW
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPACTUTW
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPACTUTW
008200******************************************************************PPACTUTW
008300     EXEC SQL                                                     PPACTUTW
008400          INCLUDE CPPDXE99                                        PPACTUTW
008500     END-EXEC.                                                    PPACTUTW
008600     SKIP3                                                        PPACTUTW
008700 0100-MAINLINE SECTION.                                           PPACTUTW
008800     SKIP1                                                        PPACTUTW
008900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPACTUTW
009000     MOVE EMPLOYEE-ID OF ACT-ROW TO WS-EMPLOYEE-ID.               PPACTUTW
009100     SKIP1                                                        PPACTUTW
009200     MOVE ACT-ROW TO ACT-ROW-DATA                                 PPACTUTW
009300     SKIP1                                                        PPACTUTW
009400     EVALUATE PPXXXUTW-FUNCTION                                   PPACTUTW
009500         WHEN 'D'                                                 PPACTUTW
009600             PERFORM 8100-DELETE-ROW                              PPACTUTW
009700         WHEN 'U'                                                 PPACTUTW
009800             PERFORM 8200-UPDATE-ROW                              PPACTUTW
009900         WHEN 'I'                                                 PPACTUTW
010000             PERFORM 8300-INSERT-ROW                              PPACTUTW
010100         WHEN OTHER                                               PPACTUTW
010200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPACTUTW
010300             SET PPXXXUTW-ERROR            TO TRUE                PPACTUTW
010400     END-EVALUATE.                                                PPACTUTW
010500     SKIP1                                                        PPACTUTW
010600     GOBACK.                                                      PPACTUTW
010700     EJECT                                                        PPACTUTW
010800******************************************************************PPACTUTW
010900*  PROCEDURE SQL                     FOR ACT VIEW                *PPACTUTW
011000******************************************************************PPACTUTW
011100 8100-DELETE-ROW SECTION.                                         PPACTUTW
011200     SKIP1                                                        PPACTUTW
011300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPACTUTW
011400     SKIP1                                                        PPACTUTW
011500     EXEC SQL                                                     PPACTUTW
011600         DELETE FROM PPPVACT2_ACT                                 PPACTUTW
011700          WHERE   EMPLOYEE_ID        = :EMPLOYEE-ID               PPACTUTW
011800            AND   APPT_NUM           = :APPT-NUM                  PPACTUTW
011900            AND   DIST_NUM           = :DIST-NUM                  PPACTUTW
012000            AND   ACTION_CODE        = :ACTION-CODE               PPACTUTW
012100     END-EXEC.                                                    PPACTUTW
012200     SKIP1                                                        PPACTUTW
012300     IF  SQLCODE    NOT = 0                                       PPACTUTW
012400         SET PPXXXUTW-ERROR TO TRUE                               PPACTUTW
012500     END-IF.                                                      PPACTUTW
012600     EJECT                                                        PPACTUTW
012700 8200-UPDATE-ROW SECTION.                                         PPACTUTW
012800     SKIP1                                                        PPACTUTW
012900     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPACTUTW
013000     SKIP1                                                        PPACTUTW
013100     EXEC SQL                                                     PPACTUTW
013200     UPDATE PPPVACT2_ACT                                          PPACTUTW
013300              SET  ACTION_EFFDATE     = :ACTION-EFFDATE           PPACTUTW
013400      WHERE   EMPLOYEE_ID        = :EMPLOYEE-ID                   PPACTUTW
013500        AND   APPT_NUM           = :APPT-NUM                      PPACTUTW
013600        AND   DIST_NUM           = :DIST-NUM                      PPACTUTW
013700        AND   ACTION_CODE        = :ACTION-CODE                   PPACTUTW
013800     END-EXEC.                                                    PPACTUTW
013900     SKIP1                                                        PPACTUTW
014000     IF  SQLCODE    NOT = 0                                       PPACTUTW
014100         SET PPXXXUTW-ERROR TO TRUE                               PPACTUTW
014200     END-IF.                                                      PPACTUTW
014300     EJECT                                                        PPACTUTW
014400 8300-INSERT-ROW SECTION.                                         PPACTUTW
014500     SKIP1                                                        PPACTUTW
014600     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPACTUTW
014700     SKIP1                                                        PPACTUTW
014800     EXEC SQL                                                     PPACTUTW
014900         INSERT INTO PPPVACT2_ACT                                 PPACTUTW
015000                VALUES (:ACT-ROW-DATA)                            PPACTUTW
015100     END-EXEC.                                                    PPACTUTW
015200     SKIP1                                                        PPACTUTW
015300     IF  SQLCODE    NOT = 0                                       PPACTUTW
015400         SET PPXXXUTW-ERROR TO TRUE                               PPACTUTW
015500     END-IF.                                                      PPACTUTW
015600     EJECT                                                        PPACTUTW
015700*999999-SQL-ERROR.                                               *PPACTUTW
015800     SKIP1                                                        PPACTUTW
015900     EXEC SQL                                                     PPACTUTW
016000          INCLUDE CPPDXP99                                        PPACTUTW
016100     END-EXEC.                                                    PPACTUTW
016200     SKIP1                                                        PPACTUTW
016300     SET PPXXXUTW-ERROR TO TRUE.                                  PPACTUTW
016400     IF NOT HERE-BEFORE                                           PPACTUTW
016500         SET HERE-BEFORE TO TRUE                                  PPACTUTW
016600     GOBACK.                                                      PPACTUTW
