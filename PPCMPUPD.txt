000101*==========================================================%      UCSD0086
000101*=    PROGRAM: PPCMPUPD                                   =%      UCSD0086
000101*=    CHANGE # 0086         PROJ. REQUEST: DT-086         =%      UCSD0086
000101*----------------------------------------------------------%      UCSD0086
000101*=    NAME: D CHILCOAT      MODIFICATION DATE: 07/08/96   =%      UCSD0086
000101*=                                                        =%      UCSD0086
000101*=    DESCRIPTION:                                        =%      UCSD0086
000101*=      WS-INIT-DATE MOVED TO STAFF-EVAL-DATE             =%      UCSD0086
000101*=      IN SECTION 9000-INITIALIZE-ROW                    =%      UCSD0086
000101*=      TEMP-DATA-VALUE MOVED TO STAFF-EVAL-CODE AND      =%      UCSD0086
000101*=      STAFF-EVAL-DATE IN 9100-EVALUATE-DATA-ELEMENT     =%      UCSD0086
000101*==========================================================%      UCSD0038
000101*=    PROGRAM: PPCMPUPD                                   =%      UCSD0038
000101*=    CHANGE # 0038         PROJ. REQUEST: DS-574         =%      UCSD0038
000101*----------------------------------------------------------%      UCSD0038
000101*=    NAME: JIM PIERCE      MODIFICATION DATE: 11/18/91   =%      UCSD0038
000101*=                                                        =%      UCSD0038
000101*=    DESCRIPTION:                                        =%      UCSD0038
000101*=      WS-INIT-DATE MOVED TO ASST-RANK-DATE,             =%      UCSD0038
000101*=      ASSO-RANK-DATE, FULL-RANK-DATE AND STEP-DATE      =%      UCSD0038
000101*=      IN SECTION 9000-INITIALIZE-ROW                    =%      UCSD0038
000101*----------------------------------------------------------%      UCSD0038
000101*=    NAME: R. P. YINGLING  MODIFICATION DATE: 10/18/91   =%      UCSD0038
000101*=                                                        =%      UCSD0038
000101*=    DESCRIPTION:                                        =%      UCSD0038
000101*=      DELETED REFERENCES TO "CAMPUS-DATA3" THRU         =%      UCSD0038
000101*=      "CAMPUS-DATA10" (NOT USED LOCALLY) AND ADDED      =%      UCSD0038
000101*=      LOCAL DATA REFERENCES.                            =%      UCSD0038
000101*=                                                        =%      UCSD0038
000101*==========================================================%      UCSD0038
000101**************************************************************/   36430795
000200*  PROGRAM: PPCMPUPD                                         */   36430795
000300*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000500*  DESCRIPTION:                                              */   36430795
000600*  MAKE INIT-CMP-ROW EXTERNAL.                               */   36430795
000700**************************************************************/   36430795
001200**************************************************************/  *36090574
001300*  PROGRAM:  PPCMPUPD                                        */  *36090574
001400*  RELEASE # ____0574____ SERVICE REQUEST NO(S)___3609_______*/  *36090574
001500*  NAME ______JAG______   MODIFICATION DATE ____06/05/91_____*/  *36090574
001600*  DESCRIPTION                                               */  *36090574
001700*    EDB CMP TABLE UPDATE DRIVER MODULE.                     */  *36090574
001800**************************************************************/  *36090574
001900 IDENTIFICATION DIVISION.                                         PPCMPUPD
002000 PROGRAM-ID. PPCMPUPD.                                            PPCMPUPD
002100 AUTHOR. UCOP.                                                    PPCMPUPD
002200 DATE-WRITTEN. SEPTEMBER 1990.                                    PPCMPUPD
002300 DATE-COMPILED.                                                   PPCMPUPD
002400*REMARKS.                                                         PPCMPUPD
002500******************************************************************PPCMPUPD
002600*                                                                *PPCMPUPD
002700*  THIS PROGRAM DRIVES THE APPLICATION OF EMPLOYEE DATA BASE     *PPCMPUPD
002800* CMP-TABLE ROWS. THIS PROGRAM IS CALLED BY PPEDBUPD.            *PPCMPUPD
002900*                                                                *PPCMPUPD
003000*  THE DETAILS OF THIS PROGRAMS FUNCTION ARE DESCRIBED IN THE    *PPCMPUPD
003100* PROCEDURE DIVISION COPYMEMBER CPPDEUPD. ADDITIONAL CODE IS     *PPCMPUPD
003200* HARCODED IN THIS SOURCE TO PROVIDE THE NECESSARY MOVEMENT OF   *PPCMPUPD
003300* CHANGE-RECORD VALUES TO XXX-TABLE COLUMN ROWS, AS WELL AS      *PPCMPUPD
003400* CMP-ROW INITIALIZATION.                                        *PPCMPUPD
003500*                                                                *PPCMPUPD
003600*  AS SHOULD BE OBVIOUS, THIS PROGRAM USES COPY CODE FOR THE     *PPCMPUPD
003700* MAJORITY OF THE PROCEDURE DIVISION. THIS COPY CODE ADDRESSES   *PPCMPUPD
003800* THE ROOT FUNCTIONS WHICH ARE COMMON TO THE PPXXXUPD PROGRAM    *PPCMPUPD
003900* SERIES.                                                        *PPCMPUPD
004000*                                                                *PPCMPUPD
004100******************************************************************PPCMPUPD
004200     EJECT                                                        PPCMPUPD
004300 ENVIRONMENT DIVISION.                                            PPCMPUPD
004400 CONFIGURATION SECTION.                                           PPCMPUPD
004500 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPCMPUPD
004600 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPCMPUPD
004700      EJECT                                                       PPCMPUPD
004800******************************************************************PPCMPUPD
004900*                                                                *PPCMPUPD
005000*                D A T A  D I V I S I O N                        *PPCMPUPD
005100*                                                                *PPCMPUPD
005200******************************************************************PPCMPUPD
005300 DATA DIVISION.                                                   PPCMPUPD
005400 WORKING-STORAGE SECTION.                                         PPCMPUPD
005500 01  MISCELLANEOUS-WS.                                            PPCMPUPD
005600******************************************************************PPCMPUPD
005700*                                                                *PPCMPUPD
005800* NOTE:                                                          *PPCMPUPD
005900*                                                                *PPCMPUPD
006000*   DATA NAMES A-STND-PROG-ID, PPXXXUTL-NAME AND PPXXXUTW-NAME   *PPCMPUPD
006100*  ARE REFERENCED IN COPY CODE CPPDEUPD. A-STND-PROG-ID IS USED  *PPCMPUPD
006200*  IN DIAGNOSTIC REFERENCES, WHILE PPXXUTL-NAME AND              *PPCMPUPD
006300*  PPXXXUTW-NAME ARE USED IN DYNAMIC CALLS TO THE APPROPRIATE    *PPCMPUPD
006400*  TABLE READ AND WRITE UTILITY MODULES, RESPECTIVELY.           *PPCMPUPD
006500*                                                                *PPCMPUPD
006600******************************************************************PPCMPUPD
006700     05  A-STND-PROG-ID      PIC X(08) VALUE 'PPCMPUPD'.          PPCMPUPD
006800     05  PPXXXUTL-NAME       PIC X(08) VALUE 'PPCMPUTL'.          PPCMPUPD
006900     05  PPXXXUTW-NAME       PIC X(08) VALUE 'PPCMPUTW'.          PPCMPUPD
007000     05  WS-INIT-DATE        PIC X(10) VALUE '0001-01-01'.        PPCMPUPD
007100     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPCMPUPD
007200         88  FIRST-TIME                VALUE 'Y'.                 PPCMPUPD
007300         88  NOT-FIRST-TIME            VALUE 'N'.                 PPCMPUPD
007400     05  WS-EMPLOYEE-ID      PIC X(9).                            PPCMPUPD
007500     SKIP3                                                        PPCMPUPD
007600 01  DATE-CONVERSION-WORK-AREAS.                                  PPCMPUPD
007700                                    COPY CPWSXDC2.                PPCMPUPD
007800     EJECT                                                        PPCMPUPD
007900 01  TEMP-DATA-AREA.                                              PPCMPUPD
008000                                    COPY CPWSXMMM.                PPCMPUPD
008100     EJECT                                                        PPCMPUPD
008200******************************************************************PPCMPUPD
008300*                                                                *PPCMPUPD
008400*  CMP-ROW IS THE ROW THAT WILL BE PASSED AROUND FOR USE BY      *PPCMPUPD
008500* HIGHER LEVEL PROGRAMS. IT IS DEFINED AS AN EXTERNAL DATA AREA  *PPCMPUPD
008600* TO ALLOW HIGHER LEVEL PROGRAMS TO AVOID PASSING THE ROW        *PPCMPUPD
008700* THROUGH UNNECESSARY CALLING CHAINS.                            *PPCMPUPD
008800*                                                                *PPCMPUPD
008900*  CMP-ROW-HOLD IS THE PURE ROW AS READ FROM THE CMP-TABLE. THIS *PPCMPUPD
009000* ROW IS HELD IN WORKING-STORAGE TO ALLOW THE REFRESH FUNCTION   *PPCMPUPD
009100* TO BE PERFORMED WITHOUT AN ADDED DATA BASE ACCESS.             *PPCMPUPD
009200*                                                                *PPCMPUPD
009300*  INIT-CMP-ROW IS THE INITIALIZED ROW. THIS IS PLACED IN        *PPCMPUPD
009400* WORKING STORAGE FOR COMPARISON PURPOSES WITHIN THE "POST"      *PPCMPUPD
009500* LOGIC OF CPPDEUPD.                                             *PPCMPUPD
009600*                                                                *PPCMPUPD
009700******************************************************************PPCMPUPD
009800 01  CMP-ROW      EXTERNAL.                                       PPCMPUPD
009900                                    COPY CPWSRCMP.                PPCMPUPD
010000     EJECT                                                        PPCMPUPD
010100 01  CMP-ROW-HOLD  EXTERNAL.                                      PPCMPUPD
010200                                    COPY CPWSRCMP.                PPCMPUPD
010300     EJECT                                                        PPCMPUPD
010400 01  CMP-ROW-HOLD1 EXTERNAL.                                      PPCMPUPD
010500                                    COPY CPWSRCMP.                PPCMPUPD
010600     EJECT                                                        PPCMPUPD
010300*01**INIT-CMP-ROW.                                                36430795
010400 01  INIT-CMP-ROW EXTERNAL.                                       36430795
010800                                    COPY CPWSRCMP.                PPCMPUPD
010900     EJECT                                                        PPCMPUPD
010700*01**PPCMPUTL-INTERFACE.                                          36430795
010710 01  PPCMPUTL-INTERFACE EXTERNAL.                                 36430795
011100                                    COPY CPLNKCMP.                PPCMPUPD
011200     EJECT                                                        PPCMPUPD
011300 01  PPXXXUTW-INTERFACE.                                          PPCMPUPD
011400                                    COPY CPLNWXXX.                PPCMPUPD
011500     EJECT                                                        PPCMPUPD
011600 01  XWHC-COMPILE-WORK-AREA.                                      PPCMPUPD
011700                                    COPY CPWSXWHC.                PPCMPUPD
011410                                                                  36430795
011420 01  ONLINE-SIGNALS                      EXTERNAL. COPY CPWSONLI. 36430795
011800     EJECT                                                        PPCMPUPD
011900 LINKAGE SECTION.                                                 PPCMPUPD
012000******************************************************************PPCMPUPD
012100*              L I N K A G E    S E C T I O N                    *PPCMPUPD
012200******************************************************************PPCMPUPD
012300*                                                                 PPCMPUPD
012400 01  PPXXXUPD-INTERFACE.            COPY CPLNUXXX.                PPCMPUPD
012500     EJECT                                                        PPCMPUPD
012600 01  EDB-CHANGE-RECORD.             COPY CPWSXCHG.                PPCMPUPD
012700     EJECT                                                        PPCMPUPD
012800 PROCEDURE DIVISION USING                                         PPCMPUPD
012900                          PPXXXUPD-INTERFACE                      PPCMPUPD
013000                          EDB-CHANGE-RECORD.                      PPCMPUPD
013100*                                                                 PPCMPUPD
013200******************************************************************PPCMPUPD
013300*            P R O C E D U R E  D I V I S I O N                  *PPCMPUPD
013400******************************************************************PPCMPUPD
013500*                                                                 PPCMPUPD
013600******************************************************************PPCMPUPD
013700*                                                                *PPCMPUPD
013800*  THE PROCEDURE DIVISION OF THIS PROGRAM IS CONTAINED PRIMARILY *PPCMPUPD
013900* IN COPY MEMBER CPPDEUPD. HERE THE GENERIC DATA-NAMES IN        *PPCMPUPD
014000* CPPDEUPD ARE REPLACED WITH THE SPECIFIC DATA-NAMES OF THE      *PPCMPUPD
014100* WORKING STORAGE TABLE ROWS AND THE PPXXXUTL MODULE INTERFACE.  *PPCMPUPD
014200*                                                                *PPCMPUPD
014300*                                                                *PPCMPUPD
014400******************************************************************PPCMPUPD
014500                                     COPY CPPDEUPD                PPCMPUPD
014600        REPLACING PP000UTL-INTERFACE   BY PPCMPUTL-INTERFACE      PPCMPUPD
014700                  PP000UTL-EMPLOYEE-ID BY PPCMPUTL-EMPLOYEE-ID    PPCMPUPD
014800                  000-ROW              BY CMP-ROW                 PPCMPUPD
014900                  000-ROW-HOLD         BY CMP-ROW-HOLD            PPCMPUPD
015000                  000-ROW-HOLD1        BY CMP-ROW-HOLD1           PPCMPUPD
015100                  INIT-000-ROW         BY INIT-CMP-ROW            PPCMPUPD
015200                  PP000UTL-ERROR       BY PPCMPUTL-ERROR          PPCMPUPD
015300                  PP000UTL-000-FOUND   BY PPCMPUTL-CMP-FOUND.     PPCMPUPD
015400     EJECT                                                        PPCMPUPD
015500 7900-FIRST-TIME SECTION.                                         PPCMPUPD
015600     SKIP1                                                        PPCMPUPD
015700     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPCMPUPD
015800     SKIP1                                                        PPCMPUPD
015900                                    COPY CPPDXWHC.                PPCMPUPD
016000     EJECT                                                        PPCMPUPD
016100 8800-CONVERT-DATES SECTION.                                      PPCMPUPD
016200     SKIP1                                                        PPCMPUPD
016300                                     COPY CPPDXDC2.               PPCMPUPD
016400     EJECT                                                        PPCMPUPD
016500 9000-INITIALIZE-ROW SECTION.                                     PPCMPUPD
016600     SKIP1                                                        PPCMPUPD
016700******************************************************************PPCMPUPD
016800*                                                                *PPCMPUPD
016900* THIS SECTION INITIALIZES THE CMP-ROW. THE INITIALIZE VERB IS   *PPCMPUPD
017000* SUPPLEMENTED WITH MOVES OF AN INITIAL DATE VALUE TO THE        *PPCMPUPD
017100* COLUMNS IN THE CMP-TABLE ROW WHICH ARE DEFINED AS DB2 DATES.   *PPCMPUPD
017200*                                                                *PPCMPUPD
017300* NOTE: IN THE BASE SYSTEM, THERE ARE NO DATE COLUMNS DEFINED    *PPCMPUPD
017400*       ON THE CMP ROW. AS A RESULT LOGIC TO INITIALIZE DATE     *PPCMPUPD
017500*       COLUMNS IS INCLUDED IN SHELL FORM AS COMMENT CODE IN     *PPCMPUPD
017600*       THE EVENT THAT LOCAL CMP TABLE DATA ELEMENTS EXIST AND   *PPCMPUPD
017700*       REQUIRE INCLUSION INTHE ROW INITIALIZATION LOGIC.        *PPCMPUPD
017800******************************************************************PPCMPUPD
017900     SKIP1                                                        PPCMPUPD
018000     INITIALIZE CMP-ROW.                                          PPCMPUPD
018100*    MOVE WS-INIT-DATE                 TO                         PPCMPUPD
018200*             DATE1                OF CMP-ROW                     PPCMPUPD
018300*             DATE2                OF CMP-ROW                     PPCMPUPD
018400*             DATE3                OF CMP-ROW                     PPCMPUPD
018500*             DATE4                OF CMP-ROW                     PPCMPUPD
018600*             DATE5                OF CMP-ROW                     PPCMPUPD
018700*             DATE6                OF CMP-ROW                     PPCMPUPD
018800*             DATE7                OF CMP-ROW                     PPCMPUPD
018900*             DATE8                OF CMP-ROW                     PPCMPUPD
019000*             DATE9                OF CMP-ROW.                    PPCMPUPD
021000     MOVE WS-INIT-DATE                 TO                         UCSD0038
021100              ASST-RANK-DATE       OF CMP-ROW                     UCSD0038
021200              ASSO-RANK-DATE       OF CMP-ROW                     UCSD0038
021300              FULL-RANK-DATE       OF CMP-ROW                     UCSD0038
021400              STEP-DATE            OF CMP-ROW                     UCSD0038
                    STAFF-EVAL-DATE      OF CMP-ROW.                    UCSD0086
019100     MOVE PPXXXUPD-EMPLOYEE-ID TO EMPLOYEE-ID OF CMP-ROW.         PPCMPUPD
019200     EJECT                                                        PPCMPUPD
019300 9100-EVALUATE-DATA-ELEMENT SECTION.                              PPCMPUPD
019400     SKIP1                                                        PPCMPUPD
019500******************************************************************PPCMPUPD
019600*                                                                *PPCMPUPD
019700* THIS SECTION TAKES THE DATA ELEMENT NUMBER FROM THE CHANGE     *PPCMPUPD
019800* RECORD TO DETERMINE WHICH COLUMN IS TO RECEIVE THE VALUE FROM  *PPCMPUPD
019900* THE CHANGE RECORD. WHILE IT IS MOST IMPORTANT TO MAKE SURE     *PPCMPUPD
020000* THAT THE NUMERIC FIELDS ARE MOVED TO LIKE LENGTH COLUMNS,      *PPCMPUPD
020100* THE USE OF LENGTH QUALIFIERS ON ALPHANUMERIC FIELD MOVES DOES  *PPCMPUPD
020200* PROVIDE SOME USEFUL DOCUMENTATION.                             *PPCMPUPD
020300*                                                                *PPCMPUPD
020400******************************************************************PPCMPUPD
020500     SKIP1                                                        PPCMPUPD
020600     EVALUATE XCHG-DATA-ELEM-NO                                   PPCMPUPD
020700         WHEN '0510'                                              PPCMPUPD
020800              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
020900                   CAMPUS-DATA1             OF CMP-ROW            PPCMPUPD
021000         WHEN '0520'                                              PPCMPUPD
021100              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
021200                   CAMPUS-DATA2             OF CMP-ROW            PPCMPUPD
020200*        WHEN '0530'                                              UCSD0038
020300*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
020400*                  CAMPUS-DATA3             OF CMP-ROW            UCSD0038
020500*        WHEN '0540'                                              UCSD0038
020600*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
020700*                  CAMPUS-DATA4             OF CMP-ROW            UCSD0038
020800*        WHEN '0550'                                              UCSD0038
020900*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
021000*                  CAMPUS-DATA5             OF CMP-ROW            UCSD0038
021100*        WHEN '0560'                                              UCSD0038
021200*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
021300*                  CAMPUS-DATA6             OF CMP-ROW            UCSD0038
021400*        WHEN '0570'                                              UCSD0038
021500*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
021600*                  CAMPUS-DATA7             OF CMP-ROW            UCSD0038
021700*        WHEN '0580'                                              UCSD0038
021800*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
021900*                  CAMPUS-DATA8             OF CMP-ROW            UCSD0038
022000*        WHEN '0590'                                              UCSD0038
022100*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
022200*                  CAMPUS-DATA9             OF CMP-ROW            UCSD0038
022300*        WHEN '0595'                                              UCSD0038
022400*             MOVE TEMP-DATA-VALUE     TO                         UCSD0038
022500*                  CAMPUS-DATA10            OF CMP-ROW            UCSD0038
022600         WHEN '0307'                                              PPCMPUPD
022700              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
023900                   CAMPUS-ADDRESS           OF CMP-ROW            PPCMPUPD
024000         WHEN '0308'                                              PPCMPUPD
024100              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
024200                   CAMPUS-ROOM              OF CMP-ROW            PPCMPUPD
024300         WHEN '0309'                                              PPCMPUPD
024400              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
024500                   CAMPUS-BLDG              OF CMP-ROW            PPCMPUPD
024600         WHEN '0310'                                              PPCMPUPD
024700              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
024800                   CAMPUS-PHONE1            OF CMP-ROW            PPCMPUPD
024900         WHEN '0311'                                              PPCMPUPD
025000              MOVE TEMP-DATA-VALUE     TO                         PPCMPUPD
025100                   CAMPUS-PHONE2            OF CMP-ROW            PPCMPUPD
024900         WHEN '0530'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   ACAD-DISCIPLINE          OF CMP-ROW            UCSD0038
024900         WHEN '0531'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   ASST-RANK-DATE           OF CMP-ROW            UCSD0038
024900         WHEN '0532'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   ASSO-RANK-DATE           OF CMP-ROW            UCSD0038
024900         WHEN '0533'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   FULL-RANK-DATE           OF CMP-ROW            UCSD0038
024900         WHEN '0534'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   STEP-DATE                OF CMP-ROW            UCSD0038
024900         WHEN '0535'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   EDU-INST                 OF CMP-ROW            UCSD0038
024900         WHEN '0536'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   EDU-LEVEL-2              OF CMP-ROW            UCSD0038
024900         WHEN '0537'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   EDU-LEVEL-2-YR           OF CMP-ROW            UCSD0038
024900         WHEN '0538'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   EDU-LEVEL-2-INST         OF CMP-ROW            UCSD0038
024900         WHEN '0539'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   PRIOR-SERVICE-INST       OF CMP-ROW            UCSD0038
024900         WHEN '0540'                                              UCSD0038
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0038
025100                   ACAD-SENATE-STATUS       OF CMP-ROW            UCSD0038
024900         WHEN '0542'                                              UCSD0086
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0086
025100                   STAFF-EVAL-CODE          OF CMP-ROW            UCSD0086
024900         WHEN '0543'                                              UCSD0086
025000              MOVE TEMP-DATA-VALUE     TO                         UCSD0086
025100                   STAFF-EVAL-DATE          OF CMP-ROW            UCSD0086
028500         WHEN OTHER                                               PPCMPUPD
028600              IF PPXXXUPD-DIAGNOSTICS-ON                          PPCMPUPD
028700                  DISPLAY A-STND-PROG-ID                          PPCMPUPD
028800                          ': UNRECOGNIZED DATA ELEMENT NUMBER ('  PPCMPUPD
028900                          XCHG-DATA-ELEM-NO                       PPCMPUPD
029000                          ')'                                     PPCMPUPD
029100              END-IF                                              PPCMPUPD
029200              SET PPXXXUPD-DATA-ELEM-ERROR TO TRUE                PPCMPUPD
029300     END-EVALUATE.                                                PPCMPUPD
029400     SKIP3                                                        PPCMPUPD
029500************************   END OF PROGRAM ************************PPCMPUPD
