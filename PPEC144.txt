000010**************************************************************/   EFIX1285
000011*  PROGRAM: PPEC144                                          */   EFIX1285
000012*  RELEASE: ___1285______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1285
000013*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___05/15/00__  */   EFIX1285
000014*  DESCRIPTION:                                              */   EFIX1285
000015*  - ERROR REPORT 1668:                                      */   EFIX1285
000016*    REMOVE PIE DATE COMPARISON FOR PPA ROW UPDATE. ASSUME   */   EFIX1285
000017*    IVR PROCESS CORRECTLY ALLOWED PLAN UPDATE SO            */   EFIX1285
000018*    UPDATE THE PPA ROWS WITH THE NEW PLAN OR DELETE THEM    */   EFIX1285
000019*    FOR AN OPTOUT.                                          */   EFIX1285
000020**************************************************************/   EFIX1285
000000**************************************************************/   32591157
000001*  PROGRAM: PPEC144                                          */   32591157
000002*  RELEASE: ___1157______ SERVICE REQUEST(S): ____13259____  */   32591157
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___12/15/97__  */   32591157
000004*  DESCRIPTION:                                              */   32591157
000005*  - BENEFITS ENROLLMENT VIA IVR.                            */   32591157
000006*    PENDING PREMIUM EDITS FOR DENTAL.                       */   32591157
000007**************************************************************/   32591157
001100                                                                  PPEC144
001200 IDENTIFICATION DIVISION.                                         PPEC144
001300 PROGRAM-ID. PPEC144.                                             PPEC144
001400 AUTHOR. UCOP.                                                    PPEC144
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEC144
001600                                                                  PPEC144
001700 DATE-WRITTEN.  NOVEMBER 1997.                                    PPEC144
001800 DATE-COMPILED.                                                   PPEC144
001900                                                                  PPEC144
002000******************************************************************PPEC144
002100**    THIS ROUTINE WILL PERFORM THE DENTAL INSURANCE PENDING    **PPEC144
002200**    PREMIUM MAINTENANCE FOR BENEFITS ENROLLMENT VIA IVR       **PPEC144
002300******************************************************************PPEC144
002400                                                                  PPEC144
002500 ENVIRONMENT DIVISION.                                            PPEC144
002600 CONFIGURATION SECTION.                                           PPEC144
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEC144
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEC144
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEC144
003000                                                                  PPEC144
003100 INPUT-OUTPUT SECTION.                                            PPEC144
003200 FILE-CONTROL.                                                    PPEC144
003300                                                                  PPEC144
003400 DATA DIVISION.                                                   PPEC144
003500 FILE SECTION.                                                    PPEC144
003600     EJECT                                                        PPEC144
003700*----------------------------------------------------------------*PPEC144
003800 WORKING-STORAGE SECTION.                                         PPEC144
003900*----------------------------------------------------------------*PPEC144
004000                                                                  PPEC144
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEC144
004220*****'PPEC144 /121597'.                                           EFIX1285
004230     'PPEC144 /051500'.                                           EFIX1285
004300                                                                  PPEC144
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEC144
004500     A-STND-PROG-ID.                                              PPEC144
004600     05  FILLER                       PIC X(03).                  PPEC144
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEC144
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEC144
004900     05  FILLER                       PIC X(08).                  PPEC144
007410                                                                  PPEC144
007420 01  CALLED-MODULES.                               COPY CPWSXRTN. PPEC144
007422                                                                  PPEC144
007423 01  MESSAGES-USED.                                               PPEC144
007435     05  M12310                       PIC X(05) VALUE '12310'.    PPEC144
007442                                                                  PPEC144
007443 01  ELEMENTS-USED.                                               PPEC144
009103     05  E0377                        PIC 9(04) VALUE 0377.       PPEC144
009104     05  E0752                        PIC 9(04) VALUE 0752.       PPEC144
009105     05  E0753                        PIC 9(04) VALUE 0753.       PPEC144
009106     05  E0754                        PIC 9(04) VALUE 0754.       PPEC144
009107     05  E0755                        PIC 9(04) VALUE 0755.       PPEC144
009108     05  E0756                        PIC 9(04) VALUE 0756.       PPEC144
009109     05  E0757                        PIC 9(04) VALUE 0757.       PPEC144
009110     05  E0758                        PIC 9(04) VALUE 0758.       PPEC144
009111     05  E0759                        PIC 9(04) VALUE 0759.       PPEC144
009115                                                                  PPEC144
009116 01  MISC-WORK-AREAS.                                             PPEC144
009117     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEC144
009118         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEC144
009119         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEC144
009127     05  PPA-SUB                 PIC S9(03) COMP VALUE 0.         PPEC144
009128     05  INIT-PPA-KEY.                                            PPEC144
009129         07  FILLER                  PIC X(2)   VALUE SPACES.     PPEC144
009130         07  FILLER             PIC X(10)  VALUE '0001-01-01'.    PPEC144
009131     05  PREMIUM-IND-SW               PIC 9(02) VALUE 0.          PPEC144
009132         88  DONT-SET-PREMIUM-IND               VALUE 0.          PPEC144
009133         88  SET-PREMIUM-IND                    VALUE 1.          PPEC144
009134                                                                  PPEC144
009135     EJECT                                                        PPEC144
009147 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEC144
009148                                                                  PPEC144
009150     EJECT                                                        PPEC144
009210 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. PPEC144
009300                                                                  PPEC144
009400     EJECT                                                        PPEC144
009500 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEC144
009600                                                                  PPEC144
009700     EJECT                                                        PPEC144
009800 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEC144
009900                                                                  PPEC144
010000     EJECT                                                        PPEC144
010400 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEC144
010500                                                                  PPEC144
010600     EJECT                                                        PPEC144
010700 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEC144
010800                                                                  PPEC144
010900     EJECT                                                        PPEC144
010910 01  PAYROLL-PROCESS-INST-CONST-2.                 COPY CPWSXIC2. PPEC144
010920                                                                  PPEC144
010930     EJECT                                                        PPEC144
011300******************************************************************PPEC144
011400**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEC144
011500**--------------------------------------------------------------**PPEC144
011600** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEC144
011700******************************************************************PPEC144
011800                                                                  PPEC144
011900 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEC144
012000                                                                  PPEC144
012100     EJECT                                                        PPEC144
012200 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEC144
012300                                                                  PPEC144
012400     EJECT                                                        PPEC144
012500 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEC144
012600                                                                  PPEC144
012700     EJECT                                                        PPEC144
012800 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEC144
012900                                                                  PPEC144
013000 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEC144
013100                                                                  PPEC144
013200 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEC144
013300                                                                  PPEC144
013400 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEC144
013500                                                                  PPEC144
013600     EJECT                                                        PPEC144
013700 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEC144
013800                                                                  PPEC144
013900     EJECT                                                        PPEC144
014000 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEC144
014100                                                                  PPEC144
014200     EJECT                                                        PPEC144
014300 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEC144
014400                                                                  PPEC144
014500     EJECT                                                        PPEC144
014540 01  XACN-ACTION-ARRAY                   EXTERNAL. COPY CPWSXACN. PPEC144
014541                                                                  PPEC144
014542     EJECT                                                        PPEC144
014600******************************************************************PPEC144
014700**  DATA BASE AREAS                                             **PPEC144
014800******************************************************************PPEC144
015600                                                                  PPEC144
015800 01  BEN-ROW                             EXTERNAL.                PPEC144
015900     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPEC144
016100     EJECT                                                        PPEC144
016110                                                                  PPEC144
016410 01  PPA-DATA-ARRAY                      EXTERNAL. COPY CPWSEPPA. PPEC144
016500     EJECT                                                        PPEC144
016600******************************************************************PPEC144
016700**  DATA BASE SAVE AREAS                                        **PPEC144
016800******************************************************************PPEC144
016900                                                                  PPEC144
017000 01  BEN-ROW-HOLD                        EXTERNAL. COPY CPWSRBEN  PPEC144
017100                                                                  PPEC144
017110         REPLACING  DENTAL-OPTOUT     BY SAVE-DENTAL-OPTOUT       PPEC144
019908                    DENTAL-PLAN       BY SAVE-DENTAL-PLAN         PPEC144
019909                    DENTAL-COVERAGE   BY SAVE-DENTAL-COVERAGE     PPEC144
019910                    DENTAL-COVEFFDATE BY SAVE-DENTAL-COVEFFDATE   PPEC144
019911                    EMP-DENTL-COVEFFDT BY SAVE-EMP-DENTL-COVEFFDT PPEC144
019921                    PIE-END-DATE      BY SAVE-PIE-END-DATE        PPEC144
019922                    PIN-SIGN-DATE     BY SAVE-PIN-SIGN-DATE       PPEC144
019923                    SUS-PREMIUM-IND   BY SAVE-SUS-PREMIUM-IND.    PPEC144
019926                                                                  PPEC144
019927 01  PPA-DATA-ARRAY-HOLD                 EXTERNAL. COPY CPWSEPPA  PPEC144
019928      REPLACING EPPA-PPA-OCCURRENCE-KEY BY HOLD-PPA-OCCURRENCE-KEYPPEC144
019929                  EPPA-PENDPREM-DATA    BY HOLD-PENDPREM-DATA     PPEC144
019930                  EPPA-PPA-BENEFIT-TYPE BY HOLD-PPA-BENEFIT-TYPE  PPEC144
019931                  EPPA-PPA-PAY-CYCLE    BY HOLD-PPA-PAY-CYCLE     PPEC144
019932                  EPPA-PPA-PAY-END-DATE BY HOLD-PPA-PAY-END-DATE  PPEC144
019933                  EPPA-PPA-PLAN-CODE    BY HOLD-PPA-PLAN-CODE     PPEC144
019934                  EPPA-PPA-COVERAGE     BY HOLD-PPA-COVERAGE      PPEC144
019935                  EPPA-PPA-FLAG         BY HOLD-PPA-FLAG          PPEC144
019936                  EPPA-PPA-ADC-CODE     BY HOLD-PPA-ADC-CODE.     PPEC144
019937                                                                  PPEC144
019938     EJECT                                                        PPEC144
019939******************************************************************PPEC144
019940**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEC144
019941******************************************************************PPEC144
019942                                                                  PPEC144
019943*----------------------------------------------------------------*PPEC144
019944 LINKAGE SECTION.                                                 PPEC144
019950*----------------------------------------------------------------*PPEC144
020000                                                                  PPEC144
020100******************************************************************PPEC144
020200**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEC144
020300**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEC144
020400******************************************************************PPEC144
020500                                                                  PPEC144
020600 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEC144
020700                                                                  PPEC144
020800     EJECT                                                        PPEC144
020900******************************************************************PPEC144
021000**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEC144
021100**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEC144
021200******************************************************************PPEC144
021300                                                                  PPEC144
021400 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEC144
021500                                                                  PPEC144
021600     EJECT                                                        PPEC144
021700******************************************************************PPEC144
021800**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEC144
021900******************************************************************PPEC144
022000                                                                  PPEC144
022100 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEC144
022200                          KMTA-MESSAGE-TABLE-ARRAY.               PPEC144
022300                                                                  PPEC144
022400                                                                  PPEC144
022500*----------------------------------------------------------------*PPEC144
022600 0000-DRIVER.                                                     PPEC144
022700*----------------------------------------------------------------*PPEC144
022800                                                                  PPEC144
022900******************************************************************PPEC144
023000**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEC144
023100******************************************************************PPEC144
023200                                                                  PPEC144
023300     IF  THIS-IS-THE-FIRST-TIME                                   PPEC144
023400         PERFORM 0100-INITIALIZE                                  PPEC144
023500     END-IF.                                                      PPEC144
023600                                                                  PPEC144
023700******************************************************************PPEC144
023800**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEC144
023900**  TO THE CALLING PROGRAM                                      **PPEC144
024000******************************************************************PPEC144
024100                                                                  PPEC144
024200     PERFORM 1000-MAINLINE-ROUTINE.                               PPEC144
024300                                                                  PPEC144
024400     EXIT PROGRAM.                                                PPEC144
024500                                                                  PPEC144
024600                                                                  PPEC144
024700     EJECT                                                        PPEC144
024800*----------------------------------------------------------------*PPEC144
024900 0100-INITIALIZE    SECTION.                                      PPEC144
025000*----------------------------------------------------------------*PPEC144
025200******************************************************************PPEC144
025300**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEC144
025400******************************************************************PPEC144
025600     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEC144
025800*                                               *-------------*   PPEC144
025900                                                 COPY CPPDXWHC.   PPEC144
026000*                                               *-------------*   PPEC144
026800******************************************************************PPEC144
026900**   SET FIRST CALL SWITCH OFF                                  **PPEC144
027000******************************************************************PPEC144
027200     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEC144
027400     EJECT                                                        PPEC144
027500*----------------------------------------------------------------*PPEC144
027600 1000-MAINLINE-ROUTINE SECTION.                                   PPEC144
027700*----------------------------------------------------------------*PPEC144
027800                                                                  PPEC144
050620******************************************************************PPEC144
050621**   IF DENTAL PLAN IS OPTED OUT AND PIE END DATE HAS NOT BEEN  **PPEC144
050622**   ACHIEVED, DELETE DENTAL TYPE PENDING PREMIUM RECORDS       **PPEC144
050623******************************************************************PPEC144
050624     MOVE DENTAL-OPTOUT   TO W88-DENTAL-OPTOUT.                   PPEC144
050625     IF  DENTAL-OPTOUT-DNTL-OPT-OUT                               PPEC144
050626*****    AND PIE-END-DATE >  XDTS-ISO-DLY-RUN-DATE                EFIX1285
050627             PERFORM 8000-DELETE-DENTAL-PPA                       PPEC144
050628     END-IF.                                                      PPEC144
062824                                                                  PPEC144
062825     IF (SAVE-DENTAL-PLAN NOT = DENTAL-PLAN) OR                   PPEC144
062826        (SAVE-DENTAL-COVERAGE NOT = DENTAL-COVERAGE)              PPEC144
062827*****    IF PIE-END-DATE > XDTS-ISO-DLY-RUN-DATE                  EFIX1285
062829            PERFORM 8050-CHANGE-DENTAL-PPA                        PPEC144
062830*****    END-IF                                                   EFIX1285
062831     END-IF.                                                      PPEC144
085233     EJECT                                                        PPEC144
085234                                                                  PPEC144
085235*----------------------------------------------------------------*PPEC144
085236 8000-DELETE-DENTAL-PPA          SECTION.                         PPEC144
085237*----------------------------------------------------------------*PPEC144
085238******************************************************************PPEC144
085239* CHECK FOR PENDING PREMIUM ACTIVITY(PPA) DATA.                  *PPEC144
085240* IF ANY PPA DENTAL DATA, TYPE 'D', IS PRESENT IT WILL BE DELETED*PPEC144
085241*    INITIALIZE THE PENDPREM ARRAY AND TURN THE CHANGE FLAG      *PPEC144
085242*    ON FOR EACH DATA ELEMENT.                                   *PPEC144
085243******************************************************************PPEC144
085244                                                                  PPEC144
085245     PERFORM VARYING PPA-SUB FROM 1 BY 1                          PPEC144
085246               UNTIL PPA-SUB > IDC-MAX-NO-PPA                     PPEC144
085247       IF EPPA-PPA-BENEFIT-TYPE (PPA-SUB) = 'D'                   PPEC144
085248          INITIALIZE  EPPA-PENDPREM-DATA (PPA-SUB)                PPEC144
085249          MOVE '0001-01-01' TO EPPA-PPA-PAY-END-DATE (PPA-SUB)    PPEC144
085250          MOVE E0753                TO DL-FIELD                   PPEC144
085251          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085260          MOVE E0754                TO DL-FIELD                   PPEC144
085270          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085271          MOVE E0755                TO DL-FIELD                   PPEC144
085272          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085273          MOVE E0756                TO DL-FIELD                   PPEC144
085274          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085275          MOVE E0757                TO DL-FIELD                   PPEC144
085276          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085277          MOVE E0758                TO DL-FIELD                   PPEC144
085278          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085279          MOVE E0759                TO DL-FIELD                   PPEC144
085280          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC144
085281       END-IF                                                     PPEC144
085282     END-PERFORM.                                                 PPEC144
085283     EJECT                                                        PPEC144
085284                                                                  PPEC144
085285*----------------------------------------------------------------*PPEC144
085286 8050-CHANGE-DENTAL-PPA          SECTION.                         PPEC144
085287*----------------------------------------------------------------*PPEC144
085288******************************************************************PPEC144
085289* CHECK FOR PENDING PREMIUM ACTIVITY(PPA) DATA.                  *PPEC144
085290* IF ANY PPA DENTAL DATA (TYPE 'D') IS PRESENT IT WILL BE CHANGED*PPEC144
085291*    MOVE THE PLAN AND COVERAGE    AND TURN THE CHANGE FLAG      *PPEC144
085292*    ON FOR EACH OF THESE DATA ELEMENTS AND SET THE PREMIUM IND  *PPEC144
085293******************************************************************PPEC144
085294     SET DONT-SET-PREMIUM-IND TO TRUE.                            PPEC144
085295                                                                  PPEC144
085296     PERFORM VARYING PPA-SUB FROM 1 BY 1                          PPEC144
085297               UNTIL PPA-SUB > IDC-MAX-NO-PPA                     PPEC144
085298       IF EPPA-PPA-BENEFIT-TYPE (PPA-SUB) = 'D'                   PPEC144
085306       AND EPPA-PPA-FLAG (PPA-SUB) NOT = '2'                      PPEC144
085307           SET SET-PREMIUM-IND TO TRUE                            PPEC144
085308           MOVE DENTAL-PLAN TO EPPA-PPA-PLAN-CODE (PPA-SUB)       PPEC144
085309           MOVE E0756                TO DL-FIELD                  PPEC144
085310           PERFORM 9050-AUDITING-RESPONSIBILITIES                 PPEC144
085311           MOVE '1' TO EPPA-PPA-FLAG (PPA-SUB)                    PPEC144
085312           MOVE E0758                TO DL-FIELD                  PPEC144
085313           PERFORM 9050-AUDITING-RESPONSIBILITIES                 PPEC144
085314           IF DENTAL-COVERAGE NOT = EPPA-PPA-COVERAGE (PPA-SUB)   PPEC144
085316              MOVE DENTAL-COVERAGE TO EPPA-PPA-COVERAGE (PPA-SUB) PPEC144
085317              MOVE E0757             TO DL-FIELD                  PPEC144
085318              PERFORM 9050-AUDITING-RESPONSIBILITIES              PPEC144
085319           END-IF                                                 PPEC144
085320       END-IF                                                     PPEC144
085321     END-PERFORM.                                                 PPEC144
085323     IF SET-PREMIUM-IND                                           PPEC144
085324        IF SUS-PREMIUM-IND NOT = 'Y'                              PPEC144
085325           MOVE 'Y' TO SUS-PREMIUM-IND                            PPEC144
085326           MOVE E0752 TO DL-FIELD                                 PPEC144
085327           PERFORM 9050-AUDITING-RESPONSIBILITIES                 PPEC144
085328        END-IF                                                    PPEC144
085329     END-IF.                                                      PPEC144
085330     EJECT                                                        PPEC144
085331*----------------------------------------------------------------*PPEC144
085332 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEC144
085333*----------------------------------------------------------------*PPEC144
085334*                                                 *-------------* PPEC144
085335                                                   COPY CPPDXDEC. PPEC144
085336*                                                 *-------------* PPEC144
085337*----------------------------------------------------------------*PPEC144
085338 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEC144
085339*----------------------------------------------------------------*PPEC144
085340*                                                 *-------------* PPEC144
085341                                                   COPY CPPDADLC. PPEC144
085342*                                                 *-------------* PPEC144
085343     EJECT                                                        PPEC144
085344*----------------------------------------------------------------*PPEC144
085345 9300-DATE-CONVERSION-DB2  SECTION.                               PPEC144
085346*----------------------------------------------------------------*PPEC144
085347*                                                 *-------------* PPEC144
085348                                                   COPY CPPDXDC3. PPEC144
085349*                                                 *-------------* PPEC144
085350     EJECT                                                        PPEC144
085357******************************************************************PPEC144
085360**   E N D  S O U R C E   ----  PPEC144 ----                    **PPEC144
085400******************************************************************PPEC144
