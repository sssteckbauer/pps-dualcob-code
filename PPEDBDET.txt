000101**************************************************************/   36430795
000200*  PROGRAM: PPEDBDET                                         */   36430795
000300*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000500*  DESCRIPTION:                                              */   36430795
000600*  USE GETMAINED AREA TO AQUIRE TABLE DATA ON-LINE           */   36430795
000700**************************************************************/   36430795
000702**************************************************************/  *36410717
000703*  PROGRAM: PPEDBDET                                         */  *36410717
000704*  RELEASE # ___0717___   SERVICE REQUEST NO(S)____3641______*/  *36410717
000705*  NAME ________UCOP___   CREATION DATE     ____11/19/92_____*/  *36410717
000706*  DESCRIPTION                                               */  *36410717
000707*   - EDB MAINTENANCE RE-ENGINEERING PROJECT                 */  *36410717
000708*     COVERT TO ACCESS DET FROM PPPDET DB2 TABLE             */  *36410717
000709**************************************************************/  *36410717
000800**************************************************************/  *36090553
000900*  PROGRAM: PPEDBDET                                         */  *36090553
001000*  RELEASE # ___0553___   SERVICE REQUEST NO(S)____3609______*/  *36090553
001100*  NAME ______JAG______   MODIFICATION DATE ____04/08/91_____*/  *36090553
001200*  DESCRIPTION                                               */  *36090553
001300*    MOVED DATA-ELEMENT-TO-EDB-TABLES OUT OF LINKAGE AND     */  *36090553
001400*    TO AN EXTERNAL AREA FOR USE BY PPEDBFET AND PPEDBUPD.   */  *36090553
001500*    ADDED SETUP LOGIC FOR DATA-ELEMENT-TO-EDB-TABLES        */  *36090553
001600*    ARRAY WHICH NOW INCLUDES DET ATTRIBUTES FOR USE BY      */  *36090553
001700*    THE EDB FETCH COMPLEX.                                  */  *36090553
001800**************************************************************/  *36090553
001900**************************************************************/  *36090513
002000*  PROGRAM: PPEDBDET                                         */  *36090513
002100*  RELEASE # ___0513___   SERVICE REQUEST NO(S)____3609______*/  *36090513
002200*  NAME ______JAG______   CREATION DATE     ____11/05/90_____*/  *36090513
002300*  DESCRIPTION                                               */  *36090513
002400*     NEW PROGRAM TO IDENTIFY THE DB2 TABLE PER DATA ELEMENT */  *36090513
002500**************************************************************/  *36090513
002600 IDENTIFICATION DIVISION.                                         PPEDBDET
002700 PROGRAM-ID. PPEDBDET.                                            PPEDBDET
002800 AUTHOR. UCOP.                                                    PPEDBDET
002900 DATE-WRITTEN. SEPTEMBER 1990.                                    PPEDBDET
003000 DATE-COMPILED.                                                   PPEDBDET
003100*REMARKS.                                                         PPEDBDET
003200     EJECT                                                        PPEDBDET
003300 ENVIRONMENT DIVISION.                                            PPEDBDET
003400 CONFIGURATION SECTION.                                           PPEDBDET
003500 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPEDBDET
003600 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPEDBDET
003700      EJECT                                                       PPEDBDET
003800*INPUT-OUTPUT SECTION.                                            PPEDBDET
003900******************************************************************PPEDBDET
004000*                D A T A  D I V I S I O N                         PPEDBDET
004100******************************************************************PPEDBDET
004200*                                                                 PPEDBDET
004300 DATA DIVISION.                                                   PPEDBDET
004400     SKIP2                                                        PPEDBDET
004500 WORKING-STORAGE SECTION.                                         PPEDBDET
004600     SKIP1                                                        PPEDBDET
004700 01  MISCELLANEOUS-WS.                                            PPEDBDET
004800     05  A-STND-PROG-ID            PIC X(08) VALUE 'PPEDBDET'.    PPEDBDET
004900     05  FIRST-TIME-SWITCH         PIC X(01) VALUE 'Y'.           PPEDBDET
005000         88  FIRST-TIME                      VALUE 'Y'.           PPEDBDET
005100         88  NOT-FIRST-TIME                  VALUE 'N'.           PPEDBDET
005200     05  END-DET-SW                PIC X(01) VALUE SPACE.         PPEDBDET
005300         88  END-DET                         VALUE 'Y'.           PPEDBDET
005400     05  CONTROL-MATCH-SW          PIC X(01) VALUE SPACE.         PPEDBDET
005500         88  CONTROL-MATCH                   VALUE 'Y'.           PPEDBDET
005600     05  WS-INDEX                  PIC S9(04) COMP VALUE ZERO.    PPEDBDET
005700     05  WS-D-E-INDEX              PIC S9(04) COMP VALUE ZERO.    PPEDBDET
005800     05  WS-DET-TBL-NAME.                                         PPEDBDET
005900         10  WS-DET-TBL-PREFIX PIC X(03).                         PPEDBDET
006000         10  WS-DET-TBL-ID     PIC X(03).                         PPEDBDET
006100         10  FILLER            PIC X(02).                         PPEDBDET
006200     05 FILLER            PIC X(01)         VALUE SPACE.          36410717
006300         88 DET-ROW-OPEN VALUE 'Y'.                               36410717
006400         88 NOT-DET-ROW-OPEN VALUE SPACE.                         36410717
006500*****05  WS-CTL-ACTION-HOLD.                                      36410717
006600*****    10  WS-CTL-MODE-HOLD  PIC X(03).                         36410717
006700*****    10  FILLER            PIC X(09).                         36410717
006800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                36410717
006900     EJECT                                                        36090553
007000 01  XWHC-COMPILE-WORK-AREA.                                      36090553
007100                                    COPY CPWSXWHC.                36090553
007200     EJECT                                                        36090553
007300 01  PPEDBUPD-INSTALLATION-DATA.                                  PPEDBDET
007400                                    COPY CPWSXICE.                PPEDBDET
007500*****EJECT                                                        36410717
007600*01**IO-FUNCTIONS.                                                36410717
007700*****                               COPY CPWSXIOF.                36410717
007800*****EJECT                                                        36410717
007900*01**CONTROL-FILE-KEYS.                                           36410717
008000*****                               COPY CPWSXCFK.                36410717
008100*****EJECT                                                        36410717
008200*01**CTL-INTERFACE.                                               36410717
008300*****                               COPY CPWSXCIF.                36410717
008400*01**CTL-SEGMENT-TABLE.                                           36410717
008500*****                               COPY CPWSXCST.                36410717
008600*****EJECT                                                        36410717
008700*01**DATA-ELEMENT-TABLE REDEFINES CTL-SEGMENT-TABLE.              36410717
008800*****                               COPY CPWSXDET.                36410717
008900     EJECT                                                        PPEDBDET
009000 01  DATA-ELEMENT-TO-EDB-TABLES   EXTERNAL.                       36090553
009100                                    COPY CPWSXDNT.                36090553
010000 01  CPWSGETM EXTERNAL.                                           36430795
010100     COPY CPWSGETM.                                               36430795
010200 01  ONLINE-SIGNALS EXTERNAL.                                     36430795
010300     COPY CPWSONLI.                                               36430795
010302******************************************************************36410717
010303*          SQL - WORKING STORAGE                                 *36410717
010304******************************************************************36410717
010305 01  DET-ROW-DATA.                                                36410717
010306     EXEC SQL                                                     36410717
010307          INCLUDE PPPVZDET                                        36410717
010308     END-EXEC.                                                    36410717
010309     EXEC SQL                                                     36410717
010310          INCLUDE SQLCA                                           36410717
010311     END-EXEC.                                                    36410717
010312*****                                                             36410717
010313     EXEC SQL                                                     36410717
010400          DECLARE DET_ROW CURSOR FOR                              36410717
010500              SELECT *                                            36410717
010600              FROM PPPVZDET_DET                                   36410717
010700              ORDER BY                                            36410717
010800                  DET_CARD_TYPE                                   36410717
010900                 ,DET_ELEM_NO                                     36410717
011000     END-EXEC.                                                    36410717
011100     EJECT                                                        PPEDBDET
011200 LINKAGE SECTION.                                                 PPEDBDET
011300******************************************************************PPEDBDET
011400*              L I N K A G E    S E C T I O N                    *PPEDBDET
011500******************************************************************PPEDBDET
011600*                                                                 PPEDBDET
011700 01  PPEDBDET-INTERFACE.                                          PPEDBDET
011800                                    COPY CPLNKDNT.                PPEDBDET
011900*01**DATA-ELEMENT-TO-EDB-TABLES   EXTERNAL.                       36090553
012000*****                               COPY CPWSXDNT.                36090553
013300     SKIP1                                                        36430795
013400 01  EIB-DUMMY                   PIC X(1).                        36430795
013500     SKIP1                                                        36430795
013600 01  COMMAREA-DUMMY              PIC X(1).                        36430795
013700 01  CPWSXCWA.                                                    36430795
013800     COPY CPWSXCWA.                                               36430795
013900 01  TABLE-AREA.                                                  36430795
014000     05 TABLE-AREA-DATA          PIC X(300000).                   36430795
014002     EJECT                                                        PPEDBDET
014003*                                                                 PPEDBDET
014004******************************************************************PPEDBDET
014005*            P R O C E D U R E  D I V I S I O N                  *PPEDBDET
014006******************************************************************PPEDBDET
014007*                                                                 PPEDBDET
014008 PROCEDURE DIVISION USING                                         PPEDBDET
014009                          PPEDBDET-INTERFACE.                     36090553
014010**************************PPEDBDET-INTERFACE,                     36090553
014011**************************DATA-ELEMENT-TO-EDB-TABLES.             36090553
014012******************************************************************36410717
014013*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *36410717
014014*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *36410717
014015******************************************************************36410717
014016     EXEC SQL                                                     36410717
014017          INCLUDE CPPDXE99                                        36410717
014018     END-EXEC.                                                    36410717
014019     MOVE 'PPEDBDET'     TO DB2MSG-PGM-ID.                        36410717
015900******************************************************************36430795
016000*    SET POINTERS TO CWA ADDRESS                                  36430795
016100******************************************************************36430795
016200     IF ENVIRON-IS-CICS                                           36430795
016300         SET ADDRESS OF EIB-DUMMY TO CPWSONLI-EIB-PTR             36430795
016400         SET ADDRESS OF COMMAREA-DUMMY TO CPWSONLI-COMMAREA-PTR   36430795
016500         SET CPWSGETM-CWAADDR-FUNCTION TO TRUE                    36430795
016600         SET CPWSGETM-PPGETMN-NAME TO TRUE                        36430795
016700         CALL CPWSGETM-PPGETMN                                    36430795
016800             USING EIB-DUMMY COMMAREA-DUMMY                       36430795
016900         SET ADDRESS OF CPWSXCWA TO CPWSGETM-CWA-PTR              36430795
017000     END-IF.                                                      36430795
017002     SKIP1                                                        PPEDBDET
017003******************************************************************PPEDBDET
017004*                                                                 PPEDBDET
017005* THIS SECTION RETRIEVES THE EDB TABLE IDENTIFIER FROM THE        PPEDBDET
017006* DATA ELEMENT TABLE AND PLACES THE THREE CHARACTER TABLE-ID      PPEDBDET
017007* IN THE DATA-ELEMENT-TO-EDB-TABLE ARRAY ENTRY FOR THE DATA       PPEDBDET
017008* ELEMENT NUMBER. AT THE SAME TIME, THE APPROPRIATE INDEX INTO    PPEDBDET
017009* THE PPEDBUPD-TABLE-SELECTIONS ARRAY (CONTAINED IN CPWSXICE)     PPEDBDET
017010* IS ESTABLISHED FOR USE BY PPEDBUPD.                             PPEDBDET
017011*                                                                 PPEDBDET
017012******************************************************************PPEDBDET
017013     SKIP1                                                        PPEDBDET
017014 100-MAINLINE SECTION.                                            PPEDBDET
017015     SKIP1                                                        PPEDBDET
017016     IF FIRST-TIME                                                PPEDBDET
017017         SET NOT-FIRST-TIME TO TRUE                               PPEDBDET
017018*********DISPLAY A-STND-PROG-ID                                   36090553
017019         PERFORM 0150-FIRST-TIME                                  36090553
017020     ELSE                                                         36090553
017021         GOBACK                                                   36090553
017022     END-IF.                                                      PPEDBDET
019200******************************************************************36430795
019300*    ON-LINE, USE DATA FROM GETMAINED AREA IF AVAILABLE           36430795
019400******************************************************************36430795
019500     IF ENVIRON-IS-CICS                                           36430795
019600         IF CPWSXCWA-PPEDBDET-PTR-9 > ZERO                        36430795
019700             SET ADDRESS OF TABLE-AREA TO                         36430795
019800                CPWSXCWA-PPEDBDET-PTR                             36430795
019900             MOVE TABLE-AREA-DATA TO                              36430795
020000                DATA-ELEMENT-TO-EDB-TABLES                        36430795
020100             GOBACK                                               36430795
020200         END-IF                                                   36430795
020300     END-IF.                                                      36430795
020302     SKIP3                                                        PPEDBDET
020303*****INITIALIZE CTL-INTERFACE.                                    36410717
020304*****INITIALIZE WS-CTL-ACTION-HOLD.                               36410717
020305*****MOVE 'FILE-STATUS' TO IO-CTL-ACTION.                         36410717
020306*****PERFORM 8100-CALL-PPIOCTL.                                   36410717
020307*****SKIP1                                                        36410717
020308*****IF IO-CTL-ACTION = 'FILE-CLOSED'                             36410717
020309*****    MOVE SEQ-CLOSE TO WS-CTL-ACTION-HOLD                     36410717
020310*****ELSE                                                         36410717
020311*****    MOVE IO-CTL-ACTION TO WS-CTL-ACTION-HOLD                 36410717
020312*****    IF WS-CTL-MODE-HOLD = 'SEQ'                              36410717
020313*****        MOVE SEQ-CLOSE TO IO-CTL-ACTION                      36410717
020314*****    ELSE                                                     36410717
020315*****        MOVE RAND-CLOSE TO IO-CTL-ACTION                     36410717
020316*****    END-IF                                                   36410717
020317*****    PERFORM 8100-CALL-PPIOCTL                                36410717
020318*****END-IF.                                                      36410717
020319*****SKIP1                                                        36410717
020320*****MOVE SEQ-OPEN-IN TO IO-CTL-ACTION.                           36410717
020321*****PERFORM 8100-CALL-PPIOCTL.                                   36410717
020322*****MOVE XCFK-DET TO IO-CTL-NOM-KEY.                             36410717
020323*****MOVE SEQ-READ TO IO-CTL-ACTION.                              36410717
020324*****PERFORM 8100-CALL-PPIOCTL.                                   36410717
020325*****MOVE ZEROES TO IO-CTL-NOM-KEY.                               36410717
020326     SKIP3                                                        PPEDBDET
020327     INITIALIZE DATA-ELEMENT-TO-EDB-TABLES.                       PPEDBDET
020328     SKIP1                                                        PPEDBDET
020329     PERFORM WITH TEST AFTER                                      PPEDBDET
020330             UNTIL END-DET OR PPEDBDET-D-E-ERROR                  PPEDBDET
020331                           OR PPEDBDET-ERROR                      PPEDBDET
020332         PERFORM 8000-FETCH-DET                                   PPEDBDET
020333*****    IF NOT END-DET AND PBP100-DB-TBL-ID NOT = SPACES         36410717
020334*****        MOVE PBP100-DB-TBL-ID  TO WS-DET-TBL-NAME            36410717
020335         IF NOT END-DET AND DET-DB2-TABLE-ID NOT = SPACES         36410717
020336             MOVE DET-DB2-TABLE-ID  TO WS-DET-TBL-NAME            36410717
020337             INITIALIZE WS-INDEX,                                 PPEDBDET
020338                        CONTROL-MATCH-SW                          PPEDBDET
020339             PERFORM WITH TEST AFTER                              PPEDBDET
020340                     UNTIL WS-INDEX = PPEDBUPD-NO-TBL-SELECTIONS  PPEDBDET
020341                        OR CONTROL-MATCH                          PPEDBDET
020342                 ADD 1 TO WS-INDEX                                PPEDBDET
020343                 IF PPEDBUPD-TABLE-NAME (WS-INDEX) =              PPEDBDET
020344                    WS-DET-TBL-ID                                 PPEDBDET
020345*****                MOVE PBP100-ELEM-NO  TO WS-D-E-INDEX         36410717
020400                     MOVE DET-ELEM-NO     TO WS-D-E-INDEX         36410717
020500                     MOVE PPEDBUPD-PPXXXUPD-ID (WS-INDEX) TO      PPEDBDET
020600                          DATA-ELEMENT-EDB-TBL-ID (WS-D-E-INDEX)  PPEDBDET
020700                     MOVE WS-INDEX                        TO      PPEDBDET
020800                          DATA-ELEMENT-EDB-TBL-IX (WS-D-E-INDEX)  PPEDBDET
020900*****                MOVE PBP100-CK-DIGITX                TO      36410717
021000                     MOVE DET-CHECK-DIGIT                 TO      36410717
021100                          DATA-ELEMENT-EDB-CHK-DIGIT              36090553
021200                                                  (WS-D-E-INDEX)  36090553
021300*****                MOVE PBP100-DATA-TYPE                TO      36410717
021400                     MOVE DET-DATA-TYPE                   TO      36410717
021500                          DATA-ELEMENT-EDB-DATA-TYPE              36090553
021600                                                  (WS-D-E-INDEX)  36090553
021700*****                MOVE PBP100-EXT-LNTH                 TO      36410717
021800                     MOVE DET-EXT-LENGTH                  TO      36410717
021900                          DATA-ELEMENT-EDB-EXT-LNGTH              36090553
022000                                                  (WS-D-E-INDEX)  36090553
022100*****                MOVE PBP100-DEC-POINT                TO      36410717
022200                     MOVE DET-DEC-PLACE                   TO      36410717
022300                          DATA-ELEMENT-EDB-DECIMALS               36090553
022400                                                  (WS-D-E-INDEX)  36090553
022500*****                MOVE PBP100-ELEM-NAME                TO      36410717
022600                     MOVE DET-ELEMENT-NAME                TO      36410717
022700                          DATA-ELEMENT-EDB-ELEM-NAME              36090553
022800                                                  (WS-D-E-INDEX)  36090553
022900                     SET CONTROL-MATCH TO TRUE                    PPEDBDET
023000                 END-IF                                           PPEDBDET
023100             END-PERFORM                                          PPEDBDET
023200             IF NOT CONTROL-MATCH                                 PPEDBDET
023300                 SET PPEDBDET-D-E-ERROR TO TRUE                   PPEDBDET
023400             END-IF                                               PPEDBDET
023500         END-IF                                                   PPEDBDET
023600     END-PERFORM.                                                 PPEDBDET
023700     SKIP1                                                        PPEDBDET
023800*****MOVE SEQ-CLOSE TO IO-CTL-ACTION.                             36410717
023900*****PERFORM 8100-CALL-PPIOCTL.                                   36410717
024000*****IF WS-CTL-ACTION-HOLD NOT = SEQ-CLOSE                        36410717
024100*****    MOVE WS-CTL-ACTION-HOLD TO IO-CTL-ACTION                 36410717
024200*****    PERFORM 8100-CALL-PPIOCTL                                36410717
024300*****END-IF.                                                      36410717
024400     SKIP1                                                        PPEDBDET
024500     PERFORM SQL-CLOSE-DET.                                       36410717
029000******************************************************************36430795
029100*    ON-LINE, DO A SHARED GETMAIN FOR CORE AND SAVE THE           36430795
029200*    TABLE JUST LOADED.                                           36430795
029300*    THE ADDRESS OF THE GETMAIN IS SAVED IN THE CWA.              36430795
029400******************************************************************36430795
029500     IF ENVIRON-IS-CICS                                           36430795
029600         MOVE LENGTH OF DATA-ELEMENT-TO-EDB-TABLES                36430795
029700             TO CPWSGETM-GETMAIN-FLENGTH                          36430795
029800         SET CPWSGETM-PPGETMN-NAME TO TRUE                        36430795
029900         SET CPWSGETM-PPEDBDET-CALL TO TRUE                       36430795
030000         SET CPWSGETM-GETMAIN-FUNCTION TO TRUE                    36430795
030100         CALL CPWSGETM-PPGETMN                                    36430795
030200             USING EIB-DUMMY COMMAREA-DUMMY                       36430795
030300         SET ADDRESS OF TABLE-AREA TO                             36430795
030400            CPWSXCWA-PPEDBDET-PTR                                 36430795
030500         MOVE DATA-ELEMENT-TO-EDB-TABLES TO                       36430795
030600         TABLE-AREA-DATA(1:LENGTH OF DATA-ELEMENT-TO-EDB-TABLES)  36430795
030700     END-IF.                                                      36430795
030702     GOBACK.                                                      PPEDBDET
030703     EJECT                                                        PPEDBDET
030704 0150-FIRST-TIME SECTION.                                         36090553
030705     SKIP1                                                        36090553
030706     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  36090553
030707                              COPY CPPDXWHC.                      36090553
030708     EJECT                                                        36090553
030709 8000-FETCH-DET SECTION.                                          PPEDBDET
030710     SKIP1                                                        PPEDBDET
030711*****PERFORM 8100-CALL-PPIOCTL.                                   36410717
030712*****IF PBP100-KEY-FILL NOT = XCFK-DET-CONSTANT OR                36410717
030713*****   PBP100-CARD-TYPE NOT = SPACE OR                           36410717
030714*****   IO-CTL-ERROR-CODE = 6                                     36410717
030715*****        SET END-DET TO TRUE.                                 36410717
030716     PERFORM SQL-FETCH-DET.                                       36410717
030717     IF SQLCODE = +100                                            36410717
030718         SET END-DET TO TRUE                                      36410717
030719     END-IF.                                                      36410717
030720*****SKIP3                                                        36410717
030721*8100-CALL-PPIOCTL SECTION.                                       36410717
030722*****SKIP1                                                        36410717
030723*****CALL 'PPIOCTL' USING CTL-INTERFACE,                          36410717
030724*****                     CTL-SEGMENT-TABLE.                      36410717
030725*****IF IO-CTL-ERROR-CODE NOT = 0 AND 6                           36410717
030726*****    SET PPEDBDET-ERROR TO TRUE.                              36410717
030727 SQL-OPEN-DET      SECTION.                                       36410717
030728     IF NOT DET-ROW-OPEN                                          36410717
030729         MOVE 'OPEN DET CURSOR' TO DB2MSG-TAG                     36410717
030730         EXEC SQL                                                 36410717
030731             OPEN DET_ROW                                         36410717
030732         END-EXEC                                                 36410717
030733         SET DET-ROW-OPEN TO TRUE                                 36410717
030734     END-IF.                                                      36410717
030735 SQL-CLOSE-DET      SECTION.                                      36410717
030736     IF DET-ROW-OPEN                                              36410717
030737         MOVE 'CLOSE DET CURSOR' TO DB2MSG-TAG                    36410717
030738         EXEC SQL                                                 36410717
030739             CLOSE DET_ROW                                        36410717
030740         END-EXEC                                                 36410717
030741         SET NOT-DET-ROW-OPEN TO TRUE                             36410717
030742     END-IF.                                                      36410717
030743 SQL-FETCH-DET      SECTION.                                      36410717
030744     IF NOT DET-ROW-OPEN                                          36410717
030745         PERFORM SQL-OPEN-DET                                     36410717
030746     END-IF.                                                      36410717
030747     MOVE 'FETCH DET ROW' TO DB2MSG-TAG.                          36410717
030748     EXEC SQL                                                     36410717
030749         FETCH DET_ROW INTO                                       36410717
030750                     :DET-ROW-DATA                                36410717
030751     END-EXEC.                                                    36410717
030752*999999-SQL-ERROR.                                               *36410717
030753     COPY CPPDXP99.                                               36410717
030754     SET PPEDBDET-ERROR TO TRUE.                                  36410717
030755     GOBACK.                                                      36410717
