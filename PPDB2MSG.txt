000101******************************************************************EFIX1007
000200*  PROGRAM: PPDB2MSG                                             *EFIX1007
000300*  RELEASE: ___1007______ SERVICE REQUEST(S): _____EFIX____      *EFIX1007
000400*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  __08/10/95____     *EFIX1007
000500*  DESCRIPTION:                                                  *EFIX1007
000600*  - ERROR REPORT 1288: CHANGES REQUIRED FOR CICS 4.1            *EFIX1007
000700*    CICS 4.1 DOES NOT SUPPORT DB2 LIBRARIES IN DFHRPL.          *EFIX1007
000800*    CHANGE CALL OF DSNTIAR TO STATIC, AND RESOLVE DRUING        *EFIX1007
000800*    LINKAGE.                                                    *EFIX1007
000900******************************************************************EFIX1007
000101******************************************************************36430795
000200*  PROGRAM: PPDB2MSG                                             *36430795
000300*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____      *36430795
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____     *36430795
000500*  DESCRIPTION:                                                  *36430795
000600*  CHANGE FOR DUAL USE; CALL UCWRPMSG WHEN RUNNING UNDER CICS    *36430795
000700*  UCWRPMSG WRITES MESSAGES TO PMSG TDQ; THIS TO AVOID           *36430795
000800*  USE OF THE DISPLAY VERB UNDER CICS                            *36430795
000900******************************************************************36430795
000902******************************************************************36090479
000903*  PROGRAM ID: PPDB2MSG                                          *36090479
000904*  RELEASE: ____0479____  SERVICE REQUEST(S): ____3609____       *36090479
000905*  NAME:___BRUCE JAMES____MODIFICATION DATE:  __06/21/90__       *36090479
000906*  DESCRIPTION:                                                  *36090479
000907*   ADDED CODE TO MOVE STANDARD ABORT MESSAGE, 00-929 TO         *36090479
000908*   LINKAGE FIELD DB2MSG-MESSNO.                                 *36090479
000909******************************************************************36090479
000910******************************************************************36050446
000911*  PROGRAM ID: PPDB2MSG                                          *36050446
000912*  RELEASE: ____0446____  SERVICE REQUEST(S): ____3605____       *36050446
000913*  NAME:___JON GOOD_______CREATION DATE:      __01/31/90__       *36050446
000914*  DESCRIPTION:                                                  *36050446
000915*   PPDB2MSG IS A GENERAL PURPOSE DB2 MESSAGE INTERPRETER        *36050446
000916*   PROGRAM.                                                     *36050446
000917******************************************************************36050446
000918 IDENTIFICATION DIVISION.                                         PPDB2MSG
001000 PROGRAM-ID. PPDB2MSG.                                            PPDB2MSG
001100 INSTALLATION. UC OFFICE OF THE PRESIDENT                         PPDB2MSG
001200 AUTHOR. JON GOOD.                                                PPDB2MSG
001300     SKIP1                                                        PPDB2MSG
001400 DATE-WRITTEN.  01/31/90.                                         PPDB2MSG
001500 DATE-COMPILED.                                                   PPDB2MSG
001600 ENVIRONMENT DIVISION.                                            PPDB2MSG
001700 CONFIGURATION SECTION.                                           PPDB2MSG
001800 SOURCE-COMPUTER. IBM-370.                                        PPDB2MSG
001900 OBJECT-COMPUTER. IBM-370.                                        PPDB2MSG
002000     SKIP2                                                        PPDB2MSG
002100******************************************************************PPDB2MSG
002200*                                                                *PPDB2MSG
002300*                       P P D B 2 M S G                          *PPDB2MSG
002400*                                                                *PPDB2MSG
002500******************************************************************PPDB2MSG
002600*                                                                *PPDB2MSG
002700*   PPDB2MSG IS A GENERAL PURPOSE DB2 MESSAGE INTERPRETER        *PPDB2MSG
002800* PROGRAM. PPDB2MSG TRANSLATES SQLCODE VALUES INTO APPROPRIATE   *PPDB2MSG
002900* TEXT MESSAGES VIA THE DB2 MESSAGE FORMATTING ROUTINE DSNTIAR   *PPDB2MSG
003000* (SUPPLIED WITH THE DB2 RUN-TIME LIBRARY).                      *PPDB2MSG
003100*                                                                *PPDB2MSG
003200*   THE CALLING PROGRAM SUPPLIES PPDB2MSG WITH THE DB2 SQL       *PPDB2MSG
003300* COMMUNICATIONS AREA (SQLCA) AND THE PPDB2MSG-INTERFACE         *PPDB2MSG
003400* (COPYMEMBER CPLNKDB2). PPPDB2MSG THEN CALLS DSNTIAR TO         *PPDB2MSG
003500* RETRIEVE THE APPROPRIATE DB2 MESSAGE TEXT. PPDB2MSG THEN       *PPDB2MSG
003600* DISPLAYS (ON THE DEFAULT SYSOUT DD-STATEMENT) A SET OF         *PPDB2MSG
003700* DIAGNOSTICS WHICH INCLUDES THE CALLING PROGRAM-ID              *PPDB2MSG
003800* (IF SUPPLIED), A LOCATOR TAG NAME (IF SUPPLIED BY THE CALLING  *PPDB2MSG
003900* PROGRAM) AND THE SQL ERROR MESSAGE TEXT.                       *PPDB2MSG
004000*  IN THE EVENT THAT DSNTIAR CANNOT SUCCESSFULLY RETRIEVE        *PPDB2MSG
004100* THE SQL MESSAGE TEXT, A CONDITION INDICATED BY A NON-ZERO      *PPDB2MSG
004200* RETURN CODE FROM DSNTIAR, PPDB2MSG WILL DISPLAY THE DSNTIAR    *PPDB2MSG
004300* RETURN-CODE VALUE ALONG WITH THE SQLCODE VALUE FOR WHICH A     *PPDB2MSG
004400* MESSAGE FETCH WAS ATTEMPTED.                                   *PPDB2MSG
004500*                                                                *PPDB2MSG
004600******************************************************************PPDB2MSG
004700     EJECT                                                        PPDB2MSG
004800 INPUT-OUTPUT SECTION.                                            PPDB2MSG
004900 FILE-CONTROL.                                                    PPDB2MSG
005000******************************************************************PPDB2MSG
005100*                                                                *PPDB2MSG
005200*                         DATA DIVISION                          *PPDB2MSG
005300*                                                                *PPDB2MSG
005400******************************************************************PPDB2MSG
005500 DATA DIVISION.                                                   PPDB2MSG
005600 FILE SECTION.                                                    PPDB2MSG
005700     SKIP2                                                        PPDB2MSG
005800******************************************************************PPDB2MSG
005900*                                                                *PPDB2MSG
006000*                    WORKING-STORAGE SECTION                     *PPDB2MSG
006100*                                                                *PPDB2MSG
006200******************************************************************PPDB2MSG
006300 WORKING-STORAGE SECTION.                                         PPDB2MSG
006400     SKIP2                                                        PPDB2MSG
006500     EXEC SQL                                                     PPDB2MSG
006600         INCLUDE SQLCA                                            PPDB2MSG
006700     END-EXEC.                                                    PPDB2MSG
008700 01  ONLINE-SIGNALS EXTERNAL.                                     36430795
008800     COPY CPWSONLI.                                               36430795
008900 01  UCWSPMSG EXTERNAL.                                           36430795
009000     COPY UCWSPMSG.                                               36430795
009002 01  MSGSQL-AREA.                                                 PPDB2MSG
009003     05  MSGSQL-LENGTH             PIC S9(4) COMP VALUE +864.     PPDB2MSG
009004     05  FILLER OCCURS 12 TIMES.                                  PPDB2MSG
009005         10  MSGSQL-LINE           PIC X(72).                     PPDB2MSG
009006 01  LRECL-SQL-AREA.                                              PPDB2MSG
009007     05  LRECLSQL                  PIC S9(9) COMP VALUE +72.      PPDB2MSG
009008 01  MISC-DATA-AREA.                                              PPDB2MSG
009800     05  WS-PGM-DSNTIAR            PIC X(08) VALUE 'DSNTIAR'.     36430795
009802     05  MAX-MSG-LINES             PIC S9(4) COMP VALUE +12.      PPDB2MSG
009803     05  MSGSQL-INDEX              PIC S9(4) COMP VALUE ZERO.     PPDB2MSG
009804     05  M00929                    PIC X(5)  VALUE '00929'.       36090479
009805     EJECT                                                        PPDB2MSG
009806 01  DETAIL-LINE.                                                 PPDB2MSG
009807     05  DETAIL-1                  PIC X(01).                     PPDB2MSG
009808     05  FILLER                    PIC X(02).                     PPDB2MSG
009809     05  DETAIL-2                  PIC X(72).                     PPDB2MSG
009810     05  FILLER                    PIC X(02).                     PPDB2MSG
009811     05  DETAIL-3                  PIC X(01).                     PPDB2MSG
009812     SKIP3                                                        PPDB2MSG
009813 01  BORDER-AND-LINE-SHELLS.                                      PPDB2MSG
009814     05  BORDER-SHELL.                                            PPDB2MSG
009815         10  FILLER                PIC X(01) VALUE '*'.           PPDB2MSG
009816         10  FILLER                PIC X(76) VALUE SPACES.        PPDB2MSG
009817         10  FILLER                PIC X(01) VALUE '*'.           PPDB2MSG
009818     05  LINE-SHELL.                                              PPDB2MSG
009819         10  FILLER                PIC X(78) VALUE ALL '*'.       PPDB2MSG
009820     SKIP3                                                        PPDB2MSG
009821 01  STOCK-MESSAGE-TEXTS.                                         PPDB2MSG
009822     SKIP1                                                        PPDB2MSG
009823     05  PPDB2MSG-IDENT-TEXT.                                     PPDB2MSG
009824         10  FILLER                PIC X(16) VALUE SPACES.        PPDB2MSG
009825         10  FILLER                PIC X(15) VALUE                PPDB2MSG
009826                 'P P D B 2 M S G'.                               PPDB2MSG
009900         10  FILLER                PIC X(04) VALUE SPACES.        PPDB2MSG
010000         10  FILLER                PIC X(21) VALUE                PPDB2MSG
010100                 'D I A G N O S T I C S'.                         PPDB2MSG
010200         10  FILLER                PIC X(16) VALUE SPACES.        PPDB2MSG
010300     SKIP1                                                        PPDB2MSG
010400     05  PPDB2MSG-ENVIRON-TEXT-1.                                 PPDB2MSG
010500         10  FILLER                PIC X(08) VALUE SPACES.        PPDB2MSG
010600         10  FILLER                PIC X(28) VALUE                PPDB2MSG
010700                 'PPDB2MSG CALLED FROM MODULE:'.                  PPDB2MSG
010800         10  FILLER                PIC X(01) VALUE SPACES.        PPDB2MSG
010900         10  ENVIRON-PGM-ID        PIC X(08) VALUE SPACES.        PPDB2MSG
011000         10  FILLER                PIC X(27) VALUE SPACES.        PPDB2MSG
011100     SKIP1                                                        PPDB2MSG
011200     05  PPDB2MSG-ENVIRON-TEXT-2.                                 PPDB2MSG
011300         10  FILLER                PIC X(03) VALUE SPACES.        PPDB2MSG
011400         10  FILLER                PIC X(33) VALUE                PPDB2MSG
011500                 'LAST TAG NAMED BY CALLING MODULE:'.             PPDB2MSG
011600         10  FILLER                PIC X(01) VALUE SPACES.        PPDB2MSG
011700         10  ENVIRON-TAG           PIC X(31) VALUE SPACES.        PPDB2MSG
011800         10  FILLER                PIC X(04) VALUE SPACES.        PPDB2MSG
011900     SKIP1                                                        PPDB2MSG
012000     05  DSNTIAR-ERROR-TEXT.                                      PPDB2MSG
012100         10  FILLER                PIC X(04) VALUE SPACES.        PPDB2MSG
012200         10  FILLER                PIC X(19) VALUE                PPDB2MSG
012300                 'DSNTIAR RETURN CODE'.                           PPDB2MSG
012400         10  FILLER                PIC X(01) VALUE SPACES.        PPDB2MSG
012500         10  DSNTIAR-RETURN-CODE   PIC 9999  VALUE ZEROES.        PPDB2MSG
012600         10  FILLER                PIC X(01) VALUE SPACES.        PPDB2MSG
012700         10  FILLER                PIC X(33) VALUE                PPDB2MSG
012800                 'ON MESSAGE TEXT FETCH FOR SQLCODE'.             PPDB2MSG
012900         10  FILLER                PIC X(01) VALUE SPACES.        PPDB2MSG
013000         10  DSNTIAR-SQLCODE-FETCH PIC +999  VALUE '+999'.        PPDB2MSG
013100         10  FILLER                PIC X(05) VALUE SPACES.        PPDB2MSG
013200     EJECT                                                        PPDB2MSG
013300******************************************************************PPDB2MSG
013400*                                                                *PPDB2MSG
013500*                      SQL WORKING-STORAGE                       *PPDB2MSG
013600*                                                                *PPDB2MSG
013700******************************************************************PPDB2MSG
013800     EJECT                                                        PPDB2MSG
013900 LINKAGE SECTION.                                                 PPDB2MSG
014000******************************************************************PPDB2MSG
014100*                                                                *PPDB2MSG
014200*                        LINKAGE SECTION                         *PPDB2MSG
014300*                                                                *PPDB2MSG
014400******************************************************************PPDB2MSG
014500     SKIP3                                                        PPDB2MSG
014600 01  PPDB2MSG-SQLCA              PIC X(136).                      PPDB2MSG
014700     SKIP1                                                        PPDB2MSG
014800 01  PPDB2MSG-INTERFACE.         COPY CPLNKDB2.                   PPDB2MSG
017400     SKIP1                                                        36430795
017500 01  EIB-DUMMY                   PIC X(1).                        36430795
017600     SKIP1                                                        36430795
017700 01  COMMAREA-DUMMY              PIC X(1).                        36430795
017702     EJECT                                                        PPDB2MSG
017703 PROCEDURE DIVISION USING PPDB2MSG-SQLCA,                         PPDB2MSG
017704                          PPDB2MSG-INTERFACE.                     PPDB2MSG
017705******************************************************************PPDB2MSG
017706*                                                                *PPDB2MSG
017707*                     SQL FROM PRE-COMPILER                      *PPDB2MSG
017708*                                                                *PPDB2MSG
017709******************************************************************PPDB2MSG
017710     EJECT                                                        PPDB2MSG
017711******************************************************************PPDB2MSG
017712*                                                                *PPDB2MSG
017713*                       PROCEDURE DIVISION                       *PPDB2MSG
017714*                                                                *PPDB2MSG
017715******************************************************************PPDB2MSG
017716     SKIP3                                                        PPDB2MSG
017717 1000-MAINLINE SECTION.                                           PPDB2MSG
017718     SKIP1                                                        PPDB2MSG
017719******************************************************************PPDB2MSG
017720*  THIS IS THE DRIVER ROUTINE FOR PPDB2MSG. THIS SECTION:        *PPDB2MSG
017721*    1) FORMATS THE PPDB2MSG IDENTIFICATION BANNER.              *PPDB2MSG
017722*    2) FORMATS THE CALLING PROGRAM IDENTIFICATION INFORMATION   *PPDB2MSG
017723*      (PASSED IN PPDB2MSG-INTERFACE).                           *PPDB2MSG
017724*    3) CALLS DSNTIAR TO RETRIEVE THE SQL MESSAGE TEXT.          *PPDB2MSG
017725*    4) FORMATS THE SQL MESSAGE TEXT OR A DSNTIAR DIAGNOSTIC,    *PPDB2MSG
017726*      DEPENDING ON WHETHER DSNTIAR EXECUTED SUCCESSFULLY OR NOT.*PPDB2MSG
017727*    5) RETURNS CONTROL TO THE CALLING PROGRAM.                  *PPDB2MSG
017728******************************************************************PPDB2MSG
017729     SKIP1                                                        PPDB2MSG
017730     MOVE PPDB2MSG-SQLCA TO SQLCA.                                PPDB2MSG
017800     SKIP1                                                        PPDB2MSG
017900     PERFORM 4900-FORMAT-IDENTIFICATION.                          PPDB2MSG
018000     SKIP1                                                        PPDB2MSG
018100     PERFORM 4100-FORMAT-ENVIRONMENT.                             PPDB2MSG
018200     SKIP1                                                        PPDB2MSG
021300*****CALL WS-PGM-DSNTIAR USING SQLCA, MSGSQL-AREA, LRECLSQL.      EFIX1007
021200     CALL 'DSNTIAR' USING SQLCA, MSGSQL-AREA, LRECLSQL.           EFIX1007
021305     SKIP1                                                        PPDB2MSG
021306     IF RETURN-CODE = ZERO                                        PPDB2MSG
021307         PERFORM 4200-FORMAT-SQL-MSG                              PPDB2MSG
021308     ELSE                                                         PPDB2MSG
021309         PERFORM 4300-FORMAT-DSNTIAR-ERROR.                       PPDB2MSG
021310     SKIP1                                                        PPDB2MSG
021311     GOBACK.                                                      PPDB2MSG
021312     EJECT                                                        PPDB2MSG
021313 4000-FORMAT-DISPLAY SECTION.                                     PPDB2MSG
021314     SKIP1                                                        PPDB2MSG
021315 4100-FORMAT-ENVIRONMENT.                                         PPDB2MSG
021316     SKIP1                                                        PPDB2MSG
021317******************************************************************PPDB2MSG
021318* FORMAT THE CALLING PROGRAM ENVIRONMENT LINES (CALLING          *PPDB2MSG
021319* PROGRAM-ID AND OPTIONAL TAG. IN THE EVENT THAT THE CALLING     *PPDB2MSG
021320* PROGRAM-ID FIELD OR THE TAG FIELD IN PPDB2MSG-INTERFACE IS     *PPDB2MSG
021321* BLANK, THEN ASSIGN THE APPROPRIATE DEFAULT IDENTIFIER.         *PPDB2MSG
021322******************************************************************PPDB2MSG
021323     SKIP1                                                        PPDB2MSG
021324     MOVE M00929 TO DB2MSG-MESSNO.                                36090479
021325     PERFORM 7100-BOX-TOP.                                        PPDB2MSG
021326     SKIP1                                                        PPDB2MSG
021327     IF DB2MSG-PGM-ID = SPACES                                    PPDB2MSG
021328         MOVE '<UNKNOWN>'          TO ENVIRON-PGM-ID              PPDB2MSG
021329     ELSE                                                         PPDB2MSG
021330         MOVE DB2MSG-PGM-ID        TO ENVIRON-PGM-ID.             PPDB2MSG
021331     MOVE BORDER-SHELL             TO DETAIL-LINE.                PPDB2MSG
021332     MOVE PPDB2MSG-ENVIRON-TEXT-1  TO DETAIL-2.                   PPDB2MSG
021333     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
021334     SKIP1                                                        PPDB2MSG
021335     MOVE BORDER-SHELL             TO DETAIL-LINE.                PPDB2MSG
021400     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
021500     SKIP1                                                        PPDB2MSG
021600     IF DB2MSG-TAG = SPACES                                       PPDB2MSG
021700         MOVE '<NONE>'             TO ENVIRON-TAG                 PPDB2MSG
021800     ELSE                                                         PPDB2MSG
021900         MOVE DB2MSG-TAG           TO ENVIRON-TAG.                PPDB2MSG
022000     MOVE PPDB2MSG-ENVIRON-TEXT-2  TO DETAIL-2.                   PPDB2MSG
022100     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
022200     SKIP1                                                        PPDB2MSG
022300     PERFORM 7200-BOX-BOTTOM.                                     PPDB2MSG
022400     SKIP3                                                        PPDB2MSG
022500 4200-FORMAT-SQL-MSG.                                             PPDB2MSG
022600     SKIP1                                                        PPDB2MSG
022700******************************************************************PPDB2MSG
022800* FORMAT THE SQL MESSAGE TEXT FOR DISPLAY WITHIN THE DIAGNOSTIC  *PPDB2MSG
022900* BOX.                                                           *PPDB2MSG
023000******************************************************************PPDB2MSG
023100     SKIP1                                                        PPDB2MSG
023200     PERFORM 7100-BOX-TOP.                                        PPDB2MSG
023300     SKIP1                                                        PPDB2MSG
023400     MOVE ZERO                     TO MSGSQL-INDEX.               PPDB2MSG
023500     PERFORM UNTIL MSGSQL-INDEX = MAX-MSG-LINES                   PPDB2MSG
023600         ADD 1 TO MSGSQL-INDEX                                    PPDB2MSG
023700         MOVE BORDER-SHELL         TO DETAIL-LINE                 PPDB2MSG
023800         MOVE MSGSQL-LINE (MSGSQL-INDEX) TO DETAIL-2              PPDB2MSG
023900         PERFORM 8000-DISPLAY-LINE                                PPDB2MSG
024000     END-PERFORM.                                                 PPDB2MSG
024100     SKIP1                                                        PPDB2MSG
024200     PERFORM 7200-BOX-BOTTOM.                                     PPDB2MSG
024300     EJECT                                                        PPDB2MSG
024400 4300-FORMAT-DSNTIAR-ERROR.                                       PPDB2MSG
024500     SKIP1                                                        PPDB2MSG
024600******************************************************************PPDB2MSG
024700* FORMAT THE DSNTIAR RETURN-CODE AND THE SQLCODE OF THE DESIRED  *PPDB2MSG
024800* MESSAGE INTO THE DIAGNOSTIC LINE WHICH INDICATES THAT DSNTIAR  *PPDB2MSG
024900* DID NOT SUCCEED IN RETRIEVING THE SQL MESSAGE TEXT.            *PPDB2MSG
025000******************************************************************PPDB2MSG
025100     SKIP1                                                        PPDB2MSG
025200     PERFORM 7100-BOX-TOP.                                        PPDB2MSG
025300     SKIP1                                                        PPDB2MSG
025400     MOVE RETURN-CODE              TO DSNTIAR-RETURN-CODE.        PPDB2MSG
025500     MOVE SQLCODE                  TO DSNTIAR-SQLCODE-FETCH.      PPDB2MSG
025600     MOVE BORDER-SHELL             TO DETAIL-LINE.                PPDB2MSG
025700     MOVE DSNTIAR-ERROR-TEXT       TO DETAIL-2.                   PPDB2MSG
025800     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
025900     SKIP1                                                        PPDB2MSG
026000     PERFORM 7200-BOX-BOTTOM.                                     PPDB2MSG
026100     SKIP3                                                        PPDB2MSG
026200 4900-FORMAT-IDENTIFICATION.                                      PPDB2MSG
026300     SKIP1                                                        PPDB2MSG
026400******************************************************************PPDB2MSG
026500*  FORMAT THE PPDB2MSG IDENTIFICATION BANNER.                    *PPDB2MSG
026600******************************************************************PPDB2MSG
026700     SKIP1                                                        PPDB2MSG
026800     MOVE LINE-SHELL               TO DETAIL-LINE.                PPDB2MSG
026900     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
027000     SKIP1                                                        PPDB2MSG
027100     MOVE BORDER-SHELL             TO DETAIL-LINE.                PPDB2MSG
027200     MOVE PPDB2MSG-IDENT-TEXT      TO DETAIL-2.                   PPDB2MSG
027300     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
027400     EJECT                                                        PPDB2MSG
027500 7000-FORMAT-BOXES SECTION.                                       PPDB2MSG
027600     SKIP1                                                        PPDB2MSG
027700 7100-BOX-TOP.                                                    PPDB2MSG
027800     SKIP1                                                        PPDB2MSG
027900     MOVE LINE-SHELL               TO DETAIL-LINE.                PPDB2MSG
028000     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
028100     SKIP1                                                        PPDB2MSG
028200     MOVE BORDER-SHELL             TO DETAIL-LINE.                PPDB2MSG
028300     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
028400     SKIP3                                                        PPDB2MSG
028500 7200-BOX-BOTTOM.                                                 PPDB2MSG
028600     SKIP1                                                        PPDB2MSG
028700     MOVE BORDER-SHELL             TO DETAIL-LINE.                PPDB2MSG
028800     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
028900     SKIP1                                                        PPDB2MSG
029000     MOVE LINE-SHELL               TO DETAIL-LINE.                PPDB2MSG
029100     PERFORM 8000-DISPLAY-LINE.                                   PPDB2MSG
029200     EJECT                                                        PPDB2MSG
029300 8000-DISPLAY SECTION.                                            PPDB2MSG
029400     SKIP1                                                        PPDB2MSG
029500 8000-DISPLAY-LINE.                                               PPDB2MSG
029600     SKIP1                                                        PPDB2MSG
032800                                                                  36430795
032900     IF ENVIRON-IS-CICS                                           36430795
033000         MOVE DETAIL-LINE TO UCWSPMSG-DISPLAY-DATA                36430795
033100         MOVE 'UCWRPMSG' TO UCWSPMSG-PGM-NAME                     36430795
033200         SET ADDRESS OF EIB-DUMMY TO CPWSONLI-EIB-PTR             36430795
033300         SET ADDRESS OF COMMAREA-DUMMY TO CPWSONLI-COMMAREA-PTR   36430795
033400         CALL UCWSPMSG-PGM-NAME                                   36430795
033500             USING EIB-DUMMY COMMAREA-DUMMY                       36430795
033600     ELSE                                                         36430795
033700         DISPLAY DETAIL-LINE                                      36430795
033800     END-IF.                                                      36430795
033900*****DISPLAY DETAIL-LINE.                                         36430795
