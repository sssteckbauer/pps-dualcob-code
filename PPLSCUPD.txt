000010**************************************************************/   36430976
000020*  PROGRAM: PPLSCUPD                                         */   36430976
000030*  RELEASE: ___0976______ SERVICE REQUEST(S): _____3643____  */   36430976
000040*  NAME:_______JLT_______ MODIFICATION DATE:  __04/18/95____ */   36430976
000050*  DESCRIPTION:                                              */   36430976
000060*  - ADD "QUICK FETCH" PROGRAM FUNCTION.                     */   36430976
000061*    (ERROR REPORT #1255)                                    */   36430976
000070**************************************************************/   36430976
000100**************************************************************/   36290827
000200*  PROGRAM: PPLSCUPD                                         */   36290827
000300*  RELEASE: ____0827____  SERVICE REQUEST(S): ____3629____   */   36290827
000400*  NAME:__J LUCAS ________CREATION DATE:      __10/21/93__   */   36290827
000500*  DESCRIPTION:                                              */   36290827
000600*    EDB LSC TABLE UPDATE DRIVER MODULE.                     */   36290827
000700**************************************************************/   36290827
000800 IDENTIFICATION DIVISION.                                         PPLSCUPD
000900 PROGRAM-ID. PPLSCUPD.                                            PPLSCUPD
001000 AUTHOR. UCOP.                                                    PPLSCUPD
001100 DATE-WRITTEN. APRIL 1993.                                        PPLSCUPD
001200 DATE-COMPILED.                                                   PPLSCUPD
001300*REMARKS.                                                         PPLSCUPD
001400******************************************************************PPLSCUPD
001500*                                                                *PPLSCUPD
001600*  THIS PROGRAM DRIVES THE APPLICATION OF EMPLOYEE DATA BASE     *PPLSCUPD
001700* LSC-TABLE ROWS. THIS PROGRAM IS CALLED BY PPEDBUPD.            *PPLSCUPD
001800*                                                                *PPLSCUPD
001900*  THE DETAILS OF THIS PROGRAMS FUNCTION ARE DESCRIBED IN THE    *PPLSCUPD
002000* PROCEDURE DIVISION COPYMEMBER CPPDEUPD. ADDITIONAL CODE IS     *PPLSCUPD
002100* HARDCODED IN THIS SOURCE TO PROVIDE THE NECESSARY MOVEMENT OF  *PPLSCUPD
002200* CHANGE-RECORD VALUES TO XXX-TABLE COLUMN ROWS, AS WELL AS      *PPLSCUPD
002300* LSC-ROW INITIALIZATION.                                        *PPLSCUPD
002400*                                                                *PPLSCUPD
002500*                                                                *PPLSCUPD
002600******************************************************************PPLSCUPD
002700     EJECT                                                        PPLSCUPD
002800 ENVIRONMENT DIVISION.                                            PPLSCUPD
002900 CONFIGURATION SECTION.                                           PPLSCUPD
003000 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPLSCUPD
003100 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPLSCUPD
003200      EJECT                                                       PPLSCUPD
003300******************************************************************PPLSCUPD
003400*                                                                *PPLSCUPD
003500*                D A T A  D I V I S I O N                        *PPLSCUPD
003600*                                                                *PPLSCUPD
003700******************************************************************PPLSCUPD
003800 DATA DIVISION.                                                   PPLSCUPD
003900 WORKING-STORAGE SECTION.                                         PPLSCUPD
004000 01  MISCELLANEOUS-WS.                                            PPLSCUPD
004100     05  A-STND-PROG-ID      PIC X(08) VALUE 'PPLSCUPD'.          PPLSCUPD
004200     05  PPXXXUTL-NAME       PIC X(08) VALUE 'PPLSCUTL'.          PPLSCUPD
004300     05  PPXXXUTW-NAME       PIC X(08) VALUE 'PPLSCUTW'.          PPLSCUPD
004400     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPLSCUPD
004500         88  FIRST-TIME                VALUE 'Y'.                 PPLSCUPD
004600         88  NOT-FIRST-TIME            VALUE 'N'.                 PPLSCUPD
004700     05  WS-EMPLOYEE-ID      PIC X(9).                            PPLSCUPD
004800     05  WS-IX1              PIC S9(4) COMP VALUE ZERO.           PPLSCUPD
004900     05  WS-IX2              PIC S9(4) COMP VALUE ZERO.           PPLSCUPD
005000     05  WS-HOLD-KEY.                                             PPLSCUPD
005100         15  WS-HOLD-TITLE-CODE     PIC X(04).                    PPLSCUPD
005200         15  WS-HOLD-TUC            PIC X(02).                    PPLSCUPD
005300         15  WS-HOLD-APPT-REP-CODE  PIC X(01).                    PPLSCUPD
005400     05  WS-LSC-OCCURRENCE-KEY.                                   PPLSCUPD
005500         15  WS-LSC-TITLE-CODE      PIC X(04).                    PPLSCUPD
005600         15  WS-LSC-TUC             PIC X(02).                    PPLSCUPD
005700         15  WS-LSC-APPT-REP-CODE   PIC X(01).                    PPLSCUPD
005720                                                                  36430976
005730 01  QUICK-FETCH-MISC.                                            36430976
005740     05  ORIG-X                       PIC S9(4)  COMP VALUE ZERO. 36430976
005750     05  UPDT-X                       PIC S9(4)  COMP VALUE ZERO. 36430976
005760     05  RECEIVE-X                    PIC S9(4)  COMP VALUE ZERO. 36430976
005770     05  TEMP-X                       PIC S9(4)  COMP VALUE ZERO. 36430976
005780     05  NEXT-HIGHEST-X               PIC S9(4)  COMP VALUE ZERO. 36430976
005790     05  LAST-LOADED-OCCUR-KEY.                                   36430976
005791         10  FILLER                   PIC X(7).                   36430976
005792     05  NEXT-HIGHEST-OCCUR-KEY.                                  36430976
005793         10  FILLER                   PIC X(7).                   36430976
005794     05  COMPLETE-SW                  PIC X   VALUE 'N'.          36430976
005795         88  COMPLETE-SW-OFF                  VALUE 'N'.          36430976
005796         88  COMPLETE-SW-ON                   VALUE 'Y'.          36430976
005800     SKIP3                                                        PPLSCUPD
005900 01  TEMP-DATA-AREA.                                              PPLSCUPD
006000                                    COPY CPWSXMMM.                PPLSCUPD
006100 01  DATE-CONVERSION-WORK-AREAS.                                  PPLSCUPD
006200                                    COPY CPWSXDC2.                PPLSCUPD
006300     EJECT                                                        PPLSCUPD
006400******************************************************************PPLSCUPD
006500* LAYOFF-CREDIT-ARRAY:                                           *PPLSCUPD
006600*        THE ROW THAT WILL BE PASSED AROUND FOR USE BY HIGHER    *PPLSCUPD
006700* LEVEL PROGRAMS. IT IS DEFINED AS AN EXTERNAL DATA AREA TO      *PPLSCUPD
006800* ALLOW HIGHER LEVEL PROGRAMS TO AVOID PASSING THE ROW           *PPLSCUPD
006900* THROUGH UNNECESSARY CALLING CHAINS.                            *PPLSCUPD
007000*                                                                *PPLSCUPD
007100* LAYOFF-CREDIT-ARRAY-HOLD:                                      *PPLSCUPD
007200*        THE PURE ROW AS READ FROM THE LSC TBL ROW.              *PPLSCUPD
007300* IT IS HELD IN WORKING-STORAGE TO ALLOW THE REFRESH FUNCTION    *PPLSCUPD
007400* TO BE PERFORMED WITHOUT AN ADDED DATA BASE ACCESS.             *PPLSCUPD
007500* THIS ARRAY IS ALSO THE REFERENCE FOR LOGIC WHICH DETERMINES    *PPLSCUPD
007600* WHICH LSC-ROWS ARE TO BE UPDATED, DELETED AND INSERTED.        *PPLSCUPD
007700*                                                                *PPLSCUPD
007800* LAYOFF-CREDIT-ARRAY-HOLD1:                                     *PPLSCUPD
007900*        THE ROW AS SET ASIDE, AFTER SOME UPDATE ACTIVITY,       *PPLSCUPD
008000* BY THE EDB UPDATE COMPLEX, TO ALLOW THE GENERATION             *PPLSCUPD
008100* OF THE "FIRST" CHANGE ENTRY IN CHANGE RECORDS.                 *PPLSCUPD
008200*                                                                *PPLSCUPD
008300******************************************************************PPLSCUPD
008400 01  LAYOFF-CREDIT-ARRAY    EXTERNAL.                             PPLSCUPD
008500                                 COPY CPWSELSC.                   PPLSCUPD
008600     EJECT                                                        PPLSCUPD
008700 01  LAYOFF-CREDIT-ARRAY-HOLD EXTERNAL.                           PPLSCUPD
008800                                 COPY CPWSELSC.                   PPLSCUPD
008900     EJECT                                                        PPLSCUPD
009000 01  LAYOFF-CREDIT-ARRAY-HOLD1 EXTERNAL.                          PPLSCUPD
009100                                 COPY CPWSELSC.                   PPLSCUPD
009200     SKIP1                                                        PPLSCUPD
009300 01  PPP080-PPP120-SET-TRAN EXTERNAL.                             PPLSCUPD
009400     05  TRAN-APPLIED-SWITCH PIC X(01).                           PPLSCUPD
009500         88  TRAN-APPLIED              VALUE 'Y'.                 PPLSCUPD
009600         88  NOT-TRAN-APPLIED          VALUE 'N'.                 PPLSCUPD
009700     EJECT                                                        PPLSCUPD
009800 01  LSC-ROW.                                                     PPLSCUPD
009900     EXEC SQL                                                     PPLSCUPD
010000         INCLUDE PPPVLSC1                                         PPLSCUPD
010100     END-EXEC.                                                    PPLSCUPD
010200     EJECT                                                        PPLSCUPD
010300 01  INIT-LSC-ARRAY    EXTERNAL.                                  PPLSCUPD
010400                                 COPY CPWSELSC.                   PPLSCUPD
010500     EJECT                                                        PPLSCUPD
010600 01  PPLSCUTL-INTERFACE.                                          PPLSCUPD
010700                                 COPY CPLNKLSC.                   PPLSCUPD
010800     EJECT                                                        PPLSCUPD
010900 01  PPXXXUTW-INTERFACE.                                          PPLSCUPD
011000                                 COPY CPLNWXXX.                   PPLSCUPD
011100     EJECT                                                        PPLSCUPD
011200 01  XWHC-COMPILE-WORK-AREA.                                      PPLSCUPD
011300                                 COPY CPWSXWHC.                   PPLSCUPD
011400     EJECT                                                        PPLSCUPD
011500 01  PAYROLL-CONSTANTS.                                           PPLSCUPD
011600                                 COPY CPWSXIC2.                   PPLSCUPD
011700 01  ONLINE-SIGNALS    EXTERNAL. COPY CPWSONLI.                   PPLSCUPD
011800     EJECT                                                        PPLSCUPD
011900 LINKAGE SECTION.                                                 PPLSCUPD
012000******************************************************************PPLSCUPD
012100*              L I N K A G E    S E C T I O N                    *PPLSCUPD
012200******************************************************************PPLSCUPD
012300*                                                                 PPLSCUPD
012400 01  PPXXXUPD-INTERFACE.            COPY CPLNUXXX.                PPLSCUPD
012500     EJECT                                                        PPLSCUPD
012600 01  EDB-CHANGE-RECORD.             COPY CPWSXCHG.                PPLSCUPD
012700     EJECT                                                        PPLSCUPD
012800 PROCEDURE DIVISION USING                                         PPLSCUPD
012900                          PPXXXUPD-INTERFACE                      PPLSCUPD
013000                          EDB-CHANGE-RECORD.                      PPLSCUPD
013100*                                                                 PPLSCUPD
013200******************************************************************PPLSCUPD
013300*            P R O C E D U R E  D I V I S I O N                  *PPLSCUPD
013400******************************************************************PPLSCUPD
013500*                                                                 PPLSCUPD
013600     SKIP3                                                        PPLSCUPD
013700 0100-MAINLINE SECTION.                                           PPLSCUPD
013800     SKIP1                                                        PPLSCUPD
013900     IF ENVIRON-IS-CICS                                           PPLSCUPD
014000         MOVE PPXXXUPD-EMPLOYEE-ID TO PPLSCUTL-EMPLOYEE-ID        PPLSCUPD
014100         MOVE PPLSCUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID              PPLSCUPD
014200     END-IF.                                                      PPLSCUPD
014300     INITIALIZE PPXXXUPD-ERROR-SWITCHES.                          PPLSCUPD
014400     SKIP1                                                        PPLSCUPD
014500     IF FIRST-TIME                                                PPLSCUPD
014600         SET NOT-FIRST-TIME TO TRUE                               PPLSCUPD
014700         PERFORM 7900-FIRST-TIME                                  PPLSCUPD
014800     END-IF.                                                      PPLSCUPD
014900     SKIP3                                                        PPLSCUPD
015000     EVALUATE PPXXXUPD-FUNCTION                                   PPLSCUPD
015100         WHEN 'A'                                                 PPLSCUPD
015200             PERFORM 1000-APPLY-DATA-ELEMENT-CHANGE               PPLSCUPD
015300         WHEN 'C'                                                 PPLSCUPD
015400             PERFORM 9000-INITIALIZE-ARRAY                        PPLSCUPD
015500         WHEN 'F'                                                 PPLSCUPD
015600             PERFORM 9000-INITIALIZE-ARRAY                        PPLSCUPD
015700             MOVE LAYOFF-CREDIT-ARRAY TO LAYOFF-CREDIT-ARRAY-HOLD PPLSCUPD
015800             MOVE PPXXXUPD-EMPLOYEE-ID TO PPLSCUTL-EMPLOYEE-ID    PPLSCUPD
015900             PERFORM 8100-CALL-PPXXXUTL                           PPLSCUPD
016000         WHEN 'H'                                                 PPLSCUPD
016100            MOVE LAYOFF-CREDIT-ARRAY TO LAYOFF-CREDIT-ARRAY-HOLD1 PPLSCUPD
016200         WHEN 'P'                                                 PPLSCUPD
016300             PERFORM 2000-POST-ROWS                               PPLSCUPD
016400         WHEN 'R'                                                 PPLSCUPD
016500             MOVE LAYOFF-CREDIT-ARRAY-HOLD TO LAYOFF-CREDIT-ARRAY PPLSCUPD
016600         WHEN 'S'                                                 36430821
016700             MOVE PPXXXUPD-EMPLOYEE-ID TO PPLSCUTL-EMPLOYEE-ID    36430821
016800             MOVE PPLSCUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID          36430821
016820         WHEN 'Q'                                                 36430976
016830             PERFORM 9400-QUICK-FETCH                             36430976
016900         WHEN OTHER                                               PPLSCUPD
017000             SET PPXXXUPD-INVALID-FUNCTION TO TRUE                PPLSCUPD
017100     END-EVALUATE.                                                PPLSCUPD
017200     SKIP3                                                        PPLSCUPD
017300     IF PPXXXUPD-ERROR-SWITCHES NOT = SPACES                      PPLSCUPD
017400         SET PPXXXUPD-ERROR TO TRUE                               PPLSCUPD
017500     SKIP3                                                        PPLSCUPD
017600     END-IF.                                                      PPLSCUPD
017700     GOBACK.                                                      PPLSCUPD
017800     EJECT                                                        PPLSCUPD
017900 1000-APPLY-DATA-ELEMENT-CHANGE SECTION.                          PPLSCUPD
018000     SKIP3                                                        PPLSCUPD
018100     IF XCHG-EMPLOYEE-ID NOT = SPACES                             PPLSCUPD
018200         IF XCHG-EMPLOYEE-ID NOT = PPLSCUTL-EMPLOYEE-ID           PPLSCUPD
018300             MOVE XCHG-EMPLOYEE-ID TO PPLSCUTL-EMPLOYEE-ID        PPLSCUPD
018400             IF NOT ENVIRON-IS-CICS                               PPLSCUPD
018500                 PERFORM 8100-CALL-PPXXXUTL                       PPLSCUPD
018600             END-IF                                               PPLSCUPD
018700         END-IF                                                   PPLSCUPD
018800         PERFORM 1100-APPLY-CHANGE                                PPLSCUPD
018900     ELSE                                                         PPLSCUPD
019000         SET PPXXXUPD-APPLY-CHANGE-ERROR TO TRUE                  PPLSCUPD
019100     END-IF.                                                      PPLSCUPD
019200     EJECT                                                        PPLSCUPD
019300 1100-APPLY-CHANGE SECTION.                                       PPLSCUPD
019400     SKIP1                                                        PPLSCUPD
019500     IF XCHG-NEW-VAL2-LNGTH NOT = 0                               PPLSCUPD
019600         MOVE XCHG-NEW-VALUE2 TO TEMP-DATA-VALUE                  PPLSCUPD
019700     ELSE                                                         PPLSCUPD
019800         IF XCHG-NEW-VAL1-LNGTH NOT = 0                           PPLSCUPD
019900             MOVE XCHG-NEW-VALUE1 TO TEMP-DATA-VALUE              PPLSCUPD
020000         ELSE                                                     PPLSCUPD
020100             SET PPXXXUPD-APPLY-CHANGE-ERROR TO TRUE              PPLSCUPD
020200         END-IF                                                   PPLSCUPD
020300     END-IF.                                                      PPLSCUPD
020400     SKIP1                                                        PPLSCUPD
020500     IF NOT PPXXXUPD-APPLY-CHANGE-ERROR                           PPLSCUPD
020600         PERFORM 9100-EVALUATE-DATA-ELEMENT                       PPLSCUPD
020700     END-IF.                                                      PPLSCUPD
020800     SKIP1                                                        PPLSCUPD
020900     EJECT                                                        PPLSCUPD
021000 2000-POST-ROWS SECTION.                                          PPLSCUPD
021100     SKIP1                                                        PPLSCUPD
021200*                                                                 PPLSCUPD
021300*    FIRST DELETE                                                 PPLSCUPD
021400*                                                                 PPLSCUPD
021500*                                                                 PPLSCUPD
021600     PERFORM VARYING WS-IX1 FROM 1 BY 1                           PPLSCUPD
021700             UNTIL (WS-IX1 > IDC-MAX-NO-LSC)                      PPLSCUPD
021800         OR (LAY-CR-OCCURRENCE-KEY OF                             PPLSCUPD
021900                               LAYOFF-CREDIT-ARRAY-HOLD (WS-IX1)  PPLSCUPD
022000             = LAY-CR-OCCURRENCE-KEY OF INIT-LSC-ARRAY (1))       PPLSCUPD
022100             MOVE LAY-CR-TITLE-CODE OF                            PPLSCUPD
022200                               LAYOFF-CREDIT-ARRAY-HOLD (WS-IX1)  PPLSCUPD
022300                            TO LSC-TITLE-CODE OF LSC-ROW          PPLSCUPD
022400             MOVE LAY-CR-TUC OF LAYOFF-CREDIT-ARRAY-HOLD (WS-IX1) PPLSCUPD
022500                            TO LSC-TUC OF LSC-ROW                 PPLSCUPD
022600             MOVE LAY-CR-APPT-REP-CODE OF                         PPLSCUPD
022700                               LAYOFF-CREDIT-ARRAY-HOLD (WS-IX1)  PPLSCUPD
022800                            TO LSC-APPT-REP-CODE OF LSC-ROW       PPLSCUPD
022900             MOVE WS-EMPLOYEE-ID TO EMPLOYEE-ID OF LSC-ROW        PPLSCUPD
023000             MOVE 'D' TO PPXXXUTW-FUNCTION                        PPLSCUPD
023100             PERFORM 8200-CALL-PPXXXUTW                           PPLSCUPD
023200     END-PERFORM.                                                 PPLSCUPD
023300*                                                                 PPLSCUPD
023400*    SAVE POINTER TO FIRST "NEW" ARRAY ELEMENT IF ANY             PPLSCUPD
023500*                                                                 PPLSCUPD
023600     MOVE WS-IX1 TO WS-IX2.                                       PPLSCUPD
023700*                                                                 PPLSCUPD
023800*    THEN ADD; STOPPING ON FIRST INITIAL ENTRY IN THE             PPLSCUPD
023900*    "NEW" PART OF THE ARRAY                                      PPLSCUPD
024000*                                                                 PPLSCUPD
024100     SKIP1                                                        PPLSCUPD
024200     PERFORM VARYING WS-IX1 FROM 1 BY 1                           PPLSCUPD
024300             UNTIL (WS-IX1 > IDC-MAX-NO-LSC)                      PPLSCUPD
024400         OR ((LAY-CR-OCCURRENCE-KEY OF                            PPLSCUPD
024500                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
024600                =  LAY-CR-OCCURRENCE-KEY OF INIT-LSC-ARRAY (1))   PPLSCUPD
024700                AND WS-IX1 NOT < WS-IX2)                          PPLSCUPD
024800         IF (LAY-CR-OCCURRENCE-KEY OF                             PPLSCUPD
024900                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
025000            NOT =  LAY-CR-OCCURRENCE-KEY OF INIT-LSC-ARRAY (1))   PPLSCUPD
025100             MOVE LAY-CR-TITLE-CODE OF                            PPLSCUPD
025200                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
025300               TO LSC-TITLE-CODE OF LSC-ROW                       PPLSCUPD
025400             MOVE LAY-CR-TUC OF LAYOFF-CREDIT-ARRAY (WS-IX1)      PPLSCUPD
025500               TO LSC-TUC OF LSC-ROW                              PPLSCUPD
025600             MOVE LAY-CR-APPT-REP-CODE OF                         PPLSCUPD
025700                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
025800               TO LSC-APPT-REP-CODE OF LSC-ROW                    PPLSCUPD
025900             MOVE LAY-CR-REG-HOURS OF                             PPLSCUPD
026000                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
026100               TO LSC-REGULAR-HOURS OF LSC-ROW                    PPLSCUPD
026200             MOVE LAY-CR-REG-HOURS-DATE OF                        PPLSCUPD
026300                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
026400               TO LSC-REG-HRS-UPDATE OF LSC-ROW                   PPLSCUPD
026500             MOVE LAY-CR-PRIOR-BAL-HRS  OF                        PPLSCUPD
026600                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
026700               TO LSC-PRIOR-BAL-HRS  OF LSC-ROW                   PPLSCUPD
026800             MOVE LAY-CR-PRIOR-BAL-DATE OF                        PPLSCUPD
026900                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
027000               TO LSC-PRIOR-BAL-DATE OF LSC-ROW                   PPLSCUPD
027100             MOVE LAY-CR-ADC-CODE OF                              PPLSCUPD
027200                                    LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
027300               TO LSC-ADC-CODE OF LSC-ROW                         PPLSCUPD
027400             MOVE WS-EMPLOYEE-ID TO EMPLOYEE-ID OF LSC-ROW        PPLSCUPD
027500             MOVE 'I' TO PPXXXUTW-FUNCTION                        PPLSCUPD
027600             PERFORM 8200-CALL-PPXXXUTW                           PPLSCUPD
027700         END-IF                                                   PPLSCUPD
027800     END-PERFORM.                                                 PPLSCUPD
027900     SKIP1                                                        PPLSCUPD
028000     EJECT                                                        PPLSCUPD
028100 7900-FIRST-TIME SECTION.                                         PPLSCUPD
028200     SKIP1                                                        PPLSCUPD
028300     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPLSCUPD
028400     SKIP1                                                        PPLSCUPD
028500                                    COPY CPPDXWHC.                PPLSCUPD
028600     EJECT                                                        PPLSCUPD
028700 8100-CALL-PPXXXUTL SECTION.                                      PPLSCUPD
028800     SKIP1                                                        PPLSCUPD
028900     CALL PPXXXUTL-NAME USING PPLSCUTL-INTERFACE,                 PPLSCUPD
029000                              LAYOFF-CREDIT-ARRAY                 PPLSCUPD
029100     SKIP1                                                        PPLSCUPD
029200     IF PPLSCUTL-ERROR                                            PPLSCUPD
029300         PERFORM 9000-INITIALIZE-ARRAY                            PPLSCUPD
029400         SET PPXXXUPD-ERROR TO TRUE                               PPLSCUPD
029500     END-IF.                                                      PPLSCUPD
029600     SKIP1                                                        PPLSCUPD
029700     IF PPXXXUPD-DIAGNOSTICS-ON                                   PPLSCUPD
029800         DISPLAY A-STND-PROG-ID ': ' PPXXXUTL-NAME,               PPLSCUPD
029900                 '-INTERFACE "' PPLSCUTL-INTERFACE '"'            PPLSCUPD
030000     END-IF.                                                      PPLSCUPD
030100     SKIP1                                                        PPLSCUPD
030200     MOVE LAYOFF-CREDIT-ARRAY TO LAYOFF-CREDIT-ARRAY-HOLD.        PPLSCUPD
030300     SKIP1                                                        PPLSCUPD
030400     MOVE PPLSCUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                 PPLSCUPD
030500     EJECT                                                        PPLSCUPD
030600 8200-CALL-PPXXXUTW SECTION.                                      PPLSCUPD
030700     SKIP1                                                        PPLSCUPD
030800     INITIALIZE PPXXXUTW-ERROR-SWITCHES.                          PPLSCUPD
030900     SKIP1                                                        PPLSCUPD
031000     CALL PPXXXUTW-NAME USING PPXXXUTW-INTERFACE,                 PPLSCUPD
031100                           LSC-ROW.                               PPLSCUPD
031200     SKIP1                                                        PPLSCUPD
031300     IF PPXXXUTW-ERROR                                            PPLSCUPD
031400         SET PPXXXUPD-ERROR TO TRUE                               PPLSCUPD
031500     END-IF.                                                      PPLSCUPD
031600     SKIP1                                                        PPLSCUPD
031700     IF PPXXXUPD-DIAGNOSTICS-ON                                   PPLSCUPD
031800         DISPLAY A-STND-PROG-ID ': PPXXXUTW-INTERFACE "',         PPLSCUPD
031900                 PPXXXUTW-INTERFACE '"',                          PPLSCUPD
032000                 'LSC-ROW="' LSC-ROW '"'                          PPLSCUPD
032100     END-IF.                                                      PPLSCUPD
032200     EJECT                                                        PPLSCUPD
032300 9000-INITIALIZE-ARRAY SECTION.                                   PPLSCUPD
032400     SKIP1                                                        PPLSCUPD
032500******************************************************************PPLSCUPD
032600*                                                                *PPLSCUPD
032700* THIS SECTION INITIALIZES THE LAYOFF CREDIT ARRAY.              *PPLSCUPD
032800* THE INITIAL DATE VALUE IS MOVED OT COLUMNS IN THE ARRAY        *PPLSCUPD
032900* WHICH ARE DEFINED AS DB2 DATES.                                *PPLSCUPD
033000*                                                                *PPLSCUPD
033100******************************************************************PPLSCUPD
033200     SKIP1                                                        PPLSCUPD
033300     INITIALIZE LAYOFF-CREDIT-ARRAY.                              PPLSCUPD
033400     PERFORM VARYING WS-IX1  FROM 1 BY 1                          PPLSCUPD
033500         UNTIL                                                    PPLSCUPD
033600         WS-IX1  > IDC-MAX-NO-LSC                                 PPLSCUPD
033700         MOVE '0001-01-01'   TO LAY-CR-REG-HOURS-DATE             PPLSCUPD
033800             OF LAYOFF-CREDIT-ARRAY (WS-IX1)                      PPLSCUPD
033900         MOVE '0001-01-01'   TO LAY-CR-PRIOR-BAL-DATE             PPLSCUPD
034000             OF LAYOFF-CREDIT-ARRAY (WS-IX1)                      PPLSCUPD
034100     END-PERFORM.                                                 PPLSCUPD
034200     MOVE  LAYOFF-CREDIT-ARRAY TO INIT-LSC-ARRAY.                 PPLSCUPD
034300     EJECT                                                        PPLSCUPD
034400 9100-EVALUATE-DATA-ELEMENT SECTION.                              PPLSCUPD
034500     SKIP1                                                        PPLSCUPD
034600******************************************************************PPLSCUPD
034700*                                                                *PPLSCUPD
034800* THIS SECTION TAKES THE DATA ELEMENT NUMBER FROM THE CHANGE     *PPLSCUPD
034900* RECORD TO DETERMINE WHICH COLUMN IS TO RECEIVE THE VALUE FROM  *PPLSCUPD
035000* THE CHANGE RECORD.                                             *PPLSCUPD
035100*                                                                *PPLSCUPD
035200******************************************************************PPLSCUPD
035300     SKIP1                                                        PPLSCUPD
035400     MOVE XCHG-OCCURENCE-KEY TO WS-HOLD-KEY.                      PPLSCUPD
035500     MOVE WS-HOLD-KEY TO WS-LSC-OCCURRENCE-KEY.                   PPLSCUPD
035600*                                                                 PPLSCUPD
035700*    LOCATE THE PROPER ARRAY ELEMENT OR THE                       PPLSCUPD
035800*    NEXT AVAILABLE                                               PPLSCUPD
035900*                                                                 PPLSCUPD
036000     PERFORM VARYING WS-IX1 FROM 1 BY 1                           PPLSCUPD
036100             UNTIL WS-IX1 > IDC-MAX-NO-LSC                        PPLSCUPD
036200         OR (LAY-CR-OCCURRENCE-KEY OF                             PPLSCUPD
036300                                   LAYOFF-CREDIT-ARRAY (WS-IX1)   PPLSCUPD
036400             = WS-LSC-OCCURRENCE-KEY)                             PPLSCUPD
036500         OR (LAY-CR-OCCURRENCE-KEY OF                             PPLSCUPD
036600                                   LAYOFF-CREDIT-ARRAY (WS-IX1)   PPLSCUPD
036700             = LAY-CR-OCCURRENCE-KEY OF INIT-LSC-ARRAY (1))       PPLSCUPD
036800     END-PERFORM.                                                 PPLSCUPD
036900*                                                                 PPLSCUPD
037000*    RETURN WITH AN ERROR IF IT IS NOT THERE                      PPLSCUPD
037100*    OR NO NEXT AVAILABLE                                         PPLSCUPD
037200*                                                                 PPLSCUPD
037300     SET TRAN-APPLIED TO TRUE.                                    PPLSCUPD
037400     IF WS-IX1 > IDC-MAX-NO-LSC                                   PPLSCUPD
037500          IF PPXXXUPD-FUNCTION = 'A'                              PPLSCUPD
037600              SET NOT-TRAN-APPLIED TO TRUE                        PPLSCUPD
037700          ELSE                                                    PPLSCUPD
037800              SET PPXXXUPD-DATA-ELEM-ERROR TO TRUE                PPLSCUPD
037900          END-IF                                                  PPLSCUPD
038000          GO TO 9100-XIT                                          PPLSCUPD
038100     END-IF.                                                      PPLSCUPD
038200                                                                  PPLSCUPD
038300*                                                                 PPLSCUPD
038400*    LOAD KEY IF THIS IS A NEW ROW                                PPLSCUPD
038500*                                                                 PPLSCUPD
038600     IF (LAY-CR-OCCURRENCE-KEY OF LAYOFF-CREDIT-ARRAY (WS-IX1)    PPLSCUPD
038700             =  LAY-CR-OCCURRENCE-KEY   OF INIT-LSC-ARRAY (1))    PPLSCUPD
038800        MOVE WS-LSC-OCCURRENCE-KEY TO                             PPLSCUPD
038900           LAY-CR-OCCURRENCE-KEY OF LAYOFF-CREDIT-ARRAY (WS-IX1)  PPLSCUPD
039000     END-IF.                                                      PPLSCUPD
039100     SKIP1                                                        PPLSCUPD
039200     EVALUATE XCHG-DATA-ELEM-NO                                   PPLSCUPD
039300         WHEN '0486'                                              PPLSCUPD
039400              MOVE TEMP-DATA-VALUE TO                             PPLSCUPD
039500                   LAY-CR-TITLE-CODE                              PPLSCUPD
039600                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
039700         WHEN '0487'                                              PPLSCUPD
039800              MOVE TEMP-DATA-VALUE TO                             PPLSCUPD
039900                   LAY-CR-TUC                                     PPLSCUPD
040000                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
040100         WHEN '0488'                                              PPLSCUPD
040200              MOVE TEMP-DATA-VALUE TO                             PPLSCUPD
040300                   LAY-CR-APPT-REP-CODE                           PPLSCUPD
040400                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
040500         WHEN '0489'                                              PPLSCUPD
040600              MOVE TEMP-NUMBER TO                                 PPLSCUPD
040700                   LAY-CR-REG-HOURS                               PPLSCUPD
040800                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
040900         WHEN '0490'                                              PPLSCUPD
041000              MOVE TEMP-NUMBER TO                                 PPLSCUPD
041100                   LAY-CR-PRIOR-BAL-HRS                           PPLSCUPD
041200                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
041300         WHEN '0491'                                              PPLSCUPD
041400              MOVE TEMP-DATA-VALUE TO                             PPLSCUPD
041500                   LAY-CR-REG-HOURS-DATE                          PPLSCUPD
041600                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
041700         WHEN '0492'                                              PPLSCUPD
041800              MOVE TEMP-DATA-VALUE TO                             PPLSCUPD
041900                   LAY-CR-PRIOR-BAL-DATE                          PPLSCUPD
042000                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
042100         WHEN '0493'                                              PPLSCUPD
042200              MOVE TEMP-DATA-VALUE TO                             PPLSCUPD
042300                   LAY-CR-ADC-CODE                                PPLSCUPD
042400                          OF LAYOFF-CREDIT-ARRAY (WS-IX1)         PPLSCUPD
042500      SKIP1                                                       PPLSCUPD
042600         WHEN OTHER                                               PPLSCUPD
042700              IF PPXXXUPD-DIAGNOSTICS-ON                          PPLSCUPD
042800                  DISPLAY A-STND-PROG-ID                          PPLSCUPD
042900                          ': UNRECOGNIZED DATA ELEMENT NUMBER ('  PPLSCUPD
043000                          XCHG-DATA-ELEM-NO                       PPLSCUPD
043100                          ')'                                     PPLSCUPD
043200              END-IF                                              PPLSCUPD
043300              SET PPXXXUPD-DATA-ELEM-ERROR TO TRUE                PPLSCUPD
043400     END-EVALUATE.                                                PPLSCUPD
043500 9100-XIT.                                                        36290827
043600     EXIT.                                                        PPLSCUPD
043700*                                                                 PPLSCUPD
043800 DATE-CONVERSION-DB2 SECTION.            COPY CPPDXDC2.           PPLSCUPD
043900*                                                                 PPLSCUPD
044000     SKIP3                                                        PPLSCUPD
044020**--------------------------------------------------------------**36430976
044030*9400-QUICK-FETCH  SECTION.  <----- COPIED SECTION                36430976
044040*                                                                 36430976
044050                                     COPY CPPDQFET                36430976
044060        REPLACING IDC-MAX-NO-000      BY IDC-MAX-NO-LSC           36430976
044070                  000-OCCURRENCE-KEY  BY LAY-CR-OCCURRENCE-KEY    36430976
044080                  000-ARRAY-ENTRY     BY LAY-CR-ARRAY             36430976
044090                  000-ARRAY           BY LAYOFF-CREDIT-ARRAY      36430976
044091                  000-ARRAY-HOLD      BY LAYOFF-CREDIT-ARRAY-HOLD 36430976
044092                  INIT-000-ARRAY      BY INIT-LSC-ARRAY           36430976
044093                  PP000UTL-ROW-COUNT  BY PPLSCUTL-ROW-COUNT.      36430976
044094**--------------------------------------------------------------**36430976
044100************************   END OF PROGRAM ************************PPLSCUPD
