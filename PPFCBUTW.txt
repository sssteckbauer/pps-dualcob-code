000800*==========================================================%      UCSD
000200*=    PROGRAM:  PPFCBUTW                                  =%      UCSD
000300*=    CHANGE #  ???         PROJ. REQUEST: PROD FIX       =%      UCSD
000400*=    NAME: G CHIU          MODIFICATION DATE:  12/02/00  =%      UCSD
000500*=                                                        =%      UCSD
000600*=    DESCRIPTION                                         =%      UCSD
000700*=    FIXED DUPLICATE ROW PROBLEM TEMPORARILY             =%      UCSD
000800*=                                                        =%      UCSD
008000*===========================================================%     UCSD
000000**************************************************************/   54541281
000001*  PROGRAM: PPFCBUTW                                         */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ CREATION DATE:      ___06/22/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT FCB ROWS.     */   54541281
000008**************************************************************/   54541281
000800 IDENTIFICATION DIVISION.                                         PPFCBUTW
000900 PROGRAM-ID. PPFCBUTW                                             PPFCBUTW
001000 AUTHOR. UCOP.                                                    PPFCBUTW
001100 DATE-WRITTEN.  JUNE, 1999.                                       PPFCBUTW
001200 DATE-COMPILED.                                                   PPFCBUTW
001300*REMARKS.                                                         PPFCBUTW
001400*            CALL 'PPFCBUTW' USING                                PPFCBUTW
001500*                            PPXXXUTW-INTERFACE                   PPFCBUTW
001600*                            FCB-ROW.                             PPFCBUTW
001700*                                                                 PPFCBUTW
001800*                                                                 PPFCBUTW
001900     EJECT                                                        PPFCBUTW
002000 ENVIRONMENT DIVISION.                                            PPFCBUTW
002100 CONFIGURATION SECTION.                                           PPFCBUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPFCBUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPFCBUTW
002400      EJECT                                                       PPFCBUTW
002500******************************************************************PPFCBUTW
002600*                                                                 PPFCBUTW
002700*                D A T A  D I V I S I O N                         PPFCBUTW
002800*                                                                 PPFCBUTW
002900******************************************************************PPFCBUTW
003000 DATA DIVISION.                                                   PPFCBUTW
003100 WORKING-STORAGE SECTION.                                         PPFCBUTW
003200 01  MISCELLANEOUS-WS.                                            PPFCBUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPFCBUTW'.         PPFCBUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPFCBUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPFCBUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPFCBUTW
003700     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPFCBUTW
003800         88  FIRST-TIME                VALUE 'Y'.                 PPFCBUTW
003900         88  NOT-FIRST-TIME            VALUE 'N'.                 PPFCBUTW
004000     EJECT                                                        PPFCBUTW
004100 01  XWHC-COMPILE-WORK-AREA.                                      PPFCBUTW
004200                                    COPY CPWSXWHC.                PPFCBUTW
004300     EJECT                                                        PPFCBUTW
004400 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPFCBUTW
004500******************************************************************PPFCBUTW
004600*          SQL - WORKING STORAGE                                 *PPFCBUTW
004700******************************************************************PPFCBUTW
004800 01  FCB-ROW-SELECT.                                              UCSDXXXX
005001     10 XFCB-EMPLOYEE-ID          PIC X(9).                       PPPVFCB1
005002     10 XFCB-BENEFIT-TYPE     PIC X(1).                           PPPVFCB1
005003     10 XFCB-COVEFF-DATE      PIC X(10).                          PPPVFCB1
005004     10 XFCB-COV-END-DATE     PIC X(10).                          PPPVFCB1
005005     10 XFCB-COVERAGE-CODE    PIC X(3).                           PPPVFCB1
005006     10 XFCB-ENRL-REAS-CODE   PIC X(2).                           PPPVFCB1
005007     10 XFCB-PLAN-INFO-DATA   PIC X(6).                           PPPVFCB1
005008     10 XFCB-ADC-CODE         PIC X(1).                           PPPVFCB1
005009******************************************************************PPPVFCB1
005010* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *PPPVFCB1
005011******************************************************************PPPVFCB1
005012     EXEC SQL DECLARE PPPVFCB1_FCB TABLE                          PPPVFCB1
005013     ( EMPLOYEE_ID                    CHAR(9)  NOT NULL,          PPPVFCB1
005014       FCB_BENEFIT_TYPE               CHAR(1)  NOT NULL,          PPPVFCB1
005015       FCB_COVEFF_DATE                CHAR(10) NOT NULL,          PPPVFCB1
005016       FCB_COV_END_DATE               CHAR(10) NOT NULL,          PPPVFCB1
005017       FCB_COVERAGE_CODE              CHAR(3)  NOT NULL,          PPPVFCB1
005018       FCB_ENRL_REAS_CODE             CHAR(2)  NOT NULL,          PPPVFCB1
005019       FCB_PLAN_INFO_DATA             CHAR(6)  NOT NULL,          PPPVFCB1
005020       FCB_ADC_CODE                   CHAR(1)  NOT NULL           PPPVFCB1
005021     ) END-EXEC.                                                  PPPVFCB1
004800 01  FCB-ROW-DATA.                                                PPFCBUTW
004900     EXEC SQL                                                     PPFCBUTW
005000          INCLUDE PPPVFCB2                                        PPFCBUTW
005100     END-EXEC.                                                    PPFCBUTW
005200     EJECT                                                        PPFCBUTW
005300     EXEC SQL                                                     PPFCBUTW
005400          INCLUDE SQLCA                                           PPFCBUTW
005500     END-EXEC.                                                    PPFCBUTW
005600******************************************************************PPFCBUTW
005700*                                                                *PPFCBUTW
005800*                SQL- SELECTS                                    *PPFCBUTW
005900*                                                                *PPFCBUTW
006000******************************************************************PPFCBUTW
006100*                                                                *PPFCBUTW
006200*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPFCBUTW
006300*                                                                *PPFCBUTW
006400*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPFCBUTW
006500*                        MEMBER NAMES                            *PPFCBUTW
006600*                                                                *PPFCBUTW
006700*  PPPVFCB2_FCB          PPPVFCB2                                *PPFCBUTW
006800*                                                                *PPFCBUTW
006900******************************************************************PPFCBUTW
007000*                                                                 PPFCBUTW
007100 LINKAGE SECTION.                                                 PPFCBUTW
007200*                                                                 PPFCBUTW
007300******************************************************************PPFCBUTW
007400*                L I N K A G E   S E C T I O N                   *PPFCBUTW
007500******************************************************************PPFCBUTW
007600 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPFCBUTW
007700 01  FCB-ROW.                       COPY CPWSRFCB.                PPFCBUTW
007800*                                                                 PPFCBUTW
007900 PROCEDURE DIVISION USING                                         PPFCBUTW
008000                          PPXXXUTW-INTERFACE                      PPFCBUTW
008100                          FCB-ROW.                                PPFCBUTW
008200******************************************************************PPFCBUTW
008300*            P R O C E D U R E  D I V I S I O N                  *PPFCBUTW
008400******************************************************************PPFCBUTW
008500******************************************************************PPFCBUTW
008600*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPFCBUTW
008700*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPFCBUTW
008800******************************************************************PPFCBUTW
008900     EXEC SQL                                                     PPFCBUTW
009000          INCLUDE CPPDXE99                                        PPFCBUTW
009100     END-EXEC.                                                    PPFCBUTW
009200     SKIP3                                                        PPFCBUTW
009300 0100-MAINLINE SECTION.                                           PPFCBUTW
009400     SKIP1                                                        PPFCBUTW
009500     IF FIRST-TIME                                                PPFCBUTW
009600         SET NOT-FIRST-TIME TO TRUE                               PPFCBUTW
009700         PERFORM 7900-FIRST-TIME                                  PPFCBUTW
009800     END-IF.                                                      PPFCBUTW
009900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPFCBUTW
010000     MOVE EMPLOYEE-ID OF FCB-ROW TO WS-EMPLOYEE-ID.               PPFCBUTW
010100     SKIP1                                                        PPFCBUTW
010200     MOVE FCB-ROW TO FCB-ROW-DATA                                 PPFCBUTW
010300     SKIP1                                                        PPFCBUTW
010400     EVALUATE PPXXXUTW-FUNCTION                                   PPFCBUTW
010500         WHEN 'D'                                                 PPFCBUTW
010600             PERFORM 8100-DELETE-ROW                              PPFCBUTW
010700         WHEN 'U'                                                 PPFCBUTW
010800             PERFORM 8200-UPDATE-ROW                              PPFCBUTW
010900         WHEN 'I'                                                 PPFCBUTW
011400             PERFORM 8310-SELECT-ROW                              UCSDXXXX
013300             IF SQLCODE = 0                                       UCSDXXXX
013400                 PERFORM 8200-UPDATE-ROW                          UCSDXXXX
013500             ELSE                                                 UCSDXXXX
013600                 PERFORM 8300-INSERT-ROW                          PPFCBUTW
013700             END-IF                                               UCSDXXXX
011100         WHEN OTHER                                               PPFCBUTW
011200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPFCBUTW
011300             SET PPXXXUTW-ERROR            TO TRUE                PPFCBUTW
011400     END-EVALUATE.                                                PPFCBUTW
011500     SKIP1                                                        PPFCBUTW
011600     GOBACK.                                                      PPFCBUTW
011700     EJECT                                                        PPFCBUTW
011800******************************************************************PPFCBUTW
011900*  PROCEDURE SQL                     FOR FCB VIEW                *PPFCBUTW
012000******************************************************************PPFCBUTW
012100 8100-DELETE-ROW SECTION.                                         PPFCBUTW
012200     SKIP1                                                        PPFCBUTW
012300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPFCBUTW
012400     SKIP1                                                        PPFCBUTW
012500     EXEC SQL                                                     PPFCBUTW
012600         DELETE FROM PPPVFCB2_FCB                                 PPFCBUTW
012700          WHERE   EMPLOYEE_ID        = :WS-EMPLOYEE-ID            PPFCBUTW
012800            AND   FCB_BENEFIT_TYPE   = :FCB-BENEFIT-TYPE          PPFCBUTW
013000            AND   FCB_COVEFF_DATE    = :FCB-COVEFF-DATE           PPFCBUTW
013100     END-EXEC.                                                    PPFCBUTW
013200     SKIP1                                                        PPFCBUTW
013300     IF  SQLCODE    NOT = 0                                       PPFCBUTW
013400         SET PPXXXUTW-ERROR TO TRUE                               PPFCBUTW
013500     END-IF.                                                      PPFCBUTW
013600     EJECT                                                        PPFCBUTW
013700 8200-UPDATE-ROW SECTION.                                         PPFCBUTW
013800     SKIP1                                                        PPFCBUTW
013900     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPFCBUTW
014000     SKIP1                                                        PPFCBUTW
014100     EXEC SQL                                                     PPFCBUTW
014200     UPDATE PPPVFCB2_FCB                                          PPFCBUTW
014300       SET                                                        PPFCBUTW
014400       FCB_ADC_CODE                  = :FCB-ADC-CODE              PPFCBUTW
014500     , FCB_COV_END_DATE              = :FCB-COV-END-DATE          PPFCBUTW
014510     , FCB_COVERAGE_CODE             = :FCB-COVERAGE-CODE         PPFCBUTW
014520     , FCB_ENRL_REAS_CODE            = :FCB-ENRL-REAS-CODE        PPFCBUTW
014600     , FCB_PLAN_INFO_DATA            = :FCB-PLAN-INFO-DATA        PPFCBUTW
014800      WHERE       EMPLOYEE_ID        = :WS-EMPLOYEE-ID            PPFCBUTW
014900        AND       FCB_BENEFIT_TYPE   = :FCB-BENEFIT-TYPE          PPFCBUTW
015100        AND       FCB_COVEFF_DATE    = :FCB-COVEFF-DATE           PPFCBUTW
015200     END-EXEC.                                                    PPFCBUTW
015300     SKIP1                                                        PPFCBUTW
015400     IF  SQLCODE    NOT = 0                                       PPFCBUTW
015500         SET PPXXXUTW-ERROR TO TRUE                               PPFCBUTW
015600     END-IF.                                                      PPFCBUTW
015700     EJECT                                                        PPFCBUTW
016200 8310-SELECT-ROW SECTION.                                         UCSDXXXX
016300                                                                  UCSDXXXX
016400     MOVE '8310-SELECT-ROW'     TO DB2MSG-TAG.                    UCSDXXXX
016500     SKIP1                                                        UCSDXXXX
016600     EXEC SQL                                                     UCSDXXXX
016700     SELECT                                                       UCSDXXXX
016701       PPPVFCB1_FCB.EMPLOYEE_ID                                   UCSDXXXX
016702      ,PPPVFCB1_FCB.FCB_BENEFIT_TYPE                              UCSDXXXX
016703      ,PPPVFCB1_FCB.FCB_COVEFF_DATE                               UCSDXXXX
016706     INTO   :XFCB-EMPLOYEE-ID                                     UCSDXXXX
016707           ,:XFCB-BENEFIT-TYPE                                    UCSDXXXX
016708           ,:XFCB-COVEFF-DATE                                     UCSDXXXX
016709     FROM PPPVFCB1_FCB                                            UCSDXXXX
016800     WHERE PPPVFCB1_FCB.EMPLOYEE_ID = :EMPLOYEE-ID AND            UCSDXXXX
016900     PPPVFCB1_FCB.FCB_BENEFIT_TYPE =  :FCB-BENEFIT-TYPE AND       UCSDXXXX
017000     PPPVFCB1_FCB.FCB_COVEFF_DATE  =  :FCB-COVEFF-DATE            UCSDXXXX
017200     END-EXEC.                                                    UCSDXXXX
017300 SKIP1                                                            UCSDXXXX
017400       IF  SQLCODE    NOT = 0                                     UCSDXXXX
017500            NEXT SENTENCE                                         UCSDXXXX
017600       ELSE                                                       UCSDXXXX
017700            DISPLAY FCB-ROW-DATA.                                 UCSDXXXX
017800                                                                  UCSDXXXX
015800 8300-INSERT-ROW SECTION.                                         PPFCBUTW
015900                                                                  PPFCBUTW
016000     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPFCBUTW
016100     SKIP1                                                        PPFCBUTW
016200     EXEC SQL                                                     PPFCBUTW
016300         INSERT INTO PPPVFCB2_FCB                                 PPFCBUTW
016310                (                                                 PPFCBUTW
016320                 EMPLOYEE_ID                                      PPFCBUTW
016330                ,FCB_BENEFIT_TYPE                                 PPFCBUTW
016340                ,FCB_COVEFF_DATE                                  PPFCBUTW
016350                ,FCB_COV_END_DATE                                 PPFCBUTW
016360                ,FCB_COVERAGE_CODE                                PPFCBUTW
016370                ,FCB_ENRL_REAS_CODE                               PPFCBUTW
016380                ,FCB_PLAN_INFO_DATA                               PPFCBUTW
016390                ,FCB_ADC_CODE                                     PPFCBUTW
016391                )                                                 PPFCBUTW
016400                VALUES                                            PPFCBUTW
016410                (:EMPLOYEE-ID                                     PPFCBUTW
016420                ,:FCB-BENEFIT-TYPE                                PPFCBUTW
016430                ,:FCB-COVEFF-DATE                                 PPFCBUTW
016440                ,:FCB-COV-END-DATE                                PPFCBUTW
016450                ,:FCB-COVERAGE-CODE                               PPFCBUTW
016460                ,:FCB-ENRL-REAS-CODE                              PPFCBUTW
016470                ,:FCB-PLAN-INFO-DATA                              PPFCBUTW
016480                ,:FCB-ADC-CODE                                    PPFCBUTW
016490                )                                                 PPFCBUTW
016500     END-EXEC.                                                    PPFCBUTW
016600     SKIP1                                                        PPFCBUTW
016700     IF  SQLCODE    NOT = 0                                       PPFCBUTW
016800         SET PPXXXUTW-ERROR TO TRUE                               PPFCBUTW
016900     END-IF.                                                      PPFCBUTW
017000     EJECT                                                        PPFCBUTW
017100 7900-FIRST-TIME SECTION.                                         PPFCBUTW
017200     SKIP1                                                        PPFCBUTW
017300     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPFCBUTW
017400     SKIP1                                                        PPFCBUTW
017500                                    COPY CPPDXWHC.                PPFCBUTW
017600     EJECT                                                        PPFCBUTW
017700*999999-SQL-ERROR.                                               *PPFCBUTW
017800     SKIP1                                                        PPFCBUTW
017900     EXEC SQL                                                     PPFCBUTW
018000          INCLUDE CPPDXP99                                        PPFCBUTW
018100     END-EXEC.                                                    PPFCBUTW
018200     SKIP1                                                        PPFCBUTW
018300     SET PPXXXUTW-ERROR TO TRUE.                                  PPFCBUTW
018400     IF NOT HERE-BEFORE                                           PPFCBUTW
018500         SET HERE-BEFORE TO TRUE                                  PPFCBUTW
018600     END-IF.                                                      PPFCBUTW
018700     GOBACK.                                                      PPFCBUTW
