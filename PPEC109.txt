000000**************************************************************/   28521025
000001*  PROGRAM: PPEC109                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/25/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEC109                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    CORPORATE CREDIT CARD CONSISTENCY EDITS                 *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEC109
001200 IDENTIFICATION DIVISION.                                         PPEC109
001300 PROGRAM-ID. PPEC109.                                             PPEC109
001400 AUTHOR. UCOP.                                                    PPEC109
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEC109
001600                                                                  PPEC109
001700 DATE-WRITTEN.  AUGUST 04,1992.                                   PPEC109
001800 DATE-COMPILED.                                                   PPEC109
001900                                                                  PPEC109
002000******************************************************************PPEC109
002100**    THIS ROUTINE WILL PERFORM THE CORPORATE CREDIT CARD       **PPEC109
002200**    CONSISTENCY EDITS                                         **PPEC109
002300******************************************************************PPEC109
002400                                                                  PPEC109
002500 ENVIRONMENT DIVISION.                                            PPEC109
002600 CONFIGURATION SECTION.                                           PPEC109
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEC109
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEC109
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEC109
003000                                                                  PPEC109
003100 INPUT-OUTPUT SECTION.                                            PPEC109
003200 FILE-CONTROL.                                                    PPEC109
003300                                                                  PPEC109
003400 DATA DIVISION.                                                   PPEC109
003500 FILE SECTION.                                                    PPEC109
003600     EJECT                                                        PPEC109
003700*----------------------------------------------------------------*PPEC109
003800 WORKING-STORAGE SECTION.                                         PPEC109
003900*----------------------------------------------------------------*PPEC109
004000                                                                  PPEC109
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEC109
004200*****'PPEC109 /111992'.                                           28521025
004210     'PPEC109 /080195'.                                           28521025
004300                                                                  PPEC109
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEC109
004500     A-STND-PROG-ID.                                              PPEC109
004600     05  FILLER                       PIC X(03).                  PPEC109
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEC109
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEC109
004900     05  FILLER                       PIC X(08).                  PPEC109
005000                                                                  PPEC109
005100 01  MESSAGES-USED.                                               PPEC109
005200     05  M08915                       PIC X(05) VALUE '08915'.    PPEC109
005300     05  M12903                       PIC X(05) VALUE '12903'.    PPEC109
005400                                                                  PPEC109
005500 01  ELEMENTS-USED.                                               PPEC109
005600     05  E0133                        PIC 9(04) VALUE 0133.       PPEC109
005700     05  E0168                        PIC 9(04) VALUE 0168.       PPEC109
005800     05  E0169                        PIC 9(04) VALUE 0169.       PPEC109
005900                                                                  PPEC109
006000 01  MISC-WORK-AREAS.                                             PPEC109
006100     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEC109
006200         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEC109
006300         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEC109
006400                                                                  PPEC109
006500     EJECT                                                        PPEC109
006600 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEC109
006700                                                                  PPEC109
006800     EJECT                                                        PPEC109
006900*01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. 28521025
006910 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. 28521025
007000                                                                  PPEC109
007100     EJECT                                                        PPEC109
007200 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEC109
007300                                                                  PPEC109
007400     EJECT                                                        PPEC109
007500 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEC109
007600                                                                  PPEC109
007700     EJECT                                                        PPEC109
007800*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
007900*****                                                             28521025
008000*****EJECT                                                        28521025
008100 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEC109
008200                                                                  PPEC109
008300     EJECT                                                        PPEC109
008400 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEC109
008500                                                                  PPEC109
008600     EJECT                                                        PPEC109
008700******************************************************************PPEC109
008800**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEC109
008900**--------------------------------------------------------------**PPEC109
009000** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEC109
009100******************************************************************PPEC109
009200                                                                  PPEC109
009300 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEC109
009400                                                                  PPEC109
009500     EJECT                                                        PPEC109
009600 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEC109
009700                                                                  PPEC109
009800     EJECT                                                        PPEC109
009900 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEC109
010000                                                                  PPEC109
010100     EJECT                                                        PPEC109
010200 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEC109
010300                                                                  PPEC109
010400 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEC109
010500                                                                  PPEC109
010600 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEC109
010700                                                                  PPEC109
010800 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEC109
010900                                                                  PPEC109
011000     EJECT                                                        PPEC109
011100 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEC109
011200                                                                  PPEC109
011300     EJECT                                                        PPEC109
011400 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEC109
011500                                                                  PPEC109
011600     EJECT                                                        PPEC109
011700 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEC109
011800                                                                  PPEC109
011900     EJECT                                                        PPEC109
012000******************************************************************PPEC109
012100**  DATA BASE AREAS                                             **PPEC109
012200******************************************************************PPEC109
012300                                                                  PPEC109
012400 01  SCR-ROW                             EXTERNAL.                PPEC109
012500     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEC109
012600                                                                  PPEC109
012700     EJECT                                                        PPEC109
012800 01  PCC-ROW                             EXTERNAL.                PPEC109
012900     EXEC SQL INCLUDE PPPVPCC1 END-EXEC.                          PPEC109
013000                                                                  PPEC109
013100     EJECT                                                        PPEC109
013200******************************************************************PPEC109
013300**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEC109
013400******************************************************************PPEC109
013500                                                                  PPEC109
013600*----------------------------------------------------------------*PPEC109
013700 LINKAGE SECTION.                                                 PPEC109
013800*----------------------------------------------------------------*PPEC109
013900                                                                  PPEC109
014000******************************************************************PPEC109
014100**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEC109
014200**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEC109
014300******************************************************************PPEC109
014400                                                                  PPEC109
014500 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEC109
014600                                                                  PPEC109
014700     EJECT                                                        PPEC109
014800******************************************************************PPEC109
014900**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEC109
015000**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEC109
015100******************************************************************PPEC109
015200                                                                  PPEC109
015300 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEC109
015400                                                                  PPEC109
015500     EJECT                                                        PPEC109
015600******************************************************************PPEC109
015700**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEC109
015800******************************************************************PPEC109
015900                                                                  PPEC109
016000 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEC109
016100                          KMTA-MESSAGE-TABLE-ARRAY.               PPEC109
016200                                                                  PPEC109
016300                                                                  PPEC109
016400*----------------------------------------------------------------*PPEC109
016500 0000-DRIVER.                                                     PPEC109
016600*----------------------------------------------------------------*PPEC109
016700                                                                  PPEC109
016800******************************************************************PPEC109
016900**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEC109
017000******************************************************************PPEC109
017100                                                                  PPEC109
017200     IF  THIS-IS-THE-FIRST-TIME                                   PPEC109
017300         PERFORM 0100-INITIALIZE                                  PPEC109
017400     END-IF.                                                      PPEC109
017500                                                                  PPEC109
017600******************************************************************PPEC109
017700**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEC109
017800**  TO THE CALLING PROGRAM                                      **PPEC109
017900******************************************************************PPEC109
018000                                                                  PPEC109
018100     PERFORM 1000-MAINLINE-ROUTINE.                               PPEC109
018200                                                                  PPEC109
018300     EXIT PROGRAM.                                                PPEC109
018400                                                                  PPEC109
018500                                                                  PPEC109
018600     EJECT                                                        PPEC109
018700*----------------------------------------------------------------*PPEC109
018800 0100-INITIALIZE    SECTION.                                      PPEC109
018900*----------------------------------------------------------------*PPEC109
019000                                                                  PPEC109
019100******************************************************************PPEC109
019200**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEC109
019300******************************************************************PPEC109
019400                                                                  PPEC109
019500     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEC109
019600                                                                  PPEC109
019700*                                               *-------------*   PPEC109
019800                                                 COPY CPPDXWHC.   PPEC109
019900*                                               *-------------*   PPEC109
020000                                                                  PPEC109
020100*****                                           *-------------*   28521025
020200*****                                            COPY CPPDDATE.   28521025
020300*****                                           *-------------*   28521025
020400*****                                                             28521025
020500*****MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 28521025
020600                                                                  28521025
020700******************************************************************PPEC109
020800**   SET FIRST CALL SWITCH OFF                                  **PPEC109
020900******************************************************************PPEC109
021000                                                                  PPEC109
021100     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEC109
021200                                                                  PPEC109
021300     EJECT                                                        PPEC109
021400*----------------------------------------------------------------*PPEC109
021500 1000-MAINLINE-ROUTINE SECTION.                                   PPEC109
021600*----------------------------------------------------------------*PPEC109
021700                                                                  PPEC109
021800     IF  KRMI-REQUESTED-BY-08                                     PPEC109
021900         PERFORM 1100-CONEDITS-MAINLINE                           PPEC109
022000     ELSE                                                         PPEC109
022100         PERFORM 5100-IMPLIED-MAINT-MAINLINE                      PPEC109
022200     END-IF.                                                      PPEC109
022300                                                                  PPEC109
022400                                                                  PPEC109
022500     EJECT                                                        PPEC109
022600******************************************************************PPEC109
022700**     C O N S I S T E N C Y    E D I T S   M A I N L I N E     **PPEC109
022800******************************************************************PPEC109
022900*----------------------------------------------------------------*PPEC109
023000 1100-CONEDITS-MAINLINE SECTION.                                  PPEC109
023100*----------------------------------------------------------------*PPEC109
023200                                                                  PPEC109
023300     IF          CREDIT-CARDTYPE       = SPACES                   PPEC109
023400         AND (   CREDIT-CARDSTATUS NOT = SPACES                   PPEC109
023500*****         OR CREDIT-CARDDATE   NOT = ISO-ZERO-DATE)           28521025
023510              OR CREDIT-CARDDATE   NOT = XDTS-ISO-ZERO-DATE)      28521025
023600         ADD 1                  TO KMTA-CTR                       PPEC109
023700         MOVE E0133             TO KMTA-FIELD-NUM       (KMTA-CTR)PPEC109
023800         MOVE CREDIT-CARDTYPE   TO KMTA-DATA-FIELD      (KMTA-CTR)PPEC109
023900         MOVE 1                 TO KMTA-LENGTH          (KMTA-CTR)PPEC109
024000         MOVE M08915            TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC109
024100         MOVE WS-ROUTINE-TYPE   TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC109
024200         MOVE WS-ROUTINE-NUM    TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC109
024300                                                                  PPEC109
024400         ADD 1                  TO KMTA-CTR                       PPEC109
024500         MOVE E0168             TO KMTA-FIELD-NUM       (KMTA-CTR)PPEC109
024600         MOVE CREDIT-CARDSTATUS TO KMTA-DATA-FIELD      (KMTA-CTR)PPEC109
024700         MOVE 1                 TO KMTA-LENGTH          (KMTA-CTR)PPEC109
024800         MOVE M08915            TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC109
024900         MOVE WS-ROUTINE-TYPE   TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC109
025000         MOVE WS-ROUTINE-NUM    TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC109
025100                                                                  PPEC109
025200         ADD 1                  TO KMTA-CTR                       PPEC109
025300         MOVE E0169             TO KMTA-FIELD-NUM       (KMTA-CTR)PPEC109
025400*****    MOVE CREDIT-CARDDATE   TO DB2-DATE                       28521025
025500*****    PERFORM CONVERT-DB2-DATE                                 28521025
025600*****    MOVE WSX-STD-DATE      TO KMTA-DATA-FIELD      (KMTA-CTR)28521025
025610         MOVE CREDIT-CARDDATE   TO XDC3-ISO-DATE                  28521025
025620         PERFORM XDC3-CONVERT-ISO-TO-STD                          28521025
025630         MOVE XDC3-STD-DATE     TO KMTA-DATA-FIELD      (KMTA-CTR)28521025
025700         MOVE 6                 TO KMTA-LENGTH          (KMTA-CTR)PPEC109
025800         MOVE M08915            TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC109
025900         MOVE WS-ROUTINE-TYPE   TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC109
026000         MOVE WS-ROUTINE-NUM    TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC109
026100     END-IF.                                                      PPEC109
026200                                                                  PPEC109
026300                                                                  PPEC109
026400     EJECT                                                        PPEC109
026500******************************************************************PPEC109
026600**   I M P L I E D   M A I N T E N A N C E   M A I N L I N E    **PPEC109
026700******************************************************************PPEC109
026800*----------------------------------------------------------------*PPEC109
026900 5100-IMPLIED-MAINT-MAINLINE SECTION.                             PPEC109
027000*----------------------------------------------------------------*PPEC109
027100     IF          CREDIT-CARDTYPE       = SPACE                    PPEC109
027200         AND (   CREDIT-CARDSTATUS NOT = SPACE                    PPEC109
027300*****         OR CREDIT-CARDDATE   NOT = ISO-ZERO-DATE)           28521025
027310              OR CREDIT-CARDDATE   NOT = XDTS-ISO-ZERO-DATE)      28521025
027400                                                                  PPEC109
027500         ADD 1                    TO KMTA-CTR                     PPEC109
027600         MOVE ZEROS               TO KMTA-ADA-OFFSET    (KMTA-CTR)PPEC109
027700         MOVE CREDIT-CARDTYPE     TO KMTA-DATA-FIELD    (KMTA-CTR)PPEC109
027800         MOVE WS-ROUTINE-TYPE     TO KMTA-ROUTINE-TYPE  (KMTA-CTR)PPEC109
027900         MOVE WS-ROUTINE-NUM      TO KMTA-ROUTINE-NUM   (KMTA-CTR)PPEC109
028000         MOVE 01                  TO KMTA-LENGTH        (KMTA-CTR)PPEC109
028100         MOVE M12903              TO KMTA-MSG-NUMBER    (KMTA-CTR)PPEC109
028200         MOVE E0133               TO KMTA-FIELD-NUM     (KMTA-CTR)PPEC109
028300                                                                  PPEC109
028400         ADD 1                    TO KMTA-CTR                     PPEC109
028500         MOVE M12903              TO KMTA-MSG-NUMBER    (KMTA-CTR)PPEC109
028600         MOVE E0168               TO KMTA-FIELD-NUM     (KMTA-CTR)PPEC109
028700         MOVE ZEROS               TO KMTA-ADA-OFFSET    (KMTA-CTR)PPEC109
028800         MOVE CREDIT-CARDSTATUS   TO KMTA-DATA-FIELD    (KMTA-CTR)PPEC109
028900         MOVE WS-ROUTINE-TYPE     TO KMTA-ROUTINE-TYPE  (KMTA-CTR)PPEC109
029000         MOVE WS-ROUTINE-NUM      TO KMTA-ROUTINE-NUM   (KMTA-CTR)PPEC109
029100         MOVE 01                  TO KMTA-LENGTH        (KMTA-CTR)PPEC109
029200                                                                  PPEC109
029300         ADD 1                    TO KMTA-CTR                     PPEC109
029400         MOVE M12903              TO KMTA-MSG-NUMBER    (KMTA-CTR)PPEC109
029500         MOVE E0169               TO KMTA-FIELD-NUM     (KMTA-CTR)PPEC109
029600         MOVE ZEROS               TO KMTA-ADA-OFFSET    (KMTA-CTR)PPEC109
029700*****    MOVE CREDIT-CARDDATE     TO DB2-DATE                     28521025
029800*****    PERFORM CONVERT-DB2-DATE                                 28521025
029900*****    MOVE WSX-STD-DATE        TO KMTA-DATA-FIELD    (KMTA-CTR)28521025
029910         MOVE CREDIT-CARDDATE     TO XDC3-ISO-DATE                28521025
029920         PERFORM XDC3-CONVERT-ISO-TO-STD                          28521025
029930         MOVE XDC3-STD-DATE       TO KMTA-DATA-FIELD    (KMTA-CTR)28521025
030000         MOVE WS-ROUTINE-TYPE     TO KMTA-ROUTINE-TYPE  (KMTA-CTR)PPEC109
030100         MOVE WS-ROUTINE-NUM      TO KMTA-ROUTINE-NUM   (KMTA-CTR)PPEC109
030200         MOVE 06                  TO KMTA-LENGTH        (KMTA-CTR)PPEC109
030300     END-IF.                                                      PPEC109
030400                                                                  PPEC109
030500                                                                  PPEC109
030600                                                                  PPEC109
030700     EJECT                                                        PPEC109
030800*----------------------------------------------------------------*PPEC109
030900 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEC109
031000*----------------------------------------------------------------*PPEC109
031100                                                                  PPEC109
031200*                                                 *-------------* PPEC109
031300                                                   COPY CPPDXDEC. PPEC109
031400*                                                 *-------------* PPEC109
031500                                                                  PPEC109
031600*----------------------------------------------------------------*PPEC109
031700 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEC109
031800*----------------------------------------------------------------*PPEC109
031900                                                                  PPEC109
032000*                                                 *-------------* PPEC109
032100                                                   COPY CPPDADLC. PPEC109
032200*                                                 *-------------* PPEC109
032300                                                                  PPEC109
032400     EJECT                                                        PPEC109
032500*----------------------------------------------------------------*PPEC109
032600 9300-DATE-CONVERSION-DB2  SECTION.                               PPEC109
032700*----------------------------------------------------------------*PPEC109
032800                                                                  PPEC109
032900*                                                 *-------------* PPEC109
033000*****                                              COPY CPPDXDC2. 28521025
033010                                                   COPY CPPDXDC3. 28521025
033100*                                                 *-------------* PPEC109
033200                                                                  PPEC109
033300******************************************************************PPEC109
033400**   E N D  S O U R C E   ----  PPEC109 ----                    **PPEC109
033500******************************************************************PPEC109
