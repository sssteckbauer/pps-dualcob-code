000100**************************************************************/  *36090513
000200*  PROGRAM: PPDBLUTW                                         */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE:      __11/05/90__   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT DBL ROWS.     */  *36090513
000700**************************************************************/  *36090513
000800 IDENTIFICATION DIVISION.                                         PPDBLUTW
000900 PROGRAM-ID. PPDBLUTW                                             PPDBLUTW
001000 AUTHOR. UCOP.                                                    PPDBLUTW
001100 DATE-WRITTEN.  SEPTEMBER 1990.                                   PPDBLUTW
001200 DATE-COMPILED.                                                   PPDBLUTW
001300*REMARKS.                                                         PPDBLUTW
001400*            CALL 'PPDBLUTW' USING                                PPDBLUTW
001500*                            PPXXXUTW-INTERFACE                   PPDBLUTW
001600*                            DBL-ROW.                             PPDBLUTW
001700*                                                                 PPDBLUTW
001800*                                                                 PPDBLUTW
001900     EJECT                                                        PPDBLUTW
002000 ENVIRONMENT DIVISION.                                            PPDBLUTW
002100 CONFIGURATION SECTION.                                           PPDBLUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPDBLUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPDBLUTW
002400      EJECT                                                       PPDBLUTW
002500******************************************************************PPDBLUTW
002600*                                                                 PPDBLUTW
002700*                D A T A  D I V I S I O N                         PPDBLUTW
002800*                                                                 PPDBLUTW
002900******************************************************************PPDBLUTW
003000 DATA DIVISION.                                                   PPDBLUTW
003100 WORKING-STORAGE SECTION.                                         PPDBLUTW
003200 01  MISCELLANEOUS-WS.                                            PPDBLUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPDBLUTW'.         PPDBLUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPDBLUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPDBLUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPDBLUTW
003700     EJECT                                                        PPDBLUTW
003800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPDBLUTW
003900******************************************************************PPDBLUTW
004000*          SQL - WORKING STORAGE                                 *PPDBLUTW
004100******************************************************************PPDBLUTW
004200 01  DBL-ROW-DATA.                                                PPDBLUTW
004300     EXEC SQL                                                     PPDBLUTW
004400          INCLUDE PPPVDBL2                                        PPDBLUTW
004500     END-EXEC.                                                    PPDBLUTW
004600     EJECT                                                        PPDBLUTW
004700     EXEC SQL                                                     PPDBLUTW
004800          INCLUDE SQLCA                                           PPDBLUTW
004900     END-EXEC.                                                    PPDBLUTW
005000******************************************************************PPDBLUTW
005100*                                                                *PPDBLUTW
005200*                SQL- SELECTS                                    *PPDBLUTW
005300*                                                                *PPDBLUTW
005400******************************************************************PPDBLUTW
005500*                                                                *PPDBLUTW
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPDBLUTW
005700*                                                                *PPDBLUTW
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPDBLUTW
005900*                        MEMBER NAMES                            *PPDBLUTW
006000*                                                                *PPDBLUTW
006100*  PPPVDBL2_DBL          PPPVDBL2                                *PPDBLUTW
006200*                                                                *PPDBLUTW
006300******************************************************************PPDBLUTW
006400*                                                                 PPDBLUTW
006500 LINKAGE SECTION.                                                 PPDBLUTW
006600*                                                                 PPDBLUTW
006700******************************************************************PPDBLUTW
006800*                L I N K A G E   S E C T I O N                   *PPDBLUTW
006900******************************************************************PPDBLUTW
007000 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPDBLUTW
007100 01  DBL-ROW.                       COPY CPWSRDBL.                PPDBLUTW
007200*                                                                 PPDBLUTW
007300 PROCEDURE DIVISION USING                                         PPDBLUTW
007400                          PPXXXUTW-INTERFACE                      PPDBLUTW
007500                          DBL-ROW.                                PPDBLUTW
007600******************************************************************PPDBLUTW
007700*            P R O C E D U R E  D I V I S I O N                  *PPDBLUTW
007800******************************************************************PPDBLUTW
007900******************************************************************PPDBLUTW
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPDBLUTW
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPDBLUTW
008200******************************************************************PPDBLUTW
008300     EXEC SQL                                                     PPDBLUTW
008400          INCLUDE CPPDXE99                                        PPDBLUTW
008500     END-EXEC.                                                    PPDBLUTW
008600     SKIP3                                                        PPDBLUTW
008700 0100-MAINLINE SECTION.                                           PPDBLUTW
008800     SKIP1                                                        PPDBLUTW
008900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPDBLUTW
009000     MOVE EMPLOYEE-ID OF DBL-ROW TO WS-EMPLOYEE-ID.               PPDBLUTW
009100     SKIP1                                                        PPDBLUTW
009200     MOVE DBL-ROW TO DBL-ROW-DATA                                 PPDBLUTW
009300     SKIP1                                                        PPDBLUTW
009400     EVALUATE PPXXXUTW-FUNCTION                                   PPDBLUTW
009500         WHEN 'D'                                                 PPDBLUTW
009600             PERFORM 8100-DELETE-ROW                              PPDBLUTW
009700         WHEN 'U'                                                 PPDBLUTW
009800             PERFORM 8200-UPDATE-ROW                              PPDBLUTW
009900         WHEN 'I'                                                 PPDBLUTW
010000             PERFORM 8300-INSERT-ROW                              PPDBLUTW
010100         WHEN OTHER                                               PPDBLUTW
010200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPDBLUTW
010300             SET PPXXXUTW-ERROR            TO TRUE                PPDBLUTW
010400     END-EVALUATE.                                                PPDBLUTW
010500     SKIP1                                                        PPDBLUTW
010600     GOBACK.                                                      PPDBLUTW
010700     EJECT                                                        PPDBLUTW
010800******************************************************************PPDBLUTW
010900*  PROCEDURE SQL                     FOR DBL VIEW                *PPDBLUTW
011000******************************************************************PPDBLUTW
011100 8100-DELETE-ROW SECTION.                                         PPDBLUTW
011200     SKIP1                                                        PPDBLUTW
011300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPDBLUTW
011400     SKIP1                                                        PPDBLUTW
011500     EXEC SQL                                                     PPDBLUTW
011600         DELETE FROM PPPVDBL2_DBL                                 PPDBLUTW
011700          WHERE EMPLOYEE_ID = :EMPLOYEE-ID                        PPDBLUTW
011800            AND GTN_NUMBER  = :GTN-NUMBER                         PPDBLUTW
011900            AND GTN_IND     = :GTN-IND                            PPDBLUTW
012000     END-EXEC.                                                    PPDBLUTW
012100     SKIP1                                                        PPDBLUTW
012200     IF  SQLCODE    NOT = 0                                       PPDBLUTW
012300         SET PPXXXUTW-ERROR TO TRUE                               PPDBLUTW
012400     END-IF.                                                      PPDBLUTW
012500     EJECT                                                        PPDBLUTW
012600 8200-UPDATE-ROW SECTION.                                         PPDBLUTW
012700     SKIP1                                                        PPDBLUTW
012800     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPDBLUTW
012900     SKIP1                                                        PPDBLUTW
013000     EXEC SQL                                                     PPDBLUTW
013100     UPDATE PPPVDBL2_DBL                                          PPDBLUTW
013200             SET  GTN_AMT            = :GTN-AMT                   PPDBLUTW
013300         WHERE EMPLOYEE_ID = :EMPLOYEE-ID                         PPDBLUTW
013400           AND GTN_NUMBER  = :GTN-NUMBER                          PPDBLUTW
013500           AND GTN_IND     = :GTN-IND                             PPDBLUTW
013600     END-EXEC.                                                    PPDBLUTW
013700     SKIP1                                                        PPDBLUTW
013800     IF  SQLCODE    NOT = 0                                       PPDBLUTW
013900         SET PPXXXUTW-ERROR TO TRUE                               PPDBLUTW
014000     END-IF.                                                      PPDBLUTW
014100     EJECT                                                        PPDBLUTW
014200 8300-INSERT-ROW SECTION.                                         PPDBLUTW
014300     SKIP1                                                        PPDBLUTW
014400     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPDBLUTW
014500     SKIP1                                                        PPDBLUTW
014600     EXEC SQL                                                     PPDBLUTW
014700         INSERT INTO PPPVDBL2_DBL                                 PPDBLUTW
014800                VALUES (:DBL-ROW-DATA)                            PPDBLUTW
014900     END-EXEC.                                                    PPDBLUTW
015000     SKIP1                                                        PPDBLUTW
015100     IF  SQLCODE    NOT = 0                                       PPDBLUTW
015200         SET PPXXXUTW-ERROR TO TRUE                               PPDBLUTW
015300     END-IF.                                                      PPDBLUTW
015400     EJECT                                                        PPDBLUTW
015500*999999-SQL-ERROR.                                               *PPDBLUTW
015600     SKIP1                                                        PPDBLUTW
015700     EXEC SQL                                                     PPDBLUTW
015800          INCLUDE CPPDXP99                                        PPDBLUTW
015900     END-EXEC.                                                    PPDBLUTW
016000     SKIP1                                                        PPDBLUTW
016100     SET PPXXXUTW-ERROR TO TRUE.                                  PPDBLUTW
016200     IF NOT HERE-BEFORE                                           PPDBLUTW
016300         SET HERE-BEFORE TO TRUE                                  PPDBLUTW
016400     GOBACK.                                                      PPDBLUTW
