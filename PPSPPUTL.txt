000100**************************************************************/  *36090574
000200*  PROGRAM:  PPSPPUTL                                        */  *36090574
000300*  RELEASE # ____0574____ SERVICE REQUEST NO(S)___3609_______*/  *36090574
000400*  NAME ______PXP______   MODIFICATION DATE:_____06/05/91____*/  *36090574
000500*  DESCRIPTION                                               */  *36090574
000600*    CHANGE ARRAY HANDLING                                   */  *36090574
000700**************************************************************/  *36090574
000701**************************************************************/  *40280572
000702*  PROGRAM: PPSPPUTL                                         */  *40280572
001000*  RELEASE: ____0574____  SERVICE REQUEST(S): ____4028____   */  *40280572
001001*  NAME:__PXP_____________CREATION DATE:      ___05/29/91_   */  *40280572
001002*  DESCRIPTION:                                              */  *40280572
001003*  - NEW PROGRAM TO ACCESS SPP TABLE                         */  *40280572
001004**************************************************************/  *40280572
001005 IDENTIFICATION DIVISION.                                         PPSPPUTL
001006 PROGRAM-ID. PPSPPUTL                                             PPSPPUTL
001007 AUTHOR. UCOP.                                                    PPSPPUTL
001100 DATE-WRITTEN.  MAY 1991.                                         PPSPPUTL
001200 DATE-COMPILED.                                                   PPSPPUTL
001300*REMARKS.                                                         PPSPPUTL
001400*            CALL 'PPSPPUTL' USING                                PPSPPUTL
001500*                            PPSPPUTL-INTERFACE.                  PPSPPUTL
001600*                                                                 PPSPPUTL
001700*                                                                 PPSPPUTL
001800     EJECT                                                        PPSPPUTL
001900 ENVIRONMENT DIVISION.                                            PPSPPUTL
002000 CONFIGURATION SECTION.                                           PPSPPUTL
002100 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPSPPUTL
002200 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPSPPUTL
002300      EJECT                                                       PPSPPUTL
002400*                D A T A  D I V I S I O N                         PPSPPUTL
002500*                                                                 PPSPPUTL
002600 DATA DIVISION.                                                   PPSPPUTL
002700 WORKING-STORAGE SECTION.                                         PPSPPUTL
002800 77  HERE-BEFORE-SW      PIC X(01) VALUE SPACE.                   PPSPPUTL
002900     88  HERE-BEFORE VALUE 'Y'.                                   PPSPPUTL
003000 77  SPP-ROW-OPEN-SW     PIC X(01) VALUE SPACE.                   PPSPPUTL
003100     88  SPP-ROW-OPEN VALUE 'Y'.                                  PPSPPUTL
003200 77  A-STND-PROG-ID      PIC X(08) VALUE 'PPSPPUTL'.              PPSPPUTL
003300 77  WS-SPP-SUB          PIC S9(02) COMP-3.                       PPSPPUTL
003400 77  WS-SPP-SUB2         PIC S9(02) COMP-3.                       PPSPPUTL
003500 01  WS-EMPLOYEE-ID      PIC X(9).                                PPSPPUTL
003600 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPSPPUTL
003700******************************************************************PPSPPUTL
003800*          SQL - WORKING STORAGE                                 *PPSPPUTL
003900******************************************************************PPSPPUTL
004000 01  SPP-ROW-DATA.                                                PPSPPUTL
004100     EXEC SQL                                                     PPSPPUTL
004200          INCLUDE PPPVSPP1                                        PPSPPUTL
004300     END-EXEC.                                                    PPSPPUTL
004400     EXEC SQL                                                     PPSPPUTL
004500          INCLUDE SQLCA                                           PPSPPUTL
004600     END-EXEC.                                                    PPSPPUTL
004700******************************************************************PPSPPUTL
004800*                                                                *PPSPPUTL
004900*                SQL- SELECTS                                    *PPSPPUTL
005000*                                                                *PPSPPUTL
005100******************************************************************PPSPPUTL
005200*                                                                *PPSPPUTL
005300*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPSPPUTL
005400*                                                                *PPSPPUTL
005500*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPSPPUTL
005600*                        MEMBER NAMES                            *PPSPPUTL
005700*                                                                *PPSPPUTL
005800*  PPPVSPP1_SPP          PPPVSPP1                                *PPSPPUTL
005900*                                                                *PPSPPUTL
006000******************************************************************PPSPPUTL
006100     EXEC SQL                                                     PPSPPUTL
006200          DECLARE SPP_ROW CURSOR FOR                              PPSPPUTL
006300              SELECT * FROM PPPVSPP1_SPP                          PPSPPUTL
006400              WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                 PPSPPUTL
006500              ORDER BY                                            PPSPPUTL
006600                  SPP_PROGRAM                                     PPSPPUTL
006700     END-EXEC.                                                    PPSPPUTL
006800*                                                                 PPSPPUTL
006900 LINKAGE SECTION.                                                 PPSPPUTL
007000*                                                                 PPSPPUTL
007100******************************************************************PPSPPUTL
007200*                L I N K A G E   S E C T I O N                   *PPSPPUTL
007300******************************************************************PPSPPUTL
007400 01  PPSPPUTL-INTERFACE.            COPY CPLNKSP2.                PPSPPUTL
008200 01  SPP-ARRAY.                                                   36090574
008300     02  SPP-ARRAY-ENTRY OCCURS 2 TIMES.                          36090574
008400                                    COPY CPWSRSPP.                36090574
008500*01**SPP-ARRAY.                     COPY CPWSXSP3.                36090574
008501*                                                                 PPSPPUTL
008502      EJECT                                                       PPSPPUTL
008503*                                                                 PPSPPUTL
008504     EJECT                                                        PPSPPUTL
008505*                                                                 PPSPPUTL
008506*                                                                 PPSPPUTL
008507 PROCEDURE DIVISION USING                                         PPSPPUTL
008508******************************************************************PPSPPUTL
008509*            P R O C E D U R E  D I V I S I O N                  *PPSPPUTL
008510******************************************************************PPSPPUTL
008600                          PPSPPUTL-INTERFACE                      PPSPPUTL
008700                          SPP-ARRAY.                              PPSPPUTL
008800******************************************************************PPSPPUTL
008900*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPSPPUTL
009000*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPSPPUTL
009100******************************************************************PPSPPUTL
009200     EXEC SQL                                                     PPSPPUTL
009300          INCLUDE CPPDXE99                                        PPSPPUTL
009400     END-EXEC.                                                    PPSPPUTL
009500 MAINLINE-100 SECTION.                                            PPSPPUTL
009600     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPSPPUTL
009700     MOVE PPSPPUTL-EMPLOYEE-ID TO WS-EMPLOYEE-ID.                 PPSPPUTL
009800     INITIALIZE PPSPPUTL-ROW-COUNT.                               PPSPPUTL
010900*****INITIALIZE PPSPPUTL-ENTRIES.                                 36090574
011000     INITIALIZE SPP-ARRAY.                                        36090574
011001     MOVE WS-EMPLOYEE-ID TO PPSPPUTL-EMPLOYEE-ID.                 PPSPPUTL
011002     PERFORM SQL-OPEN-SPP.                                        PPSPPUTL
011003     PERFORM SQL-FETCH-SPP.                                       PPSPPUTL
011004     MOVE ZERO TO WS-SPP-SUB.                                     PPSPPUTL
011005*                                                                 PPSPPUTL
011006*        NOTE THAT CELL #1 IS FOR SPP-PROGRAM = 'E'               PPSPPUTL
011007*        NOTE THAT CELL #2 IS FOR SPP-PROGRAM = 'H'               PPSPPUTL
011008*                                                                 PPSPPUTL
011009*        IF ANY OTHER CODES ARE FOUND IT IS AN ERROR              PPSPPUTL
011010*                                                                 PPSPPUTL
011020     IF  SQLCODE = +0                                             PPSPPUTL
011100         PERFORM                                                  PPSPPUTL
011200             VARYING WS-SPP-SUB FROM 1 BY 1                       PPSPPUTL
011300                 UNTIL SQLCODE = +100                             PPSPPUTL
011400                 OR PPSPPUTL-ERROR                                PPSPPUTL
011500                 OR WS-SPP-SUB > 2                                PPSPPUTL
011600         MOVE ZERO TO WS-SPP-SUB2                                 PPSPPUTL
011700         IF SPP-PROGRAM OF SPP-ROW-DATA = 'E'                     PPSPPUTL
011800             MOVE +1 TO WS-SPP-SUB2                               PPSPPUTL
011900         END-IF                                                   PPSPPUTL
012000         IF SPP-PROGRAM OF SPP-ROW-DATA = 'H'                     PPSPPUTL
012100             MOVE +2 TO WS-SPP-SUB2                               PPSPPUTL
012200         END-IF                                                   PPSPPUTL
012300         IF WS-SPP-SUB2 = +1 OR +2                                PPSPPUTL
013500         MOVE EMPLOYEE-ID OF SPP-ROW-DATA TO                      36090574
013600              EMPLOYEE-ID                                         36090574
013700              OF SPP-ARRAY (WS-SPP-SUB2)                          36090574
013701         MOVE SPP-PROGRAM OF SPP-ROW-DATA TO                      PPSPPUTL
013702              SPP-PROGRAM                                         PPSPPUTL
013703              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
013704         MOVE TOTAL-SPP-CONTRIB OF SPP-ROW-DATA TO                PPSPPUTL
013705              TOTAL-SPP-CONTRIB                                   PPSPPUTL
013706              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
013707         MOVE MONTH1-SPP-CONTRIB OF SPP-ROW-DATA TO               PPSPPUTL
013708              MONTH1-SPP-CONTRIB                                  PPSPPUTL
013709              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
013710         MOVE MONTH2-SPP-CONTRIB OF SPP-ROW-DATA TO               PPSPPUTL
013711              MONTH2-SPP-CONTRIB                                  PPSPPUTL
013712              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
013713         MOVE MONTH3-SPP-CONTRIB OF SPP-ROW-DATA TO               PPSPPUTL
013714              MONTH3-SPP-CONTRIB                                  PPSPPUTL
013800              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
013900         MOVE MONTH4-SPP-CONTRIB OF SPP-ROW-DATA TO               PPSPPUTL
014000              MONTH4-SPP-CONTRIB                                  PPSPPUTL
014100              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
014200         MOVE MONTH5-SPP-CONTRIB OF SPP-ROW-DATA TO               PPSPPUTL
014300              MONTH5-SPP-CONTRIB                                  PPSPPUTL
014400              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
014500         MOVE MONTH6-SPP-CONTRIB OF SPP-ROW-DATA TO               PPSPPUTL
014600              MONTH6-SPP-CONTRIB                                  PPSPPUTL
014700              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
014800         MOVE TOTAL-SPP-INTEREST OF SPP-ROW-DATA TO               PPSPPUTL
014900              TOTAL-SPP-INTEREST                                  PPSPPUTL
015000              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
015100         MOVE TOTAL-SPP-PAID OF SPP-ROW-DATA TO                   PPSPPUTL
015200              TOTAL-SPP-PAID                                      PPSPPUTL
015300              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
015400         MOVE SPP-DATE OF SPP-ROW-DATA TO                         PPSPPUTL
015500              SPP-DATE                                            PPSPPUTL
015600              OF SPP-ARRAY (WS-SPP-SUB2)                          PPSPPUTL
015700             ELSE                                                 PPSPPUTL
015800             SET PPSPPUTL-ERROR TO TRUE                           PPSPPUTL
015900             END-IF                                               PPSPPUTL
016000             PERFORM SQL-FETCH-SPP                                PPSPPUTL
016100         END-PERFORM                                              PPSPPUTL
016200         END-IF.                                                  PPSPPUTL
016300     IF WS-SPP-SUB > 2                                            PPSPPUTL
016400         AND SQLCODE NOT = +100                                   PPSPPUTL
016500         SET PPSPPUTL-ERROR TO TRUE                               PPSPPUTL
016600     END-IF.                                                      PPSPPUTL
016700     PERFORM SQL-CLOSE-SPP.                                       PPSPPUTL
016800     IF WS-SPP-SUB > ZERO                                         PPSPPUTL
016900         COMPUTE PPSPPUTL-ROW-COUNT = WS-SPP-SUB - 1.             PPSPPUTL
017000 MAINLINE-999.                                                    PPSPPUTL
017100     GOBACK.                                                      PPSPPUTL
017200******************************************************************PPSPPUTL
017300*  PROCEDURE SQL (OPEN/SELECT/CLOSE) FOR SPP VIEW                *PPSPPUTL
017400******************************************************************PPSPPUTL
017500 SQL-OPEN-SPP      SECTION.                                       PPSPPUTL
017600     IF NOT SPP-ROW-OPEN                                          PPSPPUTL
017700     MOVE 'OPEN SPP CURSOR' TO DB2MSG-TAG                         PPSPPUTL
017800     EXEC SQL                                                     PPSPPUTL
017900         OPEN SPP_ROW                                             PPSPPUTL
018000     END-EXEC                                                     PPSPPUTL
018100     SET SPP-ROW-OPEN TO TRUE.                                    PPSPPUTL
018200 SQL-FETCH-SPP      SECTION.                                      PPSPPUTL
018300     MOVE 'FETCH SPP ROW' TO DB2MSG-TAG.                          PPSPPUTL
018400     EXEC SQL                                                     PPSPPUTL
018500         FETCH SPP_ROW INTO :SPP-ROW-DATA                         PPSPPUTL
018600     END-EXEC.                                                    PPSPPUTL
018700 SQL-CLOSE-SPP      SECTION.                                      PPSPPUTL
018800     IF SPP-ROW-OPEN                                              PPSPPUTL
018900     MOVE 'CLOSE SPP CURSOR' TO DB2MSG-TAG                        PPSPPUTL
019000     EXEC SQL                                                     PPSPPUTL
019100         CLOSE SPP_ROW                                            PPSPPUTL
019200     END-EXEC                                                     PPSPPUTL
019300     MOVE SPACE TO SPP-ROW-OPEN-SW.                               PPSPPUTL
019400    SKIP1                                                         PPSPPUTL
019500*999999-SQL-ERROR.                                               *PPSPPUTL
019600     COPY CPPDXP99.                                               PPSPPUTL
019700     SET PPSPPUTL-ERROR TO TRUE.                                  PPSPPUTL
019800     IF NOT HERE-BEFORE                                           PPSPPUTL
019900         SET HERE-BEFORE TO TRUE                                  PPSPPUTL
020000         PERFORM SQL-CLOSE-SPP.                                   PPSPPUTL
020100     GOBACK.                                                      PPSPPUTL
