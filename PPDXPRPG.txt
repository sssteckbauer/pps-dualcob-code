000100**************************************************************/   28201068
000200*  PROGRAM: PPDXPRPG                                         */   28201068
000300*  RELEASE: ___1068______ SERVICE REQUEST(S): ____12820____  */   28201068
000400*  NAME:______M SANO_____ MODIFICATION DATE:  ___06/14/96__  */   28201068
000500*  DESCRIPTION:                                              */   28201068
000600*   - CHANGED COUNT OF STAFF APPOINTMENTS TO RECOGNIZE       */   28201068
000700*     PERSONNEL PROGRAM CODES 1 AND 2 RATHER THAN E M P S    */   28201068
000800**************************************************************/   28201068
000100**************************************************************/   17810972
000200*  PROGRAM: PPDXPRPG                                         */   17810972
000300*  RELEASE: ___0972______ SERVICE REQUEST(S): ____11781____  */   17810972
000400*  NAME:______M.SANO_____ MODIFICATION DATE:  ___03/07/95__  */   17810972
000500*  DESCRIPTION:                                              */   17810972
000600*   -ADDED STUDENT STATUS CODE VALUE DEFINITIONS FOR VALUES  */   17810972
000700*    6 AND 7, OTHER CAMPUS GRADUATES AND UNDERGRADUATES      */   17810972
000800**************************************************************/   17810972
000100**************************************************************/   36520886
000200*  PROGRAM: PPDXPRPG                                         */   36520886
000300*  RELEASE: ___0886______ SERVICE REQUEST(S): _____3652____  */   36520886
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __03/30/94____ */   36520886
000500*  DESCRIPTION:                                              */   36520886
000600*  PPS EMPLOYEE DOCUMENT DERIVATION OF DOCUMENT PERSONNEL    */   36520886
000700*  PROGRAM.                                                  */   36520886
000800**************************************************************/   36520886
000900*-------------------------------------------------------------    PPDXPRPG
001000****************************************************************  PPDXPRPG
001100* *                                                               PPDXPRPG
001200* *                      PPDXPRPG                                 PPDXPRPG
001300* *                                                               PPDXPRPG
001400* *  PPDXPRPG DERIVES THE DOCUMENT PERSONNEL PROGRAM USED FOR:    PPDXPRPG
001500* *                                                               PPDXPRPG
001600* *  1). DOCUMENT SELECTION EDIT VERIFICATION                     PPDXPRPG
001700* *  2). PERSONNEL PROGRAM ASSIGNMENT FOR "MASS" DOCUMENT         PPDXPRPG
001800* *      CREATION.                                                PPDXPRPG
001900* *                                                               PPDXPRPG
002000* *  PPDXPRPG IS CALLED BY PPWIDOC (ONLINE MODE) OR PPP750 (BATCH PPDXPRPG
002100* * MODE).                                                        PPDXPRPG
002200* *                                                               PPDXPRPG
002300* *  THE CALLING PROGRAM SUPPLIES THE EMPLOYEE ID.                PPDXPRPG
002400* *                                                               PPDXPRPG
002500* *  PPDXPRPG WILL RETURN THREE YES/NO INDICATORS:                PPDXPRPG
002600* *   1). ACADEMIC APPOINTMENT RESIDENT                           PPDXPRPG
002700* *   2). NON-ACADEMIC (I.E., STAFF) APPOINTMENT RESIDENT         PPDXPRPG
002800* *   3). STUDENT OR NON-STUDENT.                                 PPDXPRPG
002900* *                                                               PPDXPRPG
003000***********************************************************       PPDXPRPG
003100* *                                                               PPDXPRPG
003200*-------------------------------------------------------------    PPDXPRPG
003300 IDENTIFICATION DIVISION.                                         PPDXPRPG
003400 PROGRAM-ID. PPDXPRPG.                                            PPDXPRPG
003500 ENVIRONMENT DIVISION.                                            PPDXPRPG
003600 CONFIGURATION SECTION.                                           PPDXPRPG
003700 SPECIAL-NAMES.                                                   PPDXPRPG
003800     SYSPCH  IS DEBUGOUT.                                         PPDXPRPG
003900 SOURCE-COMPUTER.                                                 PPDXPRPG
004000 COPY  CPOTXUCS.                                                  PPDXPRPG
004100 DATA DIVISION.                                                   PPDXPRPG
004200*-----------------------------------------------------------------PPDXPRPG
004300 WORKING-STORAGE SECTION.                                         PPDXPRPG
004400*-----------------------------------------------------------------PPDXPRPG
004500 01  A-STND-PROG-ID             PIC X(15) VALUE                   PPDXPRPG
005400     'PPDXPRPG/030795'.                                           17810972
005500**** 'PPDXPRPG/022594'.                                           17810972
004700                                                                  PPDXPRPG
004800 01  WS-COMN.                                                     PPDXPRPG
004900     05  WS-START                          PIC X(27) VALUE        PPDXPRPG
005000         'WORKING STORAGE STARTS HERE'.                           PPDXPRPG
005100*                                                                 PPDXPRPG
005200 01  PPDB2MSG-INTERFACE.                                          PPDXPRPG
005300     COPY CPLNKDB2.                                               PPDXPRPG
005400* *                                                               PPDXPRPG
005500 01  WS-MISC-DATA.                                                PPDXPRPG
005600     05  FIRST-TIME-SW                PIC 9   VALUE ZERO.         PPDXPRPG
005700         88  FIRST-TIME-SW-OFF                VALUE ZERO.         PPDXPRPG
005800         88  FIRST-TIME-SW-ON                 VALUE 1.            PPDXPRPG
005900     05  WS-EMPLOYEE-ID                   PIC X(09) VALUE ZERO.   PPDXPRPG
006000     05  WS-ROW-COUNT                PIC S9(04) COMP VALUE ZERO.  PPDXPRPG
006100     05  WS-ROW-SELECTED-SW          PIC X(01) VALUE SPACE.       PPDXPRPG
006200         88  WS-ANY-ROW-SELECTED     VALUE 'Y'.                   PPDXPRPG
006300         88  WS-ROW-NOT-SELECTED     VALUE 'N'.                   PPDXPRPG
006400                                                                  PPDXPRPG
006500 01  WS-CALLED-ROUTINES.                                          PPDXPRPG
006600     05  PGM-PPPERUTL              PIC X(08) VALUE 'PPPERUTL'.    PPDXPRPG
006700*                                                                 PPDXPRPG
006800 01  COLUMN-88-WORK.                                              PPDXPRPG
006900     COPY CPWSW88S.                                               PPDXPRPG
007000                                                                  PPDXPRPG
007100 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPDXPRPG
007200     EJECT                                                        PPDXPRPG
007300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPDXPRPG
007400     EJECT                                                        PPDXPRPG
007500 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPDXPRPG
007600                                                                  PPDXPRPG
007700 01  PPPERUTL-INTERFACE.     COPY CPLN2PER.                       PPDXPRPG
007800                                                                  PPDXPRPG
007900 01  PER-ROW.                COPY CPWSRPER.                       PPDXPRPG
008000                                                                  PPDXPRPG
008100 01  DFHEIBLK                         PIC X(1).                   PPDXPRPG
008200******************************************************************PPDXPRPG
008300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPDXPRPG
008400**--------------------------------------------------------------**PPDXPRPG
008500*                                                                 PPDXPRPG
008600 01  CPWSDXPR        EXTERNAL.         COPY CPWSDXPR.             PPDXPRPG
008700*                                                                 PPDXPRPG
008800                                                                  PPDXPRPG
008900 01  APP-ROW.                                                     PPDXPRPG
009000     EXEC SQL                                                     PPDXPRPG
009100          INCLUDE PPPVZAPP                                        PPDXPRPG
009200     END-EXEC.                                                    PPDXPRPG
009300*                                                                 PPDXPRPG
009400     EXEC SQL                                                     PPDXPRPG
009500         INCLUDE SQLCA                                            PPDXPRPG
009600     END-EXEC.                                                    PPDXPRPG
009700*----------------------------------------------------------------*PPDXPRPG
009800*                                                                 PPDXPRPG
009900******************************************************************PPDXPRPG
010000*-------------------------------------------------------------    PPDXPRPG
010100 LINKAGE SECTION.                                                 PPDXPRPG
010200*-------------------------------------------------------------    PPDXPRPG
010300                                                                  PPDXPRPG
010400*----------------------------------------------------------------*PPDXPRPG
010500 PROCEDURE DIVISION.                                              PPDXPRPG
010600*----------------------------------------------------------------*PPDXPRPG
010700     EXEC SQL                                                     PPDXPRPG
010800          INCLUDE CPPDXE99                                        PPDXPRPG
010900     END-EXEC.                                                    PPDXPRPG
011000                                                                  PPDXPRPG
011100*-------------------------------------------------------------    PPDXPRPG
011200 0000-MAIN-LINE   SECTION.                                        PPDXPRPG
011300*-------------------------------------------------------------    PPDXPRPG
011400* *                                                               PPDXPRPG
011500***************************************************************** PPDXPRPG
011600* * THIS MODULE DETERMINES THE EMPLOYEE DOCUMENT PERSONNEL        PPDXPRPG
011700* * PROGRAM ATTRIBUTES USED FOR EDITS AND MASS DOCUMENT CREATION. PPDXPRPG
011800***************************************************************** PPDXPRPG
011900* *                                                               PPDXPRPG
012000     SET CPWSDXPR-RETURN-OK       TO TRUE.                        PPDXPRPG
012100     SET CPWSDXPR-NO-ACAD-APPTS   TO TRUE.                        PPDXPRPG
012200     SET CPWSDXPR-NO-STAFF-APPTS  TO TRUE.                        PPDXPRPG
012300     SET CPWSDXPR-NOT-STUDENT     TO TRUE.                        PPDXPRPG
012400     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPDXPRPG
012500     IF FIRST-TIME-SW-OFF                                         PPDXPRPG
012600         SET  FIRST-TIME-SW-ON   TO TRUE                          PPDXPRPG
012700         PERFORM 0050-PGM-INITIALIZE                              PPDXPRPG
012800     END-IF.                                                      PPDXPRPG
012900*                                                                 PPDXPRPG
013000     PERFORM 2000-ESTAB-PERSONNEL-PGMS.                           PPDXPRPG
013100 0000-RETURN.                                                     PPDXPRPG
013200     GOBACK.                                                      PPDXPRPG
013300                                                                  PPDXPRPG
013400 0000-EXIT.                                                       PPDXPRPG
013500     EXIT.                                                        PPDXPRPG
013600                                                                  PPDXPRPG
013700*-------------------------------------------------------------    PPDXPRPG
013800 0050-PGM-INITIALIZE   SECTION.                                   PPDXPRPG
013900*-------------------------------------------------------------    PPDXPRPG
014000     MOVE A-STND-PROG-ID   TO XWHC-DISPLAY-MODULE.                PPDXPRPG
014100*                                                 *-------------* PPDXPRPG
014200                                                   COPY CPPDXWHC. PPDXPRPG
014300*                                                 *-------------* PPDXPRPG
014400 0050-EXIT.    EXIT.                                              PPDXPRPG
014500*                                                                 PPDXPRPG
014600*-------------------------------------------------------------    PPDXPRPG
014700 2000-ESTAB-PERSONNEL-PGMS  SECTION.                              PPDXPRPG
014800*-------------------------------------------------------------    PPDXPRPG
014900     MOVE CPWSDXPR-EMPLOYEE-ID  TO WS-EMPLOYEE-ID.                PPDXPRPG
015000     PERFORM 8000-GET-PER-ROW                                     PPDXPRPG
015100     IF  NOT CPWSDXPR-RETURN-OK                                   PPDXPRPG
015200         GO TO 2000-EXIT                                          PPDXPRPG
015300     END-IF.                                                      PPDXPRPG
015400**************************************************************    PPDXPRPG
015500*  SETUP STUDENT INDICATOR           *************************    PPDXPRPG
015600**************************************************************    PPDXPRPG
015700     MOVE STUDENT-STATUS TO W88-STUDENT-STATUS.                   PPDXPRPG
015800     IF STUDENT-STATUS-STUD-UNDERGRAD  OR                         PPDXPRPG
015900        STUDENT-STATUS-STUD-GRADUATE                              PPDXPRPG
016900        OR STUDENT-STATUS-UNDERGRAD-OC                            17810972
017000        OR STUDENT-STATUS-GRADUATE-OC                             17810972
016000         SET CPWSDXPR-IS-STUDENT     TO TRUE                      PPDXPRPG
016100     END-IF.                                                      PPDXPRPG
016200**************************************************************    PPDXPRPG
016300*  SETUP STAFF INDICATOR BY COUNTING THE EMPLOYEE'S    *******    PPDXPRPG
016400*  STAFF (I.E., NON-ACADEMIC) APPOINTMENTS.            *******    PPDXPRPG
016500**************************************************************    PPDXPRPG
016600     PERFORM 8100-COUNT-STAFF-APPTS.                              PPDXPRPG
016700     IF WS-ROW-COUNT = ZERO                                       PPDXPRPG
016800         CONTINUE                                                 PPDXPRPG
016900     ELSE                                                         PPDXPRPG
017000         SET CPWSDXPR-STAFF-APPTS-FOUND  TO TRUE                  PPDXPRPG
017100     END-IF.                                                      PPDXPRPG
017200**************************************************************    PPDXPRPG
017300*  SETUP ACADEMIC INDICATOR BY COUNTING THE EMPLOYEE'S  ******    PPDXPRPG
017400*  ACADEMIC APPOINTMENTS.                               ******    PPDXPRPG
017500**************************************************************    PPDXPRPG
017600     PERFORM 8200-COUNT-ACAD-APPTS.                               PPDXPRPG
017700     IF WS-ROW-COUNT = ZERO                                       PPDXPRPG
017800         CONTINUE                                                 PPDXPRPG
017900     ELSE                                                         PPDXPRPG
018000         SET CPWSDXPR-ACAD-APPTS-FOUND TO TRUE                    PPDXPRPG
018100     END-IF.                                                      PPDXPRPG
018200 2000-EXIT.    EXIT.                                              PPDXPRPG
018300                                                                  PPDXPRPG
018400*-------------------------------------------------------------    PPDXPRPG
018500 8000-GET-PER-ROW            SECTION.                             PPDXPRPG
018600*-------------------------------------------------------------    PPDXPRPG
018700*                                                                 PPDXPRPG
018800     MOVE CPWSDXPR-EMPLOYEE-ID  TO PPPERUTL-EMPLOYEE-ID.          PPDXPRPG
018900     CALL PGM-PPPERUTL  USING                                     PPDXPRPG
019000                        PPPERUTL-INTERFACE                        PPDXPRPG
019100                        PER-ROW                                   PPDXPRPG
019200       ON EXCEPTION                                               PPDXPRPG
019300         SET CPWSDXPR-PPPERUTL-ERROR  TO TRUE                     PPDXPRPG
019400         GO TO 8000-EXIT                                          PPDXPRPG
019500     END-CALL.                                                    PPDXPRPG
019600     IF PPPERUTL-ERROR                                            PPDXPRPG
019700         SET CPWSDXPR-PPPERUTL-ERROR  TO TRUE                     PPDXPRPG
019800     ELSE                                                         PPDXPRPG
019900         IF  PPPERUTL-PER-FOUND                                   PPDXPRPG
020000             CONTINUE                                             PPDXPRPG
020100         ELSE                                                     PPDXPRPG
020200             SET CPWSDXPR-EMPL-NOT-FOUND  TO TRUE                 PPDXPRPG
020300         END-IF                                                   PPDXPRPG
020400     END-IF.                                                      PPDXPRPG
020500 8000-EXIT.   EXIT.                                               PPDXPRPG
020600                                                                  PPDXPRPG
020700*-------------------------------------------------------------    PPDXPRPG
020800 8100-COUNT-STAFF-APPTS   SECTION.                                PPDXPRPG
020900*-------------------------------------------------------------    PPDXPRPG
021000**************************************************************    PPDXPRPG
021100*  COUNT THE EMPLOYEE'S STAFF APPOINTMENTS.                  *    PPDXPRPG
021200**************************************************************    PPDXPRPG
021300     MOVE 'COUNT STAFF APP' TO DB2MSG-TAG.                        PPDXPRPG
021400     EXEC SQL                                                     PPDXPRPG
021500          SELECT COUNT (*)                                        PPDXPRPG
021600          INTO :WS-ROW-COUNT                                      PPDXPRPG
021700          FROM PPPVZAPP_APP                                       PPDXPRPG
021800          WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                     PPDXPRPG
023800            AND (PERSONNEL_PGM = '1'                              28201068
023900              OR PERSONNEL_PGM = '2')                             28201068
024000****        AND (PERSONNEL_PGM = 'S'                              28201068
024100****          OR PERSONNEL_PGM = 'E'                              28201068
024200****          OR PERSONNEL_PGM = 'M'                              28201068
024300****          OR PERSONNEL_PGM = 'P')                             28201068
022300     END-EXEC.                                                    PPDXPRPG
022400 8100-EXIT.    EXIT.                                              PPDXPRPG
022500                                                                  PPDXPRPG
022600*-------------------------------------------------------------    PPDXPRPG
022700 8200-COUNT-ACAD-APPTS   SECTION.                                 PPDXPRPG
022800*-------------------------------------------------------------    PPDXPRPG
022900**************************************************************    PPDXPRPG
023000*  COUNT THE EMPLOYEE'S ACADEMIC APPOINTMENTS.               *    PPDXPRPG
023100**************************************************************    PPDXPRPG
023200     MOVE 'COUNT ACAD APP' TO DB2MSG-TAG.                         PPDXPRPG
023300     EXEC SQL                                                     PPDXPRPG
023400          SELECT COUNT (*)                                        PPDXPRPG
023500          INTO :WS-ROW-COUNT                                      PPDXPRPG
023600          FROM PPPVZAPP_APP                                       PPDXPRPG
023700          WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                     PPDXPRPG
023800            AND PERSONNEL_PGM = 'A'                               PPDXPRPG
023900     END-EXEC.                                                    PPDXPRPG
024000 8200-EXIT.   EXIT.                                               PPDXPRPG
024100*                                                                 PPDXPRPG
024200*----------------------------------------------------------------*PPDXPRPG
024300 9300-DATE-CONVERSION-DB2  SECTION.                               PPDXPRPG
024400*----------------------------------------------------------------*PPDXPRPG
024500*                                                 *-------------* PPDXPRPG
024600                                                   COPY CPPDXDC2. PPDXPRPG
024700*                                                 *-------------* PPDXPRPG
024800*                                                                 PPDXPRPG
024900**--------------------------------------------------------------  PPDXPRPG
025000*                                                                 PPDXPRPG
025100******************************************************************PPDXPRPG
025200*999999-SQL-ERROR.                                               *PPDXPRPG
025300******************************************************************PPDXPRPG
025400*    THIS CODE IS EXECUTED IF A NEGATIVE SQLCODE IS RETURNED     *PPDXPRPG
025500******************************************************************PPDXPRPG
025600     EXEC SQL                                                     PPDXPRPG
025700          INCLUDE CPPDXP99                                        PPDXPRPG
025800     END-EXEC.                                                    PPDXPRPG
025900     SET CPWSDXPR-SQL-ERROR  TO TRUE.                             PPDXPRPG
026000     GO TO 0000-RETURN.                                           PPDXPRPG
026100*                                                                 PPDXPRPG
026200******************************************************************PPDXPRPG
026300                                                                  PPDXPRPG
026400*-------------------------------------------------------------    PPDXPRPG
026500***************    END OF SOURCE - PPDXPRPG    *******************PPDXPRPG
