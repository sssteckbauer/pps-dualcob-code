000100**************************************************************/  *36090513
000200*  PROGRAM: PPLPHUTW                                         */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE:      __11/05/90__   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT LPH ROWS.     */  *36090513
000700**************************************************************/  *36090513
000800 IDENTIFICATION DIVISION.                                         PPLPHUTW
000900 PROGRAM-ID. PPLPHUTW                                             PPLPHUTW
001000 AUTHOR. UCOP.                                                    PPLPHUTW
001100 DATE-WRITTEN.  SEPTEMBER 1990.                                   PPLPHUTW
001200 DATE-COMPILED.                                                   PPLPHUTW
001300*REMARKS.                                                         PPLPHUTW
001400*            CALL 'PPLPHUTW' USING                                PPLPHUTW
001500*                            PPXXXUTW-INTERFACE                   PPLPHUTW
001600*                            LPH-ROW.                             PPLPHUTW
001700*                                                                 PPLPHUTW
001800*                                                                 PPLPHUTW
001900     EJECT                                                        PPLPHUTW
002000 ENVIRONMENT DIVISION.                                            PPLPHUTW
002100 CONFIGURATION SECTION.                                           PPLPHUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPLPHUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPLPHUTW
002400      EJECT                                                       PPLPHUTW
002500******************************************************************PPLPHUTW
002600*                                                                 PPLPHUTW
002700*                D A T A  D I V I S I O N                         PPLPHUTW
002800*                                                                 PPLPHUTW
002900******************************************************************PPLPHUTW
003000 DATA DIVISION.                                                   PPLPHUTW
003100 WORKING-STORAGE SECTION.                                         PPLPHUTW
003200 01  MISCELLANEOUS-WS.                                            PPLPHUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPLPHUTW'.         PPLPHUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPLPHUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPLPHUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPLPHUTW
003700     EJECT                                                        PPLPHUTW
003800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPLPHUTW
003900******************************************************************PPLPHUTW
004000*          SQL - WORKING STORAGE                                 *PPLPHUTW
004100******************************************************************PPLPHUTW
004200 01  LPH-ROW-DATA.                                                PPLPHUTW
004300     EXEC SQL                                                     PPLPHUTW
004400          INCLUDE PPPVLPH2                                        PPLPHUTW
004500     END-EXEC.                                                    PPLPHUTW
004600     EJECT                                                        PPLPHUTW
004700     EXEC SQL                                                     PPLPHUTW
004800          INCLUDE SQLCA                                           PPLPHUTW
004900     END-EXEC.                                                    PPLPHUTW
005000******************************************************************PPLPHUTW
005100*                                                                *PPLPHUTW
005200*                SQL- SELECTS                                    *PPLPHUTW
005300*                                                                *PPLPHUTW
005400******************************************************************PPLPHUTW
005500*                                                                *PPLPHUTW
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPLPHUTW
005700*                                                                *PPLPHUTW
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPLPHUTW
005900*                        MEMBER NAMES                            *PPLPHUTW
006000*                                                                *PPLPHUTW
006100*  PPPVLPH2_LPH          PPPVLPH2                                *PPLPHUTW
006200*                                                                *PPLPHUTW
006300******************************************************************PPLPHUTW
006400*                                                                 PPLPHUTW
006500 LINKAGE SECTION.                                                 PPLPHUTW
006600*                                                                 PPLPHUTW
006700******************************************************************PPLPHUTW
006800*                L I N K A G E   S E C T I O N                   *PPLPHUTW
006900******************************************************************PPLPHUTW
007000 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPLPHUTW
007100 01  LPH-ROW.                       COPY CPWSRLPH.                PPLPHUTW
007200*                                                                 PPLPHUTW
007300 PROCEDURE DIVISION USING                                         PPLPHUTW
007400                          PPXXXUTW-INTERFACE                      PPLPHUTW
007500                          LPH-ROW.                                PPLPHUTW
007600******************************************************************PPLPHUTW
007700*            P R O C E D U R E  D I V I S I O N                  *PPLPHUTW
007800******************************************************************PPLPHUTW
007900******************************************************************PPLPHUTW
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPLPHUTW
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPLPHUTW
008200******************************************************************PPLPHUTW
008300     EXEC SQL                                                     PPLPHUTW
008400          INCLUDE CPPDXE99                                        PPLPHUTW
008500     END-EXEC.                                                    PPLPHUTW
008600     SKIP3                                                        PPLPHUTW
008700 0100-MAINLINE SECTION.                                           PPLPHUTW
008800     SKIP1                                                        PPLPHUTW
008900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPLPHUTW
009000     MOVE EMPLOYEE-ID OF LPH-ROW TO WS-EMPLOYEE-ID.               PPLPHUTW
009100     SKIP1                                                        PPLPHUTW
009200     MOVE LPH-ROW TO LPH-ROW-DATA                                 PPLPHUTW
009300     SKIP1                                                        PPLPHUTW
009400     EVALUATE PPXXXUTW-FUNCTION                                   PPLPHUTW
009500         WHEN 'D'                                                 PPLPHUTW
009600             PERFORM 8100-DELETE-ROW                              PPLPHUTW
009700         WHEN 'U'                                                 PPLPHUTW
009800             PERFORM 8200-UPDATE-ROW                              PPLPHUTW
009900         WHEN 'I'                                                 PPLPHUTW
010000             PERFORM 8300-INSERT-ROW                              PPLPHUTW
010100         WHEN OTHER                                               PPLPHUTW
010200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPLPHUTW
010300             SET PPXXXUTW-ERROR            TO TRUE                PPLPHUTW
010400     END-EVALUATE.                                                PPLPHUTW
010500     SKIP1                                                        PPLPHUTW
010600     GOBACK.                                                      PPLPHUTW
010700     EJECT                                                        PPLPHUTW
010800******************************************************************PPLPHUTW
010900*  PROCEDURE SQL                     FOR LPH VIEW                *PPLPHUTW
011000******************************************************************PPLPHUTW
011100 8100-DELETE-ROW SECTION.                                         PPLPHUTW
011200     SKIP1                                                        PPLPHUTW
011300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPLPHUTW
011400     SKIP1                                                        PPLPHUTW
011500     EXEC SQL                                                     PPLPHUTW
011600         DELETE FROM PPPVLPH2_LPH                                 PPLPHUTW
011700          WHERE EMPLOYEE_ID = :EMPLOYEE-ID                        PPLPHUTW
011800            AND LVPLAN_KEY  = :LVPLAN-KEY                         PPLPHUTW
011900            AND LVPLAN_CODE = :LVPLAN-CODE                        PPLPHUTW
012000     END-EXEC.                                                    PPLPHUTW
012100     SKIP1                                                        PPLPHUTW
012200     IF  SQLCODE    NOT = 0                                       PPLPHUTW
012300         SET PPXXXUTW-ERROR TO TRUE                               PPLPHUTW
012400     END-IF.                                                      PPLPHUTW
012500     EJECT                                                        PPLPHUTW
012600 8200-UPDATE-ROW SECTION.                                         PPLPHUTW
012700     SKIP1                                                        PPLPHUTW
012800     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPLPHUTW
012900     SKIP1                                                        PPLPHUTW
013000     EXEC SQL                                                     PPLPHUTW
013100     UPDATE PPPVLPH2_LPH                                          PPLPHUTW
013200              SET  LVPLAN_STARTDATE   = :LVPLAN-STARTDATE         PPLPHUTW
013300         WHERE EMPLOYEE_ID = :EMPLOYEE-ID                         PPLPHUTW
013400           AND LVPLAN_KEY  = :LVPLAN-KEY                          PPLPHUTW
013500           AND LVPLAN_CODE = :LVPLAN-CODE                         PPLPHUTW
013600     END-EXEC.                                                    PPLPHUTW
013700     SKIP1                                                        PPLPHUTW
013800     IF  SQLCODE    NOT = 0                                       PPLPHUTW
013900         SET PPXXXUTW-ERROR TO TRUE                               PPLPHUTW
014000     END-IF.                                                      PPLPHUTW
014100     EJECT                                                        PPLPHUTW
014200 8300-INSERT-ROW SECTION.                                         PPLPHUTW
014300     SKIP1                                                        PPLPHUTW
014400     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPLPHUTW
014500     SKIP1                                                        PPLPHUTW
014600     EXEC SQL                                                     PPLPHUTW
014700         INSERT INTO PPPVLPH2_LPH                                 PPLPHUTW
014800                VALUES (:LPH-ROW-DATA)                            PPLPHUTW
014900     END-EXEC.                                                    PPLPHUTW
015000     SKIP1                                                        PPLPHUTW
015100     IF  SQLCODE    NOT = 0                                       PPLPHUTW
015200         SET PPXXXUTW-ERROR TO TRUE                               PPLPHUTW
015300     END-IF.                                                      PPLPHUTW
015400     EJECT                                                        PPLPHUTW
015500*999999-SQL-ERROR.                                               *PPLPHUTW
015600     SKIP1                                                        PPLPHUTW
015700     EXEC SQL                                                     PPLPHUTW
015800          INCLUDE CPPDXP99                                        PPLPHUTW
015900     END-EXEC.                                                    PPLPHUTW
016000     SKIP1                                                        PPLPHUTW
016100     SET PPXXXUTW-ERROR TO TRUE.                                  PPLPHUTW
016200     IF NOT HERE-BEFORE                                           PPLPHUTW
016300         SET HERE-BEFORE TO TRUE                                  PPLPHUTW
016400     GOBACK.                                                      PPLPHUTW
