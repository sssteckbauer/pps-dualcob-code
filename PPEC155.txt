000000**************************************************************/   01911440
000001*  PROGRAM: PPEC155                                          */   01911440
000002*  RELEASE: ___1440______ SERVICE REQUEST(S): ____80191____  */   01911440
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___11/08/02__  */   01911440
000004*  DESCRIPTION:                                              */   01911440
000005*  - EDIT FOR PRESENCE OF MEDICAL CONTRIBUTION BASE - CURRENT*/   01911440
000006*    YEAR WHEN EMPLOYEE IS ENROLLING IN A MEDICAL PLAN.      */   01911440
000007*  - EDIT FOR PRESENCE OF MEDICAL PLAN WHEN CONTRIBUTION BASE*/   01911440
000008*    CURRENT YEAR IS BEING DELETED.                          */   01911440
000011*  - DO NOT PERFORM THIS EDIT UNLESS THE PLAN END DATE IS    */   01911440
000012*    INITIAL OR GRETAER THAN OR EQUAL TO THE CURRENT DATE.   */   01911440
000013*  - EDIT CHANGE TO PLAN END DATE TO BE CONSISTENT WITH THE  */   01911440
000014*    ABOVE.                                                  */   01911440
000020**************************************************************/   01911440
000090                                                                  PPEC155
000100 IDENTIFICATION DIVISION.                                         PPEC155
000200 PROGRAM-ID. PPEC155.                                             PPEC155
000300 AUTHOR. UCOP.                                                    PPEC155
000400 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEC155
000500 DATE-WRITTEN.  SEPTEMBER 2002.                                   PPEC155
000600 DATE-COMPILED.                                                   PPEC155
000700                                                                  PPEC155
000800******************************************************************PPEC155
000900**    THIS ROUTINE PERFORMS CONSISTENCY EDITS BETWEEN MEDICAL   **PPEC155
001000**    PLAN AND MEDICAL CONTRIBUTION BASE - CURRENT YEAR.        **PPEC155
001010**                                                              **PPEC155
001020**    IF THE MEDICAL PLAN CODE IS BEING UPDATED FROM SPACES     **PPEC155
001030**    TO A VALUE, THE MEDICAL CONTRIBUTION BASE - CURRENT YEAR  **PPEC155
001040**    MUST BE GREATER THAN ZERO. IF IT IS NOT GREATER THAN ZERO **PPEC155
001050**    AN EMPLOYEE REJECT LEVEL MESSAGE IS ISSUED.               **PPEC155
001060**                                                              **PPEC155
001070**    IF THE MEDICAL CONTRIBUTION BASE - CURRENT YEAR IS BEING  **PPEC155
001080**    INITIALIZED TO ZERO, THE MEDICAL PLAN MUST BE BLANK. IF   **PPEC155
001090**    IT IS NOT BLANK AN EMPLOYEE REJECT LEVEL MESSAGE WILL BE  **PPEC155
001091**    ISSUED.                                                   **PPEC155
001100******************************************************************PPEC155
001200                                                                  PPEC155
001300 ENVIRONMENT DIVISION.                                            PPEC155
001400 CONFIGURATION SECTION.                                           PPEC155
001500 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEC155
001600 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEC155
001700 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEC155
001800                                                                  PPEC155
001900 INPUT-OUTPUT SECTION.                                            PPEC155
002000 FILE-CONTROL.                                                    PPEC155
002100 DATA DIVISION.                                                   PPEC155
002200 FILE SECTION.                                                    PPEC155
002300     EJECT                                                        PPEC155
002400*----------------------------------------------------------------*PPEC155
002500 WORKING-STORAGE SECTION.                                         PPEC155
002600*----------------------------------------------------------------*PPEC155
002700 01  A-STND-PROG-ID            PIC X(15) VALUE                    PPEC155
002800     'PPEC155 /110802'.                                           PPEC155
002900                                                                  PPEC155
003000 01  A-STND-MSG-PARMS                 REDEFINES                   PPEC155
003100     A-STND-PROG-ID.                                              PPEC155
003200     05  FILLER                       PIC X(03).                  PPEC155
003300     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEC155
003400     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEC155
003500     05  FILLER                       PIC X(08).                  PPEC155
003600                                                                  PPEC155
003700 01  ELEMENTS-USED.                                               PPEC155
003800     05  E0289                 PIC 9(04)  VALUE 0289.             PPEC155
003910     05  E0292                 PIC 9(04)  VALUE 0292.             PPEC155
003920     05  E0300                 PIC 9(04)  VALUE 0300.             PPEC155
004000                                                                  PPEC155
004100 01  MESSAGES-USED.                                               PPEC155
004200     05  M08095                PIC X(05) VALUE '08095'.           PPEC155
004300                                                                  PPEC155
004400 01  MISC-WORK-AREAS.                                             PPEC155
004500     05  FIRST-TIME-SW          PIC X(01)  VALUE LOW-VALUES.      PPEC155
004600         88  THIS-IS-THE-FIRST-TIME        VALUE LOW-VALUES.      PPEC155
004700         88  NOT-THE-FIRST-TIME            VALUE 'Y'.             PPEC155
004800     EJECT                                                        PPEC155
004810                                                                  PPEC155
004900 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEC155
005000     EJECT                                                        PPEC155
005100 01  DATE-WORK-AREA.                               COPY CPWSDATE. PPEC155
005200     EJECT                                                        PPEC155
005300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEC155
005400     EJECT                                                        PPEC155
005500 01  INSTALLATION-DEPENDT-CONSTATNS.               COPY CPWSXIDC. PPEC155
005600     EJECT                                                        PPEC155
005700 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEC155
005800     EJECT                                                        PPEC155
005900 01  XMST-MESSAGE-SEVERITY-TABLE.                  COPY CPWSXMST. PPEC155
006000     EJECT                                                        PPEC155
006100 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. PPEC155
006300     EJECT                                                        PPEC155
006400******************************************************************PPEC155
006500**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEC155
006600**--------------------------------------------------------------**PPEC155
006700** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEC155
006800******************************************************************PPEC155
006900                                                                  PPEC155
007000 01  ONLINE-SIGNALS                      EXTERNAL. COPY CPWSONLI. PPEC155
007200     EJECT                                                        PPEC155
007300 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEC155
007500     EJECT                                                        PPEC155
007600 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEC155
007800     EJECT                                                        PPEC155
007900 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEC155
008100     EJECT                                                        PPEC155
008200 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEC155
008300     EJECT                                                        PPEC155
008400 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEC155
008500     EJECT                                                        PPEC155
008600 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEC155
008700     EJECT                                                        PPEC155
008800 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEC155
009000     EJECT                                                        PPEC155
009100 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEC155
009300     EJECT                                                        PPEC155
009400 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEC155
009600     EJECT                                                        PPEC155
009700 01  COMM-TAB                            EXTERNAL. COPY CPOTXCMF. PPEC155
009900     EJECT                                                        PPEC155
010000 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEC155
010200     EJECT                                                        PPEC155
010400******************************************************************PPEC155
010500**  DATA BASE AREAS                                             **PPEC155
010600******************************************************************PPEC155
010700 01  BEN-ROW                             EXTERNAL. COPY CPWSRBEN. PPEC155
010800     EJECT                                                        PPEC155
011900******************************************************************PPEC155
012000**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEC155
012100******************************************************************PPEC155
012200*----------------------------------------------------------------*PPEC155
012300 LINKAGE SECTION.                                                 PPEC155
012400*----------------------------------------------------------------*PPEC155
012600******************************************************************PPEC155
012700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEC155
012800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEC155
012900******************************************************************PPEC155
013000                                                                  PPEC155
013100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEC155
013300     EJECT                                                        PPEC155
013400******************************************************************PPEC155
013500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEC155
013600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEC155
013700******************************************************************PPEC155
013800                                                                  PPEC155
013900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEC155
014100     EJECT                                                        PPEC155
014200******************************************************************PPEC155
014300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEC155
014400******************************************************************PPEC155
014500 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEC155
014600                          KMTA-MESSAGE-TABLE-ARRAY.               PPEC155
014700                                                                  PPEC155
014800*----------------------------------------------------------------*PPEC155
014900 0000-DRIVER.                                                     PPEC155
015000*----------------------------------------------------------------*PPEC155
015100******************************************************************PPEC155
015200**  PERFORM THE INITIALIZATION ROUTINE IF FIRST TIME            **PPEC155
015300******************************************************************PPEC155
015400                                                                  PPEC155
015500     IF  THIS-IS-THE-FIRST-TIME                                   PPEC155
015600         PERFORM 0100-INITIALIZE                                  PPEC155
015700     END-IF.                                                      PPEC155
015800                                                                  PPEC155
015900******************************************************************PPEC155
016000**  PERFORM THE MAINLINE ROUTINE                                **PPEC155
016100******************************************************************PPEC155
016200                                                                  PPEC155
016300     PERFORM 1000-MAINLINE-ROUTINE.                               PPEC155
016400                                                                  PPEC155
016500     EXIT PROGRAM.                                                PPEC155
016600                                                                  PPEC155
016700     EJECT                                                        PPEC155
016800*----------------------------------------------------------------*PPEC155
016900 0100-INITIALIZE                 SECTION.                         PPEC155
017000*----------------------------------------------------------------*PPEC155
017100******************************************************************PPEC155
017200**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEC155
017300******************************************************************PPEC155
017400                                                                  PPEC155
017500     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEC155
017600                                                                  PPEC155
017700*                                               *-------------*   PPEC155
017800                                                 COPY CPPDXWHC.   PPEC155
017900*                                               *-------------*   PPEC155
018000******************************************************************PPEC155
018100**   SET FIRST CALL SWITCH OFF                                  **PPEC155
018200******************************************************************PPEC155
018300                                                                  PPEC155
018400     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEC155
018600     EJECT                                                        PPEC155
018700*----------------------------------------------------------------*PPEC155
018800 1000-MAINLINE-ROUTINE           SECTION.                         PPEC155
018900*----------------------------------------------------------------*PPEC155
019000                                                                  PPEC155
019240     IF  KRMI-REQUESTED-BY-08                                     PPEC155
019250         PERFORM 1100-CONEDITS-MAINLINE                           PPEC155
019280     END-IF.                                                      PPEC155
019292     EJECT                                                        PPEC155
019293******************************************************************PPEC155
019294**     C O N S I S T E N C Y    E D I T S   M A I N L I N E     **PPEC155
019295******************************************************************PPEC155
019296*----------------------------------------------------------------*PPEC155
019297 1100-CONEDITS-MAINLINE          SECTION.                         PPEC155
019298*----------------------------------------------------------------*PPEC155
019299******************************************************************PPEC155
019300**   CONSISTENCY EDIT FOR MEDICAL PLAN CODE AND MEDICAL         **PPEC155
019301**   CONTRIBUTION BASE - CURRENT YEAR.                          **PPEC155
019310******************************************************************PPEC155
019401                                                                  PPEC155
019402******************************************************************PPEC155
019403**   CHECK IF THE MEDICAL PLAN CODE HAS BEEN ENTERED. IF SO,    **PPEC155
019404**   AND IT IS NOT BEING CHANGED TO SPACES, THEN THE MEDICAL    **PPEC155
019405**   CONTRIBUTION BASE - CURRENT YEAR MUST BE GREATER THAN ZERO.**PPEC155
019406******************************************************************PPEC155
019410     MOVE E0292 TO DL-FIELD.                                      PPEC155
019420     MOVE ZERO TO DL-STATUS.                                      PPEC155
019430     PERFORM 9085-DLSEARCH.                                       PPEC155
019440     IF DL-STATUS = 1                                             PPEC155
019450        IF HLTH-PLAN = 'XX' OR 'XC' OR 'XD' OR 'DM' OR '  '       PPEC155
019460           CONTINUE                                               PPEC155
019470        ELSE                                                      PPEC155
019480           IF MED-CONT-BASE-CUR = ZERO                            PPEC155
019481              AND (HEALTH-COVEND-DATE NOT < XDTS-ISO-DLY-RUN-DATE PPEC155
019482                    OR HEALTH-COVEND-DATE = XDTS-ISO-ZERO-DATE)   PPEC155
019483              ADD 1       TO KMTA-CTR                             PPEC155
019487              MOVE E0289  TO KMTA-FIELD-NUM         (KMTA-CTR)    PPEC155
019488              MOVE MED-CONT-BASE-CUR                              PPEC155
019489                          TO KMTA-DATA-FIELD        (KMTA-CTR)    PPEC155
019490              MOVE 3      TO KMTA-LENGTH            (KMTA-CTR)    PPEC155
019491              MOVE M08095 TO KMTA-MSG-NUMBER        (KMTA-CTR)    PPEC155
019492              MOVE WS-ROUTINE-TYPE                                PPEC155
019493                          TO KMTA-ROUTINE-TYPE      (KMTA-CTR)    PPEC155
019494              MOVE WS-ROUTINE-NUM                                 PPEC155
019495                          TO KMTA-ROUTINE-NUM       (KMTA-CTR)    PPEC155
019496           END-IF                                                 PPEC155
019497        END-IF                                                    PPEC155
019498     END-IF.                                                      PPEC155
019499                                                                  PPEC155
019500     MOVE E0289 TO DL-FIELD.                                      PPEC155
019501     MOVE ZERO TO DL-STATUS.                                      PPEC155
019502     PERFORM 9085-DLSEARCH.                                       PPEC155
019503     IF DL-STATUS = 1                                             PPEC155
019504        IF HLTH-PLAN = 'XX' OR 'XC' OR 'XD' OR 'DM' OR '  '       PPEC155
019505           CONTINUE                                               PPEC155
019506        ELSE                                                      PPEC155
019507           IF MED-CONT-BASE-CUR = ZERO                            PPEC155
019508              AND (HEALTH-COVEND-DATE NOT < XDTS-ISO-DLY-RUN-DATE PPEC155
019509                    OR HEALTH-COVEND-DATE = XDTS-ISO-ZERO-DATE)   PPEC155
019511              ADD 1       TO KMTA-CTR                             PPEC155
019512              MOVE E0289  TO KMTA-FIELD-NUM         (KMTA-CTR)    PPEC155
019513              MOVE MED-CONT-BASE-CUR                              PPEC155
019514                          TO KMTA-DATA-FIELD        (KMTA-CTR)    PPEC155
019515              MOVE 3      TO KMTA-LENGTH            (KMTA-CTR)    PPEC155
019516              MOVE M08095 TO KMTA-MSG-NUMBER        (KMTA-CTR)    PPEC155
019517              MOVE WS-ROUTINE-TYPE                                PPEC155
019518                          TO KMTA-ROUTINE-TYPE      (KMTA-CTR)    PPEC155
019519              MOVE WS-ROUTINE-NUM                                 PPEC155
019520                          TO KMTA-ROUTINE-NUM       (KMTA-CTR)    PPEC155
019527           END-IF                                                 PPEC155
019528        END-IF                                                    PPEC155
019529     END-IF.                                                      PPEC155
019530                                                                  PPEC155
019540     MOVE E0300 TO DL-FIELD.                                      PPEC155
019550     MOVE ZERO TO DL-STATUS.                                      PPEC155
019560     PERFORM 9085-DLSEARCH.                                       PPEC155
019570     IF DL-STATUS = 1                                             PPEC155
019580        IF HLTH-PLAN = 'XX' OR 'XC' OR 'XD' OR 'DM' OR '  '       PPEC155
019590           CONTINUE                                               PPEC155
019591        ELSE                                                      PPEC155
019592           IF MED-CONT-BASE-CUR = ZERO                            PPEC155
019593              AND (HEALTH-COVEND-DATE NOT < XDTS-ISO-DLY-RUN-DATE PPEC155
019594                    OR HEALTH-COVEND-DATE = XDTS-ISO-ZERO-DATE)   PPEC155
019595              ADD 1       TO KMTA-CTR                             PPEC155
019596              MOVE E0289  TO KMTA-FIELD-NUM         (KMTA-CTR)    PPEC155
019597              MOVE MED-CONT-BASE-CUR                              PPEC155
019598                          TO KMTA-DATA-FIELD        (KMTA-CTR)    PPEC155
019599              MOVE 3      TO KMTA-LENGTH            (KMTA-CTR)    PPEC155
019600              MOVE M08095 TO KMTA-MSG-NUMBER        (KMTA-CTR)    PPEC155
019601              MOVE WS-ROUTINE-TYPE                                PPEC155
019602                          TO KMTA-ROUTINE-TYPE      (KMTA-CTR)    PPEC155
019603              MOVE WS-ROUTINE-NUM                                 PPEC155
019604                          TO KMTA-ROUTINE-NUM       (KMTA-CTR)    PPEC155
019605           END-IF                                                 PPEC155
019606        END-IF                                                    PPEC155
019607     END-IF.                                                      PPEC155
019608                                                                  PPEC155
019610     EJECT                                                        PPEC155
019640                                                                  PPEC155
019650*----------------------------------------------------------------*PPEC155
019700 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEC155
019800*----------------------------------------------------------------*PPEC155
019900                                                                  PPEC155
020000*                                                 *-------------* PPEC155
020100                                                   COPY CPPDXDEC. PPEC155
020200*                                                 *-------------* PPEC155
020300                                                                  PPEC155
020400*----------------------------------------------------------------*PPEC155
020500 9300-DATE-CONVERSION-DB2        SECTION.                         PPEC155
020600*----------------------------------------------------------------*PPEC155
020700                                                                  PPEC155
020800*                                                 *-------------* PPEC155
020900                                                   COPY CPPDXDC3. PPEC155
021000*                                                 *-------------* PPEC155
021100******************************************************************PPEC155
021200**   E N D  S O U R C E   ----  PPEC155 ----                    **PPEC155
021300******************************************************************PPEC155
