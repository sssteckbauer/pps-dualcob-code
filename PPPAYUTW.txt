000100*==========================================================%      UCSD9999
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000400*=                                                        =%      UCSD9999
000500*==========================================================%      UCSD9999
000600*==========================================================%      UCSD0034
000700*=    PROGRAM: PPAYUTW                                    =%      UCSD0034
000800*=    CHANGE #0034          PROJ. REQUEST: DT-034         =%      UCSD0034
000900*=    NAME: R WOLBERG       MODIFICATION DATE:  6/23/91   =%      UCSD0034
001000*=                                                        =%      UCSD0034
001010*=    DESCRIPTION: LOCAL ELEMENT USED FOR SURE-MAILDROP   =%      UCSD0034
001400*==========================================================%      UCSD0034
000100**************************************************************/   48761492
000200*  PROGRAM: PPPAYUTW                                         */   48761492
000300*  RELEASE: ___1492______ SERVICE REQUEST(S): ____14876____  */   48761492
000400*  NAME:____J KENNEDY____ MODIFICATION DATE:  ___03/12/03__  */   48761492
000500*  DESCRIPTION:                                              */   48761492
000600*  ADDED FINAL PAY CYCLE (EDB0772) & FINAL PAY DATE (EDB0773)*/   48761492
000700**************************************************************/   48761492
000000**************************************************************/   48681411
000001*  PROGRAM: PPPAYUTW                                         */   48681411
000002*  RELEASE: ___1411______ SERVICE REQUEST(S): ____14868____  */   48681411
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___04/23/02__  */   48681411
000004*  DESCRIPTION:                                              */   48681411
000005*  -  ADDED FINAL PAY INDICATOR                              */   48681411
000008**************************************************************/   48681411
000008**************************************************************/   48571379
000009*  PROGRAM: PPPAYUTW                                         */   48571379
000010*  RELEASE: ___1379______ SERVICE REQUEST(S): ____14857____  */   48571379
000011*  NAME:_______QUAN______ MODIFICATION DATE:  ___09/17/01__  */   48571379
000012*  DESCRIPTION:                                              */   48571379
000013*  - ADDED OTHER STATE LOCAL TAX INDICATOR                   */   48571379
000015**************************************************************/   48571379
000000**************************************************************/   48441325
000001*  PROGRAM: PPPAYUTW                                         */   48441325
000002*  RELEASE: ___1325______ SERVICE REQUEST(S): ____14844____  */   48441325
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___10/18/00__  */   48441325
000004*  DESCRIPTION:                                              */   48441325
000005*    ADDED W4-PROCESS-DATE, DE4-PROCESS-DATE, AND            */   48441325
000006*    TAX-PROCESSOR-ID.                                       */   48441325
000007**************************************************************/   48441325
000000**************************************************************/   32171197
000001*  PROGRAM: PPPAYUTW                                         */   32171197
000002*  RELEASE: ___1197______ SERVICE REQUEST(S): ____13217____  */   32171197
000003*  NAME:___ROY STAPLES___ MODIFICATION DATE:  ___07/02/98__  */   32171197
000004*  DESCRIPTION:                                              */   32171197
000005*    ADDED US-DATE-OF-ENTRY (EDB1169)                        */   32171197
000007**************************************************************/   32171197
000000**************************************************************/   49051194
000001*  PROGRAM: PPPAYUTW                                         */   49051194
000002*  RELEASE: ___1194______ SERVICE REQUEST(S): ____14905____  */   49051194
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/04/98__  */   49051194
000004*  DESCRIPTION:                                              */   49051194
000005*    ADDED PRIOR YEAR TO DATE TOTAL GROSS (EDB 0767)         */   49051194
000007**************************************************************/   49051194
000100**************************************************************/   17810964
000200*  PROGRAM: PPPAYUTW                                         */   17810964
000300*  RELEASE: ___0964______ SERVICE REQUEST(S): ____11781____  */   17810964
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___10/20/94__  */   17810964
000500*  DESCRIPTION:                                              */   17810964
000600*     ADDED RET-FICA-DERIVE (DE 0238)                        */   17810964
000700*                                                            */   17810964
000800**************************************************************/   17810964
000100**************************************************************/   15360842
000200*  PROGRAM: PPPAYUTW                                         */   15360842
000300*  RELEASE: ___0842______ SERVICE REQUEST(S): _____11536____ */   15360842
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___11/04/93__  */   15360842
000500*  DESCRIPTION:                                              */   15360842
000600*    ADDED OTHER-STATE-CA-RES OTHER-STATE-NAME               */   15360842
000700*                                                            */   15360842
000800**************************************************************/   15360842
000000**************************************************************/   02110712
000001*  PROGRAM: PPPAYUTW                                         */   02110712
000002*  RELEASE: ___0712______ SERVICE REQUEST(S): ____10211____  */   02110712
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/04/92__  */   02110712
000004*  DESCRIPTION:                                              */   02110712
000005*    ADDED I-9-DATE                                          */   02110712
000006*                                                            */   02110712
000007**************************************************************/   02110712
000100**************************************************************/   03070649
000200*  PROGRAM: PPPAYUTW                                         */   03070649
000300*  RELEASE: ___0649______ SERVICE REQUEST(S): ____10307____  */   03070649
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___03/16/92__  */   03070649
000500*  DESCRIPTION:                                              */   03070649
000600*  - ADDED PRIOR-YR-FWT-GROSS                                */   03070649
000700*                                                            */   03070649
000800**************************************************************/   03070649
000100**************************************************************/  *36090513
000200*  PROGRAM: PPPAYUTW                                         */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE:      __11/05/90__   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - NEW PROGRAM TO DELETE, UPDATE, AND INSERT PAY ROWS.     */  *36090513
000700**************************************************************/  *36090513
000800 IDENTIFICATION DIVISION.                                         PPPAYUTW
000900 PROGRAM-ID. PPPAYUTW                                             PPPAYUTW
001000 AUTHOR. UCOP.                                                    PPPAYUTW
001100 DATE-WRITTEN.  SEPTEMBER 1990.                                   PPPAYUTW
001200 DATE-COMPILED.                                                   PPPAYUTW
001300*REMARKS.                                                         PPPAYUTW
001400*            CALL 'PPPAYUTW' USING                                PPPAYUTW
001500*                            PPXXXUTW-INTERFACE                   PPPAYUTW
001600*                            PAY-ROW.                             PPPAYUTW
001700*                                                                 PPPAYUTW
001800*                                                                 PPPAYUTW
001900     EJECT                                                        PPPAYUTW
002000 ENVIRONMENT DIVISION.                                            PPPAYUTW
002100 CONFIGURATION SECTION.                                           PPPAYUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPPAYUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPPAYUTW
002400      EJECT                                                       PPPAYUTW
002500******************************************************************PPPAYUTW
002600*                                                                 PPPAYUTW
002700*                D A T A  D I V I S I O N                         PPPAYUTW
002800*                                                                 PPPAYUTW
002900******************************************************************PPPAYUTW
003000 DATA DIVISION.                                                   PPPAYUTW
003100 WORKING-STORAGE SECTION.                                         PPPAYUTW
003200 01  MISCELLANEOUS-WS.                                            PPPAYUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPPAYUTW'.         PPPAYUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPPAYUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPPAYUTW
003600     05  WS-EMPLOYEE-ID       PIC X(09) VALUE SPACES.             PPPAYUTW
003700     EJECT                                                        PPPAYUTW
003800 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPPAYUTW
003900******************************************************************PPPAYUTW
004000*          SQL - WORKING STORAGE                                 *PPPAYUTW
004100******************************************************************PPPAYUTW
004200 01  PAY-ROW-DATA.                                                PPPAYUTW
004300     EXEC SQL                                                     PPPAYUTW
004400          INCLUDE PPPVPAY2                                        PPPAYUTW
004500     END-EXEC.                                                    PPPAYUTW
004600     EJECT                                                        PPPAYUTW
004700     EXEC SQL                                                     PPPAYUTW
004800          INCLUDE SQLCA                                           PPPAYUTW
004900     END-EXEC.                                                    PPPAYUTW
005000******************************************************************PPPAYUTW
005100*                                                                *PPPAYUTW
005200*                SQL- SELECTS                                    *PPPAYUTW
005300*                                                                *PPPAYUTW
005400******************************************************************PPPAYUTW
005500*                                                                *PPPAYUTW
005600*  THE VIEWS USED IN THIS PROGRAM ARE:                           *PPPAYUTW
005700*                                                                *PPPAYUTW
005800*     VIEW              VIEW DDL AND DCL INCLUDE                 *PPPAYUTW
005900*                        MEMBER NAMES                            *PPPAYUTW
006000*                                                                *PPPAYUTW
006100*  PPPVPAY2_PAY          PPPVPAY2                                *PPPAYUTW
006200*                                                                *PPPAYUTW
006300******************************************************************PPPAYUTW
006400*                                                                 PPPAYUTW
006500 LINKAGE SECTION.                                                 PPPAYUTW
006600*                                                                 PPPAYUTW
006700******************************************************************PPPAYUTW
006800*                L I N K A G E   S E C T I O N                   *PPPAYUTW
006900******************************************************************PPPAYUTW
007000 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPPAYUTW
007100 01  PAY-ROW.                       COPY CPWSRPAY.                PPPAYUTW
007200*                                                                 PPPAYUTW
007300 PROCEDURE DIVISION USING                                         PPPAYUTW
007400                          PPXXXUTW-INTERFACE                      PPPAYUTW
007500                          PAY-ROW.                                PPPAYUTW
007600******************************************************************PPPAYUTW
007700*            P R O C E D U R E  D I V I S I O N                  *PPPAYUTW
007800******************************************************************PPPAYUTW
007900******************************************************************PPPAYUTW
008000*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO      *PPPAYUTW
008100*    CAUSE A BRANCH TO 999999-SQL-ERROR.                         *PPPAYUTW
008200******************************************************************PPPAYUTW
008300     EXEC SQL                                                     PPPAYUTW
008400          INCLUDE CPPDXE99                                        PPPAYUTW
008500     END-EXEC.                                                    PPPAYUTW
008600     SKIP3                                                        PPPAYUTW
008700 0100-MAINLINE SECTION.                                           PPPAYUTW
008800     SKIP1                                                        PPPAYUTW
008900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPPAYUTW
009000     MOVE EMPLOYEE-ID OF PAY-ROW TO WS-EMPLOYEE-ID.               PPPAYUTW
009100     SKIP1                                                        PPPAYUTW
009200     MOVE PAY-ROW TO PAY-ROW-DATA                                 PPPAYUTW
009300     SKIP1                                                        PPPAYUTW
009400     EVALUATE PPXXXUTW-FUNCTION                                   PPPAYUTW
009500         WHEN 'D'                                                 PPPAYUTW
009600             PERFORM 8100-DELETE-ROW                              PPPAYUTW
009700         WHEN 'U'                                                 PPPAYUTW
009800             PERFORM 8200-UPDATE-ROW                              PPPAYUTW
009900         WHEN 'I'                                                 PPPAYUTW
010000             PERFORM 8300-INSERT-ROW                              PPPAYUTW
010100         WHEN OTHER                                               PPPAYUTW
010200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPPAYUTW
010300             SET PPXXXUTW-ERROR            TO TRUE                PPPAYUTW
010400     END-EVALUATE.                                                PPPAYUTW
010500     SKIP1                                                        PPPAYUTW
010600     GOBACK.                                                      PPPAYUTW
010700     EJECT                                                        PPPAYUTW
010800******************************************************************PPPAYUTW
010900*  PROCEDURE SQL                     FOR PAY VIEW                *PPPAYUTW
011000******************************************************************PPPAYUTW
011100 8100-DELETE-ROW SECTION.                                         PPPAYUTW
011200     SKIP1                                                        PPPAYUTW
011300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPPAYUTW
011400     SKIP1                                                        PPPAYUTW
011500     EXEC SQL                                                     PPPAYUTW
011600         DELETE FROM PPPVPAY2_PAY                                 PPPAYUTW
011700                WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID               PPPAYUTW
011800     END-EXEC.                                                    PPPAYUTW
011900     SKIP1                                                        PPPAYUTW
012000     IF  SQLCODE    NOT = 0                                       PPPAYUTW
012100         SET PPXXXUTW-ERROR TO TRUE                               PPPAYUTW
012200     END-IF.                                                      PPPAYUTW
012300     EJECT                                                        PPPAYUTW
012400 8200-UPDATE-ROW SECTION.                                         PPPAYUTW
012500     SKIP1                                                        PPPAYUTW
012600     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPPAYUTW
012700     SKIP1                                                        PPPAYUTW
012800     EXEC SQL                                                     PPPAYUTW
012900     UPDATE PPPVPAY2_PAY                                          PPPAYUTW
013000              SET  CITIZEN_CODE       = :CITIZEN-CODE       ,     PPPAYUTW
013100                   VISA_TYPE          = :VISA-TYPE          ,     PPPAYUTW
013200                   COUNTRY_ORIGIN     = :COUNTRY-ORIGIN     ,     PPPAYUTW
013300                   ADDRESS_LINE1      = :ADDRESS-LINE1      ,     PPPAYUTW
013400                   ADDRESS_LINE2      = :ADDRESS-LINE2      ,     PPPAYUTW
013500                   ADDRESS_CITY       = :ADDRESS-CITY       ,     PPPAYUTW
013600                   ADDRESS_STATE      = :ADDRESS-STATE      ,     PPPAYUTW
013700                   ADDRESS_ZIP        = :ADDRESS-ZIP        ,     PPPAYUTW
013800                   FOREIGN_ADDR_IND   = :FOREIGN-ADDR-IND   ,     PPPAYUTW
013900                   SOC_SEC_NUM        = :SOC-SEC-NUM        ,     PPPAYUTW
014000                   W4_RECEIVED        = :W4-RECEIVED        ,     PPPAYUTW
014100                   FORM_8233_IND      = :FORM-8233-IND      ,     PPPAYUTW
014200                   FEDTAX_MARITAL     = :FEDTAX-MARITAL     ,     PPPAYUTW
014300                   FEDTAX_EXEMPT      = :FEDTAX-EXEMPT      ,     PPPAYUTW
014400                   MAXFED_EXEMPT      = :MAXFED-EXEMPT      ,     PPPAYUTW
014500                   STATE_TAX_MARITAL  = :STATE-TAX-MARITAL  ,     PPPAYUTW
014600                   STATE_TAX_PERDED   = :STATE-TAX-PERDED   ,     PPPAYUTW
014700                   STATE_TAX_ITMDED   = :STATE-TAX-ITMDED   ,     PPPAYUTW
014800                   STATE_TAX_MAXEXMPT = :STATE-TAX-MAXEXMPT ,     PPPAYUTW
014900                   FICA_ELIG_CODE     = :FICA-ELIG-CODE     ,     PPPAYUTW
015000                   BANK_TBL_KEY       = :BANK-TBL-KEY       ,     PPPAYUTW
015100                   BANK_ACCTNUM       = :BANK-ACCTNUM       ,     PPPAYUTW
015200                   SURE_ACCTNUM       = :SURE-ACCTNUM       ,     PPPAYUTW
015300                   SURE_TYPEIND       = :SURE-TYPEIND       ,     PPPAYUTW
015400                   SURE_PRENOTE_STAT  = :SURE-PRENOTE-STAT  ,     PPPAYUTW
015500                   SURE_CYCLEDATE     = :SURE-CYCLEDATE     ,     PPPAYUTW
015600                   SURE_ACTIVATE_CNT  = :SURE-ACTIVATE-CNT  ,     PPPAYUTW
015700                   SURE_PRENOTE_CYCLE = :SURE-PRENOTE-CYCLE ,     PPPAYUTW
015800                   SURE_BANK_KEY      = :SURE-BANK-KEY      ,     PPPAYUTW
015900                   CHK_STREET1        = :CHK-STREET1        ,     PPPAYUTW
016000                   CHK_STREET2        = :CHK-STREET2        ,     PPPAYUTW
016100                   CHK_CITY           = :CHK-CITY           ,     PPPAYUTW
016200                   CHK_STATE          = :CHK-STATE          ,     PPPAYUTW
016300                   CHK_ZIP            = :CHK-ZIP            ,     PPPAYUTW
016400                   PRIOR_EMP_ID       = :PRIOR-EMP-ID       ,     PPPAYUTW
016500                   UI_ELIG_CODE       = :UI-ELIG-CODE       ,     PPPAYUTW
016600                   TT_INCOME_CODE     = :TT-INCOME-CODE     ,     PPPAYUTW
016700                   ALT_TT_CODE        = :ALT-TT-CODE        ,     PPPAYUTW
016800                   TT_END_DATE        = :TT-END-DATE        ,     PPPAYUTW
016900                   TT_ARTICLE_NO      = :TT-ARTICLE-NO      ,     PPPAYUTW
017000                   TT_DOLLAR_LIMIT    = :TT-DOLLAR-LIMIT    ,     PPPAYUTW
018600                   NONUC_HLTH_EXPDATE = :NONUC-HLTH-EXPDATE       PPPAYUTW
018900                 , SURE_MAILDROP      = :SURE-MAILDROP            UCSD0034
018000                 , PRIOR_YR_FWT_GROSS = :PRIOR-YR-FWT-GROSS       03070649
018010                 , I_9_DATE           = :I-9-DATE                 02110712
019800                 , OTHER_STATE_CA_RES = :OTHER-STATE-CA-RES       15360842
019900                 , OTHER_STATE_NAME   = :OTHER-STATE-NAME         15360842
020800                 , RET_FICA_DERIVE    = :RET-FICA-DERIVE          17810964
019710                 , PRIOR_YR_TOTAL_GRS = :PRIOR-YR-TOTAL-GRS       49051194
019720                 , US_DATE_OF_ENTRY   = :US-DATE-OF-ENTRY         32171197
019730                 , W4_PROCESS_DATE    = :W4-PROCESS-DATE          48441325
019740                 , DE4_PROCESS_DATE   = :DE4-PROCESS-DATE         48441325
019750                 , TAX_PROCESSOR_ID   = :TAX-PROCESSOR-ID         48441325
019760                 , OTH_ST_LOC_TAX_IND = :OTH-ST-LOC-TAX-IND       48571379
019770                 , FINAL_PAY_IND      = :FINAL-PAY-IND            48681411
025900                 , FINAL_PAY_CYCLE    = :FINAL-PAY-CYCLE          48761492
026000                 , FINAL_PAY_DATE     = :FINAL-PAY-DATE           48761492
017200         WHERE EMPLOYEE_ID = :WS-EMPLOYEE-ID                      PPPAYUTW
017300     END-EXEC.                                                    PPPAYUTW
017400     SKIP1                                                        PPPAYUTW
017500     IF  SQLCODE    NOT = 0                                       PPPAYUTW
017600         SET PPXXXUTW-ERROR TO TRUE                               PPPAYUTW
017700     END-IF.                                                      PPPAYUTW
017800     EJECT                                                        PPPAYUTW
017900 8300-INSERT-ROW SECTION.                                         PPPAYUTW
018000     SKIP1                                                        PPPAYUTW
018100     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPPAYUTW
018200     SKIP1                                                        PPPAYUTW
018300     EXEC SQL                                                     PPPAYUTW
018400         INSERT INTO PPPVPAY2_PAY                                 PPPAYUTW
021100********                                                       CD 48441325
021110                VALUES                                            49051194
021120                   (:EMPLOYEE-ID                                  49051194
021130                   ,:CITIZEN-CODE                                 49051194
021140                   ,:VISA-TYPE                                    49051194
021150                   ,:COUNTRY-ORIGIN                               49051194
021160                   ,:ADDRESS-LINE1                                49051194
021170                   ,:ADDRESS-LINE2                                49051194
021180                   ,:ADDRESS-CITY                                 49051194
021190                   ,:ADDRESS-STATE                                49051194
021191                   ,:ADDRESS-ZIP                                  49051194
021192                   ,:FOREIGN-ADDR-IND                             49051194
021193                   ,:SOC-SEC-NUM                                  49051194
021194                   ,:W4-RECEIVED                                  49051194
021195                   ,:FORM-8233-IND                                49051194
021196                   ,:FEDTAX-MARITAL                               49051194
021197                   ,:FEDTAX-EXEMPT                                49051194
021198                   ,:MAXFED-EXEMPT                                49051194
021199                   ,:STATE-TAX-MARITAL                            49051194
021200                   ,:STATE-TAX-PERDED                             49051194
021201                   ,:STATE-TAX-ITMDED                             49051194
021202                   ,:STATE-TAX-MAXEXMPT                           49051194
021203                   ,:FICA-ELIG-CODE                               49051194
021204                   ,:BANK-TBL-KEY                                 49051194
021205                   ,:BANK-ACCTNUM                                 49051194
021206                   ,:SURE-ACCTNUM                                 49051194
021207                   ,:SURE-TYPEIND                                 49051194
021208                   ,:SURE-PRENOTE-STAT                            49051194
021209                   ,:SURE-CYCLEDATE                               49051194
021210                   ,:SURE-ACTIVATE-CNT                            49051194
021211                   ,:SURE-PRENOTE-CYCLE                           49051194
021212                   ,:SURE-BANK-KEY                                49051194
021213                   ,:CHK-STREET1                                  49051194
021214                   ,:CHK-STREET2                                  49051194
021215                   ,:CHK-CITY                                     49051194
021216                   ,:CHK-STATE                                    49051194
021217                   ,:CHK-ZIP                                      49051194
021218                   ,:PRIOR-EMP-ID                                 49051194
021219                   ,:UI-ELIG-CODE                                 49051194
021220                   ,:TT-INCOME-CODE                               49051194
021221                   ,:ALT-TT-CODE                                  49051194
021222                   ,:TT-END-DATE                                  49051194
021223                   ,:TT-ARTICLE-NO                                49051194
021224                   ,:TT-DOLLAR-LIMIT                              49051194
021225                   ,:NONUC-HLTH-EXPDATE                           49051194
021226                   ,:PRIOR-YR-FWT-GROSS                           49051194
021227                   ,:I-9-DATE                                     49051194
010710                   ,:SURE-VALID                                   UCSD0034
010720                   ,:SURE-TRANSIT                                 UCSD0034
010730                   ,:SURE-MAILDROP                                UCSD0034
010740                   ,:SURE-CHECK-DISP                              UCSD0034
021228                   ,:OTHER-STATE-CA-RES                           49051194
021229                   ,:OTHER-STATE-NAME                             49051194
021230                   ,:RET-FICA-DERIVE                              49051194
021231                   ,:PRIOR-YR-TOTAL-GRS                           49051194
021233                   ,:US-DATE-OF-ENTRY                             32171197
021234                   ,:W4-PROCESS-DATE                              48441325
021235                   ,:DE4-PROCESS-DATE                             48441325
021236                   ,:TAX-PROCESSOR-ID                             48441325
021237                   ,:OTH-ST-LOC-TAX-IND                           48571379
021238                   ,:FINAL-PAY-IND                                48681411
033000                   ,:FINAL-PAY-CYCLE                              48761492
033100                   ,:FINAL-PAY-DATE                               48761492
021232                   )                                              49051194
018600     END-EXEC.                                                    PPPAYUTW
018700     SKIP1                                                        PPPAYUTW
018800     IF  SQLCODE    NOT = 0                                       PPPAYUTW
018900         SET PPXXXUTW-ERROR TO TRUE                               PPPAYUTW
019000     END-IF.                                                      PPPAYUTW
019100     EJECT                                                        PPPAYUTW
019200*999999-SQL-ERROR.                                               *PPPAYUTW
019300     SKIP1                                                        PPPAYUTW
019400     EXEC SQL                                                     PPPAYUTW
019500          INCLUDE CPPDXP99                                        PPPAYUTW
019600     END-EXEC.                                                    PPPAYUTW
019700     SKIP1                                                        PPPAYUTW
019800     SET PPXXXUTW-ERROR TO TRUE.                                  PPPAYUTW
019900     IF NOT HERE-BEFORE                                           PPPAYUTW
020000         SET HERE-BEFORE TO TRUE                                  PPPAYUTW
020100     GOBACK.                                                      PPPAYUTW
