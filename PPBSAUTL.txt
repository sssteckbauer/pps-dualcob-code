000001**************************************************************/   36220651
000002*  PROGRAM:  PPBSAUTL                                        */   36220651
000003*  RELEASE # __0651____   SERVICE REQUEST NO(S)____3622______*/   36220651
000004*  NAME _______KXK_____   MODIFICATION DATE ____04/13/92__   */   36220651
000005*  DESCRIPTION                                               */   36220651
000006*                                                            */   36220651
000007*  ADDED DEDUCTION EFFECTIVE DATE TO REFLECT ADDITION OF     */   36220651
000008*  DED_EFF_DATE ON BRS TABLE.                                */   36220651
000009**************************************************************/   36220651
000001**************************************************************/   36190563
000002*  PROGRAM:  PPBSAUTL                                        */   36190563
000003*  RELEASE # ___0563___   SERVICE REQUEST NO(S)____3619______*/   36190563
000004*  NAME __C RIDLON_____   MODIFICATION DATE ____04/29/91_____*/   36190563
000005*  DESCRIPTION                                               */   36190563
000006*                                                            */   36190563
000007*  CHANGED TO DO CALLS NOT USING LITERALS                    */   36190563
000008*                                                            */   36190563
000009**************************************************************/   36190563
000010**************************************************************/  *36090496
000020*  PROGRAM: PPBSAUTL                                         */  *36090496
000030*  RELEASE: ____0496____  SERVICE REQUEST(S): ____3609____   */  *36090496
000040*  NAME:___P_PARKER_______CREATION DATE:      __08/29/90__   */  *36090496
000050*  DESCRIPTION:                                              */  *36090496
000060*  - MODULE BASED ON PPBENUTL, CODED   FOR DB2 CONVERSION.   */  *36090496
000070**************************************************************/  *36090496
000100 IDENTIFICATION DIVISION.                                         PPBSAUTL
000200 PROGRAM-ID. PPBSAUTL                                             PPBSAUTL
000300 AUTHOR. UCOP.                                                    PPBSAUTL
000400 DATE-WRITTEN.  AUGUST 1990                                       PPBSAUTL
000500 DATE-COMPILED.                                                   PPBSAUTL
000600*REMARKS.                                                         PPBSAUTL
000700     EJECT                                                        PPBSAUTL
000800 ENVIRONMENT DIVISION.                                            PPBSAUTL
000900 CONFIGURATION SECTION.                                           PPBSAUTL
001000 SOURCE-COMPUTER.        COPY CPOTXUCS.                           PPBSAUTL
001100 OBJECT-COMPUTER.        COPY CPOTXOBJ.                           PPBSAUTL
001200      EJECT                                                       PPBSAUTL
001300*                D A T A  D I V I S I O N                         PPBSAUTL
001400*                                                                 PPBSAUTL
001500 DATA DIVISION.                                                   PPBSAUTL
001600 WORKING-STORAGE SECTION.                                         PPBSAUTL
001700 01  GENERAL-WORK-AREA.                                           PPBSAUTL
001800     05  GWA-PROGRAM-ID          PIC X(15)       VALUE            PPBSAUTL
001900         'PPBSAUTL/082990'.                                       PPBSAUTL
001910     05  WS-PPBRSUTL             PIC X(8)    VALUE 'PPBRSUTL'.    36190563
001920     05  WS-INIT-DATE-ISO        PIC X(10)   VALUE '0001-01-01'.  36220651
002000*                                                                 PPBSAUTL
002100 01  PPBRSUTL-INTERFACE.             COPY CPLNKBRS.               PPBSAUTL
002200******************************************************************PPBSAUTL
002300*            PAYROLL CONSTANTS                                   *PPBSAUTL
002400******************************************************************PPBSAUTL
002500*                                                                 PPBSAUTL
002600 01  PAYROLL-CONSTANTS.              COPY CPWSXIC2.               PPBSAUTL
002700     EJECT                                                        PPBSAUTL
002800******************************************************************PPBSAUTL
002900*                L I N K A G E   S E C T I O N                   *PPBSAUTL
003000******************************************************************PPBSAUTL
003100*                                                                 PPBSAUTL
003200 LINKAGE SECTION.                                                 PPBSAUTL
003300*                                                                 PPBSAUTL
003400 01  PPBSAUTL-INTERFACE.             COPY CPLNKBSA.               PPBSAUTL
003500*                                                                 PPBSAUTL
003600     EJECT                                                        PPBSAUTL
003700 01  BENEFIT-SEGMENT-ARRAY.          COPY CPWSXBSA.               PPBSAUTL
003800*                                                                 PPBSAUTL
003900     EJECT                                                        PPBSAUTL
004000*                                                                 PPBSAUTL
004100******************************************************************PPBSAUTL
004200*            P R O C E D U R E  D I V I S I O N                  *PPBSAUTL
004300******************************************************************PPBSAUTL
004400*                                                                 PPBSAUTL
004500 PROCEDURE DIVISION USING PPBSAUTL-INTERFACE                      PPBSAUTL
004600                          BENEFIT-SEGMENT-ARRAY.                  PPBSAUTL
004700 MAINLINE-000 SECTION.                                            PPBSAUTL
004800 ENTRY-BEGIN-010.                                                 PPBSAUTL
004900     INITIALIZE PPBSAUTL-ERROR-INDICATORS.                        PPBSAUTL
005000     INITIALIZE PPBRSUTL-INTERFACE.                               PPBSAUTL
005100     MOVE PPBSAUTL-EMPLOYEE-ID TO PPBRSUTL-EMPLOYEE-ID.           PPBSAUTL
005200     PERFORM CLEAR-ARRAY-151                                      PPBSAUTL
005300       VARYING XBSA-CLEAR-SUB FROM +1 BY +1                       PPBSAUTL
005400              UNTIL XBSA-CLEAR-SUB > IDC-MAX-NO-GTN.              PPBSAUTL
005500******************************************************************PPBSAUTL
005600*    CALL PPBRSUTL TO LOAD BRS TABLE ARRAY                        PPBSAUTL
005700******************************************************************PPBSAUTL
005800**** CALL 'PPBRSUTL' USING                                        36190563
005810     CALL WS-PPBRSUTL USING                                       36190563
005900         PPBRSUTL-INTERFACE.                                      PPBSAUTL
006000     IF PPBRSUTL-ERROR                                            PPBSAUTL
006100         SET PPBSAUTL-ERROR TO TRUE                               PPBSAUTL
006200     ELSE                                                         PPBSAUTL
006300        PERFORM LOAD-BENEFIT-ARRAY-030                            PPBSAUTL
006400           VARYING XBSA-ENTRY-CNT FROM +1 BY +1                   PPBSAUTL
006500             UNTIL XBSA-ENTRY-CNT > IDC-MAX-BRSC-SEG-ENTRIES      PPBSAUTL
006600               OR  PPBSAUTL-ERROR                                 PPBSAUTL
006700               OR  XBSA-ENTRY-CNT > PPBRSUTL-ROW-COUNT.           PPBSAUTL
006800     GOBACK.                                                      PPBSAUTL
006900     EJECT                                                        PPBSAUTL
007000*                                                                 PPBSAUTL
007100******************************************************************PPBSAUTL
007200*                                                                 PPBSAUTL
007300 LOAD-BENEFIT-ARRAY-030 SECTION.                                  PPBSAUTL
007400 LOAD-ARRAY-ENTRY-030.                                            PPBSAUTL
007500     MOVE BENRATE-GTN (XBSA-ENTRY-CNT) TO XBSA-LOAD-SUB.          PPBSAUTL
007600     IF XBSA-LOAD-SUB > IDC-MAX-NO-GTN                            PPBSAUTL
007700         SET PPBSAUTL-INVALID-GTN TO TRUE                         PPBSAUTL
007800         SET PPBSAUTL-ERROR       TO TRUE                         PPBSAUTL
007900     ELSE                                                         PPBSAUTL
008000         MOVE BENRATE-TUC    (XBSA-ENTRY-CNT) TO                  PPBSAUTL
008100           XBSA-BRSC-TUC   (XBSA-LOAD-SUB)                        PPBSAUTL
008200         MOVE BENRATE-REP-CODE (XBSA-ENTRY-CNT) TO                PPBSAUTL
008300           XBSA-BRSC-REP  (XBSA-LOAD-SUB)                         PPBSAUTL
008400         MOVE BENRATE-SPCL-HNDLG (XBSA-ENTRY-CNT) TO              PPBSAUTL
008500           XBSA-BRSC-SHC     (XBSA-LOAD-SUB)                      PPBSAUTL
008600         MOVE BENRATE-DUC (XBSA-ENTRY-CNT) TO                     PPBSAUTL
008700***********XBSA-BRSC-DUC (XBSA-LOAD-SUB).                         36220651
008701           XBSA-BRSC-DUC (XBSA-LOAD-SUB)                          36220651
008710         MOVE DED-EFF-DATE (XBSA-ENTRY-CNT)                       36220651
008720           TO XBSA-DED-EFF-DATE (XBSA-LOAD-SUB).                  36220651
008800*                                                                 PPBSAUTL
008900 LOAD-ARRAY-EXIT-030.                                             PPBSAUTL
009000     EXIT.                                                        PPBSAUTL
009100******************************************************************PPBSAUTL
009200*         CLEAR THE BENEFIT ARRAY TABLE                           PPBSAUTL
009300******************************************************************PPBSAUTL
009400*                                                                 PPBSAUTL
009500 CLEAR-ARRAY-151 SECTION.                                         PPBSAUTL
009600 CLEAR-ARRAY-ENTRY-151.                                           PPBSAUTL
009700     MOVE XBSA-CLEAR-ENTRY TO                                     PPBSAUTL
009800           XBSA-BRSC-ARRAY (XBSA-CLEAR-SUB).                      PPBSAUTL
009900*                                                                 PPBSAUTL
009910     MOVE WS-INIT-DATE-ISO TO                                     36220651
009920           XBSA-DED-EFF-DATE (XBSA-CLEAR-SUB).                    36220651
009930*                                                                 36220651
010000 CLEAR-ARRAY-EXIT-151.                                            PPBSAUTL
010100     EXIT.                                                        PPBSAUTL
