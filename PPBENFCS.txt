000100**************************************************************/   36400799
000200*  PROGRAM:  PPBENFCS                                        */   36400799
000300*  RELEASE # __0799____   SERVICE REQUEST NO(S) __3640______ */   36400799
000400*  NAME _______SRS_____   MODIFICATION DATE ____10/14/92_____*/   36400799
000500*  DESCRIPTION                                               */   36400799
000600*  - NEW PPBENFCS: DB2 ACCESS AND DUAL USE FOR RUSH CHECKS   */   36400799
000700*  THIS MODULE PERFORMS FCSRS RETIREMENT CALCULATIONS     G  */   36400799
000800**************************************************************/   36400799
000900 IDENTIFICATION DIVISION.                                         PPBENFCS
001000 PROGRAM-ID. PPBENFCS.                                            PPBENFCS
001100 DATE-COMPILED.                                                   PPBENFCS
001200 DATA DIVISION.                                                   PPBENFCS
001300 WORKING-STORAGE SECTION.                                         PPBENFCS
001400                                                                  PPBENFCS
001500 01  PPBENFCS-INTERFACE EXTERNAL.    COPY 'CPLNKFCS'.             PPBENFCS
001600 01  WORK-AREA.                                                   PPBENFCS
001700     05  PRM-DATA-FLAG               PIC X    VALUE 'N'.          PPBENFCS
001800         88  PRM-DATA-ACCESSED                VALUE 'Y'.          PPBENFCS
001900                                                                  PPBENFCS
002000 01  WS-PRM.                                                      PPBENFCS
002100     EXEC SQL                                                     PPBENFCS
002200          INCLUDE PPPVZPRM                                        PPBENFCS
002300     END-EXEC.                                                    PPBENFCS
002400     EXEC SQL                                                     PPBENFCS
002500          INCLUDE SQLCA                                           PPBENFCS
002600     END-EXEC.                                                    PPBENFCS
002700                                                                  PPBENFCS
002800 PROCEDURE DIVISION.                                              PPBENFCS
002900                                                                  PPBENFCS
003000 A000-MAINLINE SECTION.                                           PPBENFCS
003100                                                                  PPBENFCS
003200     IF NOT PRM-DATA-ACCESSED                                     PPBENFCS
003300        SET PRM-DATA-ACCESSED TO TRUE                             PPBENFCS
003400        PERFORM P050-ACCESS-PRM                                   PPBENFCS
003500     END-IF.                                                      PPBENFCS
003600                                                                  PPBENFCS
003700     IF  KFCS-INVALID-RETIREMENT-GROSS                            PPBENFCS
003800      OR NOT KFCS-VALID-SYSTEM-CODE                               PPBENFCS
003900         SET KFCS-INVALID-LOOKUP-ARG TO TRUE                      PPBENFCS
004000     ELSE                                                         PPBENFCS
004100         MOVE '0' TO KFCS-INVALID-LOOKUP-ARG-FLAG                 PPBENFCS
004200     END-IF.                                                      PPBENFCS
004300                                                                  PPBENFCS
004400     IF  KFCS-RETURN-STATUS-ABORTING                              PPBENFCS
004500      OR KFCS-INVALID-LOOKUP-ARG                                  PPBENFCS
004600         MOVE ZERO TO KFCS-RATE-AMOUNT                            PPBENFCS
004700                      KFCS-DED-AMOUNT                             PPBENFCS
004800     ELSE                                                         PPBENFCS
004900         MOVE PRM-DATA TO KFCS-RATE-AMOUNT                        PPBENFCS
005000         COMPUTE KFCS-DED-AMOUNT ROUNDED                          PPBENFCS
005100               = KFCS-RETIREMENT-GROSS * PRM-DATA / 100           PPBENFCS
005200     END-IF.                                                      PPBENFCS
005300                                                                  PPBENFCS
005400     GOBACK.                                                      PPBENFCS
005500                                                                  PPBENFCS
005600 P050-ACCESS-PRM SECTION.                                         PPBENFCS
005700                                                                  PPBENFCS
005800     EXEC SQL                                                     PPBENFCS
005900         SELECT  PRM_DATA                                         PPBENFCS
006000           INTO :PRM-DATA                                         PPBENFCS
006100           FROM  PPPVZPRM_PRM                                     PPBENFCS
006200          WHERE  PRM_NUMBER = 245                                 PPBENFCS
006300     END-EXEC.                                                    PPBENFCS
006400                                                                  PPBENFCS
006500     IF   SQLCODE = ZERO                                          PPBENFCS
006600      AND PRM-DATA NOT = ZERO                                     PPBENFCS
006700          SET KFCS-RETURN-STATUS-NORMAL TO TRUE                   PPBENFCS
006800     ELSE                                                         PPBENFCS
006900          SET KFCS-RETURN-STATUS-ABORTING TO TRUE                 PPBENFCS
007000     END-IF.                                                      PPBENFCS
