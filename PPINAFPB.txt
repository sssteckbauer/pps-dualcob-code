003200**************************************************************/   36430795
003300*  PROGRAM: PPINAFPB                                         */   36430795
003400*  RELEASE # ___0795___   SERVICE REQUEST NO(S) ___3643____  */   36430795
003500*  NAME ____ET_________   MODIFICATION DATE ____07/15/93_____*/   36430795
003600*  DESCRIPTION                                               */   36430795
003700*              EDB ENTRY UPDATE SYSTEM                       */   36430795
003800*    MVS BATCH I/O MODULE FOR THE ACCOUNT-FUND PROFILE       */   36430795
003810*    INTERFACE PROGRAM (PPINAFP)                             */   36430795
004000**************************************************************/   36430795
004100 IDENTIFICATION DIVISION.                                         PPINAFPB
004200 PROGRAM-ID. PPINAFPB.                                            PPINAFPB
005200 DATE-WRITTEN.                                                    PPINAFPB
005300 DATE-COMPILED.                                                   PPINAFPB
005400     EJECT                                                        PPINAFPB
005600 ENVIRONMENT DIVISION.                                            PPINAFPB
005700 CONFIGURATION SECTION.                                           PPINAFPB
005800 SOURCE-COMPUTER.                                                 PPINAFPB
005900 COPY CPOTXUCS.                                                   PPINAFPB
005910 OBJECT-COMPUTER.                                                 PPINAFPB
005920 COPY CPOTXOBJ.                                                   PPINAFPB
006000     EJECT                                                        PPINAFPB
006100 INPUT-OUTPUT SECTION.                                            PPINAFPB
006200 FILE-CONTROL.                                                    PPINAFPB
006700     SKIP3                                                        PPINAFPB
006800     SELECT ACCOUNT-FILE                                          PPINAFPB
006900         ASSIGN TO VSAM-ACCTFILE                                  PPINAFPB
007000         ORGANIZATION IS INDEXED                                  PPINAFPB
007100         ACCESS IS RANDOM                                         PPINAFPB
007200         RECORD KEY IS ACCT-KEY                                   PPINAFPB
007400         FILE STATUS IS CPWSAFPX-VSAM-STATUS.                     PPINAFPB
007900     SKIP3                                                        PPINAFPB
008000     SELECT FUND-FILE                                             PPINAFPB
008100         ASSIGN TO VSAM-FUNDFILE                                  PPINAFPB
008200         ORGANIZATION IS INDEXED                                  PPINAFPB
008300         ACCESS IS RANDOM                                         PPINAFPB
008400         RECORD KEY IS FUND-KEY                                   PPINAFPB
008600         FILE STATUS IS CPWSAFPX-VSAM-STATUS.                     PPINAFPB
009100     SKIP3                                                        PPINAFPB
009200     SELECT ACCOUNT-FUND-FILE                                     PPINAFPB
009300         ASSIGN TO VSAM-ACCTFUND                                  PPINAFPB
009400         ORGANIZATION IS INDEXED                                  PPINAFPB
009500         ACCESS IS RANDOM                                         PPINAFPB
009600         RECORD KEY IS ACFF-KEY                                   PPINAFPB
009800         FILE STATUS IS CPWSAFPX-VSAM-STATUS.                     PPINAFPB
010300     EJECT                                                        PPINAFPB
010400 DATA DIVISION.                                                   PPINAFPB
010500 FILE SECTION.                                                    PPINAFPB
010600     SKIP3                                                        PPINAFPB
010700 FD  ACCOUNT-FILE                    COPY CPFDXACC.               PPINAFPB
010800     SKIP3                                                        PPINAFPB
010900 FD  FUND-FILE                       COPY CPFDXFND.               PPINAFPB
011000     SKIP3                                                        PPINAFPB
011100 FD  ACCOUNT-FUND-FILE               COPY CPFDXAFF.               PPINAFPB
011200     EJECT                                                        PPINAFPB
011300 WORKING-STORAGE SECTION.                                         PPINAFPB
011400     SKIP3                                                        PPINAFPB
011600 01  CPWSAFPX EXTERNAL.                                           PPINAFPB
011700     COPY CPWSAFPX.                                               PPINAFPB
016200     EJECT                                                        PPINAFPB
016300 LINKAGE SECTION.                                                 PPINAFPB
016800     EJECT                                                        PPINAFPB
016900 PROCEDURE DIVISION.                                              PPINAFPB
017000     SKIP1                                                        PPINAFPB
017100******************************************************************PPINAFPB
017110*********                                                  *******PPINAFPB
017120*********                E X E C U T I V E                 *******PPINAFPB
017122*********                  R O U T I N E                   *******PPINAFPB
017130*********                                                  *******PPINAFPB
017140******************************************************************PPINAFPB
017141                                                                  PPINAFPB
017151     EVALUATE TRUE                                                PPINAFPB
017170         WHEN CPWSAFPX-OPEN-ACCT-FILE                             PPINAFPB
017171              OPEN INPUT ACCOUNT-FILE                             PPINAFPB
017172                                                                  PPINAFPB
017173         WHEN CPWSAFPX-OPEN-FUND-FILE                             PPINAFPB
017174              OPEN INPUT FUND-FILE                                PPINAFPB
017175                                                                  PPINAFPB
017180         WHEN CPWSAFPX-OPEN-ACCT-FUND-FILE                        PPINAFPB
017181              OPEN INPUT ACCOUNT-FUND-FILE                        PPINAFPB
017182                                                                  PPINAFPB
017183         WHEN CPWSAFPX-READ-ACCT-FILE                             PPINAFPB
017184              MOVE CPWSAFPX-ACCT-KEY TO  ACCT-KEY                 PPINAFPB
017185              READ ACCOUNT-FILE INTO CPWSAFPX-ACCT-REC            PPINAFPB
017186                                                                  PPINAFPB
017187         WHEN CPWSAFPX-READ-FUND-FILE                             PPINAFPB
017188              MOVE CPWSAFPX-FUND-KEY TO FUND-KEY                  PPINAFPB
017189              READ FUND-FILE INTO CPWSAFPX-FUND-REC               PPINAFPB
017190                                                                  PPINAFPB
017191         WHEN CPWSAFPX-READ-ACCT-FUND-FILE                        PPINAFPB
017192              MOVE CPWSAFPX-ACCT-FUND-KEY TO  ACFF-KEY            PPINAFPB
017193              READ ACCOUNT-FUND-FILE INTO CPWSAFPX-ACCT-FUND-REC  PPINAFPB
017194                                                                  PPINAFPB
017195         WHEN CPWSAFPX-CLOSE-ACCT-FILE                            PPINAFPB
017196              CLOSE ACCOUNT-FILE                                  PPINAFPB
017197                                                                  PPINAFPB
017198         WHEN CPWSAFPX-CLOSE-FUND-FILE                            PPINAFPB
017199              CLOSE FUND-FILE                                     PPINAFPB
017200                                                                  PPINAFPB
017201         WHEN CPWSAFPX-CLOSE-ACCT-FUND-FILE                       PPINAFPB
017202              CLOSE ACCOUNT-FUND-FILE                             PPINAFPB
017203                                                                  PPINAFPB
017204     END-EVALUATE.                                                PPINAFPB
017205                                                                  PPINAFPB
017210     GOBACK.                                                      PPINAFPB
