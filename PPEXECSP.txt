000100*==========================================================%      UCSD9999
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000400*==========================================================%      UCSD9999
000500*==========================================================%      UCSD0041
000600*=   PROGRAM: PPEXECSP                                    =%      UCSD0041
000700*=   CHANGE: ___0041____  SERVICE REQUEST(S): ___DT-041___=%      UCSD0041
000800*=   NAME:   JIM PIERCE   MODIFICATION DATE:  __01/28/92__=%      UCSD0041
000900*=   DESCRIPTION:                                         =%      UCSD0041
000900*=   NAME:   G. CHIU   7/8/96                             =%      UCSD0041
000900*=   DESCRIPTION:                                         =%      UCSD0041
000900*=   CHANGES TO ACCOMMODATE RELEASE 1068 HERMI            =%      UCSD0041
000900*=   IMPLEMENTATION                                       =%      UCSD0041
000900*=                                                        =%      UCSD0041
000900*=   DESCRIPTION:                                         =%      UCSD0041
001000*=   PREVENT APPTS WITH NON-EXECUTIVE PERSONNEL PROGRAM   =%      UCSD0041
001100*=   CODE FROM POSSIBLY CAUSING EMPLOYEE TO BE            =%      UCSD0041
001200*=   INELIGIBLE FOR EXECUTIVE LIFE SEVERANCE PAY.         =%      UCSD0041
001300*==========================================================%      UCSD0041
000000**************************************************************/   76871348
000001*  PROGRAM: PPEXECSP                                         */   76871348
000002*  RELEASE: ___1348______ SERVICE REQUEST(S): ____17687____  */   76871348
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/05/01__  */   76871348
000004*  DESCRIPTION:                                              */   76871348
000005*  - SALARY GRADES '1', '2', '3', AND '4' HAVE BEEN          */   76871348
000006*    INCLUDED IN SENIOR MANAGEMENT TITLES 0001-0199.         */   76871348
000007*                                                            */   76871348
000008*    CURRENTLY, THE LOWEST GRADE IS 'A' TO HIGHEST GRADE 'E'.*/   76871348
000009*    NEW GRADES 1 THROUGH 4 ARE CONSIDERED LOWERED THAN      */   76871348
000010*    GRADE 'A'. THEREFORE, THE LOGIC THAT DETERMINES HIGHEST */   76871348
000011*    GRADE MUST BE CHANGED.                                  */   76871348
000012**************************************************************/   76871348
002000**************************************************************/   48261289
002100*  PROGRAM: PPEXECSP                                         */   48261289
002200*  RELEASE: ___1289______ SERVICE REQUEST(S): ____14826____  */   48261289
002300*  NAME:___ROY STAPLES___ MODIFICATION DATE:  ___10/22/99__  */   48261289
002400*  DESCRIPTION:                                              */   48261289
002500*  - MODIFIED EDIT OF GRADE IN PARAGRAPH 1000 TO COMPARE     */   48261289
002600*  ONLY FIRST BYTE OF GRADE AGAINST PREVIOUS HIGHEST VALUE.  */   48261289
002700*  THE ONLY GRADE VALUES THAT APPLY TO EXEC. SEV. PAY ARE    */   48261289
002800*  A, B, C, D, AND E.  AS A RESULT, THE CPLNKESP FIELD,      */   48261289
002900*  XESP-ELIGIBILITY-FLAG, WILL NOT REQUIRE EXPANSION.        */   48261289
003000*                                                            */   48261289
003100***  PLEASE NOTE THAT ANY FUTURE CHANGE TO THE EXECUTIVE   ***/   48261289
003200***  SEVERENCE PAY PROGRAM THAT REQUIRES EXPANSION OF THE  ***/   48261289
003300***  GRADE FIELD WILL ALSO REQUIRE THE FOLLOWING CHANGES:  ***/   48261289
003400***  1. EXPANDING THE XESP-ELIGIBILITY-FLAG FIELD IN THE   ***/   48261289
003500***     CPLNKESP COPY MEMBER,                              ***/   48261289
003600***  2. EXPANDING THE COLUMN EXEC_SPP_FLAG AND ALL         ***/   48261289
003700***     ASSOCIATED FIELDS IN COPY MEMBERS AND INCLUDES.    ***/   48261289
003800**************************************************************/   48261289
000100**************************************************************/   28201068
000200*  PROGRAM: PPEXECSP                                         */   28201068
000300*  RELEASE: ___1068______ SERVICE REQUEST(S): ____12820____  */   28201068
000400*  NAME:______M SANO_____ MODIFICATION DATE:  ___06/14/96__  */   28201068
000500*  DESCRIPTION:                                              */   28201068
000600*  - CHANGED REFERENCE TO PERSONNEL PRGM 'E' TO '2' PLUS     */   28201068
000700*    SENIOR MANAGEMENT TITLES                                */   28201068
000800**************************************************************/   28201068
000000**************************************************************/   28521025
000001*  PROGRAM: PPEXECSP                                         */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/01/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE DATE COMPARISON LOGICS HAVE BEEN MODIFIED TO INCLUDE*/   28521025
000007*    THE CENTURY IN THE DATE COMPARES.                       */   28521025
000008**************************************************************/   28521025
000100******************************************************************36090574
000200*  PROGRAM: PPEXECSP                                             *36090574
000300*  RELEASE: ____0574____  SERVICE REQUEST(S): ____3609____       *36090574
000400*  NAME:_PXP______________MODIFICATION DATE:__06/05/91____       *36090574
000500*  DESCRIPTION:                                                  *36090574
000600*  - CHANGED FOR EDB CONVERSION FROM PPIOEDB TO NATIVE DB2       *36090574
000700******************************************************************36090574
000701**************************************************************/   40280572
000702*  PROGRAM: PPEXECSP                                         */   40280572
000703*  RELEASE: ____0572____  SERVICE REQUEST(S): ____4028____   */   40280572
001100*  NAME:    ____JCW_____  MODIFICATION DATE:  __05/29/91__   */   40280572
001101*  DESCRIPTION:                                              */   40280572
001102*                                                            */   40280572
001103*    - PPEXECSP IS A NEW MODULE WHICH DETERMINES ELIGIBILTY  */   40280572
001104*      FOR THE EXECUTIVE SEVERANCE PAY PROGRAM.              */   40280572
001105**************************************************************/   40280572
001106 IDENTIFICATION DIVISION.                                         PPEXECSP
001107 PROGRAM-ID. PPEXECSP.                                            PPEXECSP
001200 INSTALLATION. UNIVERSITY OF CALIFORNIA, BASE PAYROLL SYSTEM.     PPEXECSP
001300 AUTHOR.       JEROME C. WILCOX, UCOP IS&C.                       PPEXECSP
001400 DATE-WRITTEN. APRIL 29, 1991.                                    PPEXECSP
001500 DATE-COMPILED.                                                   PPEXECSP
001600***************************************************************   PPEXECSP
001700*****                                                     *****   PPEXECSP
001800*****  CALLING PROGRAM PASSES INFORMATION VIA COPY MEMBER *****   PPEXECSP
001900*****  CPLNKESP AND ALSO PASSES IN THE EMPLOYEE'S SEGMENT *****   PPEXECSP
002000*****  SET USING COPY MEMBER CPWSXEDB.  NO ACTUAL DATA    *****   PPEXECSP
002100*****  ELEMENTS WITHIN CPWSXEDB ARE ALTERED BY PPEXECSP.  *****   PPEXECSP
002200*****  ALL RETURNED VALUES ARE PASSED BACK IN CPLNKESP.   *****   PPEXECSP
002300*****                                                     *****   PPEXECSP
002400*****  THE PROGRAM ANALYZES THE EMPLOYEE'S APPOINTMENT    *****   PPEXECSP
002500*****  STRUCTURE TO DETERMINE ELIGIBILITY FOR THE         *****   PPEXECSP
002600*****  EXECUTIVE SEVERANCE PAY PROGRAM.  THE ELIGIBILITY  *****   PPEXECSP
002700*****  RULES ARE THAT THE EMPLOYEE MUST BE A FULL-TIME,   *****   PPEXECSP
002800*****  PERMANENTLY APPOINTED MEMBER OF THE EXECUTIVE      *****   PPEXECSP
002900*****  PROGRAM, WITH THE APPOINTMENT BEING IN A GRADE     *****   PPEXECSP
003000*****  OTHER THAN 'X'.  THE FOLLOWING RULES ARE USED:     *****   PPEXECSP
003100*****                                                     *****   PPEXECSP
003200*****    1. EXCLUDE FROM EXAMINATION, ANY APPOINTMENT     *****   PPEXECSP
003300*****       WHICH IS:                                     *****   PPEXECSP
003400*****         * WITHOUT SALARY                            *****   PPEXECSP
003500*****         * BY AGREEMENT                              *****   PPEXECSP
003600*****         * INACTIVE                                  *****   PPEXECSP
003700*****    2. THE EMPLOYEE IS INELIGIBLE IF ANY ONE OF THE  *****   PPEXECSP
003800*****       REMAINING APPOINTMENTS:                       *****   PPEXECSP
003900*****         * IS NOT AN EXECUTIVE APPOINTMENT           *****   PPEXECSP
004000*****         * IS AN ACTING EXECUTIVE APPOINTMENT        *****   PPEXECSP
004100*****         * HAS A GRADE OF 'X'                        *****   PPEXECSP
004200*****                                                     *****   PPEXECSP
004300*****  ELIGIBILITY FOR PARTICIPATING IN THE PROGRAM IS    *****   PPEXECSP
004400*****  DETERMINED AS OF THE END OF THE PROCESS MONTH.     *****   PPEXECSP
004500*****                                                     *****   PPEXECSP
004600*****  NOTE THAT AT THE TIME THE EMPLOYEE'S CONTRIBUTION  *****   PPEXECSP
004700*****  WITHIN THE PROGRAM IS CALCULATED, A FURTHER RULE   *****   PPEXECSP
004800*****  REQUIRES THAT THE EMPLOYEE HAVE ACTUALLY BEEN PAID *****   PPEXECSP
004900*****  100% TIME FOR THE MONTH. THIS MODULE DOES NOT      *****   PPEXECSP
005000*****  ADDRESS THAT REQUIREMENT, BEING CONCERNED ONLY     *****   PPEXECSP
005100*****  WITH THE APPOINTMENT-LEVEL ELIGIBILITY.            *****   PPEXECSP
005200*****                                                     *****   PPEXECSP
005300***************************************************************   PPEXECSP
005400     EJECT                                                        PPEXECSP
005500 ENVIRONMENT DIVISION.                                            PPEXECSP
005600 CONFIGURATION SECTION.                                           PPEXECSP
005700 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       PPEXECSP
005800 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       PPEXECSP
005900 SPECIAL-NAMES.                                                   PPEXECSP
006000 INPUT-OUTPUT SECTION.                                            PPEXECSP
006100 FILE-CONTROL.                                                    PPEXECSP
006200     SKIP3                                                        PPEXECSP
006300******************************************************************PPEXECSP
006400*                                                                *PPEXECSP
006500*                         DATA DIVISION                          *PPEXECSP
006600*                                                                *PPEXECSP
006700******************************************************************PPEXECSP
006800 DATA DIVISION.                                                   PPEXECSP
006900 FILE SECTION.                                                    PPEXECSP
007000     EJECT                                                        PPEXECSP
007100******************************************************************PPEXECSP
007200*                                                                *PPEXECSP
007300*                    WORKING-STORAGE SECTION                     *PPEXECSP
007400*                                                                *PPEXECSP
007500******************************************************************PPEXECSP
007600 WORKING-STORAGE SECTION.                                         PPEXECSP
007700     SKIP3                                                        PPEXECSP
007800 01  WS-SUBSCRIPTS     COMP.                                      PPEXECSP
007900     05  APPT-SUB                      PIC S9(04)    VALUE  +0.   PPEXECSP
008000     EJECT                                                        PPEXECSP
008100 01  WS-SWITCHES.                                                 PPEXECSP
008200     05  WS-EXECUTIVE-APPT-SW          PIC  X(01)    VALUE  'N'.  PPEXECSP
008300         88  WS-EXECUTIVE-APPT                       VALUE  'Y'.  PPEXECSP
008400     05  WS-INELIGIBLE-APPT-SW         PIC  X(01)    VALUE  'N'.  PPEXECSP
008500         88  WS-INELIGIBLE-APPT                      VALUE  'Y'.  PPEXECSP
008600     EJECT                                                        PPEXECSP
008700 01  WS-WORK-AREAS.                                               PPEXECSP
008800     05  WS-TOTAL-2012-PCT-FULL-TIME   PIC S9V9999   COMP-3.      PPEXECSP
008900     05  WS-HIGHEST-GRADE              PIC  X(01).                PPEXECSP
015200     05  WS-WORK-GRADE.                                           48261289
015300         10 WS-WORK-GRADE-CHAR1        PIC  X(01).                48261289
015400         10 FILLER                     PIC  X(01).                48261289
011350     05  WS-WORK-GRADE-NUMERIC         REDEFINES                  76871348
011351         WS-WORK-GRADE.                                           76871348
011360         10 WS-WORK-GRADE-NUM1         PIC  X(01).                76871348
011370         10 FILLER                     PIC  X(01).                76871348
011380     05  WS-HIGHEST-GRADE-NUMERIC      PIC  X(01).                76871348
010000     EJECT                                                        36090574
010100 01  COLUMN-88-WORK.             COPY CPWSW88S.                   36090574
010200     EJECT                                                        36090574
011800 01  INSTALLATION-DEPENDNT-CONSTANT.        COPY CPWSXIDC.        28201068
011900     EJECT                                                        28201068
010310 01  DATE-CONVERSION-WORK-AREAS. COPY CPWSXDC3.                   28521025
010400     EJECT                                                        36090574
010500 01  PER-ROW EXTERNAL.                                            36090574
010600     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          36090574
010700     EJECT                                                        36090574
010800 01  AXD-ARRAY EXTERNAL.                                          36090574
010900     03  FILLER                                  OCCURS 27.       36090574
011000             07  APP-ROW-DELETE-BYTE  PIC X(1).                   36090574
011100             88  APP-ROW-DELETED VALUE HIGH-VALUES.               36090574
011200             07  APP-ROW-SEG-ID       PIC X(2).                   36090574
011300             07  FILLER                          OCCURS 3.        36090574
011400             08  APP-ROW.                                         36090574
011500*                                                                *36090574
011600*****            ***************************                 *****36090574
011700*                **  APPOINTMENT    DATA  **                     *36090574
011800*****            ***************************                 *****36090574
011900             COPY CPWSRAPP.                                       36090574
012000     03  FILLER                                  OCCURS 27.       36090574
012100             07  DIS-ROW-DELETE-BYTE  PIC X(1).                   36090574
012200             88  DIS-ROW-DELETED VALUE HIGH-VALUES.               36090574
012300             07  DIS-ROW-SEG-ID       PIC X(2).                   36090574
012400             07  FILLER                          OCCURS 3.        36090574
012500             08  DIS-ROW.                                         36090574
012600*                                                                *36090574
012700*****            ***************************                 *****36090574
012800*                **  DISTRIBUTION   DATA  **                     *36090574
012900*****            ***************************                 *****36090574
013000*                                                                *36090574
013100             COPY CPWSRDIS.                                       36090574
013101 LINKAGE SECTION.                                                 PPEXECSP
013102 01  PPEXECSP-INTERFACE.                    COPY  CPLNKESP.       PPEXECSP
013103     EJECT                                                        PPEXECSP
013104******************************************************************PPEXECSP
013105*                                                                *PPEXECSP
013106*                       PROCEDURE DIVISION                       *PPEXECSP
013107*                                                                *PPEXECSP
013108******************************************************************PPEXECSP
014000 PROCEDURE DIVISION USING PPEXECSP-INTERFACE.                     36090574
014001                                                                  PPEXECSP
014002 0000-EXECUTIVE-SEVERANCE-PAY.                                    PPEXECSP
014003     SKIP1                                                        PPEXECSP
014004     MOVE ZEROES TO XESP-MODULE-ERROR-FLAG.                       PPEXECSP
014005*-----------------------------------------------------------------PPEXECSP
014006*-------->                                                        PPEXECSP
014007*-------->   EXAMINES THE EMPLOYEE'S APPOINTMENT SET AND          PPEXECSP
014008*-------->   DETERMINES ELIGIBILITY FOR EXECUTIVE LIFE            PPEXECSP
014009*-------->                                                        PPEXECSP
014010*-----------------------------------------------------------------PPEXECSP
014011     SKIP1                                                        PPEXECSP
014012     MOVE 'N' TO WS-EXECUTIVE-APPT-SW,                            PPEXECSP
014013                 WS-INELIGIBLE-APPT-SW,                           PPEXECSP
014014                 XESP-INELIG-EXEC-FLAG.                           PPEXECSP
014015     MOVE ZERO TO WS-TOTAL-2012-PCT-FULL-TIME.                    PPEXECSP
014016     MOVE SPACE TO WS-HIGHEST-GRADE                               PPEXECSP
017410     MOVE LOW-VALUE TO WS-HIGHEST-GRADE-NUMERIC.                  76871348
014017     SKIP1                                                        PPEXECSP
014018     PERFORM 1000-CHECK-APPOINTMENT                               PPEXECSP
014019         VARYING APPT-SUB FROM 1 BY 3                             PPEXECSP
014020         UNTIL APPT-SUB > 27.                                     PPEXECSP
014021     SKIP1                                                        PPEXECSP
014022     IF WS-EXECUTIVE-APPT                                         PPEXECSP
014023         IF WS-INELIGIBLE-APPT OR                                 PPEXECSP
014024            WS-TOTAL-2012-PCT-FULL-TIME < 1.00                    PPEXECSP
014025             MOVE SPACE TO XESP-ELIGIBILITY-FLAG                  PPEXECSP
014026             MOVE 'Y' TO XESP-INELIG-EXEC-FLAG                    PPEXECSP
014027         ELSE                                                     PPEXECSP
018610************ MOVE WS-HIGHEST-GRADE TO XESP-ELIGIBILITY-FLAG       76871348
018611*------------> HIGHEST GRADE ALPHABETIC IS CONSIDERED HIGHER      76871348
018612*------------> THAN NUMERIC.                                      76871348
018620             IF WS-HIGHEST-GRADE NOT = SPACE                      76871348
018630                MOVE WS-HIGHEST-GRADE TO XESP-ELIGIBILITY-FLAG    76871348
018640             ELSE                                                 76871348
018650                IF WS-HIGHEST-GRADE-NUMERIC NOT = LOW-VALUE       76871348
018660                   MOVE WS-HIGHEST-GRADE-NUMERIC                  76871348
018670                                      TO XESP-ELIGIBILITY-FLAG    76871348
018680                ELSE                                              76871348
018681*-----------------> DEFAULT BLANK TO XESP-ELIGIBILITY-FLAG        76871348
018690                   MOVE WS-HIGHEST-GRADE                          76871348
018691                                      TO XESP-ELIGIBILITY-FLAG    76871348
018692                END-IF                                            76871348
018693             END-IF                                               76871348
014029         END-IF                                                   PPEXECSP
014030     ELSE                                                         PPEXECSP
014031         MOVE SPACE TO XESP-ELIGIBILITY-FLAG                      PPEXECSP
014032     END-IF.                                                      PPEXECSP
014033     SKIP1                                                        PPEXECSP
018410     IF XESP-INELIGIBLE-EMPLOYEE  OR                              28521025
018420        SEPARATE-DATE = XDC3-LOW-ISO-DATE                         28521025
018430         CONTINUE                                                 28521025
018440     ELSE                                                         28521025
018450         IF SEPARATE-DATE < XESP-ISO-EFFECTIVE-DATE               28521025
018460             MOVE SPACE TO XESP-ELIGIBILITY-FLAG                  28521025
018470         END-IF                                                   28521025
018480     END-IF.                                                      28521025
018104     SKIP3                                                        PPEXECSP
018105     GOBACK.                                                      PPEXECSP
018106     EJECT                                                        PPEXECSP
018107 1000-CHECK-APPOINTMENT.                                          PPEXECSP
018108     SKIP1                                                        PPEXECSP
019000     MOVE RATE-CODE (APPT-SUB, 1) TO W88-RATE-CODE.               36090574
019100     MOVE APPT-WOS-IND (APPT-SUB, 1) TO W88-APPT-WOS-IND.         36090574
019200     MOVE PERSONNEL-PGM (APPT-SUB, 1) TO W88-PERSONNEL-PGM.       36090574
020900     MOVE TITLE-CODE (APPT-SUB, 1) TO IDC-SR-MGMT-TITLE-RANGE.    28201068
019300     MOVE APPT-DURATION (APPT-SUB, 1) TO W88-APPT-DURATION.       36090574
020000     IF APPT-NUM  OF APP-ROW (APPT-SUB, 1)  = ZERO                36090574
020100        OR RATE-CODE-BY-AGREE                                     36090574
020200        OR APPT-WOS-IND-DOS-EQ-WOS                                36090574
022300        OR (NOT (PERSONNEL-PGM-MGMNT-PROF AND IDC-SR-MGMT-TITLE)) UCSD0041
021600****    OR (NOT PERSONNEL-PGM-EXECUTIVE)                          UCSD0041
020300        OR (APPT-BEGIN-DATE (APPT-SUB, 1)                         28521025
020400                                   >   XESP-ISO-EFFECTIVE-DATE)   28521025
020500        OR (APPT-END-DATE (APPT-SUB, 1)                           28521025
020600                                   <   XESP-ISO-EFFECTIVE-DATE)   28521025
020502         CONTINUE                                                 PPEXECSP
020503     ELSE                                                         PPEXECSP
022110*****                                                          CD 76871348
022300         IF PERSONNEL-PGM-MGMNT-PROF AND IDC-SR-MGMT-TITLE        28201068
021000             MOVE 'Y' TO WS-EXECUTIVE-APPT-SW                     PPEXECSP
026800             MOVE GRADE (APPT-SUB, 1) TO WS-WORK-GRADE            48261289
021100             IF APPT-DURATION-ACTING-EXEC                         36090574
022600*****                                                          CD 76871348
027100                OR WS-WORK-GRADE-CHAR1 = 'X'                      48261289
021201                 MOVE 'Y' TO WS-INELIGIBLE-APPT-SW                PPEXECSP
021202                 MOVE 27 TO APPT-SUB                              PPEXECSP
021203             ELSE                                                 PPEXECSP
021204*----------------> ELIGIBLE APPOINTMENT FOUND                     PPEXECSP
021700                 ADD  PERCENT-FULLTIME (APPT-SUB, 1)              36090574
021800                     TO  WS-TOTAL-2012-PCT-FULL-TIME              PPEXECSP
023210*****                                                          CD 76871348
023310*****            IF WS-WORK-GRADE-CHAR1 >                         76871348
023400*****                WS-HIGHEST-GRADE                             76871348
023410*****                                                          CD 76871348
023510*****                MOVE WS-WORK-GRADE-CHAR1 TO                  76871348
023600*****                    WS-HIGHEST-GRADE                         76871348
023700*****            END-IF                                           76871348
023701*****        END-IF                                               76871348
023710                 IF WS-WORK-GRADE-CHAR1 NUMERIC                   76871348
023720                    IF WS-WORK-GRADE-NUM1 >                       76871348
023730                                     WS-HIGHEST-GRADE-NUMERIC     76871348
023740                       MOVE WS-WORK-GRADE-NUM1 TO                 76871348
023750                                     WS-HIGHEST-GRADE-NUMERIC     76871348
023760                    END-IF                                        76871348
023770                 ELSE                                             76871348
023780                    IF WS-WORK-GRADE-CHAR1 >                      76871348
023790                                     WS-HIGHEST-GRADE             76871348
023792                          MOVE WS-WORK-GRADE-CHAR1 TO             76871348
023793                                     WS-HIGHEST-GRADE             76871348
023794                    END-IF                                        76871348
023795                 END-IF                                           76871348
023800             END-IF                                               76871348
022104         ELSE                                                     PPEXECSP
022105             MOVE 'Y' TO WS-INELIGIBLE-APPT-SW                    PPEXECSP
022106             MOVE 27 TO APPT-SUB                                  PPEXECSP
022107         END-IF                                                   PPEXECSP
022108     END-IF.                                                      PPEXECSP
