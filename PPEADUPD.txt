000000**************************************************************/   48381302
000001*  PROGRAM: PPEADUPD                                         */   48381302
000002*  RELEASE: ___1302______ SERVICE REQUEST(S): ____14838____  */   48381302
000003*  NAME:_______RENNER____ CREATION DATE:      ___07/30/00__  */   48381302
000004*  DESCRIPTION:                                              */   48381302
000005*    EDB EAD TABLE UPDATE DRIVER MODULE.                     */  *48381302
000008**************************************************************/   48381302
001500 IDENTIFICATION DIVISION.                                         PPEADUPD
001600 PROGRAM-ID. PPEADUPD.                                            PPEADUPD
001700 AUTHOR. UCOP.                                                    PPEADUPD
001800 DATE-WRITTEN. JULY 2000.                                         PPEADUPD
001900 DATE-COMPILED.                                                   PPEADUPD
002000*REMARKS.                                                         PPEADUPD
002100******************************************************************PPEADUPD
002200*                                                                *PPEADUPD
002300*  THIS PROGRAM DRIVES THE APPLICATION OF EMPLOYEE DATA BASE     *PPEADUPD
002400* EAD-TABLE ROWS. THIS PROGRAM IS CALLED BY PPEDBUPD.            *PPEADUPD
002500*                                                                *PPEADUPD
002600*  THE DETAILS OF THIS PROGRAMS FUNCTION ARE DESCRIBED IN THE    *PPEADUPD
002700* PROCEDURE DIVISION COPYMEMBER CPPDEUPD. ADDITIONAL CODE IS     *PPEADUPD
002800* HARCODED IN THIS SOURCE TO PROVIDE THE NECESSARY MOVEMENT OF   *PPEADUPD
002900* CHANGE-RECORD VALUES TO XXX-TABLE COLUMN ROWS, AS WELL AS      *PPEADUPD
003000* EAD-ROW INITIALIZATION.                                        *PPEADUPD
003100*                                                                *PPEADUPD
003200*  AS SHOULD BE OBVIOUS, THIS PROGRAM USES COPY CODE FOR THE     *PPEADUPD
003300* MAJORITY OF THE PROCEDURE DIVISION. THIS COPY CODE ADDRESSES   *PPEADUPD
003400* THE ROOT FUNCTIONS WHICH ARE COMMON TO THE PPXXXUPD PROGRAM    *PPEADUPD
003500* SERIES.                                                        *PPEADUPD
003600*                                                                *PPEADUPD
003700******************************************************************PPEADUPD
003800     EJECT                                                        PPEADUPD
003900 ENVIRONMENT DIVISION.                                            PPEADUPD
004000 CONFIGURATION SECTION.                                           PPEADUPD
004100 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPEADUPD
004200 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPEADUPD
004300      EJECT                                                       PPEADUPD
004400******************************************************************PPEADUPD
004500*                                                                *PPEADUPD
004600*                D A T A  D I V I S I O N                        *PPEADUPD
004700*                                                                *PPEADUPD
004800******************************************************************PPEADUPD
004900 DATA DIVISION.                                                   PPEADUPD
005000 WORKING-STORAGE SECTION.                                         PPEADUPD
005100 01  MISCELLANEOUS-WS.                                            PPEADUPD
005200******************************************************************PPEADUPD
005300*                                                                *PPEADUPD
005400* NOTE:                                                          *PPEADUPD
005500*                                                                *PPEADUPD
005600*   DATA NAMES A-STND-PROG-ID, PPXXXUTL-NAME AND PPXXXUTW-NAME   *PPEADUPD
005700*  ARE REFERENCED IN COPY CODE CPPDEUPD. A-STND-PROG-ID IS USED  *PPEADUPD
005800*  IN DIAGNOSTIC REFERENCES, WHILE PPXXUTL-NAME AND              *PPEADUPD
005900*  PPXXXUTW-NAME ARE USED IN DYNAMIC CALLS TO THE APPROPRIATE    *PPEADUPD
006000*  TABLE READ AND WRITE UTILITY MODULES, RESPECTIVELY.           *PPEADUPD
006100*                                                                *PPEADUPD
006200******************************************************************PPEADUPD
006300     05  A-STND-PROG-ID      PIC X(08) VALUE 'PPEADUPD'.          PPEADUPD
006400     05  PPXXXUTL-NAME       PIC X(08) VALUE 'PPEADUTL'.          PPEADUPD
006500     05  PPXXXUTW-NAME       PIC X(08) VALUE 'PPEADUTW'.          PPEADUPD
006600     05  WS-INIT-DATE        PIC X(10) VALUE '0001-01-01'.        PPEADUPD
006700     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPEADUPD
006800         88  FIRST-TIME                VALUE 'Y'.                 PPEADUPD
006900         88  NOT-FIRST-TIME            VALUE 'N'.                 PPEADUPD
007000     05  WS-EMPLOYEE-ID      PIC X(9).                            PPEADUPD
007100     SKIP3                                                        PPEADUPD
007200 01  DATE-CONVERSION-WORK-AREAS.                                  PPEADUPD
007300                                    COPY CPWSXDC3.                PPEADUPD
007400     EJECT                                                        PPEADUPD
007500 01  TEMP-DATA-AREA.                                              PPEADUPD
007600                                    COPY CPWSXMMM.                PPEADUPD
007700     EJECT                                                        PPEADUPD
007800******************************************************************PPEADUPD
007900*                                                                *PPEADUPD
008000*  EAD-ROW IS THE ROW THAT WILL BE PASSED AROUND FOR USE BY      *PPEADUPD
008100* HIGHER LEVEL PROGRAMS. IT IS DEFINED AS AN EXTERNAL DATA AREA  *PPEADUPD
008200* TO ALLOW HIGHER LEVEL PROGRAMS TO AVOID PASSING THE ROW        *PPEADUPD
008300* THROUGH UNNECESSARY CALLING CHAINS.                            *PPEADUPD
008400*                                                                *PPEADUPD
008500*  EAD-ROW-HOLD IS THE PURE ROW AS READ FROM THE EAD-TABLE. THIS *PPEADUPD
008600* ROW IS HELD IN WORKING-STORAGE TO ALLOW THE REFRESH FUNCTION   *PPEADUPD
008700* TO BE PERFORMED WITHOUT AN ADDED DATA BASE ACCESS.             *PPEADUPD
008800*                                                                *PPEADUPD
008900*  INIT-EAD-ROW IS THE INITIALIZED ROW. THIS IS PLACED IN        *PPEADUPD
009000* WORKING STORAGE FOR COMPARISON PURPOSES WITHIN THE "POST"      *PPEADUPD
009100* LOGIC OF CPPDEUPD.                                             *PPEADUPD
009200*                                                                *PPEADUPD
009300******************************************************************PPEADUPD
009400 01  EAD-ROW      EXTERNAL.                                       PPEADUPD
009500                                    COPY CPWSREAD.                PPEADUPD
009600     EJECT                                                        PPEADUPD
009700 01  EAD-ROW-HOLD  EXTERNAL.                                      PPEADUPD
009800                                    COPY CPWSREAD.                PPEADUPD
009900     EJECT                                                        PPEADUPD
010000 01  EAD-ROW-HOLD1 EXTERNAL.                                      PPEADUPD
010100                                    COPY CPWSREAD.                PPEADUPD
010200     EJECT                                                        PPEADUPD
010300 01  INIT-EAD-ROW EXTERNAL.                                       PPEADUPD
010400                                    COPY CPWSREAD.                PPEADUPD
010500     EJECT                                                        PPEADUPD
010600 01  PPEADUTL-INTERFACE EXTERNAL.                                 PPEADUPD
010700                                    COPY CPLNKEAD.                PPEADUPD
010800     EJECT                                                        PPEADUPD
010900 01  PPXXXUTW-INTERFACE.                                          PPEADUPD
011000                                    COPY CPLNWXXX.                PPEADUPD
011100     EJECT                                                        PPEADUPD
011200 01  XWHC-COMPILE-WORK-AREA.                                      PPEADUPD
011300                                    COPY CPWSXWHC.                PPEADUPD
011400                                                                  PPEADUPD
011500 01  ONLINE-SIGNALS                      EXTERNAL. COPY CPWSONLI. PPEADUPD
011600     EJECT                                                        PPEADUPD
011700 LINKAGE SECTION.                                                 PPEADUPD
011800******************************************************************PPEADUPD
011900*              L I N K A G E    S E C T I O N                    *PPEADUPD
012000******************************************************************PPEADUPD
012100*                                                                 PPEADUPD
012200 01  PPXXXUPD-INTERFACE.            COPY CPLNUXXX.                PPEADUPD
012300     EJECT                                                        PPEADUPD
012400 01  EDB-CHANGE-RECORD.             COPY CPWSXCHG.                PPEADUPD
012500     EJECT                                                        PPEADUPD
012600 PROCEDURE DIVISION USING                                         PPEADUPD
012700                          PPXXXUPD-INTERFACE                      PPEADUPD
012800                          EDB-CHANGE-RECORD.                      PPEADUPD
012900*                                                                 PPEADUPD
013000******************************************************************PPEADUPD
013100*            P R O C E D U R E  D I V I S I O N                  *PPEADUPD
013200******************************************************************PPEADUPD
013300*                                                                 PPEADUPD
013400******************************************************************PPEADUPD
013500*                                                                *PPEADUPD
013600*  THE PROCEDURE DIVISION OF THIS PROGRAM IS CONTAINED PRIMARILY *PPEADUPD
013700* IN COPY MEMBER CPPDEUPD. HERE THE GENERIC DATA-NAMES IN        *PPEADUPD
013800* CPPDEUPD ARE REPLACED WITH THE SPECIFIC DATA-NAMES OF THE      *PPEADUPD
013900* WORKING STORAGE TABLE ROWS AND THE PPXXXUTL MODULE INTERFACE.  *PPEADUPD
014200******************************************************************PPEADUPD
014300                                     COPY CPPDEUPD                PPEADUPD
014400        REPLACING PP000UTL-INTERFACE   BY PPEADUTL-INTERFACE      PPEADUPD
014500                  PP000UTL-EMPLOYEE-ID BY PPEADUTL-EMPLOYEE-ID    PPEADUPD
014600                  000-ROW              BY EAD-ROW                 PPEADUPD
014700                  000-ROW-HOLD         BY EAD-ROW-HOLD            PPEADUPD
014800                  000-ROW-HOLD1        BY EAD-ROW-HOLD1           PPEADUPD
014900                  INIT-000-ROW         BY INIT-EAD-ROW            PPEADUPD
015000                  PP000UTL-ERROR       BY PPEADUTL-ERROR          PPEADUPD
015100                  PP000UTL-000-FOUND   BY PPEADUTL-EAD-FOUND.     PPEADUPD
015200     EJECT                                                        PPEADUPD
015300 7900-FIRST-TIME SECTION.                                         PPEADUPD
015400     SKIP1                                                        PPEADUPD
015500     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPEADUPD
015600     SKIP1                                                        PPEADUPD
015700                                    COPY CPPDXWHC.                PPEADUPD
015800     EJECT                                                        PPEADUPD
015900 8800-CONVERT-DATES SECTION.                                      PPEADUPD
016000     SKIP1                                                        PPEADUPD
016100                                     COPY CPPDXDC3.               PPEADUPD
016200     EJECT                                                        PPEADUPD
016300 9000-INITIALIZE-ROW SECTION.                                     PPEADUPD
016400     SKIP1                                                        PPEADUPD
016500******************************************************************PPEADUPD
016600*                                                                *PPEADUPD
016700* THIS SECTION INITIALIZES THE EAD-ROW.                          *PPEADUPD
017000*                                                                *PPEADUPD
017100* NOTE: IN THE BASE SYSTEM, THERE ARE NO DATE COLUMNS DEFINED    *PPEADUPD
017200*       ON THE EAD ROW.                                          *PPEADUPD
017600******************************************************************PPEADUPD
017700     SKIP1                                                        PPEADUPD
017800     INITIALIZE EAD-ROW.                                          PPEADUPD
017900     MOVE PPXXXUPD-EMPLOYEE-ID TO EMPLOYEE-ID OF EAD-ROW.         PPEADUPD
018000     EJECT                                                        PPEADUPD
018100 9100-EVALUATE-DATA-ELEMENT SECTION.                              PPEADUPD
018200     SKIP1                                                        PPEADUPD
018300******************************************************************PPEADUPD
018400*                                                                *PPEADUPD
018500* THIS SECTION TAKES THE DATA ELEMENT NUMBER FROM THE CHANGE     *PPEADUPD
018600* RECORD TO DETERMINE WHICH COLUMN IS TO RECEIVE THE VALUE FROM  *PPEADUPD
018700* THE CHANGE RECORD. WHILE IT IS MOST IMPORTANT TO MAKE SURE     *PPEADUPD
018800* THAT THE NUMERIC FIELDS ARE MOVED TO LIKE LENGTH COLUMNS,      *PPEADUPD
018900* THE USE OF LENGTH QUALIFIERS ON ALPHANUMERIC FIELD MOVES DOES  *PPEADUPD
019000* PROVIDE SOME USEFUL DOCUMENTATION.                             *PPEADUPD
019100*                                                                *PPEADUPD
019200******************************************************************PPEADUPD
019300     SKIP1                                                        PPEADUPD
019400     EVALUATE XCHG-DATA-ELEM-NO                                   PPEADUPD
019500         WHEN '0331'                                              PPEADUPD
019600              MOVE TEMP-DATA-VALUE     TO                         PPEADUPD
019700                   EAD-CMPWRK-ADRLNE1       OF EAD-ROW            PPEADUPD
019800         WHEN '0332'                                              PPEADUPD
019900              MOVE TEMP-DATA-VALUE     TO                         PPEADUPD
020000                   EAD-CMPWRK-ADRLNE2       OF EAD-ROW            PPEADUPD
020100         WHEN '0333'                                              PPEADUPD
020200              MOVE TEMP-DATA-VALUE     TO                         PPEADUPD
020300                   EAD-CMPWRK-ADRCITY       OF EAD-ROW            PPEADUPD
020400         WHEN '0334'                                              PPEADUPD
020500              MOVE TEMP-DATA-VALUE     TO                         PPEADUPD
020600                   EAD-CMPWRK-ADSTATE       OF EAD-ROW            PPEADUPD
020700         WHEN '0335'                                              PPEADUPD
020800              MOVE TEMP-DATA-VALUE     TO                         PPEADUPD
020900                   EAD-CMPWRK-ADRZIP        OF EAD-ROW            PPEADUPD
021000         WHEN OTHER                                               PPEADUPD
021100              IF PPXXXUPD-DIAGNOSTICS-ON                          PPEADUPD
021200                  DISPLAY A-STND-PROG-ID                          PPEADUPD
021300                       ': UNRECOGNIZED DATA ELEMENT NUMBER ('     PPEADUPD
021400                           XCHG-DATA-ELEM-NO                      PPEADUPD
021500                                                   ')'            PPEADUPD
021600              END-IF                                              PPEADUPD
021700              SET PPXXXUPD-DATA-ELEM-ERROR TO TRUE                PPEADUPD
021800     END-EVALUATE.                                                PPEADUPD
021900     SKIP3                                                        PPEADUPD
022000************************   END OF PROGRAM ************************PPEADUPD
