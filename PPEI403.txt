000001**************************************************************/   32021138
000002*  PROGRAM: PPEI403                                          */   32021138
000003*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___08/04/97__  */   32021138
000005*  DESCRIPTION:                                              */   32021138
000006*  - MODIFIED FOR FULL ACCOUNTING UNIT.                      */   32021138
000007**************************************************************/   32021138
000000**************************************************************/   28521025
000001*  PROGRAM: PPEI403                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/28/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
001600*----------------------------------------------------------------*36410717
001700*|**************************************************************|*36410717
001800*|* PROGRAM: PPEI403                                           *|*36410717
001900*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
002000*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
002100*|*  DESCRIPTION:                                              *|*36410717
002200*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
002300*|*    UNCONDITIONAL IMPLIED MAINTENANCE                       *|*36410717
002400*|**************************************************************|*36410717
002500*----------------------------------------------------------------*36410717
002600                                                                  PPEI403
002700 IDENTIFICATION DIVISION.                                         PPEI403
002800 PROGRAM-ID. PPEI403.                                             PPEI403
002900 AUTHOR. UCOP.                                                    PPEI403
003000 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEI403
003100                                                                  PPEI403
003200 DATE-WRITTEN.  SEPTEMBER 10, 1992.                               PPEI403
003300 DATE-COMPILED.                                                   PPEI403
003400                                                                  PPEI403
003500     EJECT                                                        PPEI403
003600 ENVIRONMENT DIVISION.                                            PPEI403
003700 CONFIGURATION SECTION.                                           PPEI403
003800 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEI403
003900 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEI403
004000 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEI403
004100     EJECT                                                        PPEI403
004200 INPUT-OUTPUT SECTION.                                            PPEI403
004300 FILE-CONTROL.                                                    PPEI403
004400                                                                  PPEI403
004500 DATA DIVISION.                                                   PPEI403
004600 FILE SECTION.                                                    PPEI403
004700*----------------------------------------------------------------*PPEI403
004800 WORKING-STORAGE SECTION.                                         PPEI403
004900*----------------------------------------------------------------*PPEI403
005000                                                                  PPEI403
005100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEI403
003700*****'PPEI403 /111992'.                                           28521025
003710*****'PPEI403 /080195'.                                           32021138
003720     'PPEI403 /080497'.                                           32021138
005300                                                                  PPEI403
005400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEI403
005500     A-STND-PROG-ID.                                              PPEI403
005600     05  FILLER                       PIC X(03).                  PPEI403
005700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEI403
005800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEI403
005900     05  FILLER                       PIC X(08).                  PPEI403
006000                                                                  PPEI403
006100 01  MESSAGES-USED.                                               PPEI403
006200     05  M12899                PIC 9(05)  VALUE 12899.            PPEI403
006300                                                                  PPEI403
006400 01  MISC-WORK-AREAS.                                             PPEI403
006500     05  FIRST-TIME-SW        PIC X(01) VALUE LOW-VALUES.         PPEI403
006600         88  THIS-IS-THE-FIRST-TIME            VALUE LOW-VALUES.  PPEI403
006700         88  NOT-THE-FIRST-TIME                VALUE 'Y'.         PPEI403
006800     05  ROWSUB                   PIC S9(4) COMP      VALUE ZERO. PPEI403
006900     05  ACTSUB                   PIC S9(4) COMP      VALUE ZERO. PPEI403
007000     05  ACTION-NUM               PIC 9(2)            VALUE ZERO. PPEI403
007100     05  COMM-TEMP-DATE.                                          PPEI403
007200         10  FILLER                   PIC X(2).                   PPEI403
007300         10  COMM-TEMP-DATE-YYMMDD    PIC X(6).                   PPEI403
007400     05  ISO-HIRE-DATE.                                           PPEI403
007500         10  ISO-HIRE-DATE-YYYY-MM.                               PPEI403
007600             15  ISO-HIRE-DATE-YYYY   PIC 9(4).                   PPEI403
007700             15  FILLER               PIC X(1)       VALUE '-'.   PPEI403
007800             15  ISO-HIRE-DATE-MM     PIC 9(2).                   PPEI403
007900         10  FILLER                   PIC X(1)       VALUE '-'.   PPEI403
008000         10  ISO-HIRE-DATE-DD         PIC 9(2).                   PPEI403
008100                                                                  PPEI403
008200 01  XCTL-WORK-AREA.                                              PPEI403
008300     05  XCTL-INDEX                PIC 9(03) VALUE ZEROS.         PPEI403
008400     05  ACTN-COST-FLAG            PIC X(01) VALUE 'N'.           PPEI403
008500                                                                  PPEI403
008600     EJECT                                                        PPEI403
008700 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEI403
008800                                                                  PPEI403
008900     EJECT                                                        PPEI403
007500*01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. 28521025
007510 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. 28521025
009100                                                                  PPEI403
009200     EJECT                                                        PPEI403
009300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEI403
009400                                                                  PPEI403
009500     EJECT                                                        PPEI403
009600 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEI403
009700                                                                  PPEI403
009800     EJECT                                                        PPEI403
008400*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
008500*****                                                             28521025
008600*****EJECT                                                        28521025
010200 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEI403
010300                                                                  PPEI403
010400     EJECT                                                        PPEI403
010500 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEI403
010600                                                                  PPEI403
010700     EJECT                                                        PPEI403
010800 01  OFFSETS-TABLE.                                COPY CPWSXOST. PPEI403
010900                                                                  PPEI403
011000     EJECT                                                        PPEI403
011100******************************************************************PPEI403
011200**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEI403
011300**--------------------------------------------------------------**PPEI403
011400** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEI403
011500******************************************************************PPEI403
011600                                                                  PPEI403
011700 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEI403
011800                                                                  PPEI403
011900     EJECT                                                        PPEI403
012000 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEI403
012100                                                                  PPEI403
012200     EJECT                                                        PPEI403
012300 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEI403
012400                                                                  PPEI403
012500     EJECT                                                        PPEI403
012600 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEI403
012700                                                                  PPEI403
012800 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEI403
012900                                                                  PPEI403
013000 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEI403
013100                                                                  PPEI403
013200 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEI403
013300                                                                  PPEI403
013400     EJECT                                                        PPEI403
013500 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEI403
013600                                                                  PPEI403
013700     EJECT                                                        PPEI403
013800 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEI403
013900                                                                  PPEI403
014000     EJECT                                                        PPEI403
014100 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEI403
014200                                                                  PPEI403
014300     EJECT                                                        PPEI403
014400 01  XACN-ACTION-ARRAY                   EXTERNAL. COPY CPWSXACN. PPEI403
014500                                                                  PPEI403
014600     EJECT                                                        PPEI403
014700 01  XCTL-TBL                            EXTERNAL. COPY CPWSXCTL. PPEI403
014800                                                                  PPEI403
014900     EJECT                                                        PPEI403
015000 01  AXD-ARRAY                           EXTERNAL.                PPEI403
015100                                                                  PPEI403
015200     03  FILLER           OCCURS 27.                              PPEI403
015300         07  APP-ROW-DELETE-BYTE          PIC X(1).               PPEI403
015400             88  APP-ROW-DELETED VALUE HIGH-VALUES.               PPEI403
015500         07  APP-ROW-SEG-ID               PIC X(2).               PPEI403
015600         07  FILLER       OCCURS 3.                               PPEI403
015700             08  APP-ROW.                 COPY CPWSRAPP           PPEI403
015800                 REPLACING WORK-STUDY-PGM                         PPEI403
015900                        BY NULL-WORK-STUDY-PGM.                   PPEI403
016000                                                                  PPEI403
016100     03  FILLER           OCCURS 27.                              PPEI403
016200         07  DIS-ROW-DELETE-BYTE          PIC X(1).               PPEI403
016300             88  DIS-ROW-DELETED VALUE HIGH-VALUES.               PPEI403
016400         07  DIS-ROW-SEG-ID               PIC X(2).               PPEI403
016500         07  FILLER       OCCURS 3.                               PPEI403
016600             08  DIS-ROW.                 COPY CPWSRDIS.          PPEI403
016700                                                                  PPEI403
016800     EJECT                                                        PPEI403
016900 01  WORK-DIST-DATA                      EXTERNAL. COPY CPWSRDIS  PPEI403
017000                                                                  PPEI403
017100         REPLACING  EMPLOYEE-ID         BY WORK-DIST-EMPLOYEE-ID  PPEI403
017200                    APPT-NUM            BY WORK-DIST-APPT-NUM     PPEI403
017300                    DIST-NUM            BY WORK-DIST-NO           PPEI403
017400                    DIST-ADC-CODE       BY WORK-DIST-ADC-CODE     PPEI403
016000*****               LOCATION-CODE       BY WORK-LOC               32021138
016100*****               ACCOUNT-NUM         BY WORK-ACCOUNT           32021138
016200*****               FUND-NUM            BY WORK-FUND              32021138
016300*****               SUB-ACCT-NUM        BY WORK-SUB               32021138
017900                    DIST-DEPT-CODE      BY WORK-DIST-DEPT-CODE    PPEI403
018000                    DIST-FTE            BY WORK-FTE               PPEI403
018100                    PAY-BEGIN-DATE      BY WORK-PAY-BEGN-DATE     PPEI403
018200                    PAY-END-DATE        BY WORK-PAY-END-DATE      PPEI403
018300                    DIST-PERCENT        BY WORK-DIST-PCT          PPEI403
018400                    DIST-PAYRATE        BY WORK-RATE-AMOUNT       PPEI403
018500                    DIST-DOS            BY WORK-DOS               PPEI403
018600                    DIST-PERQ           BY WORK-PERQ              PPEI403
018700                    DIST-UNIT-CODE      BY WORK-DUC               PPEI403
018800                    RANGE-ADJ-DUC       BY WORK-RNG-ADJ-DUC       PPEI403
018900                    DIST-STEP           BY WORK-DIST-STEP         PPEI403
019000                    DIST-OFF-ABOVE      BY WORK-OFF-ABV-SCALE     PPEI403
017600                    WORK-STUDY-PGM      BY WORK-WORK-STUDY-PGM.   PPEI403
019900                                                                  PPEI403
020000     EJECT                                                        PPEI403
020100******************************************************************PPEI403
020200**  DATA BASE AREAS                                             **PPEI403
020300******************************************************************PPEI403
020400                                                                  PPEI403
020500 01  SCR-ROW                             EXTERNAL.                PPEI403
020600     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEI403
020700                                                                  PPEI403
020800     EJECT                                                        PPEI403
020900 01  PER-ROW                             EXTERNAL.                PPEI403
021000     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEI403
021100                                                                  PPEI403
021200     EJECT                                                        PPEI403
021300 01  PAY-ROW                             EXTERNAL.                PPEI403
021400     EXEC SQL INCLUDE PPPVPAY1 END-EXEC.                          PPEI403
021500                                                                  PPEI403
021600                                                                  PPEI403
021700                                                                  PPEI403
021800     EJECT                                                        PPEI403
021900******************************************************************PPEI403
022000**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEI403
022100******************************************************************PPEI403
022200                                                                  PPEI403
022300*----------------------------------------------------------------*PPEI403
022400 LINKAGE SECTION.                                                 PPEI403
022500*----------------------------------------------------------------*PPEI403
022600                                                                  PPEI403
022700******************************************************************PPEI403
022800**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEI403
022900**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEI403
023000******************************************************************PPEI403
023100                                                                  PPEI403
023200 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEI403
023300                                                                  PPEI403
023400     EJECT                                                        PPEI403
023500******************************************************************PPEI403
023600**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEI403
023700**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEI403
023800******************************************************************PPEI403
023900                                                                  PPEI403
024000 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEI403
024100                                                                  PPEI403
024200     EJECT                                                        PPEI403
024300******************************************************************PPEI403
024400**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEI403
024500******************************************************************PPEI403
024600                                                                  PPEI403
024700 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEI403
024800                          KMTA-MESSAGE-TABLE-ARRAY.               PPEI403
024900                                                                  PPEI403
025000                                                                  PPEI403
025100*----------------------------------------------------------------*PPEI403
025200 0000-DRIVER.                                                     PPEI403
025300*----------------------------------------------------------------*PPEI403
025400                                                                  PPEI403
025500******************************************************************PPEI403
025600**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEI403
025700******************************************************************PPEI403
025800                                                                  PPEI403
025900     IF  THIS-IS-THE-FIRST-TIME                                   PPEI403
026000         PERFORM 0100-INITIALIZE                                  PPEI403
026100     END-IF.                                                      PPEI403
026200                                                                  PPEI403
026300******************************************************************PPEI403
026400**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEI403
026500**  TO THE CALLING PROGRAM                                      **PPEI403
026600******************************************************************PPEI403
026700                                                                  PPEI403
026800     PERFORM 1000-MAINLINE-ROUTINE.                               PPEI403
026900                                                                  PPEI403
027000     EXIT PROGRAM.                                                PPEI403
027100                                                                  PPEI403
027200                                                                  PPEI403
027300     EJECT                                                        PPEI403
027400*----------------------------------------------------------------*PPEI403
027500 0100-INITIALIZE    SECTION.                                      PPEI403
027600*----------------------------------------------------------------*PPEI403
027700                                                                  PPEI403
027800******************************************************************PPEI403
027900**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEI403
028000******************************************************************PPEI403
028100                                                                  PPEI403
028200     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEI403
028300                                                                  PPEI403
028400*                                               *-------------*   PPEI403
028500                                                 COPY CPPDXWHC.   PPEI403
028600*                                               *-------------*   PPEI403
028700                                                                  PPEI403
026600*****                                           *-------------*   28521025
026700*****                                            COPY CPPDDATE.   28521025
026800*****                                           *-------------*   28521025
026900******                                                            28521025
027000*****MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 28521025
029100                                                                  PPEI403
029400******************************************************************PPEI403
029500**   SET FIRST CALL SWITCH OFF                                  **PPEI403
029600******************************************************************PPEI403
029700                                                                  PPEI403
029800     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEI403
029900                                                                  PPEI403
030000     EJECT                                                        PPEI403
030100*----------------------------------------------------------------*PPEI403
030200 1000-MAINLINE-ROUTINE SECTION.                                   PPEI403
030300*----------------------------------------------------------------*PPEI403
030400*----------------------------------------------------------------*PPEI403
030500*5900-COSTING-ACTION-RATE-CHG SECTION.                            PPEI403
030600*----------------------------------------------------------------*PPEI403
030700                                                                  PPEI403
030800     PERFORM WITH TEST BEFORE                                     PPEI403
030900         VARYING ROWSUB FROM +1 BY +1                             PPEI403
031000           UNTIL ROWSUB IS GREATER THAN 98                        PPEI403
031100              OR ACTN-COST-FLAG IS = 'Y'                          PPEI403
031200                                                                  PPEI403
031300         IF  XACN-ACTION-DATA (ROWSUB) IS = SPACES                PPEI403
031400             CONTINUE                                             PPEI403
031500         ELSE                                                     PPEI403
031600             PERFORM 5905-SEARCH-CAT-TABLE                        PPEI403
031700                 VARYING ACTSUB FROM +1 BY +1                     PPEI403
031800                   UNTIL ACTSUB IS GREATER THAN 93                PPEI403
031900         END-IF                                                   PPEI403
032000                                                                  PPEI403
032100     END-PERFORM.                                                 PPEI403
032200                                                                  PPEI403
032300******************************************************************PPEI403
032400**          COSTING ACTION AND  - CHANGE OF PAYRATE             **PPEI403
032500**--------------------------------------------------------------**PPEI403
032600******************************************************************PPEI403
032700**  THE FOLLOWING LOOPS THROUGH THE APPOINTMENT/DISTRIBUTIONS   **PPEI403
032800**  TO SEE IF  THERE WAS A PAYRATE CHANGE ON THE DISTR. IF  SO  **PPEI403
032900**  THEN IF  THERE WAS NOT CHANGE IN BEGIN DATE, A MESSAGE IS   **PPEI403
033000**  WRITTEN TO WARN OF THE EFFECT ON THE COSTING PROCESS.       **PPEI403
033100******************************************************************PPEI403
033200                                                                  PPEI403
033300     IF  ACTN-COST-FLAG  NOT =  'N'                               PPEI403
033400         PERFORM                                                  PPEI403
033500             VARYING APPT-PNTR FROM +1 BY +3                      PPEI403
033600               UNTIL APPT-PNTR > 25                               PPEI403
033700                                                                  PPEI403
033800             IF   APPT-NUM   OF APP-ROW (APPT-PNTR, 1) NOT = ZERO PPEI403
033900                                                                  PPEI403
034000                 COMPUTE NEXT-APPT-PNTR = APPT-PNTR + 3           PPEI403
034100                                                                  PPEI403
034200                 MOVE 2           TO DIST-PNTR                    PPEI403
034300                                                                  PPEI403
034400                 PERFORM                                          PPEI403
034500                     VARYING CURR-APPT-PNTR FROM APPT-PNTR BY +1  PPEI403
034600                       UNTIL CURR-APPT-PNTR NOT < NEXT-APPT-PNTR  PPEI403
034700                                                                  PPEI403
034800                     PERFORM                                      PPEI403
034900                         VARYING DIST-PNTR FROM DIST-PNTR BY +1   PPEI403
035000                           UNTIL DIST-PNTR  > +3                  PPEI403
035100                                                                  PPEI403
035200                         MOVE DIS-ROW (CURR-APPT-PNTR, DIST-PNTR) PPEI403
035300                                  TO WORK-DIST-DATA               PPEI403
035400                                                                  PPEI403
035500                         PERFORM 5910-CHG-PAYRT-ROUTINE           PPEI403
035600                                                                  PPEI403
035700                     END-PERFORM                                  PPEI403
035800                                                                  PPEI403
035900                     MOVE 1       TO DIST-PNTR                    PPEI403
036000                                                                  PPEI403
036100                 END-PERFORM                                      PPEI403
036200             END-IF                                               PPEI403
036300         END-PERFORM                                              PPEI403
036400     END-IF.                                                      PPEI403
036500                                                                  PPEI403
036600                                                                  PPEI403
036700*----------------------------------------------------------------*PPEI403
036800 5905-SEARCH-CAT-TABLE  SECTION.                                  PPEI403
036900*----------------------------------------------------------------*PPEI403
037000                                                                  PPEI403
037100******************************************************************PPEI403
037200**   THE FOLLOWING IS A SEARCH ROUTINE FOR THE ACTION CODES     **PPEI403
037300**   WHICH SHOULD BE COSTED.  ALL ACTION CODES FOR AN EMPLOYEE  **PPEI403
037400**   ARE INTERROGATED AS FOUND,  BUT ONLY THE 'VALID' ACTION    **PPEI403
037500**   ARE FLAGGED TO CHECK THE PAYRATE CHANGE AND THEN THE       **PPEI403
037600**   PAY BEGIN DATE ON THE DISTRIBUTION FOR A CHANGE.           **PPEI403
037700******************************************************************PPEI403
037800                                                                  PPEI403
037900     MOVE ACTSUB TO ACTION-NUM.                                   PPEI403
038000                                                                  PPEI403
038100     PERFORM WITH TEST BEFORE                                     PPEI403
038200         VARYING XCTL-INDEX FROM +1 BY +1                         PPEI403
038300           UNTIL XCTL-INDEX      > XCTL-TBL-COUNTER               PPEI403
038400              OR  ACTN-COST-FLAG = 'Y'                            PPEI403
038500                                                                  PPEI403
038600         IF  ACTION-NUM = XCTL-TBL-ACTION (XCTL-INDEX)            PPEI403
038700             MOVE  'Y'  TO  ACTN-COST-FLAG                        PPEI403
038800             MOVE XCTL-TBL-COUNTER TO XCTL-INDEX                  PPEI403
038900         END-IF                                                   PPEI403
039000                                                                  PPEI403
039100     END-PERFORM.                                                 PPEI403
039200                                                                  PPEI403
039300                                                                  PPEI403
039400*----------------------------------------------------------------*PPEI403
039500 5910-CHG-PAYRT-ROUTINE  SECTION.                                 PPEI403
039600*----------------------------------------------------------------*PPEI403
039700                                                                  PPEI403
039800******************************************************************PPEI403
039900**              PROCESSING FOR EACH DISTRIBUTION                **PPEI403
040000**--------------------------------------------------------------**PPEI403
040100**  IF  ACTION IS A COSTABLE ACTION, THE CHECK TO SEE IF  PAYRATE PPEI403
040200**  HAS CHANGED ON THIS DISTRIBUTION.  IF  THIS IS TRUE, CHECK  **PPEI403
040300**  TO SEE IF  PAY BEGIN DATE HAS CHANGED ALSO AND IF  NOT, PRINT PPEI403
040400**  AN ERROR MESSAGE TO WARN THAT THIS MAY AFFECT THE COSTING   **PPEI403
040500**  PROCESS FOR EFFECTIVE DATE ON THIS DISTRIBUTION RECORD      **PPEI403
040600**  GOING INTO THE COSTING PROCESS.                             **PPEI403
040700******************************************************************PPEI403
040800                                                                  PPEI403
040900     COMPUTE  DL-FIELD-R = OFF-2055-RATE-AMOUNT                   PPEI403
041000                         + ADA-SEG-BASE (APPT-PNTR, DIST-PNTR).   PPEI403
041100                                                                  PPEI403
041200     MOVE  ZERO                   TO  DL-STATUS                   PPEI403
041300                                                                  PPEI403
041400     PERFORM  9085-DLSEARCH                                       PPEI403
041500                                                                  PPEI403
041600     IF  DL-STATUS NOT = ZERO                                     PPEI403
041700         COMPUTE  DL-FIELD-R = OFF-2053-PAY-BEGN-DATE             PPEI403
041800                             + ADA-SEG-BASE (APPT-PNTR, DIST-PNTR)PPEI403
041900         MOVE  ZERO               TO  DL-STATUS                   PPEI403
042000         PERFORM  9085-DLSEARCH                                   PPEI403
042100         IF  DL-STATUS      =    ZERO                             PPEI403
042200             MOVE  WORK-PAY-BEGN-DATE                             PPEI403
040100*****                             TO DB2-DATE                     28521025
040200*****        PERFORM CONVERT-DB2-DATE                             28521025
040210                                  TO XDC3-ISO-DATE                28521025
040220             PERFORM XDC3-CONVERT-ISO-TO-STD                      28521025
042500             ADD 1                TO KMTA-CTR                     PPEI403
040400*****        MOVE WSX-STD-DATE    TO KMTA-DATA-FIELD    (KMTA-CTR)28521025
040410             MOVE XDC3-STD-DATE   TO KMTA-DATA-FIELD    (KMTA-CTR)28521025
042700             MOVE  DL-FIELD-R     TO KMTA-FIELD-NUM     (KMTA-CTR)PPEI403
042800             MOVE  6              TO KMTA-LENGTH        (KMTA-CTR)PPEI403
042900             MOVE  M12899         TO KMTA-MSG-NUMBER    (KMTA-CTR)PPEI403
043000             MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE  (KMTA-CTR)PPEI403
043100             MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM   (KMTA-CTR)PPEI403
043200         END-IF                                                   PPEI403
043300     END-IF.                                                      PPEI403
043400                                                                  PPEI403
043500     EJECT                                                        PPEI403
043600*----------------------------------------------------------------*PPEI403
043700 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEI403
043800*----------------------------------------------------------------*PPEI403
043900                                                                  PPEI403
044000*                                                 *-------------* PPEI403
044100                                                   COPY CPPDXDEC. PPEI403
044200*                                                 *-------------* PPEI403
044300                                                                  PPEI403
044400*----------------------------------------------------------------*PPEI403
044500 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEI403
044600*----------------------------------------------------------------*PPEI403
044700                                                                  PPEI403
044800*                                                 *-------------* PPEI403
044900                                                   COPY CPPDADLC. PPEI403
045000*                                                 *-------------* PPEI403
045100                                                                  PPEI403
045200     EJECT                                                        PPEI403
045300*----------------------------------------------------------------*PPEI403
045400 9300-DATE-CONVERSION-DB2  SECTION.                               PPEI403
045500*----------------------------------------------------------------*PPEI403
045600                                                                  PPEI403
045700*                                                 *-------------* PPEI403
043600*****                                              COPY CPPDXDC2. 28521025
043610                                                   COPY CPPDXDC3. 28521025
045900*                                                 *-------------* PPEI403
046000                                                                  PPEI403
046100******************************************************************PPEI403
046200**   E N D  S O U R C E   ----  PPEI403 ----                    **PPEI403
046300******************************************************************PPEI403
