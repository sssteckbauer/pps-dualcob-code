000001**************************************************************/   EFIX1285
000002*  PROGRAM: PPEC143                                          */   EFIX1285
000003*  RELEASE: ___1285______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1285
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___05/15/00__  */   EFIX1285
000005*  DESCRIPTION:                                              */   EFIX1285
000006*  - ERROR REPORT 1668:                                      */   EFIX1285
000007*    REMOVE PIE DATE COMPARISON FOR PPA ROW UPDATE. ASSUME   */   EFIX1285
000008*    IVR PROCESS CORRECTLY ALLOWED PLAN UPDATE SO            */   EFIX1285
000009*    UPDATE THE PPA ROWS WITH THE NEW PLAN OR DELETE THEM    */   EFIX1285
000010*    FOR AN OPTOUT.                                          */   EFIX1285
000011**************************************************************/   EFIX1285
000000**************************************************************/   32591157
000001*  PROGRAM: PPEC143                                          */   32591157
000002*  RELEASE: ___1157______ SERVICE REQUEST(S): ____13259____  */   32591157
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___12/15/97__  */   32591157
000004*  DESCRIPTION:                                              */   32591157
000005*  - BENEFITS ENROLLMENT VIA IVR.                            */   32591157
000006*    PENDING PREMIUM EDITS.                                  */   32591157
000007**************************************************************/   32591157
001100                                                                  PPEC143
001200 IDENTIFICATION DIVISION.                                         PPEC143
001300 PROGRAM-ID. PPEC143.                                             PPEC143
001400 AUTHOR. UCOP.                                                    PPEC143
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEC143
001600                                                                  PPEC143
001700 DATE-WRITTEN.  NOVEMBER 1997.                                    PPEC143
001800 DATE-COMPILED.                                                   PPEC143
001900                                                                  PPEC143
002000******************************************************************PPEC143
002100**    THIS ROUTINE WILL PERFORM THE HEALTH INSURANCE PENDING    **PPEC143
002200**    PREMIUM MAINTENANCE FOR BENEFITS ENROLLMENT VIA IVR       **PPEC143
002300******************************************************************PPEC143
002400                                                                  PPEC143
002500 ENVIRONMENT DIVISION.                                            PPEC143
002600 CONFIGURATION SECTION.                                           PPEC143
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEC143
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEC143
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEC143
003000                                                                  PPEC143
003100 INPUT-OUTPUT SECTION.                                            PPEC143
003200 FILE-CONTROL.                                                    PPEC143
003300                                                                  PPEC143
003400 DATA DIVISION.                                                   PPEC143
003500 FILE SECTION.                                                    PPEC143
003600     EJECT                                                        PPEC143
003700*----------------------------------------------------------------*PPEC143
003800 WORKING-STORAGE SECTION.                                         PPEC143
003900*----------------------------------------------------------------*PPEC143
004000                                                                  PPEC143
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEC143
004220*****'PPEC143 /121597'.                                           EFIX1285
004230     'PPEC143 /051500'.                                           EFIX1285
004300                                                                  PPEC143
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEC143
004500     A-STND-PROG-ID.                                              PPEC143
004600     05  FILLER                       PIC X(03).                  PPEC143
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEC143
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEC143
004900     05  FILLER                       PIC X(08).                  PPEC143
007410                                                                  PPEC143
007420 01  CALLED-MODULES.                               COPY CPWSXRTN. PPEC143
007422                                                                  PPEC143
007423 01  MESSAGES-USED.                                               PPEC143
007435     05  M12310                       PIC X(05) VALUE '12310'.    PPEC143
007442                                                                  PPEC143
007443 01  ELEMENTS-USED.                                               PPEC143
009103     05  E0377                        PIC 9(04) VALUE 0377.       PPEC143
009104     05  E0752                        PIC 9(04) VALUE 0752.       PPEC143
009105     05  E0753                        PIC 9(04) VALUE 0753.       PPEC143
009106     05  E0754                        PIC 9(04) VALUE 0754.       PPEC143
009107     05  E0755                        PIC 9(04) VALUE 0755.       PPEC143
009108     05  E0756                        PIC 9(04) VALUE 0756.       PPEC143
009109     05  E0757                        PIC 9(04) VALUE 0757.       PPEC143
009110     05  E0758                        PIC 9(04) VALUE 0758.       PPEC143
009111     05  E0759                        PIC 9(04) VALUE 0759.       PPEC143
009115                                                                  PPEC143
009116 01  MISC-WORK-AREAS.                                             PPEC143
009117     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEC143
009118         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEC143
009119         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEC143
009127     05  PPA-SUB                 PIC S9(03) COMP VALUE 0.         PPEC143
009128     05  INIT-PPA-KEY.                                            PPEC143
009129         07  FILLER                  PIC X(2)   VALUE SPACES.     PPEC143
009130         07  FILLER             PIC X(10)  VALUE '0001-01-01'.    PPEC143
009131     05  PREMIUM-IND-SW               PIC 9(02) VALUE 0.          PPEC143
009132         88  DONT-SET-PREMIUM-IND               VALUE 0.          PPEC143
009133         88  SET-PREMIUM-IND                    VALUE 1.          PPEC143
009134                                                                  PPEC143
009135     EJECT                                                        PPEC143
009147 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEC143
009148                                                                  PPEC143
009150     EJECT                                                        PPEC143
009210 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. PPEC143
009300                                                                  PPEC143
009400     EJECT                                                        PPEC143
009500 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEC143
009600                                                                  PPEC143
009700     EJECT                                                        PPEC143
009800 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEC143
009900                                                                  PPEC143
010000     EJECT                                                        PPEC143
010400 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEC143
010500                                                                  PPEC143
010600     EJECT                                                        PPEC143
010700 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEC143
010800                                                                  PPEC143
010900     EJECT                                                        PPEC143
010910 01  PAYROLL-PROCESS-INST-CONST-2.                 COPY CPWSXIC2. PPEC143
010920                                                                  PPEC143
010930     EJECT                                                        PPEC143
011300******************************************************************PPEC143
011400**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEC143
011500**--------------------------------------------------------------**PPEC143
011600** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEC143
011700******************************************************************PPEC143
011800                                                                  PPEC143
011900 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEC143
012000                                                                  PPEC143
012100     EJECT                                                        PPEC143
012200 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEC143
012300                                                                  PPEC143
012400     EJECT                                                        PPEC143
012500 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEC143
012600                                                                  PPEC143
012700     EJECT                                                        PPEC143
012800 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEC143
012900                                                                  PPEC143
013000 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEC143
013100                                                                  PPEC143
013200 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEC143
013300                                                                  PPEC143
013400 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEC143
013500                                                                  PPEC143
013600     EJECT                                                        PPEC143
013700 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEC143
013800                                                                  PPEC143
013900     EJECT                                                        PPEC143
014000 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEC143
014100                                                                  PPEC143
014200     EJECT                                                        PPEC143
014300 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEC143
014400                                                                  PPEC143
014500     EJECT                                                        PPEC143
014540 01  XACN-ACTION-ARRAY                   EXTERNAL. COPY CPWSXACN. PPEC143
014541                                                                  PPEC143
014542     EJECT                                                        PPEC143
014600******************************************************************PPEC143
014700**  DATA BASE AREAS                                             **PPEC143
014800******************************************************************PPEC143
015600                                                                  PPEC143
015800 01  BEN-ROW                             EXTERNAL.                PPEC143
015900     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPEC143
016100     EJECT                                                        PPEC143
016110                                                                  PPEC143
016410 01  PPA-DATA-ARRAY                      EXTERNAL. COPY CPWSEPPA. PPEC143
016500     EJECT                                                        PPEC143
016600******************************************************************PPEC143
016700**  DATA BASE SAVE AREAS                                        **PPEC143
016800******************************************************************PPEC143
016900                                                                  PPEC143
017000 01  BEN-ROW-HOLD                        EXTERNAL. COPY CPWSRBEN  PPEC143
017100                                                                  PPEC143
017110         REPLACING  HLTH-OPTOUT       BY SAVE-HLTH-OPTOUT         PPEC143
017120                    HLTH-PLAN         BY SAVE-HLTH-PLAN           PPEC143
017130                    HLTH-COVERCODE    BY SAVE-HLTH-COVERCODE      PPEC143
017140                    HLTH-COVEFFDATE   BY SAVE-HLTH-COVEFFDATE     PPEC143
019921                    PIE-END-DATE      BY SAVE-PIE-END-DATE        PPEC143
019922                    PIN-SIGN-DATE     BY SAVE-PIN-SIGN-DATE       PPEC143
019923                    SUS-PREMIUM-IND   BY SAVE-SUS-PREMIUM-IND.    PPEC143
019937                                                                  PPEC143
019938     EJECT                                                        PPEC143
019939******************************************************************PPEC143
019940**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEC143
019941******************************************************************PPEC143
019942                                                                  PPEC143
019943*----------------------------------------------------------------*PPEC143
019944 LINKAGE SECTION.                                                 PPEC143
019950*----------------------------------------------------------------*PPEC143
020000                                                                  PPEC143
020100******************************************************************PPEC143
020200**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEC143
020300**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEC143
020400******************************************************************PPEC143
020500                                                                  PPEC143
020600 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEC143
020700                                                                  PPEC143
020800     EJECT                                                        PPEC143
020900******************************************************************PPEC143
021000**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEC143
021100**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEC143
021200******************************************************************PPEC143
021300                                                                  PPEC143
021400 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEC143
021500                                                                  PPEC143
021600     EJECT                                                        PPEC143
021700******************************************************************PPEC143
021800**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEC143
021900******************************************************************PPEC143
022000                                                                  PPEC143
022100 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEC143
022200                          KMTA-MESSAGE-TABLE-ARRAY.               PPEC143
022300                                                                  PPEC143
022400                                                                  PPEC143
022500*----------------------------------------------------------------*PPEC143
022600 0000-DRIVER.                                                     PPEC143
022700*----------------------------------------------------------------*PPEC143
022800                                                                  PPEC143
022900******************************************************************PPEC143
023000**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEC143
023100******************************************************************PPEC143
023200                                                                  PPEC143
023300     IF  THIS-IS-THE-FIRST-TIME                                   PPEC143
023400         PERFORM 0100-INITIALIZE                                  PPEC143
023500     END-IF.                                                      PPEC143
023600                                                                  PPEC143
023700******************************************************************PPEC143
023800**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEC143
023900**  TO THE CALLING PROGRAM                                      **PPEC143
024000******************************************************************PPEC143
024100                                                                  PPEC143
024200     PERFORM 1000-MAINLINE-ROUTINE.                               PPEC143
024300                                                                  PPEC143
024400     EXIT PROGRAM.                                                PPEC143
024500                                                                  PPEC143
024600                                                                  PPEC143
024700     EJECT                                                        PPEC143
024800*----------------------------------------------------------------*PPEC143
024900 0100-INITIALIZE    SECTION.                                      PPEC143
025000*----------------------------------------------------------------*PPEC143
025200******************************************************************PPEC143
025300**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEC143
025400******************************************************************PPEC143
025600     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEC143
025800*                                               *-------------*   PPEC143
025900                                                 COPY CPPDXWHC.   PPEC143
026000*                                               *-------------*   PPEC143
026800******************************************************************PPEC143
026900**   SET FIRST CALL SWITCH OFF                                  **PPEC143
027000******************************************************************PPEC143
027200     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEC143
027400     EJECT                                                        PPEC143
027500*----------------------------------------------------------------*PPEC143
027600 1000-MAINLINE-ROUTINE SECTION.                                   PPEC143
027700*----------------------------------------------------------------*PPEC143
027800                                                                  PPEC143
050620******************************************************************PPEC143
050621**   IF HEALTH PLAN IS OPTED OUT AND PIE END DATE HAS NOT BEEN  **PPEC143
050622**   ACHIEVED, DELETE MEDICAL TYPE PENDING PREMIUM RECORDS      **PPEC143
050623******************************************************************PPEC143
050624     MOVE HLTH-OPTOUT     TO W88-HLTH-OPTOUT.                     PPEC143
050625     IF  HLTH-OPTOUT-HLTH-OPT-OUT                                 PPEC143
050626*****    AND PIE-END-DATE >  XDTS-ISO-DLY-RUN-DATE                EFIX1285
050627             PERFORM 8000-DELETE-MEDICAL-PPA                      PPEC143
050628     END-IF.                                                      PPEC143
062824                                                                  PPEC143
062825     IF (SAVE-HLTH-PLAN NOT =  HLTH-PLAN)  OR                     PPEC143
062826        (SAVE-HLTH-COVERCODE  NOT =  HLTH-COVERCODE)              PPEC143
062827*****    IF PIE-END-DATE > XDTS-ISO-DLY-RUN-DATE                  EFIX1285
062829            PERFORM 8050-CHANGE-MEDICAL-PPA                       PPEC143
062830*****    END-IF                                                   EFIX1285
062831     END-IF.                                                      PPEC143
085233     EJECT                                                        PPEC143
085234                                                                  PPEC143
085235*----------------------------------------------------------------*PPEC143
085236 8000-DELETE-MEDICAL-PPA         SECTION.                         PPEC143
085237*----------------------------------------------------------------*PPEC143
085238******************************************************************PPEC143
085239* CHECK FOR PENDING PREMIUM ACTIVITY(PPA) DATA.                  *PPEC143
085240* IF ANY PPA MEDICAL DATA, TYPE 'M', IS PRESENT IT WILL BE DELETEDPPEC143
085241*    INITIALIZE THE PENDPREM ARRAY AND TURN THE CHANGE FLAG      *PPEC143
085242*    ON FOR EACH DATA ELEMENT.                                   *PPEC143
085243******************************************************************PPEC143
085245                                                                  PPEC143
085248     PERFORM VARYING PPA-SUB FROM 1 BY 1                          PPEC143
085251               UNTIL PPA-SUB > IDC-MAX-NO-PPA                     PPEC143
085254       IF EPPA-PPA-BENEFIT-TYPE (PPA-SUB) = 'M'                   PPEC143
085255          INITIALIZE  EPPA-PENDPREM-DATA (PPA-SUB)                PPEC143
085260          MOVE '0001-01-01' TO EPPA-PPA-PAY-END-DATE (PPA-SUB)    PPEC143
085261          MOVE E0753                TO DL-FIELD                   PPEC143
085262          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085263          MOVE E0754                TO DL-FIELD                   PPEC143
085264          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085265          MOVE E0755                TO DL-FIELD                   PPEC143
085266          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085267          MOVE E0756                TO DL-FIELD                   PPEC143
085268          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085269          MOVE E0757                TO DL-FIELD                   PPEC143
085270          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085271          MOVE E0758                TO DL-FIELD                   PPEC143
085272          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085273          MOVE E0759                TO DL-FIELD                   PPEC143
085274          PERFORM 9050-AUDITING-RESPONSIBILITIES                  PPEC143
085275       END-IF                                                     PPEC143
085276     END-PERFORM.                                                 PPEC143
085278     EJECT                                                        PPEC143
085279                                                                  PPEC143
085280*----------------------------------------------------------------*PPEC143
085281 8050-CHANGE-MEDICAL-PPA         SECTION.                         PPEC143
085282*----------------------------------------------------------------*PPEC143
085283******************************************************************PPEC143
085284* CHECK FOR PENDING PREMIUM ACTIVITY(PPA) DATA.                  *PPEC143
085285* IF ANY PPA MEDICAL DATA (TYPE 'M') IS PRESENT IT WILL BE CHANGEDPPEC143
085286*    MOVE THE PLAN AND COVERAGE    AND TURN THE CHANGE FLAG      *PPEC143
085287*    ON FOR EACH OF THESE DATA ELEMENTS AND SET THE PREMIUM IND  *PPEC143
085288******************************************************************PPEC143
085289      SET DONT-SET-PREMIUM-IND TO TRUE.                           PPEC143
085290                                                                  PPEC143
085291     PERFORM VARYING PPA-SUB FROM 1 BY 1                          PPEC143
085292               UNTIL PPA-SUB > IDC-MAX-NO-PPA                     PPEC143
085293       IF  EPPA-PPA-BENEFIT-TYPE (PPA-SUB) = 'M'                  PPEC143
085301       AND EPPA-PPA-FLAG (PPA-SUB) NOT = '2'                      PPEC143
085302           SET SET-PREMIUM-IND TO TRUE                            PPEC143
085303           MOVE HLTH-PLAN TO EPPA-PPA-PLAN-CODE (PPA-SUB)         PPEC143
085304           MOVE E0756                TO DL-FIELD                  PPEC143
085305           PERFORM 9050-AUDITING-RESPONSIBILITIES                 PPEC143
085306           MOVE '1' TO EPPA-PPA-FLAG (PPA-SUB)                    PPEC143
085307           MOVE E0758                TO DL-FIELD                  PPEC143
085308           PERFORM 9050-AUDITING-RESPONSIBILITIES                 PPEC143
085309           IF HLTH-COVERCODE NOT = EPPA-PPA-COVERAGE (PPA-SUB)    PPEC143
085310              MOVE HLTH-COVERCODE TO EPPA-PPA-COVERAGE (PPA-SUB)  PPEC143
085311              MOVE E0757             TO DL-FIELD                  PPEC143
085312              PERFORM 9050-AUDITING-RESPONSIBILITIES              PPEC143
085313           END-IF                                                 PPEC143
085314       END-IF                                                     PPEC143
085315     END-PERFORM.                                                 PPEC143
085317     IF SET-PREMIUM-IND                                           PPEC143
085318        IF SUS-PREMIUM-IND NOT = 'Y'                              PPEC143
085319           MOVE 'Y' TO SUS-PREMIUM-IND                            PPEC143
085320           MOVE E0752 TO DL-FIELD                                 PPEC143
085321           PERFORM 9050-AUDITING-RESPONSIBILITIES                 PPEC143
085322        END-IF                                                    PPEC143
085323     END-IF.                                                      PPEC143
085324     EJECT                                                        PPEC143
085325*----------------------------------------------------------------*PPEC143
085326 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEC143
085327*----------------------------------------------------------------*PPEC143
085328*                                                 *-------------* PPEC143
085329                                                   COPY CPPDXDEC. PPEC143
085330*                                                 *-------------* PPEC143
085331*----------------------------------------------------------------*PPEC143
085332 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEC143
085333*----------------------------------------------------------------*PPEC143
085334*                                                 *-------------* PPEC143
085335                                                   COPY CPPDADLC. PPEC143
085336*                                                 *-------------* PPEC143
085337     EJECT                                                        PPEC143
085338*----------------------------------------------------------------*PPEC143
085339 9300-DATE-CONVERSION-DB2  SECTION.                               PPEC143
085340*----------------------------------------------------------------*PPEC143
085342*                                                 *-------------* PPEC143
085344                                                   COPY CPPDXDC3. PPEC143
085345*                                                 *-------------* PPEC143
085347     EJECT                                                        PPEC143
085357******************************************************************PPEC143
085360**   E N D  S O U R C E   ----  PPEC143 ----                    **PPEC143
085400******************************************************************PPEC143
