000100**************************************************************/   36520886
000200*  PROGRAM: PPDXSABB                                         */   36520886
000300*  RELEASE: ___0886______ SERVICE REQUEST(S): _____3652____  */   36520886
000400*  NAME:_______DXS_______ MODIFICATION DATE:  __03/30/94____ */   36520886
000500*  DESCRIPTION: CREATES THE SABBATICAL TEXT BLOCK FOR        */   36520886
000600*   THE SABBATICAL EMPLOYEE DOCUMENT.                        */   36520886
000700**************************************************************/   36520886
000800*----------------------------------------------------------------*PPDXSABB
000900 IDENTIFICATION DIVISION.                                         PPDXSABB
001000 PROGRAM-ID. PPDXSABB.                                            PPDXSABB
001100 AUTHOR. UCOP.                                                    PPDXSABB
001200 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPDXSABB
001300*REMARKS.                                                         PPDXSABB
001400******************************************************************PPDXSABB
001500*----------------------------------------------------------------*PPDXSABB
001600*                                                                *PPDXSABB
001700* FUNCTION: FORMAT TEXT BLOCK OF SABBATICAL DATA FOR AN          *PPDXSABB
001800*           EMPLOYEE DOCUMENT                                    *PPDXSABB
001900* INPUT:  CPWSDXIN - EMPLOYEE ID                                 *PPDXSABB
002000* OUTPUT: CPWSDXTX - TEXT BLOCK                                  *PPDXSABB
002100*         CPWSDXTX - ERROR/STATUS CODES                          *PPDXSABB
002200*                                                                *PPDXSABB
002300*----------------------------------------------------------------*PPDXSABB
002400 DATE-WRITTEN.  FEB. 1994.                                        PPDXSABB
002500 DATE-COMPILED. FEB 1994.                                         PPDXSABB
002600 ENVIRONMENT DIVISION.                                            PPDXSABB
002700 CONFIGURATION SECTION.                                           PPDXSABB
002800 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPDXSABB
002900 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPDXSABB
003000 INPUT-OUTPUT SECTION.                                            PPDXSABB
003100 FILE-CONTROL.                                                    PPDXSABB
003200     EJECT                                                        PPDXSABB
003300 DATA DIVISION.                                                   PPDXSABB
003400 FILE SECTION.                                                    PPDXSABB
003500*----------------------------------------------------------------*PPDXSABB
003600 WORKING-STORAGE SECTION.                                         PPDXSABB
003700*----------------------------------------------------------------*PPDXSABB
003800 01  A-STND-PROG-ID               PIC X(8) VALUE 'PPDXSABB'.      PPDXSABB
003900*                                                                 PPDXSABB
004000 01  SABB-LINE-TEXT-WORK-AREA.                                    PPDXSABB
004100     05  SABB-LINE-FULL.                                          PPDXSABB
004200         10  SABB-LINE-CC            PIC X(01).                   PPDXSABB
004300         10  SABB-LINE               PIC X(79).                   PPDXSABB
004400     05  SABB-LINEA.                                              PPDXSABB
004500         10  FILLER                  PIC X(06)  VALUE             PPDXSABB
004600             ' Your '.                                            PPDXSABB
004700         10  SABB-TYPE-OF-SABB       PIC X(34).                   PPDXSABB
004800         10  FILLER                  PIC X(04)  VALUE             PPDXSABB
004900             ' at '.                                              PPDXSABB
005000         10  SABB-DIS-PERCENT-SALARY PIC Z(06).                   PPDXSABB
005100         10  FILLER                  PIC X(22)  VALUE             PPDXSABB
005200             ' percent salary, from '.                            PPDXSABB
005300     05  SABB-LINEB.                                              PPDXSABB
005400         10  FILLER                  PIC X(23)  VALUE             PPDXSABB
005500             ' the University begins '.                           PPDXSABB
005600         10  SABB-SABB-BEGIN-DATE    PIC X(08).                   PPDXSABB
005700         10  SABB-PERIOD-1           PIC X(01)  VALUE '.'.        PPDXSABB
005800     05  SABB-LINEC.                                              PPDXSABB
005900         10  FILLER                  PIC X(54)  VALUE             PPDXSABB
006000      ' You are scheduled to return from Sabbatical Leave on '.   PPDXSABB
006100         10  SABB-SABB-END-DATE     PIC X(08).                    PPDXSABB
006200         10  SABB-PERIOD-2           PIC X(01)  VALUE '.'.        PPDXSABB
006300     05  SABB-LINED.                                              PPDXSABB
006400         10  FILLER                  PIC X(36)  VALUE             PPDXSABB
006500             ' Your Sabbatical Credit Balance is '.               PPDXSABB
006600         10  SABB-TOT-CREDIT-BAL     PIC Z(02).                   PPDXSABB
006700         10  FILLER                  PIC X(13)  VALUE             PPDXSABB
006800             ' units as of '.                                     PPDXSABB
006900         10  SABB-CREDIT-THRU-DATE  PIC X(08).                    PPDXSABB
007000         10  SABB-PERIOD-3           PIC X(01)  VALUE '.'.        PPDXSABB
007100*                                                                 PPDXSABB
007200 01  WS-MISC-DATA.                                                PPDXSABB
007300     05  CARRIAGE-CNTL          PIC X     VALUE SPACE.            PPDXSABB
007400         88  CARRIAGE-CNTL-SINGLE-SPACE   VALUE SPACE.            PPDXSABB
007500         88  CARRIAGE-CNTL-DOUBLE-SPACE   VALUE '0'.              PPDXSABB
007600         88  CARRIAGE-CNTL-TOP-PAGE       VALUE '1'.              PPDXSABB
007700     05  FIRST-TIME-SW                PIC 9   VALUE ZERO.         PPDXSABB
007800         88  FIRST-TIME-SW-OFF                VALUE ZERO.         PPDXSABB
007900         88  FIRST-TIME-SW-ON                 VALUE 1.            PPDXSABB
008000     05  WS-CEEDAYS-DATE        PIC X(10) VALUE SPACES.           PPDXSABB
008100     05  WS-ISO-ZERO-DATE       PIC X(10) VALUE '0001-01-01'.     PPDXSABB
008200     05  WS-CURR-DATE-ISO       PIC X(10) VALUE '0001-01-01'.     PPDXSABB
008300     05  WS-ISO-END-DATE        PIC X(10) VALUE '9999-12-31'.     PPDXSABB
008400     05  WS-99-END-DATE         PIC X(08) VALUE '99/99/99'.       PPDXSABB
008500     05  WS-SAVE-LINE           PIC X(79) VALUE SPACES.           PPDXSABB
008600     05  WS-PERCENT             PIC 9(3)V99 VALUE ZERO.           PPDXSABB
008700     05  WS-HOLD-PERCENT        PIC 9(3)V99 VALUE ZERO.           PPDXSABB
008800     05  WS-DIS-PERCENT         PIC Z(3).ZZ VALUE ZERO.           PPDXSABB
008900     05  WS-DOS-CODE            PIC X(3).                         PPDXSABB
009000*                                                                 PPDXSABB
009100 01  APPT-DIST-SUBSCRIPTS.                                        PPDXSABB
009200     05  SEG-SUB                  PIC 9(02)  VALUE ZERO.          PPDXSABB
009300         88  SEG-SUB-IS-APPT                 VALUES 1  4  7 10    PPDXSABB
009400                                                   13 16 19 22    PPDXSABB
009500                                                   25.            PPDXSABB
009600     05  ELEM-SUB                  PIC 9(01)  VALUE ZERO.         PPDXSABB
009700*                                                                 PPDXSABB
009800 01  WS-CTT-KEY.                                                  PPDXSABB
009900     05  WS-CTT-DATABASE-ID           PIC X(03).                  PPDXSABB
010000     05  WS-CTT-DATA-ELEM-NO         PIC X(06).                   PPDXSABB
010100     05  WS-CTT-CODE-VALUE           PIC X(06).                   PPDXSABB
010200     05  WS-CTT-TRANS-LEN            PIC S9(04) COMP.             PPDXSABB
010300*                                                                 PPDXSABB
010400 01  WS-CALLED-ROUTINES.                                          PPDXSABB
010500     05  PGM-PPCTTUTL              PIC X(08) VALUE 'PPCTTUTL'.    PPDXSABB
010600     05  PGM-PPAXDUTL              PIC X(08) VALUE 'PPAXDUTL'.    PPDXSABB
010700*                                                                 PPDXSABB
010800 01  PPAXDUTL-INTERFACE.            COPY CPLNKAXD.                PPDXSABB
010900*                                                                 PPDXSABB
011000 01  PPDB2MSG-INTERFACE.                         COPY CPLNKDB2.   PPDXSABB
011100*                                                                 PPDXSABB
011200 01  AXD-ARRAY.                                                   PPDXSABB
011300     03  FILLER      OCCURS 27.                                   PPDXSABB
011400         07  APP-ROW-DELETE-BYTE  PIC X(01).                      PPDXSABB
011500             88  APP-ROW-DELETED             VALUE HIGH-VALUES.   PPDXSABB
011600         07  APP-ROW-SEG-ID       PIC X(02).                      PPDXSABB
011700         07  FILLER  OCCURS 3.                                    PPDXSABB
011800             08  APP-ROW.                        COPY CPWSRAPP    PPDXSABB
011900                 REPLACING WORK-STUDY-PGM                         PPDXSABB
012000                        BY NULL-APPT-WORK-STUDY.                  PPDXSABB
012100     03  FILLER      OCCURS 27.                                   PPDXSABB
012200         07  DIS-ROW-DELETE-BYTE  PIC X(01).                      PPDXSABB
012300             88  DIS-ROW-DELETED             VALUE HIGH-VALUES.   PPDXSABB
012400         07  DIS-ROW-SEG-ID       PIC X(02).                      PPDXSABB
012500         07  FILLER  OCCURS 3.                                    PPDXSABB
012600             08  DIS-ROW.                        COPY CPWSRDIS.   PPDXSABB
012700*                                                                 PPDXSABB
012800 01  XWHC-COMPILE-WORK-AREA.                     COPY CPWSXWHC.   PPDXSABB
012900     EJECT                                                        PPDXSABB
013000 01  DATE-CONVERSION.                            COPY CPWSDAYS.   PPDXSABB
013100     EJECT                                                        PPDXSABB
013200******************************************************************PPDXSABB
013300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPDXSABB
013400**--------------------------------------------------------------**PPDXSABB
013500*                                                                 PPDXSABB
013600 01  CPWSDXIN        EXTERNAL.         COPY CPWSDXIN.             PPDXSABB
013700*                                                                 PPDXSABB
013800 01  CPWSDXTX        EXTERNAL.         COPY CPWSDXTX.             PPDXSABB
013900*                                                                 PPDXSABB
014000 01  CPLNKCTT        EXTERNAL.         COPY CPLNKCTT.             PPDXSABB
014100*                                                                 PPDXSABB
014200*                                                                 PPDXSABB
014300******************************************************************PPDXSABB
014400**  DATA BASE AREAS                                             **PPDXSABB
014500******************************************************************PPDXSABB
014600*                                                                 PPDXSABB
014700 01  DOC-PER-ROW     EXTERNAL.         COPY CPWSRPER.             PPDXSABB
014800*                                                                 PPDXSABB
014900     EJECT                                                        PPDXSABB
015000 01  DOS-ROW.                                                     PPDXSABB
015100     EXEC SQL INCLUDE PPPVZDOS  END-EXEC.                         PPDXSABB
015200     EJECT                                                        PPDXSABB
015300*                                                                 PPDXSABB
015400     EXEC SQL                                                     PPDXSABB
015500          INCLUDE SQLCA                                           PPDXSABB
015600     END-EXEC.                                                    PPDXSABB
015700*----------------------------------------------------------------*PPDXSABB
015800*----------------------------------------------------------------*PPDXSABB
015900 PROCEDURE DIVISION.                                              PPDXSABB
016000*----------------------------------------------------------------*PPDXSABB
016100     EXEC SQL                                                     PPDXSABB
016200          INCLUDE CPPDXE99                                        PPDXSABB
016300     END-EXEC.                                                    PPDXSABB
016400*----------------------------------------------------------------*PPDXSABB
016500*                                                                 PPDXSABB
016600     IF FIRST-TIME-SW-OFF                                         PPDXSABB
016700         SET  FIRST-TIME-SW-ON   TO TRUE                          PPDXSABB
016800         PERFORM 1000-INITIALIZATION                              PPDXSABB
016900     END-IF.                                                      PPDXSABB
017000*                                                                 PPDXSABB
017100     PERFORM 2000-CREATE-TEXT-LINES.                              PPDXSABB
017200*                                                                 PPDXSABB
017300 0500-PROGRAM-EXIT.                                               PPDXSABB
017400**                                                                PPDXSABB
017500     GOBACK.                                                      PPDXSABB
017600*                                                                 PPDXSABB
017700**--------------------------------------------------------------  PPDXSABB
017800 1000-INITIALIZATION   SECTION.                                   PPDXSABB
017900**--------------------------------------------------------------  PPDXSABB
018000*                                                                 PPDXSABB
018100     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPDXSABB
018200**--------------------------------------------------------------  PPDXSABB
018300**            SET UP DATE CONVERSION WORK AREAS                   PPDXSABB
018400**--------------------------------------------------------------  PPDXSABB
018500     PERFORM 3100-GET-CURRENT-DATE.                               PPDXSABB
018600     MOVE WS-ISO-DATE-MASK TO CEEDAYS-MASK.                       PPDXSABB
018700     MOVE +10 TO CEEDAYS-DATE-LENGTH, CEEDAYS-MASK-LENGTH.        PPDXSABB
018800     MOVE +8  TO CEEDATE-MASK-LENGTH.                             PPDXSABB
018900     MOVE WS-STD-DATE-MASK TO CEEDATE-MASK.                       PPDXSABB
019000*                                                 *-------------* PPDXSABB
019100                                                   COPY CPPDXWHC. PPDXSABB
019200*                                                 *-------------* PPDXSABB
019300                                                                  PPDXSABB
019400 1000-EXIT.    EXIT.                                              PPDXSABB
019500**--------------------------------------------------------------  PPDXSABB
019600*          CREATE TEXT LINES                                      PPDXSABB
019700**--------------------------------------------------------------  PPDXSABB
019800 2000-CREATE-TEXT-LINES       SECTION.                            PPDXSABB
019900**--------------------------------------------------------------  PPDXSABB
020000*                                                                 PPDXSABB
020100     IF CPWSDXIN-LINE-COUNT > 54                                  PPDXSABB
020200        ADD +4 TO CPWSDXIN-LINE-COUNT                             PPDXSABB
020300     END-IF.                                                      PPDXSABB
020400*                                                                 PPDXSABB
020500     MOVE SPACES TO SABB-LINE.                                    PPDXSABB
020600     SET CARRIAGE-CNTL-DOUBLE-SPACE TO TRUE.                      PPDXSABB
020700     MOVE 'SABBATICAL INFORMATION'       TO SABB-LINE.            PPDXSABB
020800     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
020900*                                                                 PPDXSABB
021000     SET CARRIAGE-CNTL-SINGLE-SPACE TO TRUE.                      PPDXSABB
021100     MOVE ALL '-' TO SABB-LINE.                                   PPDXSABB
021200     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
021300*                                                                 PPDXSABB
021400     SET CARRIAGE-CNTL-SINGLE-SPACE TO TRUE.                      PPDXSABB
021500     MOVE CPWSDXIN-ACTION-CTT-KEY-1  TO WS-CTT-KEY.               PPDXSABB
021600     MOVE WS-CTT-CODE-VALUE    TO CPLNKCTT-CODE-VALUE             PPDXSABB
021700     MOVE WS-CTT-DATABASE-ID   TO CPLNKCTT-DATABASE-ID.           PPDXSABB
021800     MOVE WS-CTT-DATA-ELEM-NO  TO CPLNKCTT-DATA-ELEM-NO.          PPDXSABB
021900     MOVE WS-CTT-TRANS-LEN     TO CPLNKCTT-TRANS-LEN.             PPDXSABB
022000     PERFORM 4000-GET-DESC-FROM-CTT.                              PPDXSABB
022100     MOVE CPLNKCTT-TRANSLATION TO SABB-TYPE-OF-SABB.              PPDXSABB
022200     PERFORM 7300-GET-APPT-DIST-ROWS.                             PPDXSABB
022300     PERFORM 2300-CHECK-ALL-APPTS.                                PPDXSABB
022400     MOVE WS-DIS-PERCENT TO SABB-DIS-PERCENT-SALARY.              PPDXSABB
022500     MOVE SABB-LINEA TO SABB-LINE.                                PPDXSABB
022600     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
022700*                                                                 PPDXSABB
022800     MOVE LOA-BEGIN-DATE OF DOC-PER-ROW TO CEEDAYS-DATE.          PPDXSABB
022900     PERFORM 3000-CALL-CEEDAYS.                                   PPDXSABB
023000     MOVE CEE-FORMATTED-DATE TO SABB-SABB-BEGIN-DATE.             PPDXSABB
023100     MOVE SABB-LINEB TO SABB-LINE.                                PPDXSABB
023200     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
023300*                                                                 PPDXSABB
023400     SET CARRIAGE-CNTL-DOUBLE-SPACE TO TRUE.                      PPDXSABB
023500     MOVE LOA-RETURN-DATE OF DOC-PER-ROW TO CEEDAYS-DATE.         PPDXSABB
023600     PERFORM 3000-CALL-CEEDAYS.                                   PPDXSABB
023700     MOVE CEE-FORMATTED-DATE TO SABB-SABB-END-DATE.               PPDXSABB
023800     MOVE SABB-LINEC TO SABB-LINE.                                PPDXSABB
023900     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
024000*                                                                 PPDXSABB
024100     SET CARRIAGE-CNTL-DOUBLE-SPACE TO TRUE.                      PPDXSABB
024200     MOVE TOT-CREDIT-BAL OF DOC-PER-ROW TO SABB-TOT-CREDIT-BAL.   PPDXSABB
024300     MOVE CREDIT-THRU-DATE OF DOC-PER-ROW TO CEEDAYS-DATE.        PPDXSABB
024400     PERFORM 3000-CALL-CEEDAYS.                                   PPDXSABB
024500     MOVE CEE-FORMATTED-DATE TO SABB-CREDIT-THRU-DATE.            PPDXSABB
024600     MOVE SABB-LINED TO SABB-LINE.                                PPDXSABB
024700     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
024800*                                                                 PPDXSABB
024900     SET CARRIAGE-CNTL-SINGLE-SPACE TO TRUE.                      PPDXSABB
025000     MOVE ALL '-' TO SABB-LINE.                                   PPDXSABB
025100     PERFORM 5500-MOVE-TEXT-LINE.                                 PPDXSABB
025200*                                                                 PPDXSABB
025300 2000-EXIT.    EXIT.                                              PPDXSABB
025400*----------------------------------------------------------------*PPDXSABB
025500 2300-CHECK-ALL-APPTS   SECTION.                                  PPDXSABB
025600*----------------------------------------------------------------*PPDXSABB
025700     MOVE ZERO    TO WS-HOLD-PERCENT                              PPDXSABB
025800                     WS-DIS-PERCENT                               PPDXSABB
025900                     ELEM-SUB.                                    PPDXSABB
026000     MOVE 1       TO SEG-SUB.                                     PPDXSABB
026100 2300-AXD-LOOP.                                                   PPDXSABB
026200     ADD 1                      TO ELEM-SUB.                      PPDXSABB
026300     IF  ELEM-SUB     > 3                                         PPDXSABB
026400         MOVE 1                 TO ELEM-SUB                       PPDXSABB
026500         ADD 1                  TO SEG-SUB                        PPDXSABB
026600     END-IF.                                                      PPDXSABB
026700     IF  SEG-SUB   > 27                                           PPDXSABB
026800         GO TO 2300-EXIT                                          PPDXSABB
026900     END-IF.                                                      PPDXSABB
027000     IF  ELEM-SUB = 1      AND                                    PPDXSABB
027100         SEG-SUB-IS-APPT                                          PPDXSABB
027200         IF APPT-NUM  OF APP-ROW (SEG-SUB, 1) > ZERO              PPDXSABB
027300             CONTINUE                                             PPDXSABB
027400         ELSE                                                     PPDXSABB
027500             ADD 3 TO SEG-SUB                                     PPDXSABB
027600             MOVE ZERO TO ELEM-SUB                                PPDXSABB
027700         END-IF                                                   PPDXSABB
027800     ELSE                                                         PPDXSABB
027900         IF DIST-NUM OF DIS-ROW (SEG-SUB, ELEM-SUB) > ZERO        PPDXSABB
028000             PERFORM 2500-PROCESS-DIST                            PPDXSABB
028100     END-IF.                                                      PPDXSABB
028200     GO TO 2300-AXD-LOOP.                                         PPDXSABB
028300 2300-EXIT.     EXIT.                                             PPDXSABB
028400     EJECT                                                        PPDXSABB
028500*----------------------------------------------------------------*PPDXSABB
028600 2500-PROCESS-DIST   SECTION.                                     PPDXSABB
028700*----------------------------------------------------------------*PPDXSABB
028800     IF PAY-END-DATE (SEG-SUB, ELEM-SUB)  <  WS-CURR-DATE-ISO     PPDXSABB
028900         GO TO 2500-EXIT                                          PPDXSABB
029000     END-IF.                                                      PPDXSABB
029100     IF PAY-BEGIN-DATE (SEG-SUB, ELEM-SUB) > WS-CURR-DATE-ISO     PPDXSABB
029200         GO TO 2500-EXIT                                          PPDXSABB
029300     END-IF.                                                      PPDXSABB
029400*                                                                 PPDXSABB
029500     IF DIST-DOS (SEG-SUB, ELEM-SUB) NOT EQUAL SPACES             PPDXSABB
029600         MOVE DIST-DOS (SEG-SUB, ELEM-SUB) TO WS-DOS-CODE         PPDXSABB
029700         PERFORM 8200-SELECT-DOS                                  PPDXSABB
029800         IF SQLCODE = +100                                        PPDXSABB
029900             CONTINUE                                             PPDXSABB
030000         ELSE                                                     PPDXSABB
030100             IF DOS-SABB-IND = 'Y'                                PPDXSABB
030200                 MOVE  ZERO TO WS-PERCENT                         PPDXSABB
030300                 COMPUTE WS-PERCENT =                             PPDXSABB
030400                      (DIST-PERCENT(SEG-SUB, ELEM-SUB) * +100)    PPDXSABB
030500                 IF WS-PERCENT > WS-HOLD-PERCENT                  PPDXSABB
030600                    MOVE WS-PERCENT TO WS-HOLD-PERCENT            PPDXSABB
030700                                       WS-DIS-PERCENT             PPDXSABB
030800                 END-IF                                           PPDXSABB
030900             END-IF                                               PPDXSABB
031000         END-IF                                                   PPDXSABB
031100     END-IF.                                                      PPDXSABB
031200*                                                                 PPDXSABB
031300 2500-EXIT.     EXIT.                                             PPDXSABB
031400     EJECT                                                        PPDXSABB
031500*----------------------------------------------------------------*PPDXSABB
031600 3000-CALL-CEEDAYS   SECTION.                                     PPDXSABB
031700*----------------------------------------------------------------*PPDXSABB
031800     MOVE CEEDAYS-DATE TO WS-CEEDAYS-DATE.                        PPDXSABB
031900     IF WS-CEEDAYS-DATE = WS-ISO-ZERO-DATE                        PPDXSABB
032000         MOVE SPACES  TO CEE-FORMATTED-DATE                       PPDXSABB
032100         GO TO 3000-EXIT                                          PPDXSABB
032200     END-IF.                                                      PPDXSABB
032300     IF WS-CEEDAYS-DATE = WS-ISO-END-DATE                         PPDXSABB
032400         MOVE WS-99-END-DATE TO CEE-FORMATTED-DATE                PPDXSABB
032500         GO TO 3000-EXIT                                          PPDXSABB
032600     END-IF.                                                      PPDXSABB
032700     CALL PGM-CEEDAYS USING CEEDAYS-INPUT, CEEDAYS-FORMAT,        PPDXSABB
032800                           LILIAN-DATE, CEE-RETURN-CODE.          PPDXSABB
032900     IF CEE-RETURN-CODE = LOW-VALUES                              PPDXSABB
033000         CALL PGM-CEEDATE USING LILIAN-DATE, CEEDATE-FORMAT,      PPDXSABB
033100                    CEE-FORMATTED-DATE, CEE-RETURN-CODE           PPDXSABB
033200         IF CEE-RETURN-CODE NOT = LOW-VALUES                      PPDXSABB
033300             MOVE SPACES  TO CEE-FORMATTED-DATE                   PPDXSABB
033400             SET CPWSDXIN-CEEDATE-ERROR  TO TRUE                  PPDXSABB
033500             MOVE 'CEEDATE ERROR FROM PPDXSABB'                   PPDXSABB
033600                                  TO CPWSDXIN-RETURN-REF-DESC     PPDXSABB
033700         END-IF                                                   PPDXSABB
033800     ELSE                                                         PPDXSABB
033900         MOVE SPACES  TO CEE-FORMATTED-DATE                       PPDXSABB
034000         SET CPWSDXIN-CEEDATE-ERROR  TO TRUE                      PPDXSABB
034100         MOVE 'CEEDAYS ERROR FROM PPDXSABB'                       PPDXSABB
034200                                  TO CPWSDXIN-RETURN-REF-DESC     PPDXSABB
034300     END-IF.                                                      PPDXSABB
034400*                                                                 PPDXSABB
034500 3000-EXIT.      EXIT.                                            PPDXSABB
034600*----------------------------------------------------------------*PPDXSABB
034700 3100-GET-CURRENT-DATE  SECTION.                                  PPDXSABB
034800*----------------------------------------------------------------*PPDXSABB
034900     CALL PGM-CEELOCT USING LILIAN-DATE, CEE-SECONDS,             PPDXSABB
035000                        CEE-GREGORIAN, CEE-RETURN-CODE.           PPDXSABB
035100     IF CEE-RETURN-CODE = LOW-VALUES                              PPDXSABB
035200         MOVE WS-ISO-DATE-MASK TO CEEDATE-MASK                    PPDXSABB
035300         MOVE +10 TO CEEDATE-MASK-LENGTH                          PPDXSABB
035400         CALL PGM-CEEDATE USING LILIAN-DATE, CEEDATE-FORMAT,      PPDXSABB
035500                    CEE-FORMATTED-DATE, CEE-RETURN-CODE           PPDXSABB
035600         IF CEE-RETURN-CODE NOT = LOW-VALUES                      PPDXSABB
035700             MOVE SPACES TO CEE-FORMATTED-DATE                    PPDXSABB
035800             SET CPWSDXIN-CEEDATE-ERROR  TO TRUE                  PPDXSABB
035900             MOVE 'CEEDATE ERROR FROM PPDXSALR'                   PPDXSABB
036000                                  TO CPWSDXIN-RETURN-REF-DESC     PPDXSABB
036100         ELSE                                                     PPDXSABB
036200             MOVE CEE-FORMATTED-DATE TO WS-CURR-DATE-ISO          PPDXSABB
036300         END-IF                                                   PPDXSABB
036400     ELSE                                                         PPDXSABB
036500         SET CPWSDXIN-CEEDATE-ERROR  TO TRUE                      PPDXSABB
036600         MOVE 'CEEDATE ERROR FROM PPDXSALR'                       PPDXSABB
036700                                  TO CPWSDXIN-RETURN-REF-DESC     PPDXSABB
036800     END-IF.                                                      PPDXSABB
036900*                                                                 PPDXSABB
037000 3100-EXIT.      EXIT.                                            PPDXSABB
037100**--------------------------------------------------------------  PPDXSABB
037200*         GET DESCRIPTION FROM CTT                                PPDXSABB
037300**--------------------------------------------------------------  PPDXSABB
037400 4000-GET-DESC-FROM-CTT       SECTION.                            PPDXSABB
037500**--------------------------------------------------------------  PPDXSABB
037600*                                                                 PPDXSABB
037700     IF CPLNKCTT-CODE-VALUE  = SPACES                             PPDXSABB
037800          MOVE SPACES       TO CPLNKCTT-TRANSLATION               PPDXSABB
037900          GO TO 4000-EXIT                                         PPDXSABB
038000     END-IF.                                                      PPDXSABB
038100     CALL PGM-PPCTTUTL.                                           PPDXSABB
038200     IF CPLNKCTT-NO-ERROR                                         PPDXSABB
038300          CONTINUE                                                PPDXSABB
038400     ELSE                                                         PPDXSABB
038500          SET CPWSDXIN-PPCTTUTL-ERROR  TO TRUE                    PPDXSABB
038600          MOVE SPACES             TO CPLNKCTT-TRANSLATION         PPDXSABB
038700          MOVE 'CTT ERROR FROM PPDXSABB'                          PPDXSABB
038800                                  TO CPWSDXIN-RETURN-REF-DESC     PPDXSABB
038900          MOVE CPLNKCTT           TO CPWSDXIN-RETURN-REF-DATA     PPDXSABB
039000     END-IF.                                                      PPDXSABB
039100 4000-EXIT.      EXIT.                                            PPDXSABB
039200*                                                                 PPDXSABB
039300**--------------------------------------------------------------  PPDXSABB
039400*          MOVE TEXT LINE TO DOCUMENT                             PPDXSABB
039500**--------------------------------------------------------------  PPDXSABB
039600 5500-MOVE-TEXT-LINE  SECTION.                                    PPDXSABB
039700**--------------------------------------------------------------  PPDXSABB
039800     IF CPWSDXIN-LINE-COUNT > 57                                  PPDXSABB
039900         MOVE SABB-LINE       TO WS-SAVE-LINE                     PPDXSABB
040000         SET CARRIAGE-CNTL-TOP-PAGE       TO TRUE                 PPDXSABB
040100         ADD 1         TO CPWSDXIN-PAGE-COUNT                     PPDXSABB
040200         MOVE CPWSDXIN-PAGE-COUNT  TO CPWSDXIN-SAVE-PG-HEAD-CNT   PPDXSABB
040300         MOVE CPWSDXIN-SAVE-PAGE-HEAD1  TO SABB-LINE              PPDXSABB
040400         PERFORM 5505-MOVE-IN-TEXT-LINE                           PPDXSABB
040500         MOVE CPWSDXIN-SAVE-PAGE-HEAD2  TO SABB-LINE              PPDXSABB
040600         PERFORM 5505-MOVE-IN-TEXT-LINE                           PPDXSABB
040700         MOVE CPWSDXIN-SAVE-PAGE-HEAD3  TO SABB-LINE              PPDXSABB
040800         PERFORM 5505-MOVE-IN-TEXT-LINE                           PPDXSABB
040900         MOVE WS-SAVE-LINE    TO SABB-LINE                        PPDXSABB
041000*  *      SET CARRIAGE CNTL FOR DOUBLE SPACE ON NEXT NORM LINE    PPDXSABB
041100         SET CARRIAGE-CNTL-DOUBLE-SPACE   TO TRUE                 PPDXSABB
041200     END-IF.                                                      PPDXSABB
041300     PERFORM 5505-MOVE-IN-TEXT-LINE.                              PPDXSABB
041400 5500-EXIT.    EXIT.                                              PPDXSABB
041500**--------------------------------------------------------------  PPDXSABB
041600 5505-MOVE-IN-TEXT-LINE  SECTION.                                 PPDXSABB
041700**--------------------------------------------------------------  PPDXSABB
041800     ADD +1        TO CPWSDXTX-LINES-RETURNED.                    PPDXSABB
041900     IF CPWSDXTX-LINES-RETURNED  > CPWSDXTX-MAX-LINES             PPDXSABB
042000         MOVE CPWSDXTX-MAX-LINES  TO CPWSDXTX-LINES-RETURNED      PPDXSABB
042100         SET  CPWSDXIN-TEXT-OVRFLO-ERR   TO TRUE                  PPDXSABB
042200     ELSE                                                         PPDXSABB
042300         MOVE CARRIAGE-CNTL   TO SABB-LINE-CC                     PPDXSABB
042400         MOVE SABB-LINE-FULL                                      PPDXSABB
042500                      TO CPWSDXTX-LINE (CPWSDXTX-LINES-RETURNED)  PPDXSABB
042600     END-IF.                                                      PPDXSABB
042700     EVALUATE  TRUE                                               PPDXSABB
042800         WHEN CARRIAGE-CNTL-TOP-PAGE                              PPDXSABB
042900              MOVE 1     TO CPWSDXIN-LINE-COUNT                   PPDXSABB
043000         WHEN CARRIAGE-CNTL-DOUBLE-SPACE                          PPDXSABB
043100              ADD 2      TO CPWSDXIN-LINE-COUNT                   PPDXSABB
043200         WHEN CARRIAGE-CNTL-SINGLE-SPACE                          PPDXSABB
043300              ADD 1      TO CPWSDXIN-LINE-COUNT                   PPDXSABB
043400     END-EVALUATE.                                                PPDXSABB
043500     SET CARRIAGE-CNTL-SINGLE-SPACE   TO TRUE.                    PPDXSABB
043600 5505-EXIT.    EXIT.                                              PPDXSABB
043700*                                                                 PPDXSABB
043800*-------------------------------------------------------------    PPDXSABB
043900 7300-GET-APPT-DIST-ROWS     SECTION.                             PPDXSABB
044000*-------------------------------------------------------------    PPDXSABB
044100*                                                                 PPDXSABB
044200     MOVE CPWSDXIN-EMPLOYEE-ID  TO PPAXDUTL-EMPLOYEE-ID.          PPDXSABB
044300     CALL PGM-PPAXDUTL  USING                                     PPDXSABB
044400                        PPAXDUTL-INTERFACE                        PPDXSABB
044500                        AXD-ARRAY                                 PPDXSABB
044600       ON EXCEPTION                                               PPDXSABB
044700         SET CPWSDXIN-CALL-FAILED  TO TRUE                        PPDXSABB
044800         MOVE 'CALL FROM PPDXSABB FAILED; MODULE IS '             PPDXSABB
044900                                   TO CPWSDXIN-RETURN-REF-DESC    PPDXSABB
045000         MOVE PGM-PPAXDUTL         TO CPWSDXIN-RETURN-REF-DATA    PPDXSABB
045100         GO TO 7300-EXIT                                          PPDXSABB
045200     END-CALL.                                                    PPDXSABB
045300     IF PPAXDUTL-ERROR                                            PPDXSABB
045400         SET CPWSDXIN-SQL-ERROR  TO TRUE                          PPDXSABB
045500         MOVE 'CALL FROM PPDXSABB FAILED WITH SQL ERROR IN: '     PPDXSABB
045600                                   TO CPWSDXIN-RETURN-REF-DESC    PPDXSABB
045700         MOVE PGM-PPAXDUTL         TO CPWSDXIN-RETURN-REF-DATA    PPDXSABB
045800     ELSE                                                         PPDXSABB
045900         SET CPWSDXIN-RETURN-OK   TO TRUE                         PPDXSABB
046000     END-IF.                                                      PPDXSABB
046100 7300-EXIT.   EXIT.                                               PPDXSABB
046200     EJECT                                                        PPDXSABB
046300******************************************************************PPDXSABB
046400*      SELECT DOS ROW FROM CTL DATA BASE                         *PPDXSABB
046500******************************************************************PPDXSABB
046600 8200-SELECT-DOS  SECTION.                                        PPDXSABB
046700*                                                                 PPDXSABB
046800     MOVE 'SELECT PPPDOS ROW' TO DB2MSG-TAG.                      PPDXSABB
046900     EXEC SQL                                                     PPDXSABB
047000        SELECT  DOS_SABB_IND                                      PPDXSABB
047100            INTO :DOS-SABB-IND                                    PPDXSABB
047200            FROM PPPVZDOS_DOS                                     PPDXSABB
047300            WHERE DOS_EARNINGS_TYPE = :WS-DOS-CODE                PPDXSABB
047400     END-EXEC.                                                    PPDXSABB
047500 8200-EXIT.      EXIT.                                            PPDXSABB
047600*                                                                 PPDXSABB
047700******************************************************************PPDXSABB
047800*999999-SQL-ERROR.                                               *PPDXSABB
047900******************************************************************PPDXSABB
048000*    THIS CODE IS EXECUTED IF A NEGATIVE SQLCODE IS RETURNED     *PPDXSABB
048100******************************************************************PPDXSABB
048200     EXEC SQL                                                     PPDXSABB
048300          INCLUDE CPPDXP99                                        PPDXSABB
048400     END-EXEC.                                                    PPDXSABB
048500     SET CPWSDXIN-SQL-ERROR TO TRUE.                              PPDXSABB
048600     MOVE 'SQL ERROR FROM PPNTRHDR'  TO CPWSDXIN-RETURN-REF-DESC. PPDXSABB
048700     GO TO 0500-PROGRAM-EXIT.                                     PPDXSABB
048800*                                                                 PPDXSABB
048900******************************************************************PPDXSABB
049000**   E N D  S O U R C E   ----  PPDXSABB ----                  ** PPDXSABB
049100******************************************************************PPDXSABB
