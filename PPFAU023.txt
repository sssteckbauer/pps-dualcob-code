000100**************************************************************/   32021138
000200*  PROGRAM: PPFAU023                                         */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:______WJG________ MODIFICATION DATE:  ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*                                                            */   32021138
000700*  - Implement flexible Full Accounting Unit (3202)          */   32021138
000800*                                                            */   32021138
000900**************************************************************/   32021138
001000*                                                            */   PPFAU023
001100*  This program is one of the Full Accounting Unit modules   */   PPFAU023
001200*  which each campus may need to modify to accommodate its   */   PPFAU023
001300*  Chart of Accounts structure.                              */   PPFAU023
001400*                                                            */   PPFAU023
001500**************************************************************/   PPFAU023
001600 IDENTIFICATION DIVISION.                                         PPFAU023
001700 PROGRAM-ID. PPFAU023.                                            PPFAU023
001800 AUTHOR.     WJG.                                                 PPFAU023
001900 INSTALLATION. University of California, Base Payroll System.     PPFAU023
002000 DATE-WRITTEN. APRIL 1997.                                        PPFAU023
002100 DATE-COMPILED.                                                   PPFAU023
002200     SKIP2                                                        PPFAU023
002300******************************************************************PPFAU023
002400*                                                                *PPFAU023
002500*  PPFAU023 - Sub-account code validation/replacement            *PPFAU023
002600*                                                                *PPFAU023
002700*  This program is written to 'localize' specific logic which    *PPFAU023
002800*  was in PPGRSERN.  That logic determines whether the sub-      *PPFAU023
002900*  account code on an earnings distribution is allowable and, if *PPFAU023
003000*  not, replaces it with sub account code '2'.  In this module   *PPFAU023
003100*  the Full Accounting Unit is received from PPGRSERN, the sub-  *PPFAU023
003200*  account code is tested and overlayed if necessary and the      PPFAU023
003300*  entire FAU is passed back to PPGRSERN.                        *PPFAU023
003400*                                                                *PPFAU023
003500******************************************************************PPFAU023
003600     SKIP2                                                        PPFAU023
003700 ENVIRONMENT DIVISION.                                            PPFAU023
003800 CONFIGURATION SECTION.                                           PPFAU023
003900     SKIP2                                                        PPFAU023
004000 SOURCE-COMPUTER.            COPY CPOTXUCS.                       PPFAU023
004100 OBJECT-COMPUTER.            COPY CPOTXOBJ.                       PPFAU023
004200     EJECT                                                        PPFAU023
004300 DATA DIVISION.                                                   PPFAU023
004400 WORKING-STORAGE SECTION.                                         PPFAU023
004500******************************************************************PPFAU023
004600*                Working Storage                                 *PPFAU023
004700******************************************************************PPFAU023
004800 01  ALPHA-CONSTANTS.                                             PPFAU023
004900     05  A-STND-PROG-ID               PIC X(15) VALUE             PPFAU023
005000                                      'PPFAU023/041597'.          PPFAU023
005100     05  GEN-ASST-SUB-ACCOUNT         PIC X(01) VALUE '2'.        PPFAU023
005200     SKIP3                                                        PPFAU023
005300 01  SWITCHES.                                                    PPFAU023
005400     05  FIRST-TIME-SW                PIC X(01) VALUE 'Y'.        PPFAU023
005500         88  THIS-IS-THE-FIRST-TIME             VALUE 'Y'.        PPFAU023
005600         88  NOT-THE-FIRST-TIME                 VALUE 'N'.        PPFAU023
005700     SKIP3                                                        PPFAU023
005800 01  DISPLAY-AREAS.                                               PPFAU023
005900     05  DISPLAY-AMOUNT               PIC ----9.                  PPFAU023
006000     SKIP3                                                        PPFAU023
006100 01  TEST-FIELDS.                                                 PPFAU023
006200     05  TEST-SUB-ACCOUNT             PIC X(01).                  PPFAU023
006300         88  ACCEPTABLE-FOR-LEAVE     VALUES '2', '5', '7', '8'.  PPFAU023
006400     EJECT                                                        PPFAU023
006500******************************************************************PPFAU023
006600*                Copymembers                                     *PPFAU023
006700******************************************************************PPFAU023
006800 01  XWHC-COMPILE-WORK-AREA.           COPY CPWSXWHC.             PPFAU023
006900     SKIP3                                                        PPFAU023
007000 01  FAUR-FAU-REDEFINITION.            COPY CPWSFAUR.             PPFAU023
007100     SKIP3                                                        PPFAU023
007200******************************************************************PPFAU023
007300 LINKAGE SECTION.                                                 PPFAU023
007400******************************************************************PPFAU023
007500*                Program Linkage                                 *PPFAU023
007600******************************************************************PPFAU023
007700 01  PPFAU023-INTERFACE.               COPY CPLNF023.             PPFAU023
007800     EJECT                                                        PPFAU023
007900 PROCEDURE DIVISION USING PPFAU023-INTERFACE.                     PPFAU023
008000******************************************************************PPFAU023
008100*                Procedure Division                              *PPFAU023
008200******************************************************************PPFAU023
008300 00000-MAINLINE SECTION.                                          PPFAU023
008400 00000-ENTRY.                                                     PPFAU023
008500*                                                                 PPFAU023
008600     IF  THIS-IS-THE-FIRST-TIME                                   PPFAU023
008700         PERFORM 10000-PROGRAM-INITIALIZATION                     PPFAU023
008800     END-IF.                                                      PPFAU023
008900*                                                                 PPFAU023
009000     MOVE F023-FAU-IN TO FAUR-FAU.                                PPFAU023
009100     MOVE FAUR-SUB-ACCOUNT TO TEST-SUB-ACCOUNT.                   PPFAU023
009200     IF  ACCEPTABLE-FOR-LEAVE                                     PPFAU023
009300         MOVE F023-FAU-IN          TO F023-FAU-OUT                PPFAU023
009400     ELSE                                                         PPFAU023
009500         MOVE GEN-ASST-SUB-ACCOUNT TO FAUR-SUB-ACCOUNT            PPFAU023
009600         MOVE FAUR-FAU             TO F023-FAU-OUT                PPFAU023
009700     END-IF.                                                      PPFAU023
009800*                                                                 PPFAU023
009900     EXIT PROGRAM.                                                PPFAU023
010000     EJECT                                                        PPFAU023
010100******************************************************************PPFAU023
010200*                Program Initialization                          *PPFAU023
010300******************************************************************PPFAU023
010400 10000-PROGRAM-INITIALIZATION SECTION.                            PPFAU023
010500 10000-ENTRY.                                                     PPFAU023
010600*                                                                 PPFAU023
010700     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPFAU023
010800     COPY CPPDXWHC.                                               PPFAU023
010900     MOVE ZEROES TO F023-FAILURE-CODE.                            PPFAU023
011000     MOVE SPACES TO F023-FAILURE-TEXT.                            PPFAU023
011100     SET NOT-THE-FIRST-TIME TO TRUE.                              PPFAU023
011200     SKIP3                                                        PPFAU023
011300 19999-EXIT.                                                      PPFAU023
011400     EXIT.                                                        PPFAU023
011500     EJECT                                                        PPFAU023
011600******************************************************************PPFAU023
011700* End of source code                                             *PPFAU023
011800******************************************************************PPFAU023
