000100**************************************************************/   26001140
000200*  PROGRAM: PPBENDLI                                         */   26001140
000300*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12600____  */   26001140
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___07/11/97__  */   26001140
000500*  DESCRIPTION:                                              */   26001140
000600*  - Modified to apply BRL_MAX_SAL_BASE as a "cap" to the    */   26001140
000700*    GLI salary base determining coverage amount.            */   26001140
000800*  - Modified to "cap" the Executive Life Salary base at the */   26001140
000900*    limit carried in System Parameter 174.                  */   26001140
001000**************************************************************/   26001140
000000**************************************************************/   28521025
000001*  PROGRAM: PPBENDLI                                         */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/23/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    MOVED BENEFITS ISO DATE COVERAGE EFFECT DATE TO         */   28521025
000007*    EFDT-ISO-DATE-CCYYMMDD.                                 */   28521025
000010**************************************************************/   28521025
000100**************************************************************/   36400799
000200*  PROGRAM:  PPBENDLI                                        */   36400799
000300*  RELEASE # __0799____   SERVICE REQUEST NO(S) __3640______ */   36400799
000400*  NAME _______SRS_____   MODIFICATION DATE ____10/06/92_____*/   36400799
000500*  DESCRIPTION                                               */   36400799
000600*  - NEW PPBENDLI: DB2 ACCESS AND DUAL USE FOR RUSH CHECKS   */   36400799
000700*  THIS MODULE PERFORMS DEPENDENT LIFE INSURANCE PROCESSING  */   36400799
000800**************************************************************/   36400799
000900 IDENTIFICATION DIVISION.                                         PPBENDLI
001000 PROGRAM-ID. PPBENDLI.                                            PPBENDLI
001100 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENDLI
001200 DATE-COMPILED.                                                   PPBENDLI
001300 DATA DIVISION.                                                   PPBENDLI
001400 WORKING-STORAGE SECTION.                                         PPBENDLI
001500                                                                  PPBENDLI
003500 01  SYSTEM-PARAMETER-INTERFACE.    COPY CPWSXSPI.                26001140
001600 01  PPBENDLI-INTERFACE    EXTERNAL.  COPY CPLNKDLI.              PPBENDLI
001700 01  DED-EFF-DATE-COMMAREA EXTERNAL.  COPY CPWSEFDT.              PPBENDLI
001800 01  WORK-AREA.                                                   PPBENDLI
001900     05  PROGRAM-STATUS-FLAG        PIC X(01)  VALUE '0'.         PPBENDLI
002000         88  PROGRAM-STATUS-NORMAL             VALUE '0'.         PPBENDLI
002100         88  PROGRAM-STATUS-EXITING            VALUE '1'.         PPBENDLI
002200     05  RATE-FLAG                  PIC X(01)  VALUE 'N'.         PPBENDLI
002300         88  RATE-FOUND                        VALUE 'Y'.         PPBENDLI
002400         88  RATE-NOT-FOUND                    VALUE 'N'.         PPBENDLI
002500     05  WS-EMPLOYEE-AGE            PIC S9(04) VALUE ZERO COMP.   PPBENDLI
002600     05  WS-SALARY-BASE             PIC S9(3)      COMP.          PPBENDLI
002700     05  WS-EXEC-BASE               PIC S9(3)      COMP.          PPBENDLI
004800     05  WS-EXEC-CAP                PIC S9(3)      COMP.          26001140
002800     05  WS-PLAN-CODE               PIC S9         COMP.          PPBENDLI
002900     05  WS-BASIC-RATE              PIC S9(3)V99   COMP-3.        PPBENDLI
003000     05  WS-SPOUSE-RATE             PIC S9V9(4)    COMP-3.        PPBENDLI
003100     05  WS-CHILD-RATE              PIC S9(3)V99   COMP-3.        PPBENDLI
003200     05  WS-PREMIUM                 PIC S9(3)V99   COMP-3.        PPBENDLI
005400     05  WS-EXEC-CAP-PARAM          PIC S9(3) COMP-3 VALUE +274.  26001140
005410     05  WS-PPPRMUT2                PIC X(08) VALUE 'PPPRMUT2'.   26001140
003300     05  WS-HOLD-BRL-KEY.                                         PPBENDLI
003400         10  WS-HLD-BRSC            PIC X(5)   VALUE SPACES.      PPBENDLI
003500         10  WS-HLD-MIN-AGE         PIC S9(04) VALUE ZERO COMP.   PPBENDLI
003600         10  WS-HLD-MAX-AGE         PIC S9(04) VALUE ZERO COMP.   PPBENDLI
003700     05  WS-BRL-BRSC.                                             PPBENDLI
003800         10 WS-BRL-CBUC             PIC XX.                       PPBENDLI
003900         10 WS-BRL-REP              PIC X.                        PPBENDLI
004000         10 WS-BRL-SHC              PIC X.                        PPBENDLI
004100         10 WS-BRL-DUC              PIC X.                        PPBENDLI
004200     05  WS-DEFAULT-BRSC            PIC X(05)  VALUE '00   '.     PPBENDLI
004300     05  WS-DEFAULT-BRL             PIC X(05)  VALUE '<<   '.     PPBENDLI
004400                                                                  PPBENDLI
004500 01  WS-BRL.                                                      PPBENDLI
004600     EXEC SQL                                                     PPBENDLI
004700          INCLUDE PPPVZBRL                                        PPBENDLI
004800     END-EXEC.                                                    PPBENDLI
004900     EXEC SQL                                                     PPBENDLI
005000          INCLUDE SQLCA                                           PPBENDLI
005100     END-EXEC.                                                    PPBENDLI
005200                                                                  PPBENDLI
005300 PROCEDURE DIVISION.                                              PPBENDLI
005400                                                                  PPBENDLI
005500 A000-MAINLINE SECTION.                                           PPBENDLI
005600                                                                  PPBENDLI
005700     PERFORM B000-INITIALIZATION.                                 PPBENDLI
005800                                                                  PPBENDLI
005900     IF PROGRAM-STATUS-NORMAL                                     PPBENDLI
006000      IF  KDLI-ACTION-RATE-RETRIEVAL                              PPBENDLI
006100       OR KDLI-ACTION-PREMIUM-CALC                                PPBENDLI
006200          PERFORM C010-GET-RATE                                   PPBENDLI
006300          IF   PROGRAM-STATUS-NORMAL                              PPBENDLI
006400           AND KDLI-ACTION-PREMIUM-CALC                           PPBENDLI
006500           AND RATE-FOUND                                         PPBENDLI
006600               PERFORM D010-CALCULATE.                            PPBENDLI
006700                                                                  PPBENDLI
006800     GOBACK.                                                      PPBENDLI
006900                                                                  PPBENDLI
007000 B000-INITIALIZATION SECTION.                                     PPBENDLI
007100                                                                  PPBENDLI
007200     SET PROGRAM-STATUS-NORMAL TO TRUE.                           PPBENDLI
007400     MOVE ZERO TO KDLI-RETURN-STATUS-FLAG                         PPBENDLI
007410     INITIALIZE KDLI-RETURN-FIELDS.                               PPBENDLI
007810     SET KDLI-DEP-NOT-EXEC-BASED TO TRUE.                         PPBENDLI
007900     IF  KDLI-ACTION-RATE-RETRIEVAL                               PPBENDLI
008000      OR KDLI-ACTION-PREMIUM-CALC                                 PPBENDLI
008100         PERFORM B030-VALIDATE-LOOKUP-ARGS.                       PPBENDLI
008110     IF NOT KDLI-INVALID-LOOKUP-ARG                               PPBENDLI
008200      IF KDLI-ACTION-PREMIUM-CALC                                 PPBENDLI
008300        PERFORM B050-VALIDATE-ENROLLMENT.                         PPBENDLI
008400                                                                  PPBENDLI
008500 B030-VALIDATE-LOOKUP-ARGS SECTION.                               PPBENDLI
008600                                                                  PPBENDLI
008700     SET KDLI-NOT-ENROLLED-IN-DEP-LIFE TO TRUE.                   PPBENDLI
008800     IF   KDLI-VALID-DEP-PLAN-CODE                                PPBENDLI
008900      AND NOT KDLI-INVALID-AGE                                    PPBENDLI
009000          MOVE '0' TO KDLI-INVALID-LOOKUP-ARG-FLAG                PPBENDLI
009100     ELSE                                                         PPBENDLI
009200          SET KDLI-INVALID-LOOKUP-ARG TO TRUE                     PPBENDLI
009300          SET PROGRAM-STATUS-EXITING TO TRUE                      PPBENDLI
009400     END-IF.                                                      PPBENDLI
009500                                                                  PPBENDLI
009600 B050-VALIDATE-ENROLLMENT SECTION.                                PPBENDLI
009700                                                                  PPBENDLI
009800     IF EFDT-INPUT-XGTN-ICED-IND NOT = 'A' AND 'V' AND 'Z'        PPBENDLI
009900        MOVE 'E' TO EFDT-INPUT-XGTN-ICED-IND                      PPBENDLI
010000     END-IF.                                                      PPBENDLI
012100*****                                                          CD 26001140
010200     MOVE KDLI-COVERAGE-EFFECT-DATE TO EFDT-ISO-DATE-CCYYMMDD.    28521025
010500     PERFORM Z010-PROCESS-EFF-DATE.                               PPBENDLI
010600     IF EFDT-DONT-TAKE-DEDUCTION                                  PPBENDLI
010700        SET PROGRAM-STATUS-EXITING TO TRUE                        PPBENDLI
010800     ELSE                                                         PPBENDLI
010900        SET KDLI-ENROLLED-IN-DEP-LIFE TO TRUE                     PPBENDLI
011000     END-IF.                                                      PPBENDLI
011100                                                                  PPBENDLI
011200 C010-GET-RATE SECTION.                                           PPBENDLI
011300                                                                  PPBENDLI
011400     IF KDLI-BRSC  = WS-DEFAULT-BRSC                              PPBENDLI
011500        MOVE WS-DEFAULT-BRL TO WS-BRL-BRSC                        PPBENDLI
011600     ELSE                                                         PPBENDLI
011700        MOVE KDLI-BRSC TO WS-BRL-BRSC                             PPBENDLI
011800     END-IF.                                                      PPBENDLI
011900     MOVE KDLI-LI-EMP-AGE TO WS-EMPLOYEE-AGE.                     PPBENDLI
012000     IF   KDLI-PERIOD-END-DATE(3:2) = '12'                        PPBENDLI
012100      AND KDLI-LI-EMP-AGE NOT = 99                                PPBENDLI
012200          ADD +1 TO WS-EMPLOYEE-AGE                               PPBENDLI
012300     END-IF.                                                      PPBENDLI
012400     IF   WS-BRL-BRSC = WS-HLD-BRSC                               PPBENDLI
012500      AND WS-HLD-MIN-AGE NOT > WS-EMPLOYEE-AGE                    PPBENDLI
012600      AND WS-HLD-MAX-AGE NOT < WS-EMPLOYEE-AGE                    PPBENDLI
012700          SET RATE-FOUND TO TRUE                                  PPBENDLI
012800     ELSE                                                         PPBENDLI
012900          SET RATE-NOT-FOUND TO TRUE                              PPBENDLI
013000          PERFORM P050-ACCESS-BRL                                 PPBENDLI
013100            UNTIL RATE-FOUND                                      PPBENDLI
013200               OR NOT PROGRAM-STATUS-NORMAL                       PPBENDLI
013300     END-IF.                                                      PPBENDLI
013400     IF RATE-FOUND                                                PPBENDLI
013500      IF KDLI-BASIC-PLAN                                          PPBENDLI
013600         MOVE BRL-DLI-BASIC-RATE TO WS-BASIC-RATE                 PPBENDLI
013700                                    KDLI-DEP-BASIC-RATE           PPBENDLI
013800      ELSE                                                        PPBENDLI
013900         IF KDLI-EXPANDED-PLAN-SPOUSAL                            PPBENDLI
014000            MOVE BRL-DLI-EXP-RATE TO WS-SPOUSE-RATE               PPBENDLI
014100                                     KDLI-DEP-SPOUSAL-RATE        PPBENDLI
014200         END-IF                                                   PPBENDLI
014300         IF KDLI-EXPANDED-PLAN-CHILDREN                           PPBENDLI
014400            MOVE BRL-DLI-CHILD-RATE TO WS-CHILD-RATE              PPBENDLI
014500                                       KDLI-DEP-CHILD-RATE        PPBENDLI
014600         END-IF                                                   PPBENDLI
014700      END-IF                                                      PPBENDLI
014800     END-IF.                                                      PPBENDLI
014900                                                                  PPBENDLI
015000 D010-CALCULATE SECTION.                                          PPBENDLI
015100                                                                  PPBENDLI
015200     IF KDLI-EXPANDED-PLAN-SPOUSAL                                PPBENDLI
015300        PERFORM D020-SET-SALARY-BASE                              PPBENDLI
015400     END-IF.                                                      PPBENDLI
015500     IF KDLI-BASIC-PLAN                                           PPBENDLI
015600        MOVE WS-BASIC-RATE TO KDLI-DEP-PREMIUM-AMT                PPBENDLI
015700     ELSE                                                         PPBENDLI
015800       IF KDLI-EXPANDED-PLAN-SPOUSAL                              PPBENDLI
015900          COMPUTE WS-PREMIUM = WS-SALARY-BASE * WS-SPOUSE-RATE    PPBENDLI
016000       ELSE                                                       PPBENDLI
016100          MOVE ZERO TO WS-PREMIUM                                 PPBENDLI
016200       END-IF                                                     PPBENDLI
016300       IF KDLI-EXPANDED-PLAN-CHILDREN                             PPBENDLI
016400          ADD WS-CHILD-RATE TO WS-PREMIUM                         PPBENDLI
016500       END-IF                                                     PPBENDLI
016600       MOVE WS-PREMIUM TO KDLI-DEP-PREMIUM-AMT                    PPBENDLI
016700     END-IF.                                                      PPBENDLI
016800                                                                  PPBENDLI
016900 D020-SET-SALARY-BASE SECTION.                                    PPBENDLI
017000                                                                  PPBENDLI
017100     IF KDLI-EMP-LIFE-PLAN-CODE = 'F'                             PPBENDLI
017200        MOVE BRL-PLANF-COV-AMT TO WS-SALARY-BASE                  PPBENDLI
017300     ELSE                                                         PPBENDLI
017400       MOVE ZERO TO WS-SALARY-BASE                                PPBENDLI
017500       IF KDLI-EMP-LIFE-PLAN-CODE IS NUMERIC                      PPBENDLI
017600          MOVE KDLI-EMP-LIFE-PLAN-CODE TO WS-PLAN-CODE            PPBENDLI
019500*-------> "Cap" the salary base at the BRL table maximum          26001140
019600          IF KDLI-EMP-LIFE-SALARY-BASE > BRL-MAX-SAL-BASE         26001140
019700             MOVE BRL-MAX-SAL-BASE TO WS-SALARY-BASE              26001140
019800          ELSE                                                    26001140
019900             MOVE KDLI-EMP-LIFE-SALARY-BASE TO WS-SALARY-BASE     26001140
020000          END-IF                                                  26001140
017700          COMPUTE WS-SALARY-BASE                                  PPBENDLI
020200                = WS-SALARY-BASE * WS-PLAN-CODE                   26001140
020300****************= WS-PLAN-CODE * KDLI-EMP-LIFE-SALARY-BASE        26001140
017900       END-IF                                                     PPBENDLI
018000     END-IF.                                                      PPBENDLI
018100     IF KDLI-EXEC-LIFE-PLAN-CODE NOT = '0' AND 'X'                PPBENDLI
020700*-------> Retrieve Parameter 274 - the Exec Life Base Limit       26001140
018200         MOVE KDLI-EXEC-LIFE-PLAN-CODE TO WS-PLAN-CODE            PPBENDLI
020900         MOVE ZERO TO XSPI-TABLE-SIZE                             26001140
021000         MOVE WS-EXEC-CAP-PARAM TO XSPI-PARM-NO                   26001140
021100         CALL WS-PPPRMUT2 USING SYSTEM-PARAMETER-INTERFACE        26001140
021200         MOVE XSPI-PARM-VALUE TO WS-EXEC-CAP                      26001140
021300*-------> "Cap" the salary base at the Parameter Limit            26001140
021400         IF KDLI-EXEC-LIFE-SALARY-BASE > WS-EXEC-CAP              26001140
021500             MOVE WS-EXEC-CAP TO WS-EXEC-BASE                     26001140
021600         ELSE                                                     26001140
021700             MOVE KDLI-EXEC-LIFE-SALARY-BASE TO WS-EXEC-BASE      26001140
021800         END-IF                                                   26001140
021900         COMPUTE WS-EXEC-BASE = WS-EXEC-BASE * WS-PLAN-CODE       26001140
022000*********COMPUTE WS-EXEC-BASE                                     26001140
022100************** = WS-PLAN-CODE * KDLI-EXEC-LIFE-SALARY-BASE        26001140
018500         IF WS-EXEC-BASE > WS-SALARY-BASE                         PPBENDLI
018600             MOVE WS-EXEC-BASE TO WS-SALARY-BASE                  PPBENDLI
018700             SET KDLI-DEP-EXEC-BASED TO TRUE                      PPBENDLI
018800         END-IF                                                   PPBENDLI
018900     END-IF.                                                      PPBENDLI
019000     DIVIDE 2 INTO WS-SALARY-BASE ROUNDED.                        PPBENDLI
019100     IF WS-SALARY-BASE > BRL-DLI-SPOUSE-LIM                       PPBENDLI
019200         MOVE BRL-DLI-SPOUSE-LIM TO WS-SALARY-BASE                PPBENDLI
019300     END-IF.                                                      PPBENDLI
019400     MOVE WS-SALARY-BASE TO KDLI-DEP-SALARY-BASE.                 PPBENDLI
019500                                                                  PPBENDLI
019600 P050-ACCESS-BRL SECTION.                                         PPBENDLI
019700                                                                  PPBENDLI
019800     EXEC SQL                                                     PPBENDLI
019900         SELECT  BRL_MIN_AGE,                                     PPBENDLI
020000                 BRL_MAX_AGE,                                     PPBENDLI
020100                 BRL_DLI_BASIC_RATE,                              PPBENDLI
020200                 BRL_DLI_EXP_RATE,                                PPBENDLI
020300                 BRL_PLANF_COV_AMT,                               PPBENDLI
020400                 BRL_DLI_SPOUSE_LIM,                              PPBENDLI
020500                 BRL_DLI_CHILD_RATE                               PPBENDLI
024300                ,BRL_MAX_SAL_BASE                                 26001140
020600           INTO :BRL-MIN-AGE,                                     PPBENDLI
020700                :BRL-MAX-AGE,                                     PPBENDLI
020800                :BRL-DLI-BASIC-RATE,                              PPBENDLI
020900                :BRL-DLI-EXP-RATE,                                PPBENDLI
021000                :BRL-PLANF-COV-AMT,                               PPBENDLI
021100                :BRL-DLI-SPOUSE-LIM,                              PPBENDLI
021200                :BRL-DLI-CHILD-RATE                               PPBENDLI
025100               ,:BRL-MAX-SAL-BASE                                 26001140
021300           FROM  PPPVZBRL_BRL                                     PPBENDLI
021400          WHERE     BRL_CBUC    = :WS-BRL-CBUC                    PPBENDLI
021500            AND     BRL_REP     = :WS-BRL-REP                     PPBENDLI
021600            AND     BRL_SHC     = :WS-BRL-SHC                     PPBENDLI
021700            AND     BRL_DUC     = :WS-BRL-DUC                     PPBENDLI
021800            AND NOT BRL_MIN_AGE > :WS-EMPLOYEE-AGE                PPBENDLI
021900            AND NOT BRL_MAX_AGE < :WS-EMPLOYEE-AGE                PPBENDLI
022000     END-EXEC.                                                    PPBENDLI
022100                                                                  PPBENDLI
022200     IF SQLCODE = ZERO                                            PPBENDLI
022300        SET RATE-FOUND TO TRUE                                    PPBENDLI
022400        MOVE BRL-MIN-AGE TO WS-HLD-MIN-AGE                        PPBENDLI
022500        MOVE BRL-MAX-AGE TO WS-HLD-MAX-AGE                        PPBENDLI
022600        MOVE WS-BRL-BRSC TO WS-HLD-BRSC                           PPBENDLI
022700     ELSE                                                         PPBENDLI
022800       INITIALIZE WS-HOLD-BRL-KEY                                 PPBENDLI
022900       IF   SQLCODE = +100                                        PPBENDLI
023000        AND WS-BRL-BRSC NOT = WS-DEFAULT-BRL                      PPBENDLI
023100            MOVE '2' TO KDLI-RETURN-STATUS-FLAG                   PPBENDLI
023200            MOVE WS-DEFAULT-BRL  TO WS-BRL-BRSC                   PPBENDLI
023300       ELSE                                                       PPBENDLI
023400            SET KDLI-RETURN-STATUS-ABORTING TO TRUE               PPBENDLI
023500            SET PROGRAM-STATUS-EXITING TO TRUE                    PPBENDLI
023600       END-IF                                                     PPBENDLI
023700     END-IF.                                                      PPBENDLI
023800                                                                  PPBENDLI
023900 Z010-PROCESS-EFF-DATE SECTION.                                   PPBENDLI
024000     COPY CPPDEFDT.                                               PPBENDLI
