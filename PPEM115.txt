000000**************************************************************/   28521025
000001*  PROGRAM: PPEM115                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/28/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* PROGRAM: PPEM115                                           *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - ESTABLISH CURRENT BENEFIT COVERAGE                      *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100                                                                  PPEM115
001200 IDENTIFICATION DIVISION.                                         PPEM115
001300 PROGRAM-ID. PPEM115.                                             PPEM115
001400 AUTHOR. UCOP.                                                    PPEM115
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEM115
001600                                                                  PPEM115
001700 DATE-WRITTEN.  SEPT 9,1992.                                      PPEM115
001800 DATE-COMPILED. XX/XX/XX.                                         PPEM115
001900                                                                  PPEM115
002000     EJECT                                                        PPEM115
002100 ENVIRONMENT DIVISION.                                            PPEM115
002200 CONFIGURATION SECTION.                                           PPEM115
002300 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEM115
002400 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEM115
002500 SPECIAL-NAMES.        C01 IS NEW-PAGE.                           PPEM115
002600     EJECT                                                        PPEM115
002700 INPUT-OUTPUT SECTION.                                            PPEM115
002800 FILE-CONTROL.                                                    PPEM115
002900                                                                  PPEM115
003000 DATA DIVISION.                                                   PPEM115
003100 FILE SECTION.                                                    PPEM115
003200     EJECT                                                        PPEM115
003300*----------------------------------------------------------------*PPEM115
003400 WORKING-STORAGE SECTION.                                         PPEM115
003500*----------------------------------------------------------------*PPEM115
003600                                                                  PPEM115
003700 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEM115
003800*****'PPEC115 /111992'.                                           28521025
003810     'PPEC115 /080195'.                                           28521025
003900                                                                  PPEM115
004000 01  A-STND-MSG-PARMS                 REDEFINES                   PPEM115
004100     A-STND-PROG-ID.                                              PPEM115
004200     05  FILLER                       PIC X(03).                  PPEM115
004300     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEM115
004400     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEM115
004500     05  FILLER                       PIC X(08).                  PPEM115
004600                                                                  PPEM115
004700 01  ELEMENTS-USED.                                               PPEM115
004800     05  E0362                        PIC 9(04) VALUE  0362.      PPEM115
004900                                                                  PPEM115
005000 01  MESSAGES-USED.                                               PPEM115
005100     05  M00000                       PIC X(05) VALUE '00000'.    PPEM115
005200                                                                  PPEM115
005300 01  MISC-WORK-AREAS.                                             PPEM115
005400     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEM115
005500         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEM115
005600         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEM115
005700     05  ISO-HIRE-DATE.                                           PPEM115
005800         10  ISO-HIRE-DATE-YYYY-MM.                               PPEM115
005900             15  ISO-HIRE-DATE-YYYY   PIC 9(4).                   PPEM115
006000             15  FILLER               PIC X(1)  VALUE '-'.        PPEM115
006100             15  ISO-HIRE-DATE-MM     PIC 9(2).                   PPEM115
006200         10  FILLER                   PIC X(1)  VALUE '-'.        PPEM115
006300         10  ISO-HIRE-DATE-DD         PIC 9(2).                   PPEM115
006400     05  AC-DATE-CONV.                                            PPEM115
006500         10  AC-DATE-YR               PIC  X(2).                  PPEM115
006600         10  AC-DATE-MO               PIC  X(2).                  PPEM115
006700             88  VALID-MONTH-31                     VALUE         PPEM115
006800                 '01' '03' '05' '07' '08' '10' '12'.              PPEM115
006900             88  VALID-MONTH-30                     VALUE         PPEM115
007000                 '04' '06' '09' '11'.                             PPEM115
007100             88  VALID-MONTH-28                     VALUE         PPEM115
007200                 '02'.                                            PPEM115
007300         10  AC-DATE-DA               PIC  X(2).                  PPEM115
007400                                                                  PPEM115
007500                                                                  PPEM115
007600     EJECT                                                        PPEM115
007700 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEM115
007800                                                                  PPEM115
007900     EJECT                                                        PPEM115
008000*01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. 28521025
008100*****                                                             28521025
008200*****EJECT                                                        28521025
008300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEM115
008100                                                                  PPEM115
008200     EJECT                                                        PPEM115
008600 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEM115
008400                                                                  PPEM115
008500     EJECT                                                        PPEM115
008900*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
009000*****                                                             28521025
009100*****EJECT                                                        28521025
009200 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEM115
009300                                                                  PPEM115
009400     EJECT                                                        PPEM115
009500 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEM115
009600                                                                  PPEM115
009700     EJECT                                                        PPEM115
009800******************************************************************PPEM115
009900**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEM115
010000**--------------------------------------------------------------**PPEM115
010100** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEM115
010200******************************************************************PPEM115
010300                                                                  PPEM115
010400 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEM115
010500                                                                  PPEM115
010600     EJECT                                                        PPEM115
010700 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEM115
010800                                                                  PPEM115
010900     EJECT                                                        PPEM115
011000 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEM115
011100                                                                  PPEM115
011200     EJECT                                                        PPEM115
011300 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEM115
011400                                                                  PPEM115
011500 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEM115
011600                                                                  PPEM115
011700 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEM115
011800                                                                  PPEM115
011900 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEM115
012000                                                                  PPEM115
012100     EJECT                                                        PPEM115
012200 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEM115
012300                                                                  PPEM115
012400     EJECT                                                        PPEM115
012500 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEM115
012600                                                                  PPEM115
012700     EJECT                                                        PPEM115
012800******************************************************************PPEM115
012900**  DATA BASE AREAS                                             **PPEM115
013000******************************************************************PPEM115
013100                                                                  PPEM115
013200 01  SCR-ROW                             EXTERNAL.                PPEM115
013300     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEM115
013400                                                                  PPEM115
013500     EJECT                                                        PPEM115
013600 01  PER-ROW                             EXTERNAL.                PPEM115
013700     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEM115
013800                                                                  PPEM115
013900     EJECT                                                        PPEM115
014000 01  BEL-ROW                             EXTERNAL.                PPEM115
014100     EXEC SQL INCLUDE PPPVBEL1 END-EXEC.                          PPEM115
014200                                                                  PPEM115
014300     EJECT                                                        PPEM115
014400 01  BEN-ROW                             EXTERNAL.                PPEM115
014500     EXEC SQL INCLUDE PPPVBEN1 END-EXEC.                          PPEM115
014600                                                                  PPEM115
014700     EJECT                                                        PPEM115
014800******************************************************************PPEM115
014900**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEM115
015000******************************************************************PPEM115
015100                                                                  PPEM115
015200*----------------------------------------------------------------*PPEM115
015300 LINKAGE SECTION.                                                 PPEM115
015400*----------------------------------------------------------------*PPEM115
015500                                                                  PPEM115
015600******************************************************************PPEM115
015700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEM115
015800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEM115
015900******************************************************************PPEM115
016000                                                                  PPEM115
016100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEM115
016200                                                                  PPEM115
016300     EJECT                                                        PPEM115
016400******************************************************************PPEM115
016500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEM115
016600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEM115
016700******************************************************************PPEM115
016800                                                                  PPEM115
016900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEM115
017000                                                                  PPEM115
017100     EJECT                                                        PPEM115
017200******************************************************************PPEM115
017300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEM115
017400******************************************************************PPEM115
017500                                                                  PPEM115
017600 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEM115
017700                          KMTA-MESSAGE-TABLE-ARRAY.               PPEM115
017800                                                                  PPEM115
017900                                                                  PPEM115
018000*----------------------------------------------------------------*PPEM115
018100 0000-DRIVER.                                                     PPEM115
018200*----------------------------------------------------------------*PPEM115
018300                                                                  PPEM115
018400******************************************************************PPEM115
018500**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEM115
018600******************************************************************PPEM115
018700                                                                  PPEM115
018800     IF  THIS-IS-THE-FIRST-TIME                                   PPEM115
018900         PERFORM 0100-INITIALIZE                                  PPEM115
019000     END-IF.                                                      PPEM115
019100                                                                  PPEM115
019200******************************************************************PPEM115
019300**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEM115
019400**  TO THE CALLING PROGRAM                                      **PPEM115
019500******************************************************************PPEM115
019600                                                                  PPEM115
019700     PERFORM 1000-MAINLINE-ROUTINE.                               PPEM115
019800                                                                  PPEM115
019900     EXIT PROGRAM.                                                PPEM115
020000                                                                  PPEM115
020100                                                                  PPEM115
020200     EJECT                                                        PPEM115
020300*----------------------------------------------------------------*PPEM115
020400 0100-INITIALIZE    SECTION.                                      PPEM115
020500*----------------------------------------------------------------*PPEM115
020600                                                                  PPEM115
020700******************************************************************PPEM115
020800**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEM115
020900******************************************************************PPEM115
021000                                                                  PPEM115
021100     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEM115
021200                                                                  PPEM115
021300*                                               *-------------*   PPEM115
021400                                                 COPY CPPDXWHC.   PPEM115
021500*                                               *-------------*   PPEM115
021600                                                                  PPEM115
021700*****                                           *-------------*   28521025
021800*****                                            COPY CPPDDATE.   28521025
021900*****                                           *-------------*   28521025
022000*****                                                             28521025
022100*****MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 28521025
022000                                                                  PPEM115
022300******************************************************************PPEM115
022400**   SET FIRST CALL SWITCH OFF                                  **PPEM115
022500******************************************************************PPEM115
022600                                                                  PPEM115
022700     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEM115
022800                                                                  PPEM115
022900                                                                  PPEM115
023000     EJECT                                                        PPEM115
023100*----------------------------------------------------------------*PPEM115
023200 1000-MAINLINE-ROUTINE    SECTION.                                PPEM115
023300*----------------------------------------------------------------*PPEM115
023400                                                                  PPEM115
023500     IF  BELI-NUMERIC-VALUE < 5                                   PPEM115
023600         IF  HLTH-PLAN = 'CM'                                     PPEM115
023700             IF  BELI-NUMERIC-VALUE = 1                           PPEM115
023800                 MOVE 'M'         TO CURR-BEN-COVERAGE            PPEM115
023900             ELSE                                                 PPEM115
024000                 MOVE 'B'         TO CURR-BEN-COVERAGE            PPEM115
024100             END-IF                                               PPEM115
024200         ELSE                                                     PPEM115
024300             IF  BELI-NUMERIC-VALUE = 1                           PPEM115
024400                 MOVE SPACE       TO CURR-BEN-COVERAGE            PPEM115
024500             ELSE                                                 PPEM115
024600                 MOVE 'L'         TO CURR-BEN-COVERAGE            PPEM115
024700             END-IF                                               PPEM115
024800         END-IF                                                   PPEM115
024900     END-IF.                                                      PPEM115
025000                                                                  PPEM115
025100     MOVE E0362                   TO DL-FIELD.                    PPEM115
025200     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      PPEM115
025300                                                                  PPEM115
025400                                                                  PPEM115
025500     EJECT                                                        PPEM115
025600*----------------------------------------------------------------*PPEM115
025700 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEM115
025800*----------------------------------------------------------------*PPEM115
025900                                                                  PPEM115
026000*                                                 *-------------* PPEM115
026100                                                   COPY CPPDXDEC. PPEM115
026200*                                                 *-------------* PPEM115
026300                                                                  PPEM115
026400*----------------------------------------------------------------*PPEM115
026500 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEM115
026600*----------------------------------------------------------------*PPEM115
026700                                                                  PPEM115
026800*                                                 *-------------* PPEM115
026900                                                   COPY CPPDADLC. PPEM115
027000*                                                 *-------------* PPEM115
027100                                                                  PPEM115
027200*****EJECT                                                        28521025
027300*****------------------------------------------------------------*28521025
027400*9300-DATE-CONVERSION-DB2  SECTION.                               28521025
027500*****------------------------------------------------------------*28521025
027600*****                                                             28521025
027700*****                                             *-------------* 28521025
027800*****                                              COPY CPPDXDC2. 28521025
027900*****                                             *-------------* 28521025
028000*****                                                             28521025
028100*****                                                             28521025
028200******************************************************************PPEM115
028300**   E N D  S O U R C E   ----  PPEM115 ----                    **PPEM115
028400******************************************************************PPEM115
