000000**************************************************************/   36450775
000001*  PROGRAM: PPAWRUTW                                         */   36450775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____3645____  */   36450775
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __06/01/93____ */   36450775
000004*  DESCRIPTION: DELETE, UPDATE, INSERT AWR ROWS.             */   36450775
000008**************************************************************/   36450775
000800 IDENTIFICATION DIVISION.                                         PPAWRUTW
000900 PROGRAM-ID. PPAWRUTW                                             PPAWRUTW
001000*AUTHOR. DDM                                                      PPAWRUTW
001100*DATE-WRITTEN.  FEB 1993                                          PPAWRUTW
001200*DATE-COMPILED.                                                   PPAWRUTW
001300*REMARKS.                                                         PPAWRUTW
001800*                                                                 PPAWRUTW
001900     EJECT                                                        PPAWRUTW
002000 ENVIRONMENT DIVISION.                                            PPAWRUTW
002100 CONFIGURATION SECTION.                                           PPAWRUTW
002200 SOURCE-COMPUTER.                   COPY CPOTXUCS.                PPAWRUTW
002300 OBJECT-COMPUTER.                   COPY CPOTXOBJ.                PPAWRUTW
002400     EJECT                                                        PPAWRUTW
002500***************************************************************** PPAWRUTW
002600*                                                                 PPAWRUTW
002700*                D A T A  D I V I S I O N                         PPAWRUTW
002800*                                                                 PPAWRUTW
002900***************************************************************** PPAWRUTW
003000 DATA DIVISION.                                                   PPAWRUTW
003100 WORKING-STORAGE SECTION.                                         PPAWRUTW
003200 01  MISCELLANEOUS-WS.                                            PPAWRUTW
003300     05  A-STND-PROG-ID       PIC X(08) VALUE 'PPAWRUTW'.         PPAWRUTW
003400     05  HERE-BEFORE-SW       PIC X(01) VALUE SPACE.              PPAWRUTW
003500         88  HERE-BEFORE                VALUE 'Y'.                PPAWRUTW
003600     05  FIRST-TIME-SWITCH   PIC X(01) VALUE 'Y'.                 PPAWRUTL
003700         88  FIRST-TIME                VALUE 'Y'.                 PPAWRUTL
003800         88  NOT-FIRST-TIME            VALUE 'N'.                 PPAWRUTL
004300     EJECT                                                        PPAWRUTW
004301 01  ONLINE-SIGNALS EXTERNAL.                                     PPAWRUTL
004302                                    COPY CPWSONLI.                PPAWRUTL
004303     EJECT                                                        PPAWRUTL
004304 01  XWHC-COMPILE-WORK-AREA.                                      PPAWRUTL
004305                                    COPY CPWSXWHC.                PPAWRUTL
004310     EJECT                                                        PPAWRUTW
004400 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPAWRUTW
004410     EJECT                                                        PPAWRUTW
004500***************************************************************** PPAWRUTW
004600*          SQL - WORKING STORAGE                                  PPAWRUTW
004700***************************************************************** PPAWRUTW
004800 01  AWR-ROW-DATA.                                                PPAWRUTW
004900     EXEC SQL                                                     PPAWRUTW
005000          INCLUDE PPPVAWR2                                        PPAWRUTW
005100     END-EXEC.                                                    PPAWRUTW
005200     EJECT                                                        PPAWRUTW
005300     EXEC SQL                                                     PPAWRUTW
005400          INCLUDE SQLCA                                           PPAWRUTW
005500     END-EXEC.                                                    PPAWRUTW
005510     EJECT                                                        PPAWRUTW
005600***************************************************************** PPAWRUTW
005700*                                                                 PPAWRUTW
005800*                SQL- SELECTS                                     PPAWRUTW
005900*                                                                 PPAWRUTW
006000***************************************************************** PPAWRUTW
006100*                                                                 PPAWRUTW
006200*  THE VIEWS USED IN THIS PROGRAM ARE:                            PPAWRUTW
006300*                                                                 PPAWRUTW
006400*     VIEW              VIEW DDL AND DCL INCLUDE                  PPAWRUTW
006500*                        MEMBER NAMES                             PPAWRUTW
006600*                                                                 PPAWRUTW
006700*  PPPVAWR2_AWR          PPPVAWR2                                 PPAWRUTW
006800*                                                                 PPAWRUTW
006900****************************************************************  PPAWRUTW
007000*                                                                 PPAWRUTW
007100 LINKAGE SECTION.                                                 PPAWRUTW
007101     EJECT                                                        PPAWRUTW
007110*                                                                 PPAWRUTW
007120******************************************************************PPAWRUTW
007130*                L I N K A G E   S E C T I O N                   *PPAWRUTW
007140******************************************************************PPAWRUTW
007150 01  PPXXXUTW-INTERFACE.            COPY CPLNWXXX.                PPAWRUTW
007160 01  AWR-ROW.                       COPY CPWSRAWR.                PPAWRUTW
007170*                                                                 PPAWRUTW
007180 PROCEDURE DIVISION USING                                         PPAWRUTW
007190                          PPXXXUTW-INTERFACE                      PPAWRUTW
007191                          AWR-ROW.                                PPAWRUTW
008110 0000-PARA-FOR-PRECOMP-PAGING.                                    PPAWRUTW
008120****************************************************************  PPAWRUTW
008130****************************************************************  PPAWRUTW
008200     EJECT                                                        PPAWRUTW
008210***************************************************************** PPAWRUTW
008300*            P R O C E D U R E  D I V I S I O N                   PPAWRUTW
008400***************************************************************** PPAWRUTW
008500***************************************************************** PPAWRUTW
008600*    THE FOLLOWING SQL WILL CAUSE ANY 'ABNORMAL' SQLCODE TO       PPAWRUTW
008700*    CAUSE A BRANCH TO 999999-SQL-ERROR.                          PPAWRUTW
008800***************************************************************** PPAWRUTW
008900     EXEC SQL                                                     PPAWRUTW
009000          INCLUDE CPPDXE99                                        PPAWRUTW
009100     END-EXEC.                                                    PPAWRUTW
009200*                                                                 PPAWRUTW
009210*                                                                 PPAWRUTW
009220*                                                                 PPAWRUTW
009300 0100-MAINLINE SECTION.                                           PPAWRUTW
009310****************************************************************  PPAWRUTW
009320****************************************************************  PPAWRUTW
009400*                                                                 PPAWRUTW
009500     IF FIRST-TIME                                                PPAWRUTL
009600         SET NOT-FIRST-TIME TO TRUE                               PPAWRUTL
009700         IF  NOT ENVIRON-IS-CICS                                  PPAWRUTL
009800             PERFORM 7900-FIRST-TIME                              PPAWRUTL
009810         END-IF                                                   PPAWRUTL
009820     END-IF.                                                      PPAWRUTL
009900     MOVE A-STND-PROG-ID TO DB2MSG-PGM-ID.                        PPAWRUTW
010200     MOVE AWR-ROW  TO AWR-ROW-DATA.                               PPAWRUTW
010300*                                                                 PPAWRUTW
010400     EVALUATE PPXXXUTW-FUNCTION                                   PPAWRUTW
010500         WHEN 'D'                                                 PPAWRUTW
010600             PERFORM 8100-DELETE-ROW                              PPAWRUTW
010700         WHEN 'U'                                                 PPAWRUTW
010800             PERFORM 8200-UPDATE-ROW                              PPAWRUTW
010900         WHEN 'I'                                                 PPAWRUTW
011000             PERFORM 8300-INSERT-ROW                              PPAWRUTW
011100         WHEN OTHER                                               PPAWRUTW
011200             SET PPXXXUTW-INVALID-FUNCTION TO TRUE                PPAWRUTW
011300             SET PPXXXUTW-ERROR            TO TRUE                PPAWRUTW
011400     END-EVALUATE.                                                PPAWRUTW
011500*                                                                 PPAWRUTW
011600     GOBACK.                                                      PPAWRUTW
011610     EJECT                                                        PPAWRUTL
011640 7900-FIRST-TIME SECTION.                                         PPAWRUTL
011641****************************************************************  PPAWRUTW
011642****************************************************************  PPAWRUTW
011650*                                                                 PPAWRUTL
011660     MOVE A-STND-PROG-ID TO XWHC-DISPLAY-MODULE.                  PPAWRUTL
011670*                                                                 PPAWRUTL
011680                                    COPY CPPDXWHC.                PPAWRUTL
011700     EJECT                                                        PPAWRUTW
011800***************************************************************** PPAWRUTW
011900*  PROCEDURE SQL                     FOR AWR VIEW                 PPAWRUTW
012000***************************************************************** PPAWRUTW
012100 8100-DELETE-ROW SECTION.                                         PPAWRUTW
012110****************************************************************  PPAWRUTW
012120****************************************************************  PPAWRUTW
012200*                                                                 PPAWRUTW
012300     MOVE '8100-DELETE-ROW'     TO DB2MSG-TAG.                    PPAWRUTW
012400*                                                                 PPAWRUTW
012500     EXEC SQL                                                     PPAWRUTW
012600         DELETE FROM PPPVAWR2_AWR                                 PPAWRUTW
012700          WHERE   EMPLOYEE_ID        = :AWR-EMPLOYEE-ID           PPAWRUTW
012800            AND   AWARD_ID           = :AWR-AWARD-ID              PPAWRUTW
012900     END-EXEC.                                                    PPAWRUTW
013000*                                                                 PPAWRUTW
013100     IF  SQLCODE    NOT = 0                                       PPAWRUTW
013200         SET PPXXXUTW-ERROR TO TRUE                               PPAWRUTW
013300     END-IF.                                                      PPAWRUTW
013400     EJECT                                                        PPAWRUTW
013500 8200-UPDATE-ROW SECTION.                                         PPAWRUTW
013510****************************************************************  PPAWRUTW
013520****************************************************************  PPAWRUTW
013600*                                                                 PPAWRUTW
013700     MOVE '8200-UPDATE-ROW'     TO DB2MSG-TAG.                    PPAWRUTW
013800*                                                                 PPAWRUTW
013900     EXEC SQL                                                     PPAWRUTW
014000     UPDATE PPPVAWR2_AWR                                          PPAWRUTW
014100       SET  AWARD_TYPE             = :AWR-AWARD-TYPE              PPAWRUTW
014200         ,  AWARD_DATE             = :AWR-AWARD-DATE              PPAWRUTW
014700         ,  AWARD_SOURCE           = :AWR-AWARD-SOURCE            PPAWRUTW
014710         ,  AWARD_AMOUNT           = :AWR-AWARD-AMOUNT            PPAWRUTW
014730         ,  AWARD_ADC_CD           = :AWR-AWARD-ADC-CD            PPAWRUTW
014800     WHERE  EMPLOYEE_ID            = :AWR-EMPLOYEE-ID             PPAWRUTW
014900       AND  AWARD_ID               = :AWR-AWARD-ID                PPAWRUTW
015000     END-EXEC.                                                    PPAWRUTW
015100*                                                                 PPAWRUTW
015200     IF  SQLCODE    NOT = 0                                       PPAWRUTW
015300         SET PPXXXUTW-ERROR TO TRUE                               PPAWRUTW
015400     END-IF.                                                      PPAWRUTW
015500     EJECT                                                        PPAWRUTW
015600 8300-INSERT-ROW SECTION.                                         PPAWRUTW
015610****************************************************************  PPAWRUTW
015620****************************************************************  PPAWRUTW
015700*                                                                 PPAWRUTW
015800     MOVE '8300-INSERT-ROW'     TO DB2MSG-TAG.                    PPAWRUTW
015900*                                                                 PPAWRUTW
016000     EXEC SQL                                                     PPAWRUTW
016100         INSERT INTO PPPVAWR2_AWR                                 PPAWRUTW
016200                VALUES (:AWR-ROW-DATA)                            PPAWRUTW
016300     END-EXEC.                                                    PPAWRUTW
016400*                                                                 PPAWRUTW
016500     IF  SQLCODE    NOT = 0                                       PPAWRUTW
016600         SET PPXXXUTW-ERROR TO TRUE                               PPAWRUTW
016700     END-IF.                                                      PPAWRUTW
017400     EJECT                                                        PPAWRUTW
017500*999999-SQL-ERROR.                                                PPAWRUTW
017510****************************************************************  PPAWRUTW
017511*** INCLUDE SQL ERROR PROCESS HERE                                PPAWRUTW
017520****************************************************************  PPAWRUTW
017700     EXEC SQL                                                     PPAWRUTW
017800          INCLUDE CPPDXP99                                        PPAWRUTW
017900     END-EXEC.                                                    PPAWRUTW
018000*                                                                 PPAWRUTW
018100     SET PPXXXUTW-ERROR TO TRUE.                                  PPAWRUTW
018200     IF NOT HERE-BEFORE                                           PPAWRUTW
018300         SET HERE-BEFORE TO TRUE                                  PPAWRUTW
018400     END-IF.                                                      PPAWRUTW
018500     GOBACK.                                                      PPAWRUTW
