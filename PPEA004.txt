000000**************************************************************/   28521025
000001*  PROGRAM: PPEA004                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/25/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
000100**************************************************************/   36410753
000200*  PROGRAM: PPEA004                                          */   36410753
000300*  RELEASE: ___0753______ SERVICE REQUEST(S): ____3641_____  */   36410753
000400*  NAME:_____JLT_________ MODIFICATION DATE:  ___03/31/93__  */   36410753
000500*  DESCRIPTION:                                              */   36410753
000600*  - INITIAL IMPLEMENTATION.                                 */   36410753
000700**************************************************************/   36410753
000800*----------------------------------------------------------------*PPEA004
000900 IDENTIFICATION DIVISION.                                         PPEA004
001000 PROGRAM-ID. PPEA004.                                             PPEA004
001100 AUTHOR. UCOP.                                                    PPEA004
001200 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEA004
001300                                                                  PPEA004
001400 DATE-WRITTEN.  MARCH 1,1993.                                     PPEA004
001500 DATE-COMPILED.                                                   PPEA004
001600                                                                  PPEA004
001700******************************************************************PPEA004
001800*     THIS ROUTINE WILL PERFORM THE PERSONNEL ACTION 04          *PPEA004
001900*                  M E R I T   A W A R D                         *PPEA004
002000******************************************************************PPEA004
002100                                                                  PPEA004
002200 ENVIRONMENT DIVISION.                                            PPEA004
002300 CONFIGURATION SECTION.                                           PPEA004
002400 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEA004
002500 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEA004
002600 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEA004
002700                                                                  PPEA004
002800 INPUT-OUTPUT SECTION.                                            PPEA004
002900 FILE-CONTROL.                                                    PPEA004
003000                                                                  PPEA004
003100 DATA DIVISION.                                                   PPEA004
003200 FILE SECTION.                                                    PPEA004
003300     EJECT                                                        PPEA004
003400*----------------------------------------------------------------*PPEA004
003500 WORKING-STORAGE SECTION.                                         PPEA004
003600*----------------------------------------------------------------*PPEA004
003700                                                                  PPEA004
003800 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEA004
003900*****'PPEA004 /030193'.                                           28521025
003910     'PPEA004 /080195'.                                           28521025
004000                                                                  PPEA004
004100 01  A-STND-MSG-PARMS                 REDEFINES                   PPEA004
004200     A-STND-PROG-ID.                                              PPEA004
004300     05  FILLER                       PIC X(03).                  PPEA004
004400     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEA004
004500     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEA004
004600     05  FILLER                       PIC X(08).                  PPEA004
004700                                                                  PPEA004
004800 01  MESSAGES-USED.                                               PPEA004
004900     05  M08903                       PIC X(05) VALUE '08903'.    PPEA004
005000                                                                  PPEA004
005100 01  ELEMENTS-USED.                                               PPEA004
005200     05  E0153                        PIC 9(04) VALUE 0153.       PPEA004
005300                                                                  PPEA004
005400                                                                  PPEA004
005500 01  MISC-WORK-AREAS.                                             PPEA004
005600     05  ACTION-ON                    PIC X(01) VALUE 'Y'.        PPEA004
005700     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEA004
005800         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEA004
005900         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEA004
006000     05  WS-C-DATE-HOLD.                                          PPEA004
006100         10  WS-C-YEAR           PIC 9(04) VALUE 0.               PPEA004
006200         10  WS-C-YR REDEFINES WS-C-YEAR.                         PPEA004
006300             15  WS-C-CC             PIC 9(02).                   PPEA004
006400             15  WS-C-YY             PIC 9(02).                   PPEA004
006500         10  WS-C-SEP-1          PIC X(01).                       PPEA004
006600         10  WS-C-MM             PIC 9(02)      VALUE 0.          PPEA004
006700         10  WS-C-SEP-2          PIC X(01).                       PPEA004
006800         10  WS-C-DD             PIC 9(02)      VALUE 0.          PPEA004
006900     05  WS-CURRENT-DATE.                                         PPEA004
007000         10  WS-CURR-YEAR        PIC 9(04) VALUE 0.               PPEA004
007100         10  WS-CURR-YR REDEFINES WS-CURR-YEAR.                   PPEA004
007200             15  WS-CURR-CC          PIC 9(02).                   PPEA004
007300             15  WS-CURR-YY          PIC 9(02).                   PPEA004
007400         10  WS-CURR-MM          PIC 9(02)      VALUE 0.          PPEA004
007500         10  WS-CURR-DD          PIC 9(02)      VALUE 0.          PPEA004
007600     05  WS-CURRENT-DATE-R REDEFINES WS-CURRENT-DATE.             PPEA004
007700         10  WS-CURR-CC-R        PIC 9(02).                       PPEA004
007800         10  WS-CURR-YMD.                                         PPEA004
007900             15  WS-CURR-YY-R        PIC 9(02).                   PPEA004
008000             15  WS-CURR-MM-R        PIC 9(02).                   PPEA004
008100             15  WS-CURR-DD-R        PIC 9(02).                   PPEA004
008200     03  WRK-DATE-NUM1               PIC 9(6).                    PPEA004
008300     03  WRK-DATE-NUM2               PIC 9(6).                    PPEA004
008400                                                                  PPEA004
008500 01  NUMERIC-NON-INTEGER.                                         PPEA004
008600     05  COMP-XEDB-0153-MERIT-PCT                                 PPEA004
008700                             PIC 99V99.                           PPEA004
008800     05  DISP-XEDB-0153-MERIT-PCT REDEFINES                       PPEA004
008900         COMP-XEDB-0153-MERIT-PCT                                 PPEA004
009000                             PIC X(04).                           PPEA004
009100                                                                  PPEA004
009200 01  CALLED-MODULES.                               COPY CPWSXRTN. PPEA004
009300                                                                  PPEA004
009400 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEA004
009401                                                                  PPEA004
009410 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEA004
009500                                                                  PPEA004
009600     EJECT                                                        PPEA004
009700 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. PPEA004
009800                                                                  PPEA004
009900     EJECT                                                        PPEA004
010000 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEA004
010100                                                                  PPEA004
010200     EJECT                                                        PPEA004
010300 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEA004
010400                                                                  PPEA004
010500     EJECT                                                        PPEA004
010600*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
010700*****                                                             28521025
010800*****EJECT                                                        28521025
010900 01  INSTALLATION-CONSTANTS.                       COPY CPWSXIDC. PPEA004
011000                                                                  PPEA004
011100     EJECT                                                        PPEA004
011200******************************************************************PPEA004
011300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEA004
011400**--------------------------------------------------------------**PPEA004
011500** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEA004
011600******************************************************************PPEA004
011700                                                                  PPEA004
011800 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEA004
011810                                                                  PPEA004
011820 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEA004
011900                                                                  PPEA004
012000     EJECT                                                        PPEA004
012100 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEA004
012200                                                                  PPEA004
012300     EJECT                                                        PPEA004
012400 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEA004
012500                                                                  PPEA004
012600     EJECT                                                        PPEA004
012700 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEA004
012800                                                                  PPEA004
012900 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEA004
013000                                                                  PPEA004
013100 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEA004
013200                                                                  PPEA004
013300 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEA004
013400                                                                  PPEA004
013500     EJECT                                                        PPEA004
013600 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEA004
013700                                                                  PPEA004
013800     EJECT                                                        PPEA004
013900 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEA004
014000                                                                  PPEA004
014100     EJECT                                                        PPEA004
014200 01  XACN-ACTION-ARRAY                   EXTERNAL. COPY CPWSXACN. PPEA004
014300                                                                  PPEA004
014400                                                                  PPEA004
014500     EJECT                                                        PPEA004
014600******************************************************************PPEA004
014700**  DATA BASE AREAS                                             **PPEA004
014800******************************************************************PPEA004
014900                                                                  PPEA004
015000 01  SCR-ROW                             EXTERNAL.                PPEA004
015100     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEA004
015200                                                                  PPEA004
015300     EJECT                                                        PPEA004
015400 01  PER-ROW                             EXTERNAL.                PPEA004
015500     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEA004
015600                                                                  PPEA004
015700     EJECT                                                        PPEA004
015800******************************************************************PPEA004
015900**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEA004
016000******************************************************************PPEA004
016100                                                                  PPEA004
016200*----------------------------------------------------------------*PPEA004
016300 LINKAGE SECTION.                                                 PPEA004
016400*----------------------------------------------------------------*PPEA004
016500                                                                  PPEA004
016600******************************************************************PPEA004
016700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEA004
016800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEA004
016900******************************************************************PPEA004
017000                                                                  PPEA004
017100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEA004
017200                                                                  PPEA004
017300     EJECT                                                        PPEA004
017400******************************************************************PPEA004
017500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEA004
017600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEA004
017700******************************************************************PPEA004
017800                                                                  PPEA004
017900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEA004
018000                                                                  PPEA004
018100     EJECT                                                        PPEA004
018200******************************************************************PPEA004
018300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEA004
018400******************************************************************PPEA004
018500                                                                  PPEA004
018600 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEA004
018700                          KMTA-MESSAGE-TABLE-ARRAY.               PPEA004
018800                                                                  PPEA004
018900                                                                  PPEA004
019000*----------------------------------------------------------------*PPEA004
019100 0000-DRIVER.                                                     PPEA004
019200*----------------------------------------------------------------*PPEA004
019300                                                                  PPEA004
019400******************************************************************PPEA004
019500**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEA004
019600******************************************************************PPEA004
019700                                                                  PPEA004
019800     IF  THIS-IS-THE-FIRST-TIME                                   PPEA004
019900         PERFORM 0100-INITIALIZE                                  PPEA004
020000     END-IF.                                                      PPEA004
020100                                                                  PPEA004
020200******************************************************************PPEA004
020300**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEA004
020400**  TO THE CALLING PROGRAM                                      **PPEA004
020500******************************************************************PPEA004
020600                                                                  PPEA004
020700     PERFORM 1000-MAINLINE-ROUTINE.                               PPEA004
020800                                                                  PPEA004
020900     EXIT PROGRAM.                                                PPEA004
021000                                                                  PPEA004
021100                                                                  PPEA004
021200     EJECT                                                        PPEA004
021300*----------------------------------------------------------------*PPEA004
021400 0100-INITIALIZE    SECTION.                                      PPEA004
021500*----------------------------------------------------------------*PPEA004
021600                                                                  PPEA004
021700******************************************************************PPEA004
021800**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEA004
021900******************************************************************PPEA004
022000                                                                  PPEA004
022100     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEA004
022200                                                                  PPEA004
022300*                                               *-------------*   PPEA004
022400                                                 COPY CPPDXWHC.   PPEA004
022500*                                               *-------------*   PPEA004
022600                                                                  PPEA004
022700*****                                           *-------------*   28521025
022800*****                                            COPY CPPDDATE.   28521025
022900*****                                           *-------------*   28521025
023000                                                                  PPEA004
023100     MOVE SCR-CURRENT-DATE       TO WS-C-DATE-HOLD.               PPEA004
023200     MOVE WS-C-YEAR TO WS-CURR-YEAR.                              PPEA004
023300     MOVE WS-C-MM TO WS-CURR-MM.                                  PPEA004
023400     MOVE WS-C-DD TO WS-CURR-DD.                                  PPEA004
023500*****MOVE PRE-EDIT-DATE            TO DATE-WO-CC.                 28521025
023600                                                                  PPEA004
023700******************************************************************PPEA004
023800**   SET FIRST CALL SWITCH OFF                                  **PPEA004
023900******************************************************************PPEA004
024000                                                                  PPEA004
024100     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEA004
024200                                                                  PPEA004
024300     EJECT                                                        PPEA004
024400*----------------------------------------------------------------*PPEA004
024500 1000-MAINLINE-ROUTINE SECTION.                                   PPEA004
024600*----------------------------------------------------------------*PPEA004
024700                                                                  PPEA004
024800     IF  KRMI-REQUESTED-BY-08                                     PPEA004
024900         PERFORM 1100-CONEDITS-MAINLINE                           PPEA004
025000     END-IF.                                                      PPEA004
025100                                                                  PPEA004
025200                                                                  PPEA004
025300******************************************************************PPEA004
025400**     C O N S I S T E N C Y    E D I T S   M A I N L I N E     **PPEA004
025500******************************************************************PPEA004
025600*----------------------------------------------------------------*PPEA004
025700 1100-CONEDITS-MAINLINE SECTION.                                  PPEA004
025800*----------------------------------------------------------------*PPEA004
025900                                                                  PPEA004
026000     IF MERIT-PERCENT IS EQUAL TO ZERO                            PPEA004
026100         ADD 1                     TO KMTA-CTR                    PPEA004
026200         MOVE M08903               TO KMTA-MSG-NUMBER   (KMTA-CTR)PPEA004
026300         MOVE 4                    TO KMTA-LENGTH       (KMTA-CTR)PPEA004
026400         MOVE MERIT-PERCENT  TO COMP-XEDB-0153-MERIT-PCT          PPEA004
026500         MOVE DISP-XEDB-0153-MERIT-PCT                            PPEA004
026600                                   TO KMTA-DATA-FIELD   (KMTA-CTR)PPEA004
026700         MOVE E0153                TO KMTA-FIELD-NUM    (KMTA-CTR)PPEA004
026800         MOVE WS-ROUTINE-TYPE      TO KMTA-ROUTINE-TYPE (KMTA-CTR)PPEA004
026900         MOVE WS-ROUTINE-NUM       TO KMTA-ROUTINE-NUM  (KMTA-CTR)PPEA004
027000     END-IF.                                                      PPEA004
027100*                                                                 PPEA004
027200     EJECT                                                        PPEA004
027300*----------------------------------------------------------------*PPEA004
027400 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEA004
027500*----------------------------------------------------------------*PPEA004
027600                                                                  PPEA004
027700*                                                 *-------------* PPEA004
027800                                                   COPY CPPDXDEC. PPEA004
027900*                                                 *-------------* PPEA004
028000                                                                  PPEA004
028100*----------------------------------------------------------------*PPEA004
028200*----------------------------------------------------------------*PPEA004
028300 9300-DATE-CONVERSION-DB2  SECTION.                               PPEA004
028400*----------------------------------------------------------------*PPEA004
028500                                                                  PPEA004
028600*                                                 *-------------* PPEA004
028700                                                   COPY CPPDXDC2. PPEA004
028800*                                                 *-------------* PPEA004
028900                                                                  PPEA004
029000******************************************************************PPEA004
029100**   E N D  S O U R C E   ----  PPEA004 ----                    **PPEA004
029200******************************************************************PPEA004
