000100**************************************************************/   28201068
000200*  PROGRAM: PPEC611                                          */   28201068
000300*  RELEASE: ___1068______ SERVICE REQUEST(S): ____12820____  */   28201068
000400*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___06/14/96__  */   28201068
000500*  DESCRIPTION:                                              */   28201068
000600*  - HRMI - CHANGE TEST FOR SALARY REVIEW DATE TO:           */   28201068
000700*           1) TEST PERSONNEL PROGRAM CODE IS 1 OR 2         */   28201068
000800*           2) NOT IDC-SR-MGMT-TITLE                         */   28201068
000900*           3) TEST APPT TYPE CODE IS 2 OR 7                 */   28201068
001000**************************************************************/   28201068
000000**************************************************************/   28521025
000001*  PROGRAM: PPEC611                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/01/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    REPLACED CPPDXDC2 WITH CPPDXDC3.                        */   28521025
000007**************************************************************/   28521025
000010**************************************************************/   36180791
000020*  PROGRAM: PPEC611                                          */   36180791
000030*  RELEASE: ___0791______ SERVICE REQUEST(S): ____3618_____  */   36180791
000040*  NAME:_____JLT_________ MODIFICATION DATE:  ___07/07/93__  */   36180791
000050*  DESCRIPTION:                                              */   36180791
000061*  - INITIAL INSTALLATION OF MODULE.                         */   36180791
000070**************************************************************/   36180791
000100*----------------------------------------------------------------*PPEC611
001000*----------------------------------------------------------------*PPEC611
001200 IDENTIFICATION DIVISION.                                         PPEC611
001300 PROGRAM-ID. PPEC611.                                             PPEC611
001400 AUTHOR. UCOP.                                                    PPEC611
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEC611
001600                                                                  PPEC611
001700 DATE-WRITTEN.  JUNE 1,1993.                                      PPEC611
001800 DATE-COMPILED.                                                   PPEC611
001900                                                                  PPEC611
002000******************************************************************PPEC611
002100** THIS ROUTINE VERIFYS THAT THE NEXT SALARY REVIEW DATE IS VALID PPEC611
002200** BASED ON THE PERSONNEL PROGRAM CODE AND THE APPOINTMENT TYPE.  PPEC611
002300******************************************************************PPEC611
002400                                                                  PPEC611
002500 ENVIRONMENT DIVISION.                                            PPEC611
002600 CONFIGURATION SECTION.                                           PPEC611
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEC611
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEC611
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEC611
003000                                                                  PPEC611
003100 INPUT-OUTPUT SECTION.                                            PPEC611
003200 FILE-CONTROL.                                                    PPEC611
003300                                                                  PPEC611
003400 DATA DIVISION.                                                   PPEC611
003500 FILE SECTION.                                                    PPEC611
003600     EJECT                                                        PPEC611
003700*----------------------------------------------------------------*PPEC611
003800 WORKING-STORAGE SECTION.                                         PPEC611
003900*----------------------------------------------------------------*PPEC611
004000                                                                  PPEC611
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEC611
004200*****'PPEC611 /060193'.                                           28521025
004210     'PPEC611 /080195'.                                           28521025
004300                                                                  PPEC611
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEC611
004500     A-STND-PROG-ID.                                              PPEC611
004600     05  FILLER                       PIC X(03).                  PPEC611
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEC611
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEC611
004900     05  FILLER                       PIC X(08).                  PPEC611
005000                                                                  PPEC611
005100 01  MESSAGES-USED.                                               PPEC611
005500     05  M08241                       PIC X(05) VALUE '08241'.    PPEC611
005600     05  M08242                       PIC X(05) VALUE '08242'.    PPEC611
007100                                                                  PPEC611
007200 01  ELEMENTS-USED.                                               PPEC611
007300     05  E0135                        PIC 9(04) VALUE 0135.       PPEC611
007400     05  E0136                        PIC 9(04) VALUE 0136.       PPEC611
008100                                                                  PPEC611
008200 01  MISC-WORK-AREAS.                                             PPEC611
009700     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEC611
009800         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEC611
009900         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEC611
009910     05  NEED-VALID-REVIEW-DATE-SW    PIC 9(01) VALUE ZERO.       PPEC611
009920*****05  ISO-END-OF-MONTH.                                        28521025
009930*****    10  ISO-END-OF-MONTH-YYYY-MM.                            28521025
009940*****        15  ISO-END-OF-MONTH-YYYY    PIC X(4).               28521025
009950*****        15  FILLER                   PIC X(1).               28521025
009960*****        15  ISO-END-OF-MONTH-MM      PIC X(2).               28521025
009970*****    10  FILLER                       PIC X(3).               28521025
009980*****05  ISO-WK-DATE.                                             28521025
009990*****    10  ISO-WK-YYYY-MM.                                      28521025
009991*****        15  ISO-WK-YYYY              PIC X(4).               28521025
009992*****        15  FILLER                   PIC X(1).               28521025
009993*****        15  ISO-WK-MM                PIC X(2).               28521025
009994*****    10  FILLER                       PIC X(3).               28521025
009997     05  ISO-DATE-EDIT.                                           PPEC611
009998         10  ISO-DATE-EDIT-YYYY-MM.                               PPEC611
009999             15  ISO-DATE-EDIT-YYYY       PIC X(4).               PPEC611
010000             15  FILLER                   PIC X(1).               PPEC611
010001             15  ISO-DATE-EDIT-MM         PIC X(2).               PPEC611
010002         10  FILLER                       PIC X(3).               PPEC611
010003*****05  EARLIEST-APPT-BEGN-DATE          PIC X(10).              28521025
010004     05  EARLIEST-ISO-APPT-BEGN-DATE      PIC X(10).              28521025
010010     05  WRK-MESSAGE                      PIC X(5) VALUE ZEROS.   PPEC611
010100                                                                  PPEC611
010400     EJECT                                                        PPEC611
010410 01  APPT-DIST-SUBSCRIPTS.                                        PPEC611
010420     05  SEG-SUB                  PIC 9(02)  VALUE ZERO.          PPEC611
010430         88  SEG-SUB-IS-APPT                 VALUES 1  4  7 10    PPEC611
010440                                                   13 16 19 22    PPEC611
010450                                                   25.            PPEC611
010460     05  ELEMENT-SUB               PIC 9(01)  VALUE ZERO.         PPEC611
010470                                                                  PPEC611
010471     05  COMP-FIELD-NUM            PIC 9(04)  VALUE ZERO.         PPEC611
010472                                                                  PPEC611
010480 01  EDB-MAINTENANCE-PASS-AREA.                    COPY CPWSEMPA. PPEC611
010490                                                                  PPEC611
010491 01  APPT-DISTR-ELEM-NUM-TABLE.                    COPY CPWSADEA. PPEC611
010492                                                                  PPEC611
010500 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEC611
010600                                                                  PPEC611
010700     EJECT                                                        PPEC611
010800*01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. 28521025
010810 01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC3. 28521025
010900                                                                  PPEC611
011000     EJECT                                                        PPEC611
011100 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEC611
011200                                                                  PPEC611
011300     EJECT                                                        PPEC611
011400 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEC611
011500                                                                  PPEC611
011600     EJECT                                                        PPEC611
011700*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
011800*****                                                             28521025
011900*****EJECT                                                        28521025
012000 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEC611
012400                                                                  PPEC611
012500     EJECT                                                        PPEC611
012600 01  XUTF-UTILITY-FUNCTIONS.                       COPY CPWSXUTF. PPEC611
012601                                                                  PPEC611
012800     EJECT                                                        PPEC611
012900 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEC611
013000                                                                  PPEC611
013100     EJECT                                                        PPEC611
013200******************************************************************PPEC611
013300**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEC611
013400**--------------------------------------------------------------**PPEC611
013500** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEC611
013600******************************************************************PPEC611
013700                                                                  PPEC611
013800 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEC611
013900                                                                  PPEC611
014000     EJECT                                                        PPEC611
014100 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEC611
014200                                                                  PPEC611
014300     EJECT                                                        PPEC611
014400 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEC611
014500                                                                  PPEC611
014600     EJECT                                                        PPEC611
014700 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEC611
014800                                                                  PPEC611
014900 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEC611
015000                                                                  PPEC611
015100 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEC611
015200                                                                  PPEC611
015300 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEC611
015400                                                                  PPEC611
015500     EJECT                                                        PPEC611
015600 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEC611
015700                                                                  PPEC611
015800     EJECT                                                        PPEC611
015900 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEC611
016300                                                                  PPEC611
016400     EJECT                                                        PPEC611
016500 01  SYSTEM-PARAMETERS                   EXTERNAL. COPY CPWSXSP2. PPEC611
016600                                                                  PPEC611
019700     EJECT                                                        PPEC611
019800******************************************************************PPEC611
019900**  DATA BASE AREAS                                             **PPEC611
020000******************************************************************PPEC611
020100                                                                  PPEC611
020200 01  SCR-ROW                             EXTERNAL.                PPEC611
020300     EXEC SQL INCLUDE PPPVSCR1 END-EXEC.                          PPEC611
020400                                                                  PPEC611
020500     EJECT                                                        PPEC611
020600 01  PER-ROW                             EXTERNAL.                PPEC611
020700     EXEC SQL INCLUDE PPPVPER1 END-EXEC.                          PPEC611
020800                                                                  PPEC611
020900     EJECT                                                        PPEC611
021000 01  AXD-ARRAY                           EXTERNAL.                PPEC611
021100     03  FILLER  OCCURS 27 TIMES.                                 PPEC611
021200     05  APP-ROW-DELETE-BYTE       PIC X(01).                     PPEC611
021300         88  APP-ROW-DELETED                   VALUE HIGH-VALUES. PPEC611
021400     05  APP-ROW-SEG-ID            PIC X(02).                     PPEC611
021500     05  FILLER  OCCURS 3 TIMES.                                  PPEC611
021600         08  APP-ROW.                              COPY CPWSRAPP  PPEC611
021700                REPLACING WORK-STUDY-PGM                          PPEC611
021800                       BY NULL-APPT-WORK-STUDY.                   PPEC611
021900                                                                  PPEC611
022000     03  FILLER  OCCURS 27 TIMES.                                 PPEC611
022100     05  DIS-ROW-DELETE-BYTE       PIC X(01).                     PPEC611
022200         88  DIS-ROW-DELETED                   VALUE HIGH-VALUES. PPEC611
022300     05  DIS-ROW-SEG-ID            PIC X(02).                     PPEC611
022400     05  FILLER  OCCURS 3 TIMES.                                  PPEC611
022500         08  DIS-ROW.                              COPY CPWSRDIS. PPEC611
022600                                                                  PPEC611
022700     EJECT                                                        PPEC611
022800******************************************************************PPEC611
022900**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEC611
023000******************************************************************PPEC611
023100                                                                  PPEC611
023200*----------------------------------------------------------------*PPEC611
023300 LINKAGE SECTION.                                                 PPEC611
023400*----------------------------------------------------------------*PPEC611
023500                                                                  PPEC611
023600******************************************************************PPEC611
023700**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEC611
023800**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEC611
023900******************************************************************PPEC611
024000                                                                  PPEC611
024100 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEC611
024200                                                                  PPEC611
024300     EJECT                                                        PPEC611
024400******************************************************************PPEC611
024500**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEC611
024600**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEC611
024700******************************************************************PPEC611
024800                                                                  PPEC611
024900 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEC611
025000                                                                  PPEC611
025100     EJECT                                                        PPEC611
025200******************************************************************PPEC611
025300**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEC611
025400******************************************************************PPEC611
025500                                                                  PPEC611
025600 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEC611
025700                          KMTA-MESSAGE-TABLE-ARRAY.               PPEC611
025800                                                                  PPEC611
025900                                                                  PPEC611
026000*----------------------------------------------------------------*PPEC611
026100 0000-DRIVER.                                                     PPEC611
026200*----------------------------------------------------------------*PPEC611
026300                                                                  PPEC611
026800     IF  THIS-IS-THE-FIRST-TIME                                   PPEC611
026900         PERFORM 0100-INITIALIZE                                  PPEC611
027000     END-IF.                                                      PPEC611
027100                                                                  PPEC611
027700     PERFORM 1000-MAINLINE-ROUTINE.                               PPEC611
027800                                                                  PPEC611
027900     EXIT PROGRAM.                                                PPEC611
028000                                                                  PPEC611
028200     EJECT                                                        PPEC611
028300*----------------------------------------------------------------*PPEC611
028400 0100-INITIALIZE    SECTION.                                      PPEC611
028500*----------------------------------------------------------------*PPEC611
028600                                                                  PPEC611
028700******************************************************************PPEC611
028800**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEC611
028900******************************************************************PPEC611
029000                                                                  PPEC611
029100     MOVE A-STND-PROG-ID      TO XWHC-DISPLAY-MODULE.             PPEC611
029300*                                               *-------------*   PPEC611
029400                                                 COPY CPPDXWHC.   PPEC611
029500*                                               *-------------*   PPEC611
029700*****                                           *-------------*   28521025
029800*****                                            COPY CPPDDATE.   28521025
029900*****                                           *-------------*   28521025
030100*****MOVE PRE-EDIT-DATE       TO DATE-WO-CC.                      28521025
030200*****                                                             28521025
030300*****MOVE END-OF-MONTH-DATE  TO WSX-STD-DATE.                     28521025
030400*****PERFORM CONVERT-STD-DATE.                                    28521025
030500*****MOVE DB2-DATE    TO ISO-END-OF-MONTH.                        28521025
030600******************************************************************PPEC611
030700**   SET FIRST CALL SWITCH OFF                                  **PPEC611
030800******************************************************************PPEC611
031000     SET NOT-THE-FIRST-TIME   TO TRUE.                            PPEC611
031200*                                                                 PPEC611
031300*----------------------------------------------------------------*PPEC611
031400 1000-MAINLINE-ROUTINE SECTION.                                   PPEC611
031500*----------------------------------------------------------------*PPEC611
031691                                                                  PPEC611
031692     IF NEXT-SALARY-REV   = '5'                                   PPEC611
031693         GO TO 1000-EXIT                                          PPEC611
031694     END-IF.                                                      PPEC611
031707*****MOVE ISO-ZERO-DATE    TO EARLIEST-APPT-BEGN-DATE.            28521025
031708     MOVE XDTS-ISO-ZERO-DATE    TO EARLIEST-ISO-APPT-BEGN-DATE.   28521025
031708     MOVE ZERO             TO NEED-VALID-REVIEW-DATE-SW.          PPEC611
031709     PERFORM 2000-CHECK-ALL-APPTS.                                PPEC611
031710     IF NEED-VALID-REVIEW-DATE-SW  = ZERO                         PPEC611
031711         GO TO 1000-EXIT                                          PPEC611
031712     END-IF.                                                      PPEC611
031713     MOVE NEXT-SALREV-DATE  TO ISO-DATE-EDIT.                     PPEC611
031714     IF ISO-DATE-EDIT-YYYY  > '0001'  AND                         PPEC611
031715        ISO-DATE-EDIT-YYYY  < '9999'                              PPEC611
031717         IF (NEXT-SALARY-REV  = '1' OR '2')  AND                  PPEC611
031718*****        NEXT-SALREV-DATE  NOT > EARLIEST-APPT-BEGN-DATE      28521025
031719             NEXT-SALREV-DATE  NOT > EARLIEST-ISO-APPT-BEGN-DATE  28521025
031719             MOVE M08242          TO WRK-MESSAGE                  PPEC611
031720             PERFORM 1005-ISSUE-SALARY-REVIEW-MSG                 PPEC611
031721         ELSE                                                     PPEC611
031722             CONTINUE                                             PPEC611
031723         END-IF                                                   PPEC611
031724     ELSE                                                         PPEC611
031725         MOVE M08241          TO WRK-MESSAGE                      PPEC611
031726         PERFORM 1005-ISSUE-SALARY-REVIEW-MSG                     PPEC611
031727     END-IF.                                                      PPEC611
031728 1000-EXIT.    EXIT.                                              PPEC611
031729                                                                  PPEC611
031730*----------------------------------------------------------------*PPEC611
031731 1005-ISSUE-SALARY-REVIEW-MSG  SECTION.                           PPEC611
031732*----------------------------------------------------------------*PPEC611
031738* * ISSUE MESSAGE FOR NEXT-SALARY-REVIEW-CODE                     PPEC611
031739     ADD 1                TO KMTA-CTR                             PPEC611
031740     MOVE E0135           TO KMTA-FIELD-NUM        (KMTA-CTR)     PPEC611
031743     MOVE NEXT-SALARY-REV TO KMTA-DATA-FIELD       (KMTA-CTR)     PPEC611
031744     MOVE 1               TO KMTA-LENGTH           (KMTA-CTR)     PPEC611
031746     MOVE WRK-MESSAGE     TO KMTA-MSG-NUMBER       (KMTA-CTR)     PPEC611
031747     MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE     (KMTA-CTR)     PPEC611
031748     MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM      (KMTA-CTR)     PPEC611
031749* * ISSUE MESSAGE FOR NEXT-SALARY-REVIEW-DATE                     PPEC611
031750     ADD 1                TO KMTA-CTR                             PPEC611
031751     MOVE E0136           TO KMTA-FIELD-NUM        (KMTA-CTR)     PPEC611
031752*****MOVE NEXT-SALREV-DATE   TO DB2-DATE                          28521025
031753*****PERFORM CONVERT-DB2-DATE                                     28521025
031754*****MOVE WSX-STD-DATE-LNG4 TO KMTA-DATA-FIELD     (KMTA-CTR)     28521025
031755     MOVE NEXT-SALREV-DATE   TO XDC3-ISO-DATE                     28521025
031756     PERFORM XDC3-CONVERT-ISO-TO-STD                              28521025
031757     MOVE XDC3-STD-DATE-LNG4 TO KMTA-DATA-FIELD    (KMTA-CTR)     28521025
031755     MOVE 4               TO KMTA-LENGTH           (KMTA-CTR)     PPEC611
031757     MOVE WRK-MESSAGE     TO KMTA-MSG-NUMBER       (KMTA-CTR)     PPEC611
031758     MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE     (KMTA-CTR)     PPEC611
031759     MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM      (KMTA-CTR).    PPEC611
031760 1005-EXIT.     EXIT.                                             PPEC611
073100                                                                  PPEC611
073400*----------------------------------------------------------------*PPEC611
073500 2000-CHECK-ALL-APPTS   SECTION.                                  PPEC611
073600*----------------------------------------------------------------*PPEC611
073610     MOVE 1                     TO ELEMENT-SUB.                   PPEC611
073620     PERFORM WITH TEST BEFORE                                     PPEC611
073630        VARYING SEG-SUB  FROM 1  BY 3                             PPEC611
073640          UNTIL SEG-SUB > 27                                      PPEC611
073650             IF APPT-NUM      OF APP-ROW (SEG-SUB, 1) = ZERO  OR  PPEC611
073651                APPT-ADC-CODE OF APP-ROW (SEG-SUB, 1) = 'D'       PPEC611
073652                 CONTINUE                                         PPEC611
073653             ELSE                                                 PPEC611
073654                 PERFORM 2100-PROCESS-APPT                        PPEC611
073655             END-IF                                               PPEC611
073656     END-PERFORM.                                                 PPEC611
073722 2000-EXIT.     EXIT.                                             PPEC611
073723                                                                  PPEC611
073724*----------------------------------------------------------------*PPEC611
073725 2100-PROCESS-APPT  SECTION.                                      PPEC611
073726*----------------------------------------------------------------*PPEC611
073727                                                                  PPEC611
073728***************************************************************** PPEC611
073729* * BYPASS EXPIRED APPOINTMENTS                                   PPEC611
073730***************************************************************** PPEC611
073731*****MOVE APPT-END-DATE  OF APP-ROW (SEG-SUB, 1) TO ISO-WK-DATE.  28521025
073732*****IF ISO-WK-YYYY-MM  < ISO-END-OF-MONTH-YYYY-MM                28521025
073733     IF APPT-END-DATE  OF APP-ROW (SEG-SUB, 1) (1:7)              28521025
073734                        < XDTS-ISO-END-OF-MNTH-CCYY-MM            28521025
073733         GO TO 2100-EXIT                                          PPEC611
073734     END-IF.                                                      PPEC611
073737*****IF EARLIEST-APPT-BEGN-DATE  = ISO-ZERO-DATE  OR              28521025
073738     IF EARLIEST-ISO-APPT-BEGN-DATE  = XDTS-ISO-ZERO-DATE  OR     28521025
073738        APPT-BEGIN-DATE  OF APP-ROW (SEG-SUB, 1)                  PPEC611
073740*****                               <  EARLIEST-APPT-BEGN-DATE    28521025
073741                                   <  EARLIEST-ISO-APPT-BEGN-DATE 28521025
073740         MOVE APPT-BEGIN-DATE  OF APP-ROW (SEG-SUB, 1)            PPEC611
073743*****                              TO  EARLIEST-APPT-BEGN-DATE    28521025
073744                                   TO  EARLIEST-ISO-APPT-BEGN-DATE28521025
073742     END-IF.                                                      PPEC611
037000*****IF (PERSONNEL-PGM  OF APP-ROW (SEG-SUB, 1)  = 'M' OR 'P'     28201068
037100*****                                                  OR 'S')    28201068
037200*****  AND APPT-TYPE    OF APP-ROW (SEG-SUB, 1)  = '2'            28201068
037300     MOVE TITLE-CODE OF APP-ROW (SEG-SUB, 1) TO                   28201068
037400                                    IDC-SR-MGMT-TITLE-RANGE.      28201068
037500     IF (PERSONNEL-PGM  OF APP-ROW (SEG-SUB, 1)  = '1' OR '2')    28201068
037600       AND (APPT-TYPE   OF APP-ROW (SEG-SUB, 1)  = '2' OR '7')    28201068
037700       AND NOT IDC-SR-MGMT-TITLE                                  28201068
073746         MOVE 1  TO NEED-VALID-REVIEW-DATE-SW                     PPEC611
073747     END-IF.                                                      PPEC611
073784 2100-EXIT.    EXIT.                                              PPEC611
073785                                                                  PPEC611
073786*----------------------------------------------------------------*PPEC611
073787 7000-CALC-APT-DATA-ELMT-NUM SECTION.                             PPEC611
073788*----------------------------------------------------------------*PPEC611
073789     MOVE APPT-NUM OF APP-ROW (SEG-SUB, 1)                        PPEC611
073790                                TO ADA-SUBSCRIPTS-9.              PPEC611
073791     ADD 1                      TO DISTR-SUB.                     PPEC611
073792     COMPUTE  COMP-FIELD-NUM  =                                   PPEC611
073793              ADA-SEG-BASE (APPT-SUB, DISTR-SUB)  +  ADA-OFFSET.  PPEC611
073794 7000-EXIT.    EXIT.                                              PPEC611
075200                                                                  PPEC611
076200*----------------------------------------------------------------*PPEC611
076300 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEC611
076400*----------------------------------------------------------------*PPEC611
076500                                                                  PPEC611
076600*                                                 *-------------* PPEC611
076700                                                   COPY CPPDXDEC. PPEC611
076800*                                                 *-------------* PPEC611
076900                                                                  PPEC611
077000*----------------------------------------------------------------*PPEC611
077100 9060-APPT-DIST-LOC-CALCULATION  SECTION.                         PPEC611
077200*----------------------------------------------------------------*PPEC611
077300                                                                  PPEC611
077400*                                                 *-------------* PPEC611
077500                                                   COPY CPPDADLC. PPEC611
077600*                                                 *-------------* PPEC611
077700                                                                  PPEC611
077800     EJECT                                                        PPEC611
077900*----------------------------------------------------------------*PPEC611
078000 9300-DATE-CONVERSION-DB2  SECTION.                               PPEC611
078100*----------------------------------------------------------------*PPEC611
078200                                                                  PPEC611
078300*                                                 *-------------* PPEC611
078400*****                                              COPY CPPDXDC2. 28521025
078410                                                   COPY CPPDXDC3. 28521025
078500*                                                 *-------------* PPEC611
078600                                                                  PPEC611
078700******************************************************************PPEC611
078800**   E N D  S O U R C E   ----  PPEC611 ----                    **PPEC611
078900******************************************************************PPEC611
