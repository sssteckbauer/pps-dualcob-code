000100**************************************************************/   26001140
000200*  PROGRAM: PPBENXLI                                         */   26001140
000300*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12600____  */   26001140
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___07/11/97__  */   26001140
000500*  DESCRIPTION:                                              */   26001140
000600*  - Modified to apply BRL_MAX_SAL_BASE as a "cap" to the    */   26001140
000700*    GLI salary base before calculating the premium.         */   26001140
000800**************************************************************/   26001140
000000**************************************************************/   28521087
000001*  PROGRAM: PPBENXLI                                         */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/06/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  - CHANGE CODE TO NOT ABORT SIMPLY BECAUSE THE RATE IS NOT */   28521087
000006*    FOUND ON PPPBRL TABLE DUE TO THE EMPLOYEE'S AGE UNDER   */   28521087
000007*    MINIMUM AGE 16 OR OVER 99.                              */   28521087
000008**************************************************************/   28521087
000000**************************************************************/   28521025
000001*  PROGRAM: PPBENXLI                                         */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/23/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    MOVED BENEFITS ISO DATE COVERAGE EFFECT DATE TO         */   28521025
000007*    EFDT-ISO-DATE-CCYYMMDD.                                 */   28521025
000010**************************************************************/   28521025
000100**************************************************************/   36400799
000200*  PROGRAM:  PPBENXLI                                        */   36400799
000300*  RELEASE # __0799____   SERVICE REQUEST NO(S) __3640______ */   36400799
000400*  NAME _______SRS_____   MODIFICATION DATE ____10/06/92_____*/   36400799
000500*  DESCRIPTION                                               */   36400799
000600*  - NEW PPBENXLI: DB2 ACCESS AND DUAL USE FOR RUSH CHECKS   */   36400799
000700*  THIS MODULE PERFORMS LIFE INSURANCE CALCULATIONS          */   36400799
000800**************************************************************/   36400799
000900 IDENTIFICATION DIVISION.                                         PPBENXLI
001000 PROGRAM-ID. PPBENXLI.                                            PPBENXLI
001100 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENXLI
001200 DATE-COMPILED.                                                   PPBENXLI
001300 DATA DIVISION.                                                   PPBENXLI
001400 WORKING-STORAGE SECTION.                                         PPBENXLI
001500                                                                  PPBENXLI
001600 01  PPBENXLI-INTERFACE    EXTERNAL.  COPY CPLNKXLI.              PPBENXLI
001700 01  DED-EFF-DATE-COMMAREA EXTERNAL.  COPY CPWSEFDT.              PPBENXLI
001800 01  WORK-AREA.                                                   PPBENXLI
001900     05  PROGRAM-STATUS-FLAG        PIC X(01)  VALUE '0'.         PPBENXLI
002000         88  PROGRAM-STATUS-NORMAL             VALUE '0'.         PPBENXLI
002100         88  PROGRAM-STATUS-EXITING            VALUE '1'.         PPBENXLI
002200     05  RATE-FLAG                  PIC X(01)  VALUE 'N'.         PPBENXLI
002300         88  RATE-FOUND                        VALUE 'Y'.         PPBENXLI
002400         88  RATE-NOT-FOUND                    VALUE 'N'.         PPBENXLI
002500     05  WS-EMPLOYEE-AGE            PIC S9(04) VALUE ZERO COMP.   PPBENXLI
002600     05  WS-PLAN                    PIC S9.                       PPBENXLI
002700     05  WS-HOLD-BRL-KEY.                                         PPBENXLI
002800         10  WS-HLD-BRSC            PIC X(5)   VALUE SPACES.      PPBENXLI
002900         10  WS-HLD-MIN-AGE         PIC S9(04) VALUE ZERO COMP.   PPBENXLI
003000         10  WS-HLD-MAX-AGE         PIC S9(04) VALUE ZERO COMP.   PPBENXLI
003100     05  WS-BRL-BRSC.                                             PPBENXLI
003200         10 WS-BRL-CBUC             PIC XX.                       PPBENXLI
003300         10 WS-BRL-REP              PIC X.                        PPBENXLI
003400         10 WS-BRL-SHC              PIC X.                        PPBENXLI
003500         10 WS-BRL-DUC              PIC X.                        PPBENXLI
003600     05  WS-DEFAULT-BRSC            PIC X(05)  VALUE '00   '.     PPBENXLI
003700     05  WS-DEFAULT-BRL             PIC X(05)  VALUE '<<   '.     PPBENXLI
003800                                                                  PPBENXLI
003900 01  WS-BRL.                                                      PPBENXLI
004000     EXEC SQL                                                     PPBENXLI
004100          INCLUDE PPPVZBRL                                        PPBENXLI
004200     END-EXEC.                                                    PPBENXLI
004300     EXEC SQL                                                     PPBENXLI
004400          INCLUDE SQLCA                                           PPBENXLI
004500     END-EXEC.                                                    PPBENXLI
004600                                                                  PPBENXLI
004700 PROCEDURE DIVISION.                                              PPBENXLI
004800                                                                  PPBENXLI
004900 A000-MAINLINE SECTION.                                           PPBENXLI
005000                                                                  PPBENXLI
005100     PERFORM B000-INITIALIZATION.                                 PPBENXLI
005200                                                                  PPBENXLI
005300     IF PROGRAM-STATUS-NORMAL                                     PPBENXLI
005400      IF  KXLI-ACTION-RATE-RETRIEVAL                              PPBENXLI
005500       OR KXLI-ACTION-PREMIUM-CALC                                PPBENXLI
005600          PERFORM C010-GET-RATE                                   PPBENXLI
005700          IF   PROGRAM-STATUS-NORMAL                              PPBENXLI
005800           AND KXLI-ACTION-PREMIUM-CALC                           PPBENXLI
005900           AND RATE-FOUND                                         PPBENXLI
006000               PERFORM D010-CALCULATE.                            PPBENXLI
006100                                                                  PPBENXLI
006200     GOBACK.                                                      PPBENXLI
006300                                                                  PPBENXLI
006400 B000-INITIALIZATION SECTION.                                     PPBENXLI
006500                                                                  PPBENXLI
006600     SET PROGRAM-STATUS-NORMAL TO TRUE.                           PPBENXLI
006700     MOVE ZERO TO KXLI-RETURN-STATUS-FLAG.                        PPBENXLI
006800     INITIALIZE KXLI-RETURN-FIELDS.                               PPBENXLI
007200     IF  KXLI-ACTION-RATE-RETRIEVAL                               PPBENXLI
007300      OR KXLI-ACTION-PREMIUM-CALC                                 PPBENXLI
007400         PERFORM B030-VALIDATE-LOOKUP-ARGS.                       PPBENXLI
007500     IF NOT KXLI-INVALID-LOOKUP-ARG                               PPBENXLI
007510      IF KXLI-ACTION-PREMIUM-CALC                                 PPBENXLI
007600         PERFORM B050-VALIDATE-ENROLLMENT.                        PPBENXLI
007700                                                                  PPBENXLI
007800 B030-VALIDATE-LOOKUP-ARGS SECTION.                               PPBENXLI
007900                                                                  PPBENXLI
008000     SET KXLI-NOT-ENROLLED-IN-LIFE TO TRUE.                       PPBENXLI
008100     IF   KXLI-VALID-PLAN-CODE                                    PPBENXLI
008200      AND KXLI-LI-EMP-AGE IS NUMERIC                              PPBENXLI
008300      AND KXLI-LI-EMP-AGE > ZERO                                  PPBENXLI
008400          MOVE '0' TO KXLI-INVALID-LOOKUP-ARG-FLAG                PPBENXLI
008500     ELSE                                                         PPBENXLI
008600          SET KXLI-INVALID-LOOKUP-ARG TO TRUE                     PPBENXLI
008700          SET PROGRAM-STATUS-EXITING TO TRUE                      PPBENXLI
008800     END-IF.                                                      PPBENXLI
008801     IF  KXLI-INVALID-LOOKUP-ARG-FLAG = '0'                       28521087
008802         IF KXLI-INVALID-AGE  OR                                  28521087
008803            KXLI-LI-EMP-AGE > 99                                  28521087
008804            SET KXLI-INVALID-LOOKUP-ARG TO TRUE                   28521087
008805            SET PROGRAM-STATUS-EXITING TO TRUE                    28521087
008806         ELSE                                                     28521087
008810            IF KXLI-PERIOD-END-DATE(3:2) = '12'                   28521087
008820               IF KXLI-LI-EMP-AGE > 98                            28521087
008830                  SET KXLI-INVALID-LOOKUP-ARG TO TRUE             28521087
008840                  SET PROGRAM-STATUS-EXITING TO TRUE              28521087
008841               END-IF                                             28521087
008842            END-IF                                                28521087
008891         END-IF                                                   28521087
008892     END-IF.                                                      28521087
008900                                                                  PPBENXLI
009000 B050-VALIDATE-ENROLLMENT SECTION.                                PPBENXLI
009100                                                                  PPBENXLI
009200     IF EFDT-INPUT-XGTN-ICED-IND NOT = 'A' AND 'V' AND 'Z'        PPBENXLI
009300        MOVE 'E' TO EFDT-INPUT-XGTN-ICED-IND                      PPBENXLI
009400     END-IF.                                                      PPBENXLI
009600     MOVE KXLI-COVERAGE-EFFECT-DATE TO EFDT-ISO-DATE-CCYYMMDD.    28521025
009900     PERFORM Z010-PROCESS-EFF-DATE.                               PPBENXLI
010000     IF EFDT-DONT-TAKE-DEDUCTION                                  PPBENXLI
010100        SET PROGRAM-STATUS-EXITING TO TRUE                        PPBENXLI
010200     ELSE                                                         PPBENXLI
010300        SET KXLI-ENROLLED-IN-LIFE TO TRUE                         PPBENXLI
010400     END-IF.                                                      PPBENXLI
010600     IF   KXLI-LI-BASE-SALARY = ZERO                              PPBENXLI
010610      AND NOT KXLI-FLAT-RATE                                      PPBENXLI
010700        SET KXLI-INVALID-LOOKUP-ARG TO TRUE                       PPBENXLI
010800        SET PROGRAM-STATUS-EXITING TO TRUE                        PPBENXLI
010900     END-IF.                                                      PPBENXLI
011000                                                                  PPBENXLI
011100 C010-GET-RATE SECTION.                                           PPBENXLI
011200                                                                  PPBENXLI
011300     IF KXLI-BRSC  = WS-DEFAULT-BRSC                              PPBENXLI
011400        MOVE WS-DEFAULT-BRL TO WS-BRL-BRSC                        PPBENXLI
011500     ELSE                                                         PPBENXLI
011600        MOVE KXLI-BRSC TO WS-BRL-BRSC                             PPBENXLI
011700     END-IF.                                                      PPBENXLI
011800     MOVE KXLI-LI-EMP-AGE TO WS-EMPLOYEE-AGE.                     PPBENXLI
011900     IF KXLI-PERIOD-END-DATE(3:2) = '12'                          PPBENXLI
012000        ADD +1 TO WS-EMPLOYEE-AGE                                 PPBENXLI
012100     END-IF                                                       PPBENXLI
012200     IF   WS-BRL-BRSC = WS-HLD-BRSC                               PPBENXLI
012300      AND WS-HLD-MIN-AGE NOT > WS-EMPLOYEE-AGE                    PPBENXLI
012400      AND WS-HLD-MAX-AGE NOT < WS-EMPLOYEE-AGE                    PPBENXLI
012500          SET RATE-FOUND TO TRUE                                  PPBENXLI
012600     ELSE                                                         PPBENXLI
012700          SET RATE-NOT-FOUND TO TRUE                              PPBENXLI
012800          PERFORM P050-ACCESS-BRL                                 PPBENXLI
012900            UNTIL RATE-FOUND                                      PPBENXLI
013000               OR NOT PROGRAM-STATUS-NORMAL                       PPBENXLI
013100     END-IF.                                                      PPBENXLI
013200     IF RATE-FOUND                                                PPBENXLI
013300        MOVE BRL-RATE-PER-1000 TO KXLI-INS-RATE-AMT               PPBENXLI
013400        IF KXLI-FLAT-RATE                                         PPBENXLI
013500           MOVE BRL-PLANF-COV-AMT TO KXLI-FLAT-RATE-AMT           PPBENXLI
013600        END-IF                                                    PPBENXLI
013700     END-IF.                                                      PPBENXLI
013800                                                                  PPBENXLI
013900 D010-CALCULATE SECTION.                                          PPBENXLI
014000                                                                  PPBENXLI
017600*----> "Cap" the salary base at the BRL table maximum             26001140
017700     IF KXLI-LI-BASE-SALARY > BRL-MAX-SAL-BASE                    26001140
017800        MOVE BRL-MAX-SAL-BASE TO KXLI-ACTUAL-SALARY-BASE          26001140
017900     ELSE                                                         26001140
018000        MOVE KXLI-LI-BASE-SALARY TO KXLI-ACTUAL-SALARY-BASE       26001140
018100     END-IF.                                                      26001140
018200*                                                                 26001140
014100     IF KXLI-FLAT-RATE                                            PPBENXLI
014200        MOVE BRL-PLANF-COV-AMT TO KXLI-INS-COVERAGE-AMT           PPBENXLI
014300        COMPUTE KXLI-INS-PREMIUM-AMT ROUNDED                      PPBENXLI
014400              = BRL-PLANF-COV-AMT * BRL-RATE-PER-1000             PPBENXLI
014500     ELSE                                                         PPBENXLI
014600       MOVE KXLI-PLAN-CODE TO WS-PLAN                             PPBENXLI
014700       COMPUTE KXLI-INS-COVERAGE-AMT                              PPBENXLI
019000             = KXLI-ACTUAL-SALARY-BASE * WS-PLAN                  26001140
019100*************= KXLI-LI-BASE-SALARY * WS-PLAN                      26001140
014900       COMPUTE KXLI-INS-PREMIUM-AMT ROUNDED                       PPBENXLI
015000             = KXLI-INS-COVERAGE-AMT * BRL-RATE-PER-1000          PPBENXLI
015100     END-IF.                                                      PPBENXLI
015200                                                                  PPBENXLI
015300 P050-ACCESS-BRL SECTION.                                         PPBENXLI
015400                                                                  PPBENXLI
015500     EXEC SQL                                                     PPBENXLI
015600         SELECT  BRL_MIN_AGE,                                     PPBENXLI
015700                 BRL_MAX_AGE,                                     PPBENXLI
015800                 BRL_RATE_PER_1000,                               PPBENXLI
015900                 BRL_PLANF_COV_AMT                                PPBENXLI
020300                ,BRL_MAX_SAL_BASE                                 26001140
016000           INTO :BRL-MIN-AGE,                                     PPBENXLI
016100                :BRL-MAX-AGE,                                     PPBENXLI
016200                :BRL-RATE-PER-1000,                               PPBENXLI
016300                :BRL-PLANF-COV-AMT                                PPBENXLI
020800               ,:BRL-MAX-SAL-BASE                                 26001140
016400           FROM  PPPVZBRL_BRL                                     PPBENXLI
016500          WHERE     BRL_CBUC    = :WS-BRL-CBUC                    PPBENXLI
016600            AND     BRL_REP     = :WS-BRL-REP                     PPBENXLI
016700            AND     BRL_SHC     = :WS-BRL-SHC                     PPBENXLI
016800            AND     BRL_DUC     = :WS-BRL-DUC                     PPBENXLI
016900            AND NOT BRL_MIN_AGE > :WS-EMPLOYEE-AGE                PPBENXLI
017000            AND NOT BRL_MAX_AGE < :WS-EMPLOYEE-AGE                PPBENXLI
017100     END-EXEC.                                                    PPBENXLI
017200                                                                  PPBENXLI
017300     IF SQLCODE = ZERO                                            PPBENXLI
017400        SET RATE-FOUND TO TRUE                                    PPBENXLI
017500        MOVE BRL-MIN-AGE TO WS-HLD-MIN-AGE                        PPBENXLI
017600        MOVE BRL-MAX-AGE TO WS-HLD-MAX-AGE                        PPBENXLI
017700        MOVE WS-BRL-BRSC TO WS-HLD-BRSC                           PPBENXLI
017800     ELSE                                                         PPBENXLI
017900       INITIALIZE WS-HOLD-BRL-KEY                                 PPBENXLI
018000       IF   SQLCODE = +100                                        PPBENXLI
018100        AND WS-BRL-BRSC NOT = WS-DEFAULT-BRL                      PPBENXLI
018200            MOVE '2' TO KXLI-RETURN-STATUS-FLAG                   PPBENXLI
018300            MOVE WS-DEFAULT-BRL  TO WS-BRL-BRSC                   PPBENXLI
018400       ELSE                                                       PPBENXLI
018500            SET KXLI-RETURN-STATUS-ABORTING TO TRUE               PPBENXLI
018600            SET PROGRAM-STATUS-EXITING TO TRUE                    PPBENXLI
018700       END-IF                                                     PPBENXLI
018800     END-IF.                                                      PPBENXLI
018900                                                                  PPBENXLI
019000 Z010-PROCESS-EFF-DATE SECTION.                                   PPBENXLI
019100     COPY CPPDEFDT.                                               PPBENXLI
