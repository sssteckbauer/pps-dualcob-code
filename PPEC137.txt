000100**************************************************************/   32161191
000200*  PROGRAM: PPEC137                                          */   32161191
000220*  RELEASE: ___1191______ SERVICE REQUEST(S): ____13216____  */   32161191
000230*  NAME:______KEN FORD___ MODIFICATION DATE:  ___06/15/98__  */   32161191
000240*  DESCRIPTION:                                              */   32161191
000250*  - ADD EDITS AND IMPLIED MAINTENANCE FOR CITIZEN CODE "F"  */   32161191
000260**************************************************************/   32161191
000000**************************************************************/   28521025
000001*  PROGRAM: PPEC137                                          */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/27/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    THE NAMES REFERENCED FROM COPYMEMBER CPWSXDTS HAVE      */   28521025
000007*    BEEN CHANGED TO THE NEW NAMES IN CPWSXDTS.              */   28521025
000010**************************************************************/   28521025
000000**************************************************************/   15400936
000001*  PROGRAM: PPEC137                                          */   15400936
000002*  RELEASE: ___0936______ SERVICE REQUEST(S): ____11540____  */   15400936
000003*  NAME:_______QUAN______ CREATION DATE:      ___08/22/94__  */   15400936
000004*  DESCRIPTION:                                              */   15400936
000005*    TAX TREATY CONSISTENCY EDITS                            */   15400936
000007**************************************************************/   15400936
001100                                                                  PPEC137
001200 IDENTIFICATION DIVISION.                                         PPEC137
001300 PROGRAM-ID. PPEC137.                                             PPEC137
001400 AUTHOR. UCOP.                                                    PPEC137
001500 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPEC137
001600                                                                  PPEC137
001700 DATE-WRITTEN.  AUGUST 22, 1994.                                  PPEC137
001800 DATE-COMPILED.                                                   PPEC137
001900                                                                  PPEC137
002000******************************************************************PPEC137
002100**    THIS ROUTINE WILL PERFORM THE TAX TREATY INCOME CODE/     **PPEC137
002200**    TAX TREATY GROSS YTD CONSISTENCY EDITS.                   **PPEC137
002300******************************************************************PPEC137
002400                                                                  PPEC137
002500 ENVIRONMENT DIVISION.                                            PPEC137
002600 CONFIGURATION SECTION.                                           PPEC137
002700 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPEC137
002800 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPEC137
002900 SPECIAL-NAMES.                C01 IS NEW-PAGE.                   PPEC137
003000                                                                  PPEC137
003100 INPUT-OUTPUT SECTION.                                            PPEC137
003200 FILE-CONTROL.                                                    PPEC137
003300                                                                  PPEC137
003400 DATA DIVISION.                                                   PPEC137
003500 FILE SECTION.                                                    PPEC137
003600     EJECT                                                        PPEC137
003700*----------------------------------------------------------------*PPEC137
003800 WORKING-STORAGE SECTION.                                         PPEC137
003900*----------------------------------------------------------------*PPEC137
004000                                                                  PPEC137
004100 01  A-STND-PROG-ID                   PIC X(15) VALUE             PPEC137
005410*****                                                          CD 32161191
005600*****'PPEC137 /080195'.                                           32161191
005610     'PPEC137 /061598'.                                           32161191
004300                                                                  PPEC137
004400 01  A-STND-MSG-PARMS                 REDEFINES                   PPEC137
004500     A-STND-PROG-ID.                                              PPEC137
004600     05  FILLER                       PIC X(03).                  PPEC137
004700     05  WS-ROUTINE-TYPE              PIC X(01).                  PPEC137
004800     05  WS-ROUTINE-NUM               PIC 9(03).                  PPEC137
004900     05  FILLER                       PIC X(08).                  PPEC137
005000                                                                  PPEC137
005100 01  MESSAGES-USED.                                               PPEC137
005200     05  M08431                       PIC X(05) VALUE '08431'.    PPEC137
005201     05  M08432                       PIC X(05) VALUE '08432'.    PPEC137
005210     05  M12431                       PIC X(05) VALUE '12431'.    PPEC137
005220     05  M12432                       PIC X(05) VALUE '12432'.    PPEC137
005300                                                                  PPEC137
005400 01  ELEMENTS-USED.                                               PPEC137
005500     05  E0109                        PIC 9(04) VALUE 0109.       PPEC137
005510     05  E1170                        PIC 9(04) VALUE 1170.       PPEC137
005600     05  E1171                        PIC 9(04) VALUE 1171.       PPEC137
005700                                                                  PPEC137
005800 01  MISC-WORK-AREAS.                                             PPEC137
005900     05  FIRST-TIME-SW                PIC X(01) VALUE LOW-VALUES. PPEC137
006000         88  THIS-IS-THE-FIRST-TIME             VALUE LOW-VALUES. PPEC137
006100         88  NOT-THE-FIRST-TIME                 VALUE 'Y'.        PPEC137
006200                                                                  PPEC137
006300     EJECT                                                        PPEC137
006700 01  DL-FIELD-WORK-AREA.                           COPY CPWSXDLS. PPEC137
006800                                                                  PPEC137
006900     EJECT                                                        PPEC137
007000*01  DATE-CONVERSION-WORK-AREAS.                   COPY CPWSXDC2. 28521025
007100                                                                  PPEC137
007200     EJECT                                                        PPEC137
007300 01  XWHC-COMPILE-WORK-AREA.                       COPY CPWSXWHC. PPEC137
007400                                                                  PPEC137
007500     EJECT                                                        PPEC137
007600 01  COLUMN-88-WORK.                               COPY CPWSW88S. PPEC137
007700                                                                  PPEC137
007800     EJECT                                                        PPEC137
007900*01  DATE-WORK-AREA.                               COPY CPWSDATE. 28521025
009410*****                                                          CD 32161191
008200 01  APPT-DIST-LOC-WORK-AREAS.                     COPY CPWSADLC. PPEC137
008300                                                                  PPEC137
008400     EJECT                                                        PPEC137
008500 01  INSTALLATION-DEPENDT-CONSTANTS.               COPY CPWSXIDC. PPEC137
008600                                                                  PPEC137
008700     EJECT                                                        PPEC137
008800******************************************************************PPEC137
008900**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPEC137
009000**--------------------------------------------------------------**PPEC137
009100** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPEC137
009200******************************************************************PPEC137
009300                                                                  PPEC137
009400 01  DATA-ELEMENT-CHANGE-INDICATORS      EXTERNAL. COPY CPWSXDEC. PPEC137
009500                                                                  PPEC137
009600     EJECT                                                        PPEC137
009700 01  XCSS-COMMON-SYSTEM-SWITCHES         EXTERNAL. COPY CPWSSWCH. PPEC137
009800                                                                  PPEC137
009900     EJECT                                                        PPEC137
010000 01  XDTS-COMMON-SYSTEM-DATES            EXTERNAL. COPY CPWSXDTS. PPEC137
010100                                                                  PPEC137
010200     EJECT                                                        PPEC137
010300 01  ECES-CON-EDIT-SWITCH                EXTERNAL. COPY CPWSECES. PPEC137
010400                                                                  PPEC137
010500 01  EGES-GTN-EDIT-SWITCH                EXTERNAL. COPY CPWSEGES. PPEC137
010600                                                                  PPEC137
010700 01  EIMS-IMPL-MAINT-SWITCH              EXTERNAL. COPY CPWSEIMS. PPEC137
010800                                                                  PPEC137
010900 01  EAXD-APPT-ARRAY-SWITCH              EXTERNAL. COPY CPWSEAXD. PPEC137
011000                                                                  PPEC137
011100     EJECT                                                        PPEC137
011200 01  XDER-DATA-ELMT-RTN-POINTERS         EXTERNAL. COPY CPWSXDER. PPEC137
011300                                                                  PPEC137
011400     EJECT                                                        PPEC137
011500 01  XDRT-DET-ROUTINE-TRIGGERS           EXTERNAL. COPY CPWSXDRT. PPEC137
011600                                                                  PPEC137
011700     EJECT                                                        PPEC137
011800 01  EDB-MAINTENANCE-PASS-AREA           EXTERNAL. COPY CPWSEMPA. PPEC137
011900                                                                  PPEC137
012000     EJECT                                                        PPEC137
012100******************************************************************PPEC137
012200**  DATA BASE AREAS                                             **PPEC137
012300******************************************************************PPEC137
012400                                                                  PPEC137
012500 01  PCM-ROW                             EXTERNAL.                PPEC137
012600     EXEC SQL INCLUDE PPPVPCM1 END-EXEC.                          PPEC137
012700                                                                  PPEC137
012800     EJECT                                                        PPEC137
012900 01  PAY-ROW                             EXTERNAL.                PPEC137
013000     EXEC SQL INCLUDE PPPVPAY1 END-EXEC.                          PPEC137
013001******************************************************************PPEC137
013002**  DATA BASE SAVE AREAS                                        **PPEC137
013003******************************************************************PPEC137
013010     EJECT                                                        PPEC137
013022 01  PAY-ROW-HOLD                        EXTERNAL. COPY CPWSRPAY  PPEC137
013023         REPLACING  CHK-STREET2        BY SAVE-CHK-STREET2        PPEC137
013024                    CHK-DISP-CODE      BY SAVE-CHK-DISP-CODE      PPEC137
013025                    UI-ELIG-CODE       BY SAVE-UI-ELIG-CODE       PPEC137
013026                    W4-RECEIVED        BY SAVE-W4-RECEIVED        PPEC137
013027                    PRI-PAY-SCHED      BY SAVE-PRI-PAY-SCHED      PPEC137
013028                    FEDTAX-EXEMPT      BY SAVE-FEDTAX-EXEMPT      PPEC137
013029                    STATE-TAX-PERDED   BY SAVE-STATE-TAX-PERDED   PPEC137
013030                    STATE-TAX-ITMDED   BY SAVE-STATE-TAX-ITMDED   PPEC137
013050                    MAXFED-EXEMPT      BY SAVE-MAXFED-EXEMPT      PPEC137
013060                    TT-INCOME-CODE     BY SAVE-TT-INCOME-CODE     PPEC137
013070                    ALT-TT-CODE        BY SAVE-ALT-TT-CODE.       PPEC137
013100                                                                  PPEC137
013200     EJECT                                                        PPEC137
013300******************************************************************PPEC137
013400**          L  I  N  K  A  G  E      S  E  C  T  I  O  N        **PPEC137
013500******************************************************************PPEC137
013600                                                                  PPEC137
013700*----------------------------------------------------------------*PPEC137
013800 LINKAGE SECTION.                                                 PPEC137
013900*----------------------------------------------------------------*PPEC137
014000                                                                  PPEC137
014100******************************************************************PPEC137
014200**  THE KRMI AREA IS A COMMON AREA THAT IS USED TO PASS         **PPEC137
014300**  INFORMATION THAT IS REQUIRED FOR THIS MODULE TO EXECUTE     **PPEC137
014400******************************************************************PPEC137
014500                                                                  PPEC137
014600 01  KRMI-LINKAGE-SECTION.                         COPY CPLNKRMI. PPEC137
014700                                                                  PPEC137
014800     EJECT                                                        PPEC137
014900******************************************************************PPEC137
015000**  THIS MESSAGE TABLE ARRAY WILL CONTAIN ALL MESSAGES THAT     **PPEC137
015100**  THIS ROUTINE CAN LOGICALLY ISSUE AT ONE TIME                **PPEC137
015200******************************************************************PPEC137
015300                                                                  PPEC137
015400 01  KMTA-MESSAGE-TABLE-ARRAY.                     COPY CPLNKMTA. PPEC137
015500                                                                  PPEC137
015600     EJECT                                                        PPEC137
015700******************************************************************PPEC137
015800**    P  R  O  C  E  D  U  R  E      D  I  V  I  S  I  O  N     **PPEC137
015900******************************************************************PPEC137
016000                                                                  PPEC137
016100 PROCEDURE DIVISION USING KRMI-LINKAGE-SECTION,                   PPEC137
016200                          KMTA-MESSAGE-TABLE-ARRAY.               PPEC137
016300                                                                  PPEC137
016400                                                                  PPEC137
016500*----------------------------------------------------------------*PPEC137
016600 0000-DRIVER.                                                     PPEC137
016700*----------------------------------------------------------------*PPEC137
016800                                                                  PPEC137
016900******************************************************************PPEC137
017000**  PERFORM THE INITIALIZATION ROUTINE NOW                      **PPEC137
017100******************************************************************PPEC137
017200                                                                  PPEC137
017300     IF  THIS-IS-THE-FIRST-TIME                                   PPEC137
017400         PERFORM 0100-INITIALIZE                                  PPEC137
017500     END-IF.                                                      PPEC137
017600                                                                  PPEC137
017700******************************************************************PPEC137
017800**  PERFORM THE MAIN ROUTINE NOW - AFTER WHICH WE WILL RETURN   **PPEC137
017900**  TO THE CALLING PROGRAM                                      **PPEC137
018000******************************************************************PPEC137
018100                                                                  PPEC137
018200     PERFORM 1000-MAINLINE-ROUTINE.                               PPEC137
018300                                                                  PPEC137
018400     EXIT PROGRAM.                                                PPEC137
018500                                                                  PPEC137
018600                                                                  PPEC137
018700     EJECT                                                        PPEC137
018800*----------------------------------------------------------------*PPEC137
018900 0100-INITIALIZE    SECTION.                                      PPEC137
019000*----------------------------------------------------------------*PPEC137
019100                                                                  PPEC137
019200******************************************************************PPEC137
019300**  DISPLAY THE PROGRAM ID AND DATE/TIME COMPILED               **PPEC137
019400******************************************************************PPEC137
019500                                                                  PPEC137
019600     MOVE A-STND-PROG-ID           TO XWHC-DISPLAY-MODULE.        PPEC137
019700                                                                  PPEC137
019800*                                               *-------------*   PPEC137
019900                                                 COPY CPPDXWHC.   PPEC137
020000*                                               *-------------*   PPEC137
020100                                                                  PPEC137
023210*****                                                          CD 32161191
020500                                                                  PPEC137
020800******************************************************************PPEC137
020900**   SET FIRST CALL SWITCH OFF                                  **PPEC137
021000******************************************************************PPEC137
021100                                                                  PPEC137
021200     SET NOT-THE-FIRST-TIME        TO TRUE.                       PPEC137
021300                                                                  PPEC137
021400     EJECT                                                        PPEC137
021500*----------------------------------------------------------------*PPEC137
021600 1000-MAINLINE-ROUTINE SECTION.                                   PPEC137
021700*----------------------------------------------------------------*PPEC137
021800                                                                  PPEC137
021900                                                                  PPEC137
022300     MOVE CITIZEN-CODE OF PAY-ROW TO W88-CITIZEN-CODE.            PPEC137
022301                                                                  PPEC137
022302     IF CITIZEN-CODE-NONRESID-ALN-EXMP                            PPEC137
025400     OR CITIZEN-CODE-RESID-ALN-EXMP                               32161191
022310        IF    (TT-INCOME-CODE = SPACES OR '00')                   PPEC137
022320            AND                                                   PPEC137
022321              (ALT-TT-CODE = SPACES OR '00')                      PPEC137
022322           ADD 1                TO KMTA-CTR                       PPEC137
022323           MOVE E1170           TO KMTA-FIELD-NUM       (KMTA-CTR)PPEC137
022324           MOVE TT-INCOME-CODE  TO KMTA-DATA-FIELD      (KMTA-CTR)PPEC137
022325           MOVE 2               TO KMTA-LENGTH          (KMTA-CTR)PPEC137
022326           IF KRMI-REQUESTED-BY-08                                PPEC137
022327              MOVE M08431       TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022328           ELSE                                                   PPEC137
022329              MOVE M12431       TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022330           END-IF                                                 PPEC137
022331           MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC137
022332           MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC137
022333                                                                  PPEC137
022334           ADD 1                TO KMTA-CTR                       PPEC137
022335           MOVE E1171           TO KMTA-FIELD-NUM       (KMTA-CTR)PPEC137
022336           MOVE ALT-TT-CODE     TO KMTA-DATA-FIELD      (KMTA-CTR)PPEC137
022337           MOVE 2               TO KMTA-LENGTH          (KMTA-CTR)PPEC137
022338           IF KRMI-REQUESTED-BY-08                                PPEC137
022339              MOVE M08431       TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022340           ELSE                                                   PPEC137
022341              MOVE M12431       TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022342           END-IF                                                 PPEC137
022343           MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC137
022344           MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC137
022345           ADD 1                TO KMTA-CTR                       PPEC137
022346                                                                  PPEC137
022347           MOVE E0109           TO KMTA-FIELD-NUM       (KMTA-CTR)PPEC137
022348           MOVE CITIZEN-CODE OF PAY-ROW                           PPEC137
022349                           TO KMTA-DATA-FIELD      (KMTA-CTR)     PPEC137
022350           MOVE 1               TO KMTA-LENGTH          (KMTA-CTR)PPEC137
022351           IF KRMI-REQUESTED-BY-08                                PPEC137
022352               MOVE M08431      TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022353           ELSE                                                   PPEC137
022354               MOVE M12431      TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022355           END-IF                                                 PPEC137
022356           MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC137
022357           MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC137
022358        END-IF                                                    PPEC137
022359     END-IF.                                                      PPEC137
022360                                                                  PPEC137
022370     IF TT-INCOME-CODE NOT = SAVE-TT-INCOME-CODE                  PPEC137
022380        IF     ((TT-INCOME-CODE = SPACES OR '00')                 PPEC137
022410              AND YTD-TT-GROSS NOT = ZERO)                        PPEC137
022500           ADD 1                TO KMTA-CTR                       PPEC137
022810           IF KRMI-REQUESTED-BY-08                                PPEC137
022900               MOVE M08432      TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022901           ELSE                                                   PPEC137
022902               MOVE SAVE-TT-INCOME-CODE  TO TT-INCOME-CODE        PPEC137
022910               MOVE M12432      TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
022920           END-IF                                                 PPEC137
023000           MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC137
023100           MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC137
024000        END-IF                                                    PPEC137
024010     END-IF.                                                      PPEC137
024100                                                                  PPEC137
024200     IF ALT-TT-CODE NOT = SAVE-ALT-TT-CODE                        PPEC137
024211        IF     ((ALT-TT-CODE = SPACES OR '00')                    PPEC137
024212              AND YTD-ALT-TT-GROSS NOT = ZERO)                    PPEC137
024213           ADD 1                TO KMTA-CTR                       PPEC137
024214           IF KRMI-REQUESTED-BY-08                                PPEC137
024215               MOVE M08432      TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
024216           ELSE                                                   PPEC137
024217               MOVE SAVE-ALT-TT-CODE  TO ALT-TT-CODE              PPEC137
024218               MOVE M12432      TO KMTA-MSG-NUMBER      (KMTA-CTR)PPEC137
024221           END-IF                                                 PPEC137
024222           MOVE WS-ROUTINE-TYPE TO KMTA-ROUTINE-TYPE    (KMTA-CTR)PPEC137
024223           MOVE WS-ROUTINE-NUM  TO KMTA-ROUTINE-NUM     (KMTA-CTR)PPEC137
024224        END-IF                                                    PPEC137
024225     END-IF.                                                      PPEC137
024226                                                                  PPEC137
024230     EJECT                                                        PPEC137
024300*----------------------------------------------------------------*PPEC137
024400 9050-AUDITING-RESPONSIBILITIES  SECTION.                         PPEC137
024500*----------------------------------------------------------------*PPEC137
024600                                                                  PPEC137
024700*                                                 *-------------* PPEC137
024800                                                   COPY CPPDXDEC. PPEC137
024900*                                                 *-------------* PPEC137
025000                                                                  PPEC137
033510*****                                                          CD 32161191
026800******************************************************************PPEC137
026900**   E N D  S O U R C E   ----  PPEC137 ----                    **PPEC137
027000******************************************************************PPEC137
